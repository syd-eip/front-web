export const environment = {
  production: false,
  development: false,
  test: false,

  frontHost: [],

  apiHost: '',
  apiVersion: '',
  apiWebSocket: '',

  recaptchaKey: ''
};
