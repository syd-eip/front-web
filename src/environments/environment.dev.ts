export const environment = {
  production: false,
  development: true,

  frontHost: ['qualif.shareyourdreams.fr', 'www.shareyourdreams.fr', 'shareyourdreams.fr'],

  apiHost: 'https://apidev.shareyourdreams.fr',
  apiVersion: '/v1',
  apiWebSocket: 'ws://apidev.shareyourdreams.fr/ws',

  recaptchaKey: '6Ld9ELQZAAAAAAf9MVGj_NToW3E1bYNskxuuHuhK'
};
