export const environment = {
  production: true,
  development: false,

  frontHost: ['qualif.shareyourdreams.fr', 'www.shareyourdreams.fr', 'shareyourdreams.fr'],

  apiHost: 'https://api.shareyourdreams.fr',
  apiVersion: '/v1',
  apiWebSocket: 'ws://api.shareyourdreams.fr/ws',

  recaptchaKey: '6Ld9ELQZAAAAAAf9MVGj_NToW3E1bYNskxuuHuhK'
};
