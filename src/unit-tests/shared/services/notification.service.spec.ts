import { TestBed } from '@angular/core/testing';
import { NotificationService } from '../../../app/shared/services/notification/notification.service';
import { AppDevModule } from '../../../app/app-dev.module';
import { NotificationType } from '../../../app/shared/services/notification/notification-class/notification.class';
import { TranslationService } from '../../../app/shared/services/translation/translation.service';
import { NetworkClass } from '../../../app/shared/services/network/network.class';
import { CookieService } from 'ngx-cookie-service';

describe('NotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [AppDevModule],
  }));

  beforeEach(() => {
    const notif: NotificationService = TestBed.get(NotificationService);
    const translator: TranslationService = TestBed.get(TranslationService);
    const network: NetworkClass = TestBed.get(NetworkClass);
    const cookies: CookieService = TestBed.get(CookieService);

    // @ts-ignore
    translator['_network'] = network;
    // @ts-ignore
    translator['_cookies'] = cookies;
    // @ts-ignore
    notif['translationService'] = translator;
  });

  it('should be created', () => {
    const service: NotificationService = TestBed.get(NotificationService);
    expect(service).toBeTruthy();
  });

  it('Should create a debug notification', async () => {
    const service: NotificationService = TestBed.get(NotificationService);
    const translator: TranslationService = TestBed.get(TranslationService);
    const network: NetworkClass = TestBed.get(NetworkClass);
    const cookies: CookieService = TestBed.get(CookieService);

    // @ts-ignore
    translator['_network'] = network;
    // @ts-ignore
    translator['_cookies'] = cookies;
    // @ts-ignore
    service['translationService'] = translator;

    const f = () => {
      return new Promise((resolve, reject) => {
        service.observable().subscribe((value) => {
          expect(value).toBeDefined();
          expect(value.type).toBe(NotificationType.Debug);
          expect(value.duration).toBe(1000);
          expect(value.message).toBe('debug');
          expect(value.title).toBe('title');
          resolve();
        }, reject);
      });
    };

    const awaiter = f();
    service.debug('debug', 1000, 'title');
    await awaiter;
  });

  it('Should create a info notification', async () => {
    const service: NotificationService = TestBed.get(NotificationService);

    const f = () => {
      return new Promise((resolve, reject) => {
        service.observable().subscribe((value) => {
          expect(value).toBeDefined();
          expect(value.type).toBe(NotificationType.Info);
          expect(value.duration).toBe(1000);
          expect(value.message).toBe('info');
          expect(value.title).toBe('title');
          resolve();
        }, reject);
      });
    };

    const awaiter = f();
    service.info('info', 1000, 'title');
    await awaiter;
  });

  it('Should create a success notification', async () => {
    const service: NotificationService = TestBed.get(NotificationService);

    const f = () => {
      return new Promise((resolve, reject) => {
        service.observable().subscribe((value) => {
          expect(value).toBeDefined();
          expect(value.type).toBe(NotificationType.Success);
          expect(value.duration).toBe(1000);
          expect(value.message).toBe('success');
          expect(value.title).toBe('title');
          resolve();
        }, reject);
      });
    };

    const awaiter = f();
    service.success('success', 1000, 'title');
    await awaiter;
  });

  it('Should create a warning notification', async () => {
    const service: NotificationService = TestBed.get(NotificationService);

    const f = () => {
      return new Promise((resolve, reject) => {
        service.observable().subscribe((value) => {
          expect(value).toBeDefined();
          expect(value.type).toBe(NotificationType.Warning);
          expect(value.duration).toBe(1000);
          expect(value.message).toBe('warning');
          expect(value.title).toBe('title');
          resolve();
        }, reject);
      });
    };

    const awaiter = f();
    service.warning('warning', 1000, 'title');
    await awaiter;
  });

  it('Should create an debug without the accessor ', async () => {
    const service: NotificationService = TestBed.get(NotificationService);
    const f = () => {
      return new Promise((resolve, reject) => {
        service.observable().subscribe((value) => {
          expect(value).toBeDefined();
          expect(value.type).toBe(NotificationType.Debug);
          expect(value.duration).toBe(1000);
          expect(value.message).toBe('debug');
          expect(value.title).toBe('title');
          resolve();
        }, reject);
      });
    };

    const awaiter = f();
    service.notify(NotificationType.Debug, 'debug', 1000, 'title');
    await awaiter;
  });

  it('Should create an info without the accessor ', async () => {
    const service: NotificationService = TestBed.get(NotificationService);
    const f = () => {
      return new Promise((resolve, reject) => {
        service.observable().subscribe((value) => {
          expect(value).toBeDefined();
          expect(value.type).toBe(NotificationType.Info);
          expect(value.duration).toBe(1000);
          expect(value.message).toBe('info');
          expect(value.title).toBe('title');
          resolve();
        }, reject);
      });
    };

    const awaiter = f();
    service.notify(NotificationType.Info, 'info', 1000, 'title');
    await awaiter;
  });

  it('Should create an success without the accessor ', async () => {
    const service: NotificationService = TestBed.get(NotificationService);
    const f = () => {
      return new Promise((resolve, reject) => {
        service.observable().subscribe((value) => {
          expect(value).toBeDefined();
          expect(value.type).toBe(NotificationType.Success);
          expect(value.duration).toBe(1000);
          expect(value.message).toBe('success');
          expect(value.title).toBe('title');
          resolve();
        }, reject);
      });
    };

    const awaiter = f();
    service.notify(NotificationType.Success, 'success', 1000, 'title');
    await awaiter;
  });

  it('Should create an warning without the accessor ', async () => {
    const service: NotificationService = TestBed.get(NotificationService);
    const f = () => {
      return new Promise((resolve, reject) => {
        service.observable().subscribe((value) => {
          expect(value).toBeDefined();
          expect(value.type).toBe(NotificationType.Warning);
          expect(value.duration).toBe(1000);
          expect(value.message).toBe('warning');
          expect(value.title).toBe('title');
          resolve();
        }, reject);
      });
    };

    const awaiter = f();
    service.notify(NotificationType.Warning, 'warning', 1000, 'title');
    await awaiter;
  });

  it('Should create a error notification', async () => {
    const service: NotificationService = TestBed.get(NotificationService);

    const f = () => {
      return new Promise((resolve, reject) => {
        service.observable().subscribe((value) => {
          expect(value).toBeDefined();
          expect(value.type).toBe(NotificationType.Error);
          expect(value.duration).toBe(1000);
          expect(value.message).toBe('error');
          expect(value.title).toBe('title');
          resolve();
        }, reject);
      });
    };

    const awaiter = f();
    service.error('error', 1000, 'title');
    await awaiter;
  });
});
