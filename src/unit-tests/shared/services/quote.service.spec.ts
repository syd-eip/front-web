import { TestBed } from '@angular/core/testing';
import { LoaderQuoteInterface, LoaderQuoteService } from '../../../app/shared/services/loader/loader-quote.service';
import { AppDevModule } from '../../../app/app-dev.module';

describe('LoaderQuoteService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [AppDevModule],
  }));

  it('should be created', () => {
    const service: LoaderQuoteService = TestBed.get(LoaderQuoteService);
    expect(service).toBeTruthy();
  });

  it('Should serve a specific quote', () => {
    const service: LoaderQuoteService = TestBed.get(LoaderQuoteService);
    const quote: LoaderQuoteInterface = {
      proverb: 'simpletest',
      origin: 'karma',
    };

    service.quotes = [quote];
    expect(service.random.origin === quote.origin && service.random.proverb === quote.proverb).toBeTruthy();
  });

  it('Should crash', () => {
    const service: LoaderQuoteService = TestBed.get(LoaderQuoteService);

    service.quotes = [];
    expect(service.random).toBeUndefined();
  });
});
