import {Component, HostListener, Inject, OnInit, ViewChild} from '@angular/core';
import {NavigationStart, Router} from '@angular/router';
import 'rxjs/add/operator/filter';
import {DOCUMENT, Location} from '@angular/common';
import {NavbarComponent} from './shared/components/navbar/navbar.component';
import {LoaderService} from './shared/services/loader/loader.service';
import {navbarBackground} from './shared/data/navbar/navbar-background.data';
import {UserService} from './shared/services/web/user.service';
import {environment} from '../environments/environment';

declare let recaptchaV3: any;

@Component({
  selector: 'app-root',
  template: '<app-loadable loaderId="GLOBAL" [initialState]="true" [fixed]="true">\n' +
    '  <app-loadable-spinner>\n' +
    '    <app-spinner>\n' +
    '    </app-spinner>\n' +
    '  </app-loadable-spinner>\n' +
    '  <app-loadable-description>\n' +
    '    <app-quotes></app-quotes>\n' +
    '  </app-loadable-description>\n' +
    '  <app-loadable-content>\n' +
    '    <app-navbar></app-navbar>\n' +
    '    <app-column>\n' +
    '      <router-outlet></router-outlet>\n' +
    '    </app-column>\n' +
    '<app-footer></app-footer>\n' +
    '  </app-loadable-content>\n' +
    '</app-loadable>\n' +
    '<app-notification-host></app-notification-host>\n'
})
export class AppComponent implements OnInit {

  /**
   * @description A reference to the navigation bar component
   */
  @ViewChild(NavbarComponent, {static: true}) private _navbar: NavbarComponent;

  /**
   * Construct AppComponent
   *
   * @param document A DI Token representing the main rendering context. In a browser this is the DOM Document
   * @param router A reference to an injectable service used to navigate from one view to the next
   * @param location A service that applications can use to interact with a browser's URL
   * @param loader A reference to the LoaderService service
   * @param userService A reference to the application user service
   */
  constructor(@Inject(DOCUMENT) private document: Document, private router: Router, public location: Location,
              private loader: LoaderService, private userService: UserService) {
    setTimeout(() => {
      this.loader.unload('GLOBAL');
    }, 0);
  }

  /**
   * @description Decorator that declares a DOM event to listen for, and provides a handler method to run when that event occurs.
   * This DOM event listen to 'click' event
   *
   * @param event DOM event
   */
  @HostListener('click', ['$event'])
  onClick(event: any) {
    const idTarget: Array<string> = ['sidebar-toggle', 'sidebar-toggle-bar-1', 'sidebar-toggle-bar-2', 'sidebar-toggle-bar-3'];
    const classes: Array<string> = document.getElementsByTagName('html')[0].className.split(' ');

    if (classes.find(item => item === 'nav-open') && !idTarget.find(item => item === event.target.id)) {
      if (event.screenX < document.getElementById('sidebar').offsetLeft) {
        this.navbar.close();
      }
    }
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.executeCaptcha();

    this.navbar.close();
    this.userService.isAuthenticated().then();

    this.router.events.subscribe(event => {

      if (event instanceof NavigationStart) {
        document.getElementsByClassName('navbar')[0].classList.remove('navbar-transparent');
        document.getElementsByClassName('navbar')[0].classList.remove('bg-white');

        let path: { path: Array<string>, background: string };
        let url: string = '';

        for (const item of event.url) {
          if (item === '?' || item === ';') {
            break;
          }
          url += item;
        }

        for (const item of navbarBackground) {
          if (item.path.find(next => next === url) !== undefined) {
            path = item;
            break;
          }
        }

        if (path) {
          this.navbar.transparent = path.background !== 'bg-white';
          document.getElementsByClassName('navbar')[0].classList.add(path.background);
        } else {
          this.navbar.transparent = false;
          document.getElementsByClassName('navbar')[0].classList.add('bg-white');
        }
      }
    });
  }

  /**
   * @description
   */
  public executeCaptcha(): void {
    recaptchaV3.execute(environment.recaptchaKey, {action: 'homepage'}).then((token) => {
      console.log('Recaptcha token : ' + token);
    });
  }

  /**
   * @description The method allows you to retrieve the navigation bar component
   */
  get navbar(): NavbarComponent {
    return this._navbar;
  }

  /**
   * @description The method allows you to assign the visibility of the navigation bar component
   *
   * @param value The new assigned value for the navigation bar component
   */
  set navbar(value: NavbarComponent) {
    this._navbar = value;
  }
}
