/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

/**
 * Import of layouts
 */
import {RoutingErrorLayout} from './routing-error.layout';

/**
 * ROuting error layout module of the Application. This module retrieve routing error layout
 * You can also find Angular's modules
 */
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    RoutingErrorLayout
  ],
  exports: [
    RoutingErrorLayout
  ],
  providers: []
})
export class RoutingErrorModule {
}
