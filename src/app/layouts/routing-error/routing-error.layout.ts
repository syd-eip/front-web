import {Component, OnDestroy, OnInit} from '@angular/core';
import {TranslatableClass} from 'src/app/shared/services/translation/translation-class/translatable.class';
import {TranslationService} from 'src/app/shared/services/translation/translation.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-routing-error',
  templateUrl: './routing-error.layout.html'
})
export class RoutingErrorLayout extends TranslatableClass implements OnInit, OnDestroy {

  /**
   * @description Constructor of RoutingErrorLayout layout
   *
   * The constructor creates an instance of the RoutingErrorLayout layout and specifies the default values
   * the input and output variables of the component.
   *
   * @param translationService A reference to the i18n translation service of the application
   * @param router Implements the Angular Router service , which enables navigation from one view to the next as users
   * perform application tasks
   */
  constructor(translationService: TranslationService, private router: Router) {
    super(translationService);
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit() {
    (document.getElementsByClassName('layout-column')[0] as HTMLElement).style.background = 'white';
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular deletes the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
   */
  ngOnDestroy(): void {
    (document.getElementsByClassName('layout-column')[0] as HTMLElement).removeAttribute('style');
  }

  /**
   * @description The method uses the Angular router and allows you to navigate the application's routes
   *
   * @param path Path to navigate to
   */
  navigateTo(path: string): void {
    this.router.navigate([path])
      .then(_ => {
      });
  }
}
