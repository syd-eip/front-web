/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of application layout's modules
 */
import {TeamModule} from './team/team.module';
import {RoutingErrorModule} from './routing-error/routing-error.module';
import {PrivacyPoliciesModule} from './privacy-policies/privacy-policies.module';
import {HomeModule} from './home/home.module';
import {LayoutRoutingModule} from './layout-routing.module';
import {EventModule} from './event/event.module';
import {UserModule} from './user/user.module';
import {AssociationModule} from './association/association.module';

@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule,
    HomeModule,
    PrivacyPoliciesModule,
    PrivacyPoliciesModule,
    UserModule,
    TeamModule,
    RoutingErrorModule,
    EventModule,
    HomeModule,
    AssociationModule
  ],
  exports: [
    HomeModule,
    PrivacyPoliciesModule,
    UserModule,
    TeamModule,
    RoutingErrorModule,
    EventModule,
    HomeModule,
    AssociationModule
  ]
})
export class LayoutsModule {
}
