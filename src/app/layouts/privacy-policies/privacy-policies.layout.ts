import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-privacy-policies',
  templateUrl: './privacy-policies.layout.html',
})
export class PrivacyPoliciesLayout implements OnInit, OnDestroy {

  /**
   * @description ID found in the optional parameters of the URL
   */
  private _urlID: number;

  /**
   * @description Array which contains all the topics titles
   */
  private _titles: Array<string>;

  /**
   * @description Constructor of PrivacyPoliciesLayout layout
   *
   * The constructor creates an instance of the PrivacyPoliciesLayout layout and specifies the default values
   * the input and output variables of the layout.
   *
   * @param activatedRoute Provides access to information about a route associated with a component that is loaded in an outlet
   * @param router Implements the Angular Router service , which enables navigation from one view to the next as users
   * perform application tasks
   */
  constructor(private activatedRoute: ActivatedRoute, private router: Router) {
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit() {
    this.activatedRoute.queryParamMap.subscribe(value => {
      if (value.get('topic')) {
        this.changeUrlID(parseInt(value.get('topic'), 10));
      }
    });

    this.urlID = parseInt(this.activatedRoute.snapshot.queryParamMap.get('topic'), 10);

    if (this.urlID === null || isNaN(this.urlID) || this.urlID === undefined) {
      this.urlID = 0;

        this.router.navigate(['.', {}], {
          relativeTo: this.activatedRoute,
          queryParams: {topic: this.urlID},
          queryParamsHandling: 'merge',
        });
    }
    if (this.urlID > 2) {
      this.urlID = 0;

      this.router.navigate(['.', {}], {
        relativeTo: this.activatedRoute,
        queryParams: {topic: this.urlID},
        queryParamsHandling: 'merge',
      });
    }

    this.titles = ['Terms of use', 'Terms of sale', 'Legal Mentions'];
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
   */
  ngOnDestroy(): void {
  }

  changeUrlID(value: number): void {
    this.urlID = value;

    this.router.navigate(['.', {}], {
      relativeTo: this.activatedRoute,
      queryParams: {topic: this.urlID},
      queryParamsHandling: 'merge',
    });
  }

  /**
   * @description The method allows you to retrieve the ID found in the optional parameters of the URL
   */
  get urlID(): number {
    return this._urlID;
  }

  /**
   * @description The method allows you to assign the ID found in the optional parameters of the URL
   *
   * @param value Value of the new ID found in the optional parameters of the URL
   */
  set urlID(value: number) {
    this._urlID = value;
  }

  /**
   * @description The method allows you to retrieve the array which contains all the topics titles
   */
  get titles(): Array<string> {
    return this._titles;
  }

  /**
   * @description The method allows you to assign the array which contains all the topics titles
   *
   * @param value Value of the new array which contains all the topics titles
   */
  set titles(value: Array<string>) {
    this._titles = value;
  }
}
