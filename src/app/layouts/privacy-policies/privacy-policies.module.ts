/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of angular material's modules
 */
import {MatExpansionModule} from '@angular/material';
/**
 * Import of layouts
 */
import {PrivacyPoliciesLayout} from './privacy-policies.layout';
import {PrivacyPoliciesPartTwoLayout,} from './privacy-policies-parts/privacy-policies-part-two/privacy-policies-part-two.layout';
import {PrivacyPoliciesPartOneLayout} from './privacy-policies-parts/privacy-policies-part-one/privacy-policies-part-one.layout';
import {PrivacyPoliciesPartThreeLayout} from './privacy-policies-parts/privacy-policies-part-three/privacy-policies-part-three.layout';
import {PrivacyPoliciesMenuLayout} from './privacy-policies-menu/privacy-policies-menu.layout';


@NgModule({
  imports: [
    CommonModule,
    MatExpansionModule
  ],
  declarations: [
    PrivacyPoliciesLayout,
    PrivacyPoliciesMenuLayout,
    PrivacyPoliciesPartOneLayout,
    PrivacyPoliciesPartTwoLayout,
    PrivacyPoliciesPartThreeLayout,
  ],
  exports: [
    PrivacyPoliciesLayout,
    PrivacyPoliciesMenuLayout,
    PrivacyPoliciesPartOneLayout,
    PrivacyPoliciesPartTwoLayout,
    PrivacyPoliciesPartThreeLayout,
  ]
})
export class PrivacyPoliciesModule {
}
