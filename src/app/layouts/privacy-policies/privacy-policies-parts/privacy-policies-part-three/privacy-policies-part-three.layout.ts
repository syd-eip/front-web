import {Component} from '@angular/core';
import {TranslatableClass} from 'src/app/shared/services/translation/translation-class/translatable.class';
import {TranslationService} from 'src/app/shared/services/translation/translation.service';

@Component({
  selector: 'app-privacy-policies-part-three',
  templateUrl: './privacy-policies-part-three.layout.html'
})
export class PrivacyPoliciesPartThreeLayout extends TranslatableClass {

  /**
   * @description Construct the PrivacyPoliciesPartThreeLayout layout
   *
   * @param translationService A reference to the translation service
   */
  constructor(translationService: TranslationService) {
    super(translationService);
  }
}
