import {Component} from '@angular/core';
import {TranslatableClass} from 'src/app/shared/services/translation/translation-class/translatable.class';
import {TranslationService} from 'src/app/shared/services/translation/translation.service';

@Component({
  selector: 'app-privacy-policies-part-two',
  templateUrl: './privacy-policies-part-two.layout.html'
})
export class PrivacyPoliciesPartTwoLayout extends TranslatableClass {

  /**
   * @description Construct the PrivacyPoliciesPartTwoLayout layout
   *
   * @param translationService A reference to the translation service
   */
  constructor(translationService: TranslationService) {
    super(translationService);
  }
}
