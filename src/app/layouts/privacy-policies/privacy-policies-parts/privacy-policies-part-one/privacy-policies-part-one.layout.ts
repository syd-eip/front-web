import {Component} from '@angular/core';
import {TranslatableClass} from 'src/app/shared/services/translation/translation-class/translatable.class';
import {TranslationService} from 'src/app/shared/services/translation/translation.service';

@Component({
  selector: 'app-privacy-policies-part-one',
  templateUrl: './privacy-policies-part-one.layout.html'
})
export class PrivacyPoliciesPartOneLayout extends TranslatableClass {

  /**
   * @description Construct the PrivacyPoliciesPartOneLayout layout
   *
   * @param translationService A reference to the translation service
   */
  constructor(translationService: TranslationService) {
    super(translationService);
  }
}
