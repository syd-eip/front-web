import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewportScroller} from '@angular/common';
import {TranslationService} from 'src/app/shared/services/translation/translation.service';
import {TranslatableClass} from 'src/app/shared/services/translation/translation-class/translatable.class';
import {PrivacyPoliciesMenuItemClass} from '../../../shared/classes/privacy-policies-menu/privacy-policies-menu-item.class';
import {privacyPoliciesItems} from '../../../shared/data/privacy-policies/privacy-policies-items.data';

@Component({
  selector: 'app-privacy-policies-menu',
  templateUrl: './privacy-policies-menu.layout.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PrivacyPoliciesMenuLayout extends TranslatableClass implements OnInit {

  /**
   * @description identifier of the active topic
   */
  @Input() public activeID: number;

  /**
   * @description This EventEmitter variable emits customized events synchronously
   * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
   *
   * In the PrivacyPoliciesMenuLayout, this EventEmitter allows to inform subscribed components that the topic has changed
   */
  @Output() public activeIDEmitter: EventEmitter<number>;

  /**
   * @description Array that defines the expanded state of each topic in the menu
   */
  private _expandedItem: Array<boolean>;

  /**
   * @description Menu component and subcomponents
   */
  private _menuItem: Array<PrivacyPoliciesMenuItemClass>;

  /**
   * @description Constructor of PrivacyPoliciesMenuLayout layout
   *
   * The constructor creates an instance of the PrivacyPoliciesMenuLayout layout and specifies the default values
   * the input and output variables of the layout.
   *
   * @param viewportScroller defines a scroll position manager. Implemented by BrowserViewportScroller
   * @param translationService A reference to the translation service of the application
   */
  constructor(private viewportScroller: ViewportScroller, translationService: TranslationService) {
    super(translationService);

    this.activeIDEmitter = new EventEmitter<number>();
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.menuItem = privacyPoliciesItems;
    this.expandedItem = [false, false, false];
  }

  /**
   * @description This method allows you to scroll between anchors
   *
   * @param anchorID a specified anchor identifier
   */
  scroll(anchorID: string): void {
    this.viewportScroller.scrollToAnchor(anchorID);
  }

  /**
   * @description This method allows you to change the displayed topic
   *
   * @param id ID of the specified topic
   */
  change(id: number): void {
    this.activeIDEmitter.emit(id);
  }

  /**
   * @description This method makes it possible to know which topic is displayed
   *
   * @param id ID of the specified topic
   */
  expanded(id: number): boolean {
    this.expandedItem[id] = this.activeID === id;

    return this.activeID === id;
  }

  /**
   * @description The method allows you to retrieve the menu component and subcomponents
   */
  get menuItem(): Array<PrivacyPoliciesMenuItemClass> {
    return this._menuItem;
  }

  /**
   * @description The method allows you to assign the menu component and subcomponents
   *
   * @param value Value of the new menu component and subcomponents
   */
  set menuItem(value: Array<PrivacyPoliciesMenuItemClass>) {
    this._menuItem = value;
  }

  /**
   * @description The method allows you to retrieve the array that defines the expanded state of each topic in the menu
   */
  get expandedItem(): Array<boolean> {
    return this._expandedItem;
  }

  /**
   * @description The method allows you to assign the array that defines the expanded state of each topic in the menu
   *
   * @param value Value of the new array that defines the expanded state of each topic in the menu
   */
  set expandedItem(value: Array<boolean>) {
    this._expandedItem = value;
  }
}
