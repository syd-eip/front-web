import {Component, ElementRef, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslatableClass} from '../../shared/services/translation/translation-class/translatable.class';
import {TranslationService} from '../../shared/services/translation/translation.service';
import {EventService} from '../../shared/services/web/event-service';
import {EventModel} from '../../shared/classes/models/event.class';
import {UserService} from '../../shared/services/web/user.service';
import {AssociationService} from '../../shared/services/web/association.service';
import {AssociationModel} from '../../shared/classes/models/association.model';
import {LoaderService} from '../../shared/services/loader/loader.service';
import {Semaphore} from '../../shared/classes/semaphore/semaphore.class';
import {ImageClass} from '../../shared/classes/models/image.class';
import {ModalService} from '../../shared/services/modal/modal.service';
import {ModalParticipationComponent} from '../../shared/components/modal/modal-participation/modal-participation.component';
import {ImageService} from '../../shared/services/web/image.service';
import {UserPublicClass} from '../../shared/classes/models/user-public.class';
import {environment} from '../../../environments/environment';

const DEFAULT_ASSOCIATION_IMAGE = '/assets/img/default-banner.jpg'
const DEFAULT_EVENT_IMAGE = '/assets/img/default-event.png'
const DEFAULT_PROFILE_IMAGE = '/assets/img/default-profile.png'

@Component({
  selector: 'app-event-layout',
  templateUrl: './event.layout.html',
})
export class EventLayout extends TranslatableClass implements OnInit {
  /**
   * @desc The current displayed event id (the value comes from the URL)
   */
  private _evtId: string = '';

  /**
   * @desc The current displayed event data (the value comes from the API)
   */
  private _eventData: EventModel = null;

  /**
   * @desc Contain all informations about the event influencer
   */
    // @ts-ignore
  private _influencerData: UserPublicClass = null;

  /**
   * @desc Contain all information about the event association
   */
  private _associationData: AssociationModel = null;

  /**
   * @desc The event banner image class. The attribute 'content' will contain the URI like image
   */
  public banner: ImageClass = null;

  /**
   * @desc The influencer image. Same as 'banner'
   */
  private _influencerImage: string = null;

  /**
   * @desc Contain a boolean that tells if the user is authenticated or not
   */
  private _authed: boolean = false;

  /**
   * @desc An accessor to the HTML container of the banner
   */
  @ViewChildren('banner') bannerElement: QueryList<ElementRef<HTMLDivElement>>;

  /**
   * @desc An accessor to the HTML container of the blurred banner (the background)
   */
  @ViewChildren('blurredBanner') blurredBannerElement: QueryList<ElementRef<HTMLDivElement>>;

  /**
   * @description Constructor of EventLayout layout
   *
   * The constructor creates an instance of the EventLayout layout and specifies the default values
   * the input and output variables of the layout.
   *
   * @param _translationService Provides access to information about the website translations. This enable the possibility to display text in French, English ...
   * @param _eventService Provides access to information or operations about any event. This simplify and unify the way to query the events.
   * @param _userService Provides access to information or operations about any users. This simplify and unify the way to query the users.
   * @param _associationService Provide access to information or operation about any associations. This simplify and unify the way to query the association.
   * @param _activatedRoute Provides access to information about a route associated with a component that is loaded in an outlet
   * @param router Implements the Angular Router service , which enables navigation from one view to the next as users perform application tasks
   * @param _loaderService Allow the layout to interact with the website loaders
   * @param _modalService Allow the layout to interact / invoke modals.
   * @param _imageService Provides access to information or operations about any images. This simplify and unify the way to query the images.
   */
  constructor(private _translationService: TranslationService, private _eventService: EventService,
              private _userService: UserService, private _associationService: AssociationService,
              private _activatedRoute: ActivatedRoute, private router: Router,
              private _loaderService: LoaderService, private _modalService: ModalService,
              private _imageService: ImageService) {
    super(_translationService);
  }

  /**
   * @description: This method is part of the component life cycle
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit() {
    this._evtId = this._activatedRoute.snapshot.paramMap.get('id');

    this._userService.isAuthenticated()
      .then(authed => this._authed = authed);
    const semaphore = new Semaphore(2);
    this._loaderService.load('GLOBAL');
    this._loaderService.attachSemaphore('GLOBAL', semaphore);
    this.loadEvent()
      .then((evt) => {
        this._userService.get('/user/public/id/' + evt.userId)
          .then((res) => {
            this._influencerData = res.responseData.data as UserPublicClass;
            semaphore.sub();
          })
          .catch((err) => {
            console.error(err);
            semaphore.sub();
          });
        this._associationService.getById(evt.associationId)
          .then((res) => {
            this._associationData = res;
            semaphore.sub();
          })
          .catch((err) => {
            console.error(err);
            semaphore.sub();
          });
      });
  }

  /**
   * @desc This function loads the event data from the API
   */
  async loadEvent(): Promise<EventModel> {
    const event = await this._eventService.getEvent(this.eventId);
    this._eventData = event;
    this.banner = this._eventData.images.find(item => item.name === 'banner')
    return event;
  }

  /**
   * @description The method opens a modal that allow the user to bet tokens and more ...
   */
  participate() {
    if (!this._authed) {
      this.navigateTo('/user/sign/in', {redirectTo: window.location.pathname});
    } else {
      this._modalService.open(ModalParticipationComponent, {centered: true, windowClass: 'modal-custom'});
      this._modalService.modalRef.componentInstance.id = this.eventId;
      this._modalService.modalRef.componentInstance.name = this._eventData.information.name;
    }
  }

  /**
   * @description The method uses the Angular router and allows you to navigate the application's routes
   *
   * @param path Path to navigate to
   * @param qp Query params to add
   */
  navigateTo(path: string, qp: { [key: string]: any } = undefined): void {
    this.router.navigate([path], {queryParams: qp})
      .then(value => {
      });
  }

  public openTab(url: string) {
    window.open(window.location.protocol + '//' + window.location.host + url, '_blank').focus()
  }

  /**
   * @desc This accessor tells if the current user own the event or not.
   *
   * An user own the event if the event is created by him or if he can edit it.
   */
  get ownEvent(): boolean {
    if (!this._eventData || !this._authed) {
      return false;
    }
    return this._eventData.userId === this._userService.user.id;
  }

  /**
   * @desc This accessor make the internal event id accessible in readonly
   */
  get eventId(): string {
    return this._evtId;
  }

  /**
   * @desc This accessor make the internal event data accessible in readonly
   */
  get event(): EventModel {
    return this._eventData;
  }

  /**
   * @desc This accessor respond an image and ensure that even if the event has no image, an 'empty' image will be displayed.
   */
  get bannerImage(): string {
    if (this.banner && this.banner.content) {
      return environment.apiHost + environment.apiVersion + '/image/id/' + this.banner.content;
    }
    if (this._eventData && this._eventData.images && this._eventData.images.find(i => i.name === 'banner')) {
      const img = this._eventData.images.find(i => i.name === 'banner');
      return environment.apiHost + environment.apiVersion + '/image/id/' + img.content;
    }
    return DEFAULT_EVENT_IMAGE;
  }

  /**
   * @desc This accessor respond an image and ensure that even if the user has no image, an 'empty' image will be displayed.
   */
  get influencerImage(): string {
    let img: ImageClass;
    if (!this._influencerData || !this._influencerData.images || !(img = this._influencerData.images.find((i) => i.name === 'profile'))) {
      return DEFAULT_PROFILE_IMAGE;
    }
    return environment.apiHost + environment.apiVersion + '/image/id/' + img.content;
  }

  /**
   * @desc This accessor respond an image and ensure that even if the association has no image, an 'empty' image will be displayed.
   */
  get associationImage(): string {
    let img: ImageClass;

    if (!this._associationData || ! this._associationData.images) {
      return DEFAULT_ASSOCIATION_IMAGE;
    } else if (!(img = this._associationData.images.find(i => i.name === 'profile'))) {
      return DEFAULT_ASSOCIATION_IMAGE;
    } else {
      return environment.apiHost + environment.apiVersion + '/image/id/' + img.content;
    }
  }

  /**
   * @desc This accessor makes the internal influencer data accessible in readonly
   */
  get influencer(): UserPublicClass {
    return this._influencerData;
  }

  /**
   * @desc This accessor makes the internal association data accessible in readonly
   */
  get association(): AssociationModel {
    return this._associationData;
  }

  /**
   * @desc This accessor tells how much participants an event has
   */
  get lotteryParticipants(): number {
    let participants = 0;

    if (!this._eventData || !this._eventData.lotteries || !this._eventData.lotteries.length) {
      return participants;
    }

    for (let lottery of this._eventData.lotteries) {
      participants += lottery.participations.length;
    }

    return participants
  }
}
