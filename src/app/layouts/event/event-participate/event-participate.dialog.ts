import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {EventModel} from '../../../shared/classes/models/event.class';
import {UserClass} from '../../../shared/classes/models/user.class';
import {AssociationModel} from '../../../shared/classes/models/association.model';

export interface Input {
  event: EventModel;
  owner: UserClass;
  assocoation: AssociationModel;
}

@Component({
  selector: 'app-event-participate',
  templateUrl: './event-participate.dialog.html',
  styleUrls: []
})
export class EventParticipateDialog implements OnInit {

  constructor(public ref: MatDialogRef<any>, @Inject(MAT_DIALOG_DATA) public data: Input) {
  }

  private _repartition: number = .5;
  public tokenAmount: number = 100;

  ngOnInit() {
  }

  onNoClick() {
    this.ref.close()
  }

  public setRepartiton(newVal: number) {
    newVal = newVal / 100;
    if (newVal !== this._repartition) {
      this._repartition = newVal;
    }
  }

  public changeTokensAmmount(evt: Event) {
    // @ts-ignore
    const val = Math.round(parseInt(evt.target.value) / 100) * 100;
    this.tokenAmount = val;
  }

  public participate() {
    this.ref.close({
      tokens: this.tokenAmount,
      repartition: this._repartition,
    });
  }

  get avgTokens(): number {
    let tokens = 0;
    let users = 0;

    for (let info of this.data.event.lotteries) {
      tokens += info.totalValue;
      users += info.participations.length;
    }

    if (!tokens || !users) {
      return 0;
    }

    return (tokens / users);
  }

  get totalTokens(): number {
    let tokens = 0;

    for (let info of this.data.event.lotteries) {
      tokens += info.totalValue;
    }

    return tokens;
  }

  get winRate(): number {
    const wR = Math.round(this.tokenAmount / this.totalTokens * 100000000) / 10000000;
    if (!Number.isFinite(wR)) {
      return 100;
    }
    return wR;
  }

  get influencer(): string {
    return 'Emma Cupkake';
  }

  get association(): string {
    return 'Medecins du monde';
  }

  get repartition(): number {
    return Math.round(this._repartition * 100);
  }

  get thousandSeparator(): RegExp {
    return /\B(?=(\d{3})+(?!\d))/g;
  }
}
