/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

/**
 * Import of layouts
 */
import {EventLayout} from './event.layout';
import {EventRoutingModule} from "./event-routing.module";
import {EventParticipateDialog} from './event-participate/event-participate.dialog';
import {MatDialogModule} from "@angular/material/dialog";
import {MatSliderModule} from "@angular/material/slider";
import {MatInputModule} from "@angular/material/input";
import {NgbDatepickerModule, NgbDropdownModule} from "@ng-bootstrap/ng-bootstrap";
import {EventConfirmDialog} from "./event-confirm/event-confirm.dialog";
import {TagInputModule} from "ngx-chips";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PipesModule} from "../../shared/pipes/pipes.module";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {EventEditionLayout} from "./event-edition/event-edition.layout";
import {NgSelectModule} from "@ng-select/ng-select";

/**
 * Team layout module of the Application. This module retrieve team layout
 * You can also find Angular's modules
 */
@NgModule({
  imports: [
    CommonModule,
    EventRoutingModule,
    MatDialogModule,
    MatSliderModule,
    MatInputModule,
    NgbDropdownModule,
    TagInputModule,
    FormsModule,
    PipesModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgbDatepickerModule,
  ],
  entryComponents: [
    EventParticipateDialog,
    EventConfirmDialog,
  ],
  declarations: [
    EventParticipateDialog,
    EventConfirmDialog,
    EventLayout,
    EventEditionLayout
  ],
  exports: [
    EventLayout,
    EventEditionLayout,
  ],
  providers: []
})
export class EventModule {
}
