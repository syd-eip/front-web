import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  NgZone,
  OnInit,
  QueryList,
  ViewChildren,
}                                     from '@angular/core';
import {
  ActivatedRoute,
  Router,
}                                     from '@angular/router';
import { FormControl }                from '@angular/forms';
import { NgbDate }                    from '@ng-bootstrap/ng-bootstrap';
import { TranslatableClass }          from '../../../shared/services/translation/translation-class/translatable.class';
import { TranslationService }         from '../../../shared/services/translation/translation.service';
import { EventService }               from '../../../shared/services/web/event-service';
import { EventModel }                 from '../../../shared/classes/models/event.class';
import { UserClass }                  from '../../../shared/classes/models/user.class';
import { UserService }                from '../../../shared/services/web/user.service';
import { AssociationService }         from '../../../shared/services/web/association.service';
import { AssociationModel }           from '../../../shared/classes/models/association.model';
import { NotificationService }        from '../../../shared/services/notification/notification.service';
import { LoaderService }              from '../../../shared/services/loader/loader.service';
import { NgxImageCompressService }    from 'ngx-image-compress';
import { ImageClass }                 from '../../../shared/classes/models/image.class';
import { ImageService }               from '../../../shared/services/web/image.service';
import { AlertService }               from '../../../shared/services/alert/alert.service';
import { ModalService }               from '../../../shared/services/modal/modal.service';
import { DatetimeSelectionComponent } from '../../../shared/components/datetime-selection/datetime-selection.component';
import { environment }                from '../../../../environments/environment';

@Component({
  selector: 'app-event-edition-layout',
  templateUrl: './event-edition.layout.html',
})
export class EventEditionLayout extends TranslatableClass implements OnInit {
  private _evtId: string = '';
  private _eventData: EventModel = null;
  // @ts-ignore
  private _oldEvent: EventModel = {};
  private _influencerData: UserClass = null;
  private _associationData: AssociationModel = null;
  private _associationList: AssociationModel[] = [];
  public image: ImageClass = null;
  public tags: string[] = [];
  public nwinners: number = 1;

  public eventDate: Date = new Date((Date.now() - Date.now() % 900000) + 900000 + 86400000 * 2);
  public drawDate: Date = new Date((Date.now() - Date.now() % 900000) + 900000 + 86400000);
  public displayedEventDate: Date;
  public displayedDrawDate: Date;
  private _createMode: boolean = false;

  public associationControl: FormControl = new FormControl(null)

  @ViewChildren('title') titleElement: QueryList<ElementRef<HTMLHeadingElement>>;
  @ViewChildren('description') descriptionElement: QueryList<ElementRef<HTMLParagraphElement>>;
  @ViewChildren('banner') bannerElement: QueryList<ElementRef<HTMLDivElement>>;
  @ViewChildren('blurredBanner') blurredBannerElement: QueryList<ElementRef<HTMLDivElement>>;

  /**
   * @description Constructor of EventLayout layout
   *
   * The constructor creates an instance of the EventLayout layout and specifies the default values
   * the input and output variables of the layout.
   *
   * @param _translationService Provides access to information about the website translations. This enable the
   *   possibility to display text in French, English ...
   * @param _eventService Provides access to information or operations about any event. This simplify and unify the way
   *   to query the events.
   * @param _userService Provides access to information or operations about any users. This simplify and unify the way
   *   to query the users.
   * @param _associationService Provide access to information or operation about any associations. This simplify and
   *   unify the way to query the association.
   * @param _activatedRoute Provides access to information about a route associated with a component that is loaded in
   *   an outlet
   * @param router Implements the Angular Router service , which enables navigation from one view to the next as users
   *   perform application tasks
   * @param _notificationService Allow the layout to display and manage notifications
   * @param _loaderService Allow the layout to interact with the website loaders
   * @param _imageCompressor Allow the layout to load and compress an image that come from the user computer
   * @param _modalService
   * @param cdr
   * @param _imageService Provides access to information or operations about any images. This simplify and unify the
   *   way to query the images.
   * @param _alertService Allow the layout to display and manage alerts
   * @param _zone A reference to the component runtime
   */
  constructor(private _translationService: TranslationService, private _eventService: EventService,
              private _userService: UserService, private _associationService: AssociationService,
              private _activatedRoute: ActivatedRoute, private router: Router, private _notificationService: NotificationService,
              private _loaderService: LoaderService, private _imageCompressor: NgxImageCompressService,
              private _modalService: ModalService, private cdr: ChangeDetectorRef,
              private _imageService: ImageService, private _alertService: AlertService, private _zone: NgZone) {
    super(_translationService);
  }

  /**
   * @description: This method is part of the component life cycle
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit() {
    this._evtId = this._activatedRoute.snapshot.paramMap.get('id');
    this._createMode = this._activatedRoute.snapshot.routeConfig.path === 'create';

    this._userService.update()
    this._userService.userSubject.asObservable().subscribe((data) => {
      this._loaderService.unload('GLOBAL')
      this._zone.run(() => {
        this._influencerData = data
      });
    });
    this._influencerData = this._userService.user;
    if (!this._influencerData) {
      this._loaderService.load('GLOBAL')
    }

    if (!this._createMode) {
      this._loaderService.load('GLOBAL')
      this.loadEvent()
        .then((evt) => {
          this._associationService.getById(evt.associationId)
            .then((res) => {
              this._associationData = res;
              this._loaderService.unload('GLOBAL')
            })
            .catch((err) => {
              console.error(err)
              this._loaderService.unload('GLOBAL')
            });
        })
    } else {
      this._associationService.getAll()
        .then(associations => this._associationList = associations)
    }
    this.drawDate = new Date((Date.now() - Date.now() % 900000) + 900000 + 86400000);

    // Fix for https://stackoverflow.com/a/61082536/11526439
    let tmp = this.drawDate;
    tmp.setMinutes(tmp.getMinutes() - tmp.getTimezoneOffset());
    this.displayedDrawDate = tmp;

    tmp = this.eventDate;
    tmp.setMinutes(tmp.getMinutes() - tmp.getTimezoneOffset());
    this.displayedEventDate = tmp;
  }

  /**
   * @desc This function loads the event data from the API
   */
  async loadEvent(): Promise<EventModel> {
    const event = await this._eventService.getEvent(this.eventId)

    this._eventData = event;
    this._oldEvent = Object.assign({}, event)

    this.tags = event.information.tags;
    return event;
  }

  /**
   * @desc This function evaluates and build the deltas between the 'old' version and the 'new' and persist them in the
   *   API
   */
  saveChanges() {
    this._alertService.confirm(this.translate('EVENTS_LAYOUT/EDITING_SAVE_CONFIRM'))
      .subscribe(async (res) => {
        if (!res) {
          return;
        }

        this._loaderService.load('GLOBAL')
        const description = this.descriptionElement.first.nativeElement.innerText;
        const name = this.titleElement.first.nativeElement.innerText;
        const tags = this.tags;

        let differences = {};
        if (description !== this._eventData.information.description) {
          differences = {description, ...differences};
        }
        if (name !== this._eventData.information.name) {
          differences = {name, ...differences};
        }
        if (tags !== undefined) {
          differences = {tags: this.tags, ...differences};
        }
        if (this.image && this.image.name) {
          differences = {image: this.image, ...differences};
        }

        await this._eventService.patch(this._evtId, differences)
        this._alertService.success(this.translate('EVENTS_LAYOUT/CREATE_SUCCESS'))
        this._loaderService.unload('GLOBAL')
        this.navigateTo('/event/' + this._evtId)
      })
  }

  /**
   * @desc This function build an event form and persist it in the API with his images
   */
  createEvent() {
    const event = {
      name: this.titleElement.first.nativeElement.innerText,
      description: this.descriptionElement.first.nativeElement.innerText,
      association: this.associationControl.value,
      drawDate: this.drawDate,
      eventDate: this.eventDate,
      lotteriesSize: 1,
      tags: this.tags
    }
    let failure = false;

    if (!event.name) {
      this._notificationService.error(this.translate('EVENTS_LAYOUT/ERROR_NO_TITLE'))
      failure = true;
    }
    if (!event.description) {
      this._notificationService.error(this.translate('EVENTS_LAYOUT/ERROR_NO_DESCRIPTION'))
      failure = true
    }
    if (!event.association) {
      this._notificationService.error(this.translate('EVENTS_LAYOUT/ERROR_NO_ASSOCIATION'))
      failure = true
    }
    if (!event.drawDate) {
      this._notificationService.error(this.translate('EVENTS_LAYOUT/ERROR_NO_DRAW_DATE'))
      failure = true
    }
    if (!event.eventDate) {
      this._notificationService.error(this.translate('EVENTS_LAYOUT/ERROR_NO_EVENT_DATE'))
      failure = true
    }
    if (event.lotteriesSize <= 0) {
      this._notificationService.error(this.translate('EVENTS_LAYOUT/ERROR_WRONG_LOTTERY_SIZE'))
      failure = true
    }
    if (event.drawDate >= event.eventDate) {
      this._notificationService.error(this.translate('EVENTS_LAYOUT/ERROR_DATE_DRAW_HIGHER_THAN_EVENT'))
      failure = true
    }
    if (event.drawDate < new Date()) {
      this._notificationService.error(this.translate('EVENTS_LAYOUT/ERROR_DATE_DRAW_LOWER_THAN_NOW'))
      failure = true
    }
    if (event.eventDate < new Date()) {
      this._notificationService.error(this.translate('EVENTS_LAYOUT/ERROR_DATE_EVENT_LOWER_THAN_NOW'))
      failure = true
    }
    if (failure) {
      return;
    }
    this._alertService.confirm(this.translate('EVENTS_LAYOUT/CREATE_CONFIRM'))
      .subscribe(async (res) => {
        if (!res) {
          return;
        }
        this._loaderService.load('GLOBAL')
        this._eventService.create(event, this.image)
          .then(id => {
            this._alertService.success(this.translate('EVENTS_LAYOUT/CREATE_SUCCESS'))
            this._loaderService.unload('GLOBAL')
            this.navigateTo('/event/' + id)
          })
      });
  }

  /**
   * This function is called when a date input change and set the date in the internal variable
   * @param date The date from the input
   * @param type The date to change
   */
  onDateChange(date: NgbDate, type: string) {
    const day: string = (date.day < 10) ? '0' + date.day : date.day.toString();
    const month: string = (date.month < 10) ? '0' + date.month : date.month.toString();

    const final: Date = new Date(`${date.year}-${month}-${day}T00:00:00`);
    switch (type) {
      case 'event':
        this.eventDate = final;
        break;
      // case 'draw':
      //   this.drawDate = final;
      //   break;
    }
  }

  /**
   * @desc This function adds a tag to the internal list each time the input has one more
   * @param tag
   */
  onAddTag(tag: any) {
    if (this.tags.indexOf(tag.value) === -1) {
      this.tags.push(tag)
    }
  }

  /**
   * @desc This function removes a tag to the internal list each time the input has one less
   * @param tag
   */
  onRemoveTag(tag: any) {
    this.tags = this.tags.filter((v => v !== tag.value))
  }

  /**
   * @desc This function is called each time the user click on the banner.
   *
   * This will ask the user to select an image, will reduce and compress it and store it for further operations
   */
  changeBanner() {
    this._imageCompressor.uploadFile()
      .then(({image, orientation}) => {

        this._imageCompressor.compressFile(image, orientation, 30, 50).then(content => {
          this.blurredBannerElement.first.nativeElement.style.backgroundImage = 'url(\'' + content + '\')';
          this.bannerElement.first.nativeElement.style.backgroundImage = 'url(\'' + content + '\')';

          this.image = {name: 'banner', content};
          console.log(this.image)
        });
      });
  }

  /**
   * @desc This function is called when the user click on the 'discard' button.
   *
   * This will simply redirect on '/' when the user is in the create mode and the event review otherwise
   */
  discardChanges() {
    this._alertService.confirm(this.translate('EVENTS_LAYOUT/EDITING_DISCARD_CONFIRM'))
      .subscribe(async (res) => {
        if (!res) {
          return;
        }
        if (this.createMode) {
          this.navigateTo('/');
        } else {
          this.navigateTo('/event/' + this.eventId);
        }
      })
  }

  /**
   * @description The method uses the Angular router and allows you to navigate the application's routes
   *
   * @param path Path to navigate to
   * @param qp Query params to add
   */
  navigateTo(path: string, qp: { [key: string]: any } = undefined): void {
    this.router.navigate([path], {queryParams: qp})
      .then(value => {
      });
  }

  selectDate($event: Event, type: string) {
    $event.preventDefault()
    $event.stopPropagation()
    const ref = this._modalService.open(DatetimeSelectionComponent, {
      centered:    true,
      windowClass: 'modal-date-custom',
    });

    const component = ref.componentInstance;
    const now = new Date();
    now.setDate(now.getDate() + 1);

    // Round time at the next hh:15:00 (in the future if not now) + one day
    // Formula is (Now - Now % 00:15:00) + 00:15:00 + 1d
    // The output time is tomorrow at any hour but 00, 15, 30, or 45 minutes.
    const roundedNow = (now.getTime() - now.getTime() % 900000) + 900000;

    component.minDate = now;
    if (type === 'draw') {
      if (this.eventDate) {
        component.value = this.drawDate || new Date(roundedNow)
      } else {
        component.value = new Date(roundedNow)
      }
    } else if (type === 'event') {
      if (this.drawDate) {
        component.minDate = this.drawDate

        const tmp = this.drawDate
        tmp.setDate(tmp.getDate() + 1)
        component.value = this.eventDate || this.drawDate
      } else {
        component.minDate = new Date(roundedNow)
        component.value = new Date(roundedNow)
      }
    }

    // component.value = new Date(roundedNow)
    component.valueChange.subscribe((date) => {

      switch (type) {
        case 'event':
          this.eventDate = date as Date;
          this.displayedEventDate = date.setMinutes(date.getMinutes() - date.getTimezoneOffset())
          console.log('set event date', date.toJSON())
          break;
        case 'draw':
          this.drawDate = date as Date;
          this.displayedDrawDate = date.setMinutes(date.getMinutes() - date.getTimezoneOffset())
          console.log('set draw date', date.toJSON())
          break;
      }
      ref.dismiss();
    });
  }

  /**
   * @desc This accessor tells if the user is in the creation mode
   */
  get createMode(): boolean {
    return this._createMode;
  }

  /**
   * @desc This accessor makes the internal event id accessible in readonly
   */
  get eventId(): string {
    return this._evtId;
  }

  /**
   * @desc This accessor makes the internal event data accessible in readonly
   */
  get event(): EventModel {
    return this._eventData;
  }

  /**
   * @desc This accessor respond an image and ensure that even if the event has no image, an 'empty' image will be
   *   displayed.
   */
  get bannerImage(): string {
    if (this.image && this.image.content) {
      return this.image.content;
    } else if (this._eventData && this._eventData.images && this._eventData.images.find(i => i.name === 'banner')) {
      const img = this._eventData.images.find(i => i.name === 'banner');
      return environment.apiHost + environment.apiVersion + '/image/id/' + img.content;
    }
    return '/assets/img/default-banner.jpg';
  }

  /**
   * @desc This accessor respond an image and ensure that even if the user has no image, an 'empty' image will be
   *   displayed.
   */
  get influencerImage(): string {
    let img: ImageClass;
    if (!this._influencerData || !this._influencerData.images || !(img = this._influencerData.images.find((i) => i.name === 'profile'))) {
      return '/assets/img/account.png';
    }
    return environment.apiHost + environment.apiVersion + '/image/id/' + img.content;
  }

  /**
   * @desc This accessor respond an image and ensure that even if the association has no image, an 'empty' image will
   *   be displayed.
   */
  get associationImage(): string {
    let img: ImageClass;

    if (!this._associationData || ! this._associationData.images) {
      return '/assets/img/default_profile.png';
    } else if (!(img = this._associationData.images.find(i => i.name === 'profile'))) {
      return '/assets/img/default_profile.png';
    } else {
      return environment.apiHost + environment.apiVersion + '/image/id/' + img.content;
    }
  }


  /**
   * @desc This accessor makes the internal influencer data accessible in readonly
   */
  get influencer(): UserClass {
    return this._influencerData;
  }

  /**
   * @desc This accessor makes the internal association list accessible in readonly
   */
  get associationList(): AssociationModel[] {
    return this._associationList;
  }

  /**
   * @desc This accessor makes the internal association data accessible in readonly
   */
  get association(): AssociationModel {
    return this._associationData;
  }

  /**
   * @desc This accessor tells how much participants an event has
   */
  get lotteryParticipants(): number {
    let participants = 0;

    if (!this._eventData || !this._eventData.lotteries || !this._eventData.lotteries.length) {
      return participants;
    }

    for (const lottery of this._eventData.lotteries) {
      participants += lottery.participations.length
    }

    return participants
  }

  isMobile(): boolean {
    return window.innerWidth <= 992;
  }
}
