/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
/**
 * Import of layout
 */
import {RoutingErrorLayout} from '../routing-error/routing-error.layout';
import {EventLayout} from "./event.layout";
import {EventOwnerGuardService} from "../../shared/services/guards/event-owner-guard-service";
import {EventEditionLayout} from "./event-edition/event-edition.layout";

/**
 * Route of the application
 */
const routes: Routes = [
  {
    path: 'create',
    component: EventEditionLayout,
    pathMatch: 'full',
  },
  {
    path: ':id',
    children: [
      {
        path: '',
        component: EventLayout,
        pathMatch: 'full'
      },
      {
        path: 'edit',
        component: EventEditionLayout,
        canActivate: [EventOwnerGuardService],
        pathMatch: 'full',
      }
    ],
  },
  {
    path: '**',
    component: RoutingErrorLayout
  },
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule,
  ],
})
export class EventRoutingModule {
}
