import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TranslatableClass} from '../../../shared/services/translation/translation-class/translatable.class';
import {TranslationService} from '../../../shared/services/translation/translation.service';

@Component({
  selector: 'app-event-confirm',
  templateUrl: './event-confirm.dialog.html',
  styleUrls: []
})
export class EventConfirmDialog extends TranslatableClass implements OnInit {

  public readonly message: string;

  constructor(translationService: TranslationService, public ref: MatDialogRef<boolean>, @Inject(MAT_DIALOG_DATA) public data: string) {
    super(translationService)
    this.message = data;
  }

  ngOnInit() {
  }

  no() {
    this.ref.close(false)
  }

  yes() {
    this.ref.close(true)
  }
}
