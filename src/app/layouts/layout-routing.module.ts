/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
/**
 * Import of layout
 */
import {PrivacyPoliciesLayout} from './privacy-policies/privacy-policies.layout';
import {RoutingErrorLayout} from './routing-error/routing-error.layout';
import {HomeLayout} from './home/home.layout';

/**
 * Route of the application
 */
const routes: Routes = [
  {
    path: '',
    component: HomeLayout
  },
  {
    path: 'user',
    loadChildren: () => import('./user/user.module')
      .then(m => m.UserModule)
  },
  {
    path: 'event',
    loadChildren: () => import('./event/event.module')
      .then(m => m.EventModule)
  },
  {
    path: 'association',
    loadChildren: () => import('./association/association.module')
      .then(m => m.AssociationModule)
  },
  {
    path: 'privacy-policies',
    component: PrivacyPoliciesLayout
  },
  {path: '**', component: RoutingErrorLayout}
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule,
  ],
})
export class LayoutRoutingModule {
}
