/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
/**
 * Import of layout
 */
import {AssociationLayout} from './association.layout';
import {RoutingErrorLayout} from '../routing-error/routing-error.layout';
import {AssociationListLayout} from './association-list/association-list.layout';
import {AssociationEntityLayout} from './association-entity/association-entity.layout';
import {AssociationGuardService} from '../../shared/services/guards/association-guard.service';

/**
 * Route of the application
 */
const routes: Routes = [
  {
    path: '',
    component: AssociationLayout,
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        component: AssociationListLayout
      },
      {
        path: ':name',
        canActivate: [AssociationGuardService],
        component: AssociationEntityLayout
      },
      {
        path: '**',
        component: RoutingErrorLayout
      }
    ]
  }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule,
  ],
})
export class AssociationRoutingModule {
}
