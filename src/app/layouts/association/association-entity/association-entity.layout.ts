import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {
  GET_ASSOCIATION_BY_ID,
  PATCH_ASSOCIATION_DESCRIPTION,
  PATCH_ASSOCIATION_EMAIL,
  PATCH_ASSOCIATION_IMAGE,
  PATCH_ASSOCIATION_LINK,
  PATCH_ASSOCIATION_LOCATION,
  PATCH_ASSOCIATION_NAME,
  PATCH_ASSOCIATION_PHONE
} from '../../../shared/data/api/api-path.data';
import {TranslatableClass} from '../../../shared/services/translation/translation-class/translatable.class';
import {TranslationService} from '../../../shared/services/translation/translation.service';
import {AlertService} from '../../../shared/services/alert/alert.service';
import {Semaphore} from '../../../shared/classes/semaphore/semaphore.class';
import * as deepEqual from 'deep-equal';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgxImageCompressService} from 'ngx-image-compress';
import {SafeResourceUrl} from '@angular/platform-browser';
import Swiper from 'swiper';
import {UserService} from '../../../shared/services/web/user.service';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-association-entity',
  templateUrl: './association-entity.layout.html'
})
export class AssociationEntityLayout extends TranslatableClass implements OnInit, OnDestroy {

  /**
   * @description Association id;
   */
  private _id: string;

  /**
   * @description Touch slider with hardware accelerated transitions and native behavior
   */
  private swiper: Swiper;

  /**
   * @description Boolean indicating edit mode
   */
  private _canEdit: boolean;

  /**
   * @description Component that tracks the value and validity state of a group of FormControl instances.
   */
  private _formGroup: FormGroup;

  /**
   * @description Image map containing the profile image (the association's logo) and the wall image
   */
  private _images: Map<string, SafeResourceUrl>;

  /**
   * @description Data model received by the API corresponding to an association
   */
  private _association: { last: any, current: any };

  /**
   * @description Image array containing the carousel images
   */
  private _imagesCarousel: Array<{ name: string, content: SafeResourceUrl }>;


  /**
   * @description Constructor of AssociationEntityLayout component
   *
   * The constructor creates an instance of the AssociationEntityLayout component and specifies the default values
   * the input and output variables of the component.
   *
   * @param translationService A reference to the translation service of the application
   * @param activatedRoute Provides access to information about a route associated with a component that is loaded in an outlet
   * @param userService A reference to the user service of the application
   * @param router Implements the Angular Router service , which enables navigation from one view to the next as users
   * perform application tasks
   * @param alertService A reference to the alert service of the application
   * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
   * @param imageCompress Angular utility for compressing images to a satisfying size
   */
  constructor(translationService: TranslationService, private activatedRoute: ActivatedRoute, private userService: UserService,
              private alertService: AlertService, private router: Router, private formBuilder: FormBuilder,
              private imageCompress: NgxImageCompressService) {
    super(translationService);

    this.association = {last: null, current: null};

    this.images = new Map<string, SafeResourceUrl>();
    this.imagesCarousel = new Array<{ name: string, content: SafeResourceUrl }>();
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    const name: string = this.activatedRoute.snapshot.paramMap.get('name');
    this.id = this.activatedRoute.snapshot.queryParamMap.get('id');

    if (name && this.id) {
      this.loadAssociation(this.id);

      this.formGroup = this.formBuilder.group(
        {
          name: [null, Validators.compose([
            Validators.required
          ])],
          description: [null, Validators.compose([
            Validators.required,
          ])],
          email: [null, Validators.compose([
            Validators.required,
          ])],
        });
    } else {
      this.router.navigate(['/association/list']).then();
    }

    (document.getElementsByClassName('layout-column')[0] as HTMLElement).style.backgroundColor = 'white';

    this.initSwiper();

    this.canEdit = (this.userService.userSubject.last && this.userService.connectedSubject.last &&
      this.userService.userSubject.last.role.find(item => item === 'SUPER_ADMIN'));
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
   */
  ngOnDestroy(): void {
    delete this.association;
    (document.getElementsByClassName('layout-column')[0] as HTMLElement).removeAttribute('style');
  }

  /**
   * @description This method allows to load an image and to compress it before saving it in a database thanks to the API
   *
   * @param event Event occured
   * @param name Image name
   */
  uploadImage(event: Event, name: string): void {
    const parent = this;
    const reader = new FileReader();

    reader.onloadend = function (event) {
      parent.patchImage(name, reader.result.toString());
    };
    reader.readAsDataURL((event.target as HTMLInputElement).files[0]);
  }

  /**
   * @description This method makes it possible to know the size of the screen.
   * If the size is less than 992px, the function will return the value true
   */
  isMobile(): boolean {
    return window.innerWidth < 992;
  }

  /**
   * @description This method returns the value of the focus of an HTML element
   *
   * @param value HTMLElement element
   */
  isFocused(value: HTMLElement): boolean {
    return document.activeElement === value;
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to retrieve an association thanks to its id
   *
   * It calls the method Get of the NetworkService service
   */
  private loadAssociation(id: string): void {
    this.userService.activeLoader(new Semaphore(1));

    this.userService.get(GET_ASSOCIATION_BY_ID + id)
      .then(response => {
        this.association.current = response.responseData.data;
        this.association.last = JSON.parse(JSON.stringify(response.responseData.data));

        this.loadImages();
      })
      .catch(error => {
        if (error.responseError && error.responseError.message) {
          this.alertService.error(this.translate(error.responseError.message));
        } else {
          this.alertService.error(this.translate('ERROR/ASSOCIATION_RETRIEVED'));
        }
      })
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to retrieve all association images
   *
   * It calls the method Get of the NetworkService service
   */
  private loadImages(): void {
    if (this.association.current.images) {
      this.association.current.images.forEach(image => {

        if (image.name === 'profile' || image.name === 'banner') {
          this.images.set(image.name, environment.apiHost + environment.apiVersion + '/image/id/' + image.content);
        } else {
          this.imagesCarousel = [...this.imagesCarousel, {
            name: image.name,
            content: environment.apiHost + environment.apiVersion + '/image/id/' + image.content
          }];
        }
      });
    }
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to update an association name thanks to its id
   *
   * It calls the method Patch of the NetworkService service
   */
  public patchName(): void {
    if (this.association.current && this.association.current.name) {

      if (!deepEqual(this.association.current.name, this.association.last.name)) {
        this.userService.activeLoader(new Semaphore(1));

        this.userService.patch({name: this.association.current.name}, PATCH_ASSOCIATION_NAME.replace('(?id?)', this.id))
          .then(response => {
            this.association.last.name = response.responseData.data;
            this.alertService.success(this.translate(response.responseData.message));
          })
          .catch(error => this.alertService.error(this.translate(error.responseError.message)));
      }
    }
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to update an association webiste link thanks to its id
   *
   * It calls the method Patch of the NetworkService service
   */
  public patchLink(): void {
    if (this.association.current && this.association.current.link) {

      if (!deepEqual(this.association.current.link, this.association.last.link)) {
        this.userService.activeLoader(new Semaphore(1));

        this.userService.patch({link: this.association.current.link}, PATCH_ASSOCIATION_LINK.replace('(?id?)', this.id))
          .then(response => {
            this.association.last.link = response.responseData.data;
            this.alertService.success(this.translate(response.responseData.message));
          })
          .catch(error => this.alertService.error(this.translate(error.responseError.message)));
      }
    }
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to update an association email thanks to its id
   *
   * It calls the method Patch of the NetworkService service
   */
  private patchEmail(): void {
    if (this.association.current.email) {

      if (!deepEqual(this.association.current.email, this.association.last.email)) {

        this.userService.activeLoader(new Semaphore(1));

        this.userService.patch({email: this.association.current.email}, PATCH_ASSOCIATION_EMAIL.replace('(?id?)', this.id))
          .then(response => {
            this.association.last.email = response.responseData.data;
            this.alertService.success(this.translate(response.responseData.message));
          })
          .catch(error => this.alertService.error(this.translate(error.responseError.message)));
      }
    }
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to update an association description thanks to its id
   *
   * It calls the method Patch of the NetworkService service
   */
  private patchDescription(): void {
    if (this.association.current.description) {

      if (!deepEqual(this.association.current.description, this.association.last.description)) {
        this.userService.activeLoader(new Semaphore(1));

        this.userService.patch({description: this.association.current.description},
          PATCH_ASSOCIATION_DESCRIPTION.replace('(?id?)', this.id))
          .then(response => {
            this.association.last.description = response.responseData.data;
            this.alertService.success(this.translate(response.responseData.message));
          })
          .catch(error => this.alertService.error(this.translate(error.responseError.message)));
      }
    }
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to update an association phone number thanks to its id
   *
   * It calls the method Patch of the NetworkService service
   *
   * @param event Event received by the SelectDialCodeComponent component
   */
  private patchPhone(event?: any): void {
    if (this.association.current.phone) {

      this.association.current.phone.nationalNumber = this.association.current.phone.nationalNumber.replace(' ', '');
      this.association.current.phone.countryCode = (event) ? event.countryCode : this.association.current.phone.countryCode;

      if (!deepEqual(this.association.current.phone.nationalNumber, this.association.last.phone.nationalNumber)) {

        this.userService.activeLoader(new Semaphore(1));

        this.userService.patch(this.association.current.phone, PATCH_ASSOCIATION_PHONE.replace('(?id?)', this.id))
          .then(response => {
            this.association.current.phone = response.responseData.data;
            this.association.last.phone = JSON.parse(JSON.stringify(response.responseData.data));

            this.alertService.success(this.translate(response.responseData.message));
          })
          .catch(error => this.alertService.error(this.translate(error.responseError.message)));
      }
    }
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to update an association location thanks to its id
   *
   * It calls the method Patch of the NetworkService service
   *
   * @param event Event received by the SelectCountryComponent component
   */
  private patchLocation(event?: any): void {
    if (this.association.current.location) {

      this.association.current.location.country = (event) ? event.country : this.association.current.location.country;
      this.association.current.location.countryCode = (event) ? event.countryCode : this.association.current.location.countryCode;

      if (!deepEqual(this.association.current.location, this.association.last.location)) {
        this.userService.activeLoader(new Semaphore(1));

        this.userService.patch(this.association.current.location, PATCH_ASSOCIATION_LOCATION.replace('(?id?)', this.id))
          .then(response => {
            this.association.last.location = response.responseData.data;
            this.alertService.success(this.translate(response.responseData.message));
          })
          .catch(error => this.alertService.error(this.translate(error.responseError.message)));
      }
    }
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to update an association images thanks to its id
   *
   * It calls the method Patch of the NetworkService service
   *
   * @param name Image name
   * @param value Content of the image
   * @param index Index of the current slider
   */
  private patchImage(name: string, value: string, index?: number): void {
    this.userService.activeLoader(new Semaphore(1));

    value = value.split(',')[1];
    this.userService.patch({name: name, content: value}, PATCH_ASSOCIATION_IMAGE.replace('(?id?)', this.id))
      .then(res => {
        if (name === 'profile' || name === 'banner') {
          this.images.set(name, environment.apiHost + environment.apiVersion + '/image/id/' + res.responseData.data.find(item => item.name === name).content);
        } else {
          this.imagesCarousel = (value !== '') ? [...this.imagesCarousel, {
              name: name,
              content: environment.apiHost + environment.apiVersion + '/image/id/' + res.responseData.data.find(item => item.name === name).content
            }] :
            this.imagesCarousel.filter(item => item.name !== name);

          (value === '') ? this.swiper.removeSlide(index) : this.initSwiper();
        }
        this.alertService.success(this.translate(res.responseData.message));
      })
      .catch(error => {
        this.alertService.error(this.translate(error.responseError.message));
      })
  }

  /**
   * @description This method is used to initialize the slider present in the component (Swiper slider)
   */
  private initSwiper(): void {
    this.swiper = new Swiper('.swiper-container', {
      initialSlide: 3,
      centeredSlides: true,
      updateOnImagesReady: true,
      preventClicks: false,
      preventClicksPropagation: false,
      slidesPerView: 3,
      spaceBetween: 12,
      speed: 500,
      autoplay: {
        delay: 4000
      }
    });
  }

  /**
   * @description The method allows you to retrieve the component that tracks the value and validity state of a group of
   * FormControl instances
   */
  get formGroup(): FormGroup {
    return this._formGroup;
  }

  /**
   * @description The method allows you to assign the component that tracks the value and validity state of a group of
   * FormControl instances
   *
   * @param value Value of the new component that tracks the value and validity state of a group of FormControl instances
   */
  set formGroup(value: FormGroup) {
    this._formGroup = value;
  }

  /**
   * @description The method allows you to retrieve the data model received by the API corresponding to an association
   */
  get association(): { last: any, current: any } {
    return this._association;
  }

  /**
   * @description The method allows you to assign the data model received by the API corresponding to an association
   *
   * @param value Value of the new data model received by the API corresponding to an association
   */
  set association(value: { last: any, current: any }) {
    this._association = value;
  }

  /**
   * @description The method allows you to retrieve the association id
   */
  get id(): string {
    return this._id;
  }

  /**
   * @description The method allows you to assign the association id
   *
   * @param value Value of the new association id
   */
  set id(value: string) {
    this._id = value;
  }

  /**
   * @description The method allows you to retrieve the image map containing the profile image (the association's logo) and the wall image
   */
  get images(): Map<string, SafeResourceUrl> {
    return this._images;
  }

  /**
   * @description The method allows you to assign the image map containing the profile image (the association's logo) and the wall image
   *
   * @param value Value of the image map containing the profile image (the association's logo) and the wall image
   */
  set images(value: Map<string, SafeResourceUrl>) {
    this._images = value;
  }

  /**
   * @description The method allows you to retrieve the boolean indicating edit mode
   */
  get canEdit(): boolean {
    return this._canEdit;
  }

  /**
   * @description The method allows you to assign the boolean indicating edit mode
   *
   * @param value Value of the new boolean indicating edit mode
   */
  set canEdit(value: boolean) {
    this._canEdit = value;
  }

  /**
   * @description The method allows you to retrieve the image array containing the carousel images
   */
  get imagesCarousel(): Array<{ name: string; content: SafeResourceUrl }> {
    return this._imagesCarousel;
  }

  /**
   * @description The method allows you to assign the image array containing the carousel images
   *
   * @param value Value of the new image array containing the carousel images
   */
  set imagesCarousel(value: Array<{ name: string; content: SafeResourceUrl }>) {
    this._imagesCarousel = value;
  }
}

