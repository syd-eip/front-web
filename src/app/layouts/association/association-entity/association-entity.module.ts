/**
 * Import of Angular's module
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
/**
 * Import of application's layout
 */
import {AssociationEntityLayout} from './association-entity.layout';
/**
 * Import of application's module
 */
import {PipesModule} from '../../../shared/pipes/pipes.module';
import {ComponentsModule} from '../../../shared/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PipesModule,
    ComponentsModule
  ],
  declarations: [
    AssociationEntityLayout
  ],
  exports: [
    AssociationEntityLayout
  ],
})
export class AssociationEntityModule {
}
