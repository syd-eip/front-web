/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of application layout's modules
 */
import {AssociationListModule} from './association-list/association-list.module';
import {AssociationEntityModule} from './association-entity/association-entity.module';
/**
 * Import of application layout's components
 */
import {AssociationLayout} from './association.layout';
import {RouterModule} from '@angular/router';
import {AssociationRoutingModule} from './association-routing.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AssociationRoutingModule,
    AssociationEntityModule,
    AssociationListModule
  ],
  declarations: [
    AssociationLayout
  ],
  exports: [
    AssociationLayout,
    AssociationEntityModule,
    AssociationListModule
  ]
})
export class AssociationModule {
}
