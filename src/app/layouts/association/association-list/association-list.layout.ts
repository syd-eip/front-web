import {Component, OnInit} from '@angular/core';
import {NetworkService} from '../../../shared/services/web/network.service';
import {Semaphore} from '../../../shared/classes/semaphore/semaphore.class';
import {GET_ASSOCIATIONS} from '../../../shared/data/api/api-path.data';
import {AlertService} from '../../../shared/services/alert/alert.service';
import {TranslatableClass} from '../../../shared/services/translation/translation-class/translatable.class';
import {TranslationService} from '../../../shared/services/translation/translation.service';
import {Router} from '@angular/router';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-association-list',
  templateUrl: './association-list.layout.html'
})
export class AssociationListLayout extends TranslatableClass implements OnInit {

  /**
   * @description List of all associations of the application
   */
  private _associations: Array<any>;

  /**
   * @description Constructor of AssociationListLayout component
   *
   * The constructor creates an instance of the AssociationListLayout component and specifies the default values
   * the input and output variables of the component.
   *
   * @param translationService A reference to the i18n translation service of the application
   * @param networkService A reference to the http request service of the application
   * @param alertService A reference to the alert service of the application
   * @param router A reference to the Angular injectable router service
   */
  constructor(translationService: TranslationService, private networkService: NetworkService, private alertService: AlertService,
              private router: Router) {
    super(translationService);

    this.associations = [];
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.loadAssociations();
  }

  /**
   * @description The method uses the Angular router and allows you to navigate the application's routes
   *
   * @param name Association name used to navigate
   * @param id Association id used to retrieve the association
   */
  navigateTo(name: string, id: number): void {
    this.router.navigate(['/association/' + name], {queryParams: {id: id}}).then();
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to retrieve all associations of the platform
   *
   * It calls the method Get of the NetworkService service
   */
  private loadAssociations(): void {
    this.networkService.activeLoader(new Semaphore(1));

    this.networkService.get(GET_ASSOCIATIONS)
      .then(response => {
        this.associations = [...this.associations, ...response.responseData.data];
        this.loadImages();
      })
      .catch(error => {
        if (error.responseError && error.responseError.message) {
          this.alertService.error(this.translate(error.responseError.message));
        } else {
          this.alertService.error(this.translate('ERROR/ASSOCIATION_RETRIEVED'));
        }
      })
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to retrieve the association logo
   *
   * It calls the method Get of the NetworkService service
   */
  private loadImages(): void {
    this.associations.forEach(association => {
      if (association.images && association.images.length > 0 && association.images.find(item => item.name === 'profile')) {

        association.images = {
          name: 'profile',
          content: environment.apiHost + environment.apiVersion + '/image/id/' +
            association.images.find(item => item.name === 'profile').content
        };
      } else {
        association.images = {name: 'profile', content: '/assets/img/syd_object.png'};
      }
    });
  }

  /**
   * @description The method allows you to retrieve the list of all associations of the application
   */
  get associations(): Array<any> {
    return this._associations;
  }

  /**
   * @description The method allows you to assign the list of all associations of the application
   *
   * @param value Value of the new list of all associations of the application
   */
  set associations(value: Array<any>) {
    this._associations = value;
  }
}
