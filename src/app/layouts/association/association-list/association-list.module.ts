/**
 * Import of Angular's module
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
/**
 * Import of application's layout
 */
import {AssociationListLayout} from './association-list.layout';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    AssociationListLayout,
  ],
  exports: [
    AssociationListLayout,
  ],
})
export class AssociationListModule {
}
