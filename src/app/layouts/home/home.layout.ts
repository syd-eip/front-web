import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {TranslatableClass} from '../../shared/services/translation/translation-class/translatable.class';
import {TranslationService} from '../../shared/services/translation/translation.service';
import Swiper from 'swiper';
import {UserService} from '../../shared/services/web/user.service';
import {ModalPreferencesComponent} from '../../shared/components/modal/modal-preferences/modal-preferences.component';
import {ModalService} from '../../shared/services/modal/modal.service';
import {map} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {
  GET_ASSOCIATION_BY_ID,
  GET_EVENT_BY_TAG_NAME, GET_EVENTS,
  GET_TAGS,
  GET_USER_PUBLIC_BY_ID
} from '../../shared/data/api/api-path.data';
import {AlertService} from '../../shared/services/alert/alert.service';
import {createChunkedArray} from '../../shared/functions/get-chunked-array.function';
import {Semaphore} from '../../shared/classes/semaphore/semaphore.class';
import {environment} from '../../../environments/environment';
import {NetworkParamsClass} from '../../shared/services/network/network-params/network-params.class';

@Component({
  selector: 'app-home-page',
  templateUrl: './home.layout.html'
})
export class HomeLayout extends TranslatableClass implements OnInit, OnDestroy, AfterViewInit {

  /**
   * @description Value that indicates the login status of a user account
   */
  private _connected: boolean;

  /**
   * @description Tags available
   */
  private _tags: Map<string, Array<any>>;

  /**
   * @description Touch slider with hardware accelerated transitions and native behavior
   */
  private swiper: Swiper;

  /**
   * @description Events present in the database loaded via the API
   */
  private _events: Array<any>;

  /**
   * @description Constructor of PrivacyPoliciesLayout layout
   *
   * The constructor creates an instance of the PrivacyPoliciesLayout layout and specifies the default values
   * the input and output variables of the layout.
   *
   * @param translationService A reference to the translation service of the application
   * @param userService A reference to the user service of the application
   * @param modalService A reference to the modal service of the application
   * @param activatedRoute
   * @param alertService A reference to the alert service of the application
   * @param router Implements the Angular Router service, which enables navigation from one view to the next as users
   * perform application tasks
   */
  constructor(translationService: TranslationService, private userService: UserService, private modalService: ModalService,
              private activatedRoute: ActivatedRoute, private alertService: AlertService, private router: Router) {
    super(translationService);

    this.tags = new Map();
    this.events = new Array<any>();
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit() {
    this.userService.connectedSubject.subscribe(value => {
      this.connected = value;
    });
    this.userService.isAuthenticated().then(res => this.connected = res);

    this.loadTags();
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular has finished to initialize the view of a component.
   * Defining an ngAfterViewInit() method allows you to manage any additional initialization tasks.
   */
  ngAfterViewInit(): void {
    this.activatedRoute.paramMap
      .pipe(map(() => window.history.state))
      .subscribe(value => {
        (value.hasOwnProperty('data')) ? this.loadPreferences(value.data) : undefined;
      });
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
   */
  ngOnDestroy(): void {
  }

  /**
   * @description This method makes it possible to know the size of the screen.
   * If the size is less than 992px, the function will return the value true
   */
  isMobile(): boolean {
    return window.innerWidth < 992;
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to retrieve all tags of the platform
   *
   * It calls the method Get of the NetworkService service
   */
  private loadTags(): void {
    if (!this.connected || this.userService.userSubject.last.favorites.tags.length < 1) {

      this.userService.get(GET_TAGS)
        .then(res => {
          res.responseData.data.forEach(item => this.tags.set(item.name, []));
          this.loadEvents();
        })
        .catch(error => this.alertService.error(error.responseError.message));
    } else {

      this.userService.userSubject.last.favorites.tags.forEach(item => this.tags.set(item, []));
      this.loadEvents();
    }
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to retrieve an event by given tags names
   *
   * It calls the method Get of the NetworkService service
   */
  private loadEvents(): void {
    this.tags.forEach((value: Array<any>, key: string) => {
      this.userService.activeLoader(new Semaphore(1));

      this.userService.get(GET_EVENT_BY_TAG_NAME.replace('(?name1&name2?)', key))
        .then(res => {
          res.responseData.data.forEach((item, index) => {
            this.loadUser(item);
            this.loadAssociation(item);

            if (index === 0 && this.events.length < 4) {
              this.events = [...this.events, item];
            }
          });

          if (res.responseData.data.length === 0) {
            this.loadAllEvents();
          }

          this.tags.set(key, createChunkedArray(res.responseData.data, 4));
        })
        .catch();
    });
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to retrieve all events
   *
   * It calls the method Get of the NetworkService service
   */
  private loadAllEvents(): void {
    const params: NetworkParamsClass = new NetworkParamsClass();

    params.headers = [{name: 'Page-Index', value: '1'}, {name: 'Page-Size', value: '12'}]

    this.userService.get(GET_EVENTS)
      .then(res => {

        res.responseData.data.forEach((item, index) => {
          this.loadUser(item);
          this.loadAssociation(item);

        });

        this.tags.set('EVENTS_LAYOUT/RECOMMENDED_BY_US', createChunkedArray(res.responseData.data, 4));
      })
      .catch();
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to retrieve a user by a given id
   *
   * It calls the method Get of the NetworkService service
   */
  private loadUser(event: any): void {
    this.userService.activeLoader(new Semaphore(1));

    this.userService.get(GET_USER_PUBLIC_BY_ID + event.userId)
      .then(response => {
        event.user = response.responseData.data;

        if (event.user.images.find(img => img.name === 'profile') && event.user.images.find(img => img.name === 'profile').content) {
          event.user.images = environment.apiHost + environment.apiVersion + '/image/id/' +
            event.user.images.find(img => img.name === 'profile').content;
        } else {
          event.user.images = '/assets/img/account.png';
        }

        this.initSwiper();
      })
      .catch(() => event['user'] = null);

  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to retrieve an association by a given id
   *
   * It calls the method Get of the NetworkService service
   */
  private loadAssociation(event: any): void {
    this.userService.activeLoader(new Semaphore(1));

    this.userService.get(GET_ASSOCIATION_BY_ID + event.associationId)
      .then(response => {
        event['association'] = response.responseData.data;
        if (event.association.images.find(img => img.name === 'profile') && event.association.images.find(img => img.name === 'profile').content) {
          event.association.images = environment.apiHost + environment.apiVersion + '/image/id/' +
            event.association.images.find(img => img.name === 'profile').content;
        } else {
          event.association.images = '';
        }

        this.initSwiper();
      })
      .catch((e) => {
        event['association'] = null
      })
  }

  /**
   * @description This method allows a user who has not entered his preferences to be able to enter them
   * It asks the modal service to open the modal of user preferences
   *
   * @param data Data recovered via the router
   */
  private loadPreferences(data: any): void {
    if ((data.tags as Array<any>).length === 0 && !this.isMobile()) {
      this.modalService.open(ModalPreferencesComponent, {centered: true});
    }
  }

  /**
   * @description This method is used to initialize the slider present in the component (Swiper slider)
   */
  private initSwiper(): void {
    if (this.events.length > 2) {
      this.swiper = new Swiper('.swiper-container', {
        effect: 'coverflow',
        loop: true,
        initialSlide: this.events.length > 2 ? 3 : 1,
        centeredSlides: true,
        slideToClickedSlide: true,
        updateOnImagesReady: true,
        preventClicks: false,
        preventClicksPropagation: false,
        slidesPerView: 'auto',
        speed: 500,
        coverflowEffect: {
          rotate: 50,
          stretch: 10,
          depth: 100,
          modifier: 1,
          slideShadows: false,
        }
      });
    }

    document.querySelector('.swiper-wrapper').addEventListener('click', (event) => {
      ((event.target as HTMLButtonElement).localName === 'button') ?
        this.router.navigate(['/event/' + (event.target as HTMLButtonElement).id]).then() : undefined;
    });
  }

  /**
   * @description The method allows you to retrieve the events present in the database loaded via the API
   */
  get events(): Array<any> {
    return this._events;
  }

  /**
   * @description The method allows you to assign the events present in the database loaded via the API
   *
   * @param value Value of the new events present in the database loaded via the API
   */
  set events(value: Array<any>) {
    this._events = value;
  }

  /**
   * @description The method allows you to retrieve the value that indicates the login status of a user account
   */
  get connected(): boolean {
    return this._connected;
  }

  /**
   * @description The method allows you to assign the value that indicates the login status of a user account
   *
   * @param value Value of the new value that indicates the login status of a user account
   */
  set connected(value: boolean) {
    this._connected = value;
  }

  /**
   * @description The method allows you to retrieve the available tags
   */
  get tags(): Map<string, any> {
    return this._tags;
  }

  /**
   * @description The method allows you to assign the available tags
   *
   * @param value Value of the new available tags
   */
  set tags(value: Map<string, any>) {
    this._tags = value;
  }
}
