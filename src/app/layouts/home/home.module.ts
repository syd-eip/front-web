/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of dependencies modules
 */
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
/**
 * Import of layouts
 */
import {HomeLayout} from './home.layout';
/**
 * Import of application's modules
 */
import {ComponentsModule} from '../../shared/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    ComponentsModule
  ],
  declarations: [
    HomeLayout
  ],
  exports: [
    HomeLayout
  ],
  providers: []
})
export class HomeModule {
}
