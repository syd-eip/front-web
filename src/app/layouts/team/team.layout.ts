import {Component} from '@angular/core';
import {TranslatableClass} from '../../shared/services/translation/translation-class/translatable.class';
import {TranslationService} from '../../shared/services/translation/translation.service';

@Component({
  selector: 'app-team-page',
  templateUrl: './team.layout.html'
})
export class TeamLayout extends TranslatableClass {

  /**
   * @description Constructor of TeamLayout layout
   *
   * The constructor creates an instance of the TeamLayout layout and specifies the default values
   * the input and output variables of the component.
   *
   * @param translationService A reference to the i18n translation service of the application
   */
  constructor(translationService: TranslationService) {
    super(translationService);
  }
}
