/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

/**
 * Import of layouts
 */
import {TeamLayout} from './team.layout';

/**
 * Team layout module of the Application. This module retrieve team layout
 * You can also find Angular's modules
 */
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TeamLayout
  ],
  exports: [
    TeamLayout
  ],
  providers: []
})
export class TeamModule {
}
