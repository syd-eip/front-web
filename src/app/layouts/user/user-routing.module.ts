/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
/**
 * Import of layout
 */
import {UserLayout} from './user.layout';
import {RoutingErrorLayout} from '../routing-error/routing-error.layout';
import {ProfileLayout} from './profile/profile.layout';
import {AuthenticationGuardService} from '../../shared/services/guards/authentication-guard.service';
import {LoginLayout} from './login/login.layout';
import {RegisterLayout} from './register/register.layout';
import {ResetPasswordGuardService} from '../../shared/services/guards/reset-password-guard.service';
import {ResetPasswordLayout} from './reset-password/reset-password.layout';
import {PaymentLayout} from './payment/payment.layout';
import {NoAuthenticationGuardService} from '../../shared/services/guards/no-authentication-guard.service';

/**
 * Route of the application
 */
const routes: Routes = [
  {
    path: '',
    component: UserLayout,
    children: [
      {
        path: '',
        redirectTo: 'profile',
        pathMatch: 'full'
      },
      {
        path: 'profile/:username',
        component: ProfileLayout
      },
      {
        path: 'sign',
        canActivate: [NoAuthenticationGuardService],
        children: [
          {
            path: '',
            redirectTo: 'in',
            pathMatch: 'full'
          },
          {
            path: 'in',
            component: LoginLayout
          },
          {
            path: 'up',
            component: RegisterLayout,
          }
        ]
      },
      {
        path: 'reset-password',
        canActivate: [ResetPasswordGuardService],
        component: ResetPasswordLayout
      },
      {
        path: 'purchase',
        canActivate: [AuthenticationGuardService],
        component: PaymentLayout
      },
      {
        path: '**',
        component: RoutingErrorLayout
      }
    ]
  }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule,
  ],
})
export class UserRoutingModule {
}
