/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of dependencies modules
 */
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
/**
 * Import of layouts
 */
import {ProfileLayout} from './profile.layout';
import {UserProfileLayout} from './user-profile/user-profile.layout';
import {InfluencerProfileLayout} from './influencer-profile/influencer-profile.layout';
/**
 * Import of application's modules
 */
import {ComponentsModule} from '../../../shared/components/components.module';
import {EventPreviewInfluencerPageModule} from '../../../shared/components/event-preview-influencer-page/event-preview-influencer-page.module';
import {PipesModule} from '../../../shared/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    ComponentsModule,
    EventPreviewInfluencerPageModule,
    PipesModule
  ],
  declarations: [
    ProfileLayout,
    UserProfileLayout,
    InfluencerProfileLayout
  ],
  exports: [
    ProfileLayout,
    UserProfileLayout,
    InfluencerProfileLayout
  ],
  providers: []
})
export class ProfileModule {
}
