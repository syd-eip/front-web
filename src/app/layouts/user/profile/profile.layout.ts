import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from 'src/app/shared/services/web/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {GET_EVENTS_BY_USER_ID, GET_USER_PUBLIC_BY_ID} from '../../../shared/data/api/api-path.data';
import {UserClass} from '../../../shared/classes/models/user.class';
import {UserPublicClass} from '../../../shared/classes/models/user-public.class';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {ImageConvertedClass} from '../../../shared/classes/models/image-converted.class';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.layout.html'
})
export class ProfileLayout implements OnInit, OnDestroy {

  /**
   * @description Requested user loaded thanks to it's username found in the URL
   */
  private _requestedUserId: string;

  /**
   * @description User information model
   */
  private _requestedUser: UserPublicClass;

  /**
   * @description User events information model
   */
  private _requestedUserEvents: Array<any>;

  /**
   * @description User images model
   */
  private _requestedUserImages: Array<ImageConvertedClass>;

  /**
   * @description user's login status
   */
  private _connected: boolean;

  /**
   * @description User (me) information model
   */
  private _me: any;

  /**
   * @description images used on the page
   */
  private _images: Map<string, SafeResourceUrl>;

  /**
   * @description Boolean indicating whether the authenticated user matches the one searched for
   */
  private _isMe: boolean;

  /**
   * @description Constructor of ProfileLayout layout
   *
   * The constructor creates an instance of the ProfileLayout layout and specifies the default values
   * the input and output variables of the layout.
   *
   * @param userService A reference to the user service of the application
   * @param activatedRoute A reference to our application injectable service used to navigate from one view to the next
   * @param router A reference to the Angular injectable router service
   * @param sanitizer A reference to the Angular injectable sanitizer service used to convert base64 image
   */
  constructor(private userService: UserService, private activatedRoute: ActivatedRoute,
              private router: Router, private sanitizer: DomSanitizer) {
    this.isMe = false;
  }

  /**
   * @description: This method is part of the component life cycle: lifecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.images = new Map<string, SafeResourceUrl>();
    this.requestedUserId = this.activatedRoute.snapshot.paramMap.get('username');

    this.userService.userSubject.subscribe(value => {
      this.me = value;
      if (this.me.id === this.requestedUserId) {
        this.isMe = true;
      }
    });

    this.userService.connectedSubject.subscribe(value => {
      this.connected = value;
    });

    this.me = (this.userService.userSubject.last === null) ? new UserClass() : this.userService.userSubject.last;

    this.userService.get(GET_USER_PUBLIC_BY_ID + this.requestedUserId)
      .then((res) => {
        this.requestedUser = res.responseData.data;
        if (this.requestedUser.id === this.me.id) {
          this.isMe = true;
        }

        if (this.requestedUser.isInfluencer) {
          this.userService.get(GET_EVENTS_BY_USER_ID + this.requestedUserId).then((res) => {
            this.requestedUserEvents = res.responseData.data;
          }).catch(() => {

          });
        }
      }).catch((err) => {
      if (err.responseError.code === 404) {
        this.router.navigate(['not-found']).then(() => {
        });
      }
    });

  }

  /**
   * @description: This method is part of the component life cycle: lifecycle hook.
   *
   * It is called when Angular deletes the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
   */
  ngOnDestroy(): void {
  }

  /**
   * @description This method allows you to retrieve the requested user
   */
  get requestedUserId(): string {
    return this._requestedUserId;
  }

  /**
   * @description This method allows you to assign the requested user
   *
   * @param value Value of the new the requested user
   */
  set requestedUserId(value: string) {
    this._requestedUserId = value;
  }

  /**
   * @description This method allows you to retrieve the boolean indicating whether the authenticated user matches the one searched for
   */
  get isMe(): boolean {
    return this._isMe;
  }

  /**
   * @description This method allows you to assign the boolean indicating whether the authenticated user matches the one searched for
   *
   * @param value Value of the new boolean indicating whether the authenticated user matches the one searched for
   */
  set isMe(value: boolean) {
    this._isMe = value;
  }

  /**
   * @description The method allows you to retrieve requested user information
   */
  get requestedUser(): UserPublicClass {
    return this._requestedUser;
  }

  /**
   * @description The method allows you to assign the requested user information
   *
   * @param value The new assigned value for the requested user information
   */
  set requestedUser(value: UserPublicClass) {
    this._requestedUser = value;
  }

  /**
   * @description The method allows you to retrieve user information
   */
  get me(): any {
    return this._me;
  }

  /**
   * @description The method allows you to assign the  user information
   *
   * @param value The new assigned value for the user information
   */
  set me(value: any) {
    this._me = value;
  }

  /**
   * @description The method allows you to retrieve the user's login status
   */
  get connected(): boolean {
    return this._connected;
  }

  /**
   * @description The method allows you to assign a boolean representing the user's login status
   *
   * @param value The new assigned value for a boolean representing the user's login status
   */
  set connected(value: boolean) {
    this._connected = value;
  }

  /**
   * @description The method allows you to retrieve the user's events
   */
  get requestedUserEvents(): Array<any> {
    return this._requestedUserEvents;
  }

  /**
   * @description The method allows you to assign an Array of events created by a user
   *
   * @param value The new assigned value for a Array of events created by a user
   */
  set requestedUserEvents(value: Array<any>) {
    this._requestedUserEvents = value;
  }

  /**
   * @description The method allows you to retrieve user's images
   */
  get requestedUserImages(): Array<ImageConvertedClass> {
    return this._requestedUserImages;
  }

  /**
   * @description The method allows you to assign user's images
   *
   * @param value The new assigned value for user's images
   */
  set requestedUserImages(value: Array<ImageConvertedClass>) {
    this._requestedUserImages = value;
  }

  /**
   * @description The method allows you to retrieve the value of the images
   */
  get images(): Map<string, SafeResourceUrl> {
    return this._images;
  }

  /**
   * @description The method allows you to set the value of the images
   */
  set images(value: Map<string, SafeResourceUrl>) {
    this._images = value;
  }

}
