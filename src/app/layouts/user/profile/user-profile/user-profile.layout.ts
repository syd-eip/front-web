import {AfterViewInit, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {UserPublicClass} from '../../../../shared/classes/models/user-public.class';
import {UserClass} from '../../../../shared/classes/models/user.class';
import {TranslationService} from '../../../../shared/services/translation/translation.service';
import {TranslatableClass} from '../../../../shared/services/translation/translation-class/translatable.class';
import {ModalService} from '../../../../shared/services/modal/modal.service';
import {ModalSettingsComponent} from '../../../../shared/components/modal/modal-settings/modal-settings.component';
import {environment} from '../../../../../environments/environment';
import {UserService} from '../../../../shared/services/web/user.service';
import {GET_USER_PUBLIC_BY_ID} from '../../../../shared/data/api/api-path.data';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.layout.html'
})
export class UserProfileLayout extends TranslatableClass implements OnInit, OnDestroy, AfterViewInit {

  /**
   * @description Value that indicates if the profile is the user that request the page
   * It's a props received on its selector
   */
  @Input() isMe: boolean;

  /**
   * @description Value that corresponds to the user to display
   * It's a props received on its selector
   */
  @Input() user: UserPublicClass | UserClass;

  /**
   * @description Value that corresponds to the user id to display
   * It's a props received on its selector
   */
  @Input() userId: string;

  /**
   * @description User profile image
   */
  public profile: string;

  /**
   * @description User background image
   */
  public background: string;

  /**
   * @description UserProfileLayout's constructor
   *
   * The constructor creates an instance of the UserProfileLayout layouts and specifies the default values of the layout's
   * input variables
   *
   * @param modalService A reference to the ModalService service of the application
   * @param translationService A reference to the TranslationService service of the application
   */
  constructor(translationService: TranslationService, private modalService: ModalService, private userService: UserService) {
    super(translationService)

    this.profile = '/assets/img/account.png';
    this.background = '/assets/img/default-banner.jpg';
  }

  /**
   * @description: This method is part of the component life cycle: lifecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    document.getElementsByTagName('body')[0].classList.add('user-profile');

    this.loadImages();
  }

  ngAfterViewInit() {
    if (this.isMe) {
      this.userService.userSubject.asObservable().subscribe(value => {
        this.user = value;
        this.loadImages();
      })
    } else {
      this.userService.get(GET_USER_PUBLIC_BY_ID + this.userId)
        .then(res => {
          this.user = res.responseData.data
          this.loadImages();
        })
    }
  }

  /**
   * @description: This method is part of the component life cycle: lifecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional deletion tasks.
   */
  ngOnDestroy(): void {
    document.getElementsByTagName('body')[0].classList.remove('user-profile');
  }

  /**
   * @description: This method is called to display the modal settings component
   */
  settings(): void {
    this.modalService.open(ModalSettingsComponent, {centered: true, windowClass: 'modal-custom'});
  }

  loadImages(): void {
    if (this.user) {
      this.profile = this.user.images.find(item => item.name === 'profile') ?
        environment.apiHost + environment.apiVersion + '/image/id/' + this.user.images.find(item => item.name === 'profile').content :
        '/assets/img/account.png';

      this.background = this.user.images.find(item => item.name === 'background') ?
        environment.apiHost + environment.apiVersion + '/image/id/' + this.user.images.find(item => item.name === 'background').content :
        '/assets/img/default-banner.jpg';
    }
  }
}
