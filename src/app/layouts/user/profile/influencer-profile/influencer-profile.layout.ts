import {Component, OnDestroy, OnInit, Input} from '@angular/core';
import {UserPublicClass} from '../../../../shared/classes/models/user-public.class';
import {DomSanitizer} from '@angular/platform-browser';
import {GET_ASSOCIATION_BY_ID, GET_EVENTS_BY_USER_ID, GET_USER_PUBLIC_BY_ID} from '../../../../shared/data/api/api-path.data';
import {UserService} from '../../../../shared/services/web/user.service';
import {EventClass} from '../../../../shared/classes/models/event.class';
import {TranslationService} from '../../../../shared/services/translation/translation.service';
import {TranslatableClass} from '../../../../shared/services/translation/translation-class/translatable.class';
import {ModalService} from '../../../../shared/services/modal/modal.service';
import {ModalSettingsComponent} from '../../../../shared/components/modal/modal-settings/modal-settings.component';
import {Router} from '@angular/router';
import {environment} from '../../../../../environments/environment';
import {createChunkedArray} from '../../../../shared/functions/get-chunked-array.function';
import {Semaphore} from '../../../../shared/classes/semaphore/semaphore.class';
import {NetworkParamsInterface} from "../../../../shared/services/network/network-params/network-params.interface";
import {NetworkParamsClass} from "../../../../shared/services/network/network-params/network-params.class";

@Component({
  selector: 'app-influencer-profile',
  templateUrl: './influencer-profile.layout.html'
})
export class InfluencerProfileLayout extends TranslatableClass implements OnInit, OnDestroy {

  /**
   * @description Value that indicates if the profile is the user that request the page
   * It's a props received on its selector
   */
  @Input() isMe: boolean = true;

  /**
   * @description Value that corresponds to the user to display
   * It's a props received on its selector
   */
  @Input() user: UserPublicClass;

  /**
   * @description Value that corresponds to all the event created by the user
   */
  public events: Array<EventClass>;

  /**
   * @description Value that corresponds to all the current events
   */
  public currentEvents: Array<EventClass>;

  /**
   * @description Value that corresponds to all the past events
   */
  public pastEvents: Array<EventClass>;

  /**
   * @description User profile image
   */
  public profile: string;

  /**
   * @description User background image
   */
  public background: string;

  /**
   * @description Value that corresponds to all the past events in a chunked array
   */
  public caPastEvents: Array<any>

  /**
   * @description Value that corresponds to all the current events in a chunked array
   */
  public caEvents: Array<any>

  public pagesNb: number = 0;

  public pageActual: number = 1;

  public paginationHeading : NetworkParamsInterface;

  /**
   * @description Constructor of InfluencerProfileLayout layout
   *
   * The constructor creates an instance of the InfluencerProfileLayout layout and specifies the default values
   * the input and output variables of the layout.
   *
   * @param userService A reference to the user service of the application
   * @param sanitizer A reference to the Angular injectable sanitizer service used to convert base64 image
   * @param translationService A reference to the translation service of the application
   * @param modalService A reference to the model service of the application
   * @param router A reference to the router service of the application
   */
  constructor(private userService: UserService, private sanitizer: DomSanitizer, translationService: TranslationService,
              private modalService: ModalService, private router: Router) {
    super(translationService);

    this.profile = '/assets/img/account.png';
    this.background = '/assets/img/default-banner.jpg';

    this.caPastEvents = new Array<EventClass>();
    this.caEvents = new Array<EventClass>();

    this.pastEvents = new Array<EventClass>();
    this.currentEvents = new Array<EventClass>();

    this.paginationHeading = new NetworkParamsClass();
  }

  /**
   * @description: This method is part of the component life cycle: lifecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    document.getElementsByTagName('body')[0].classList.add('influencer-layout');

    this.paginationHeading.headers.push({name: 'Page-Index', value: '1'}, {name: 'Page-Size', value: '3'})

    this.loadEvents();
    this.loadImages();

    this.userService.userSubject.asObservable().subscribe(value => {
      if (this.isMe)
        this.user = value;
      this.loadImages();
    });
  }

  /**
   * @description: This method is part of the component life cycle: lifecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
   */
  ngOnDestroy(): void {
    document.getElementsByTagName('body')[0].classList.remove('influencer-layout');
  }

  loadMoreEvents() : void {
    this.paginationHeading = new NetworkParamsClass();
    this.pageActual += 1
    this.paginationHeading.headers.push({name: 'Page-Index', value: this.pageActual.toString()}, {name: 'Page-Size', value: '3'})

    this.userService.get(GET_EVENTS_BY_USER_ID + this.user.id, null, this.paginationHeading)
      .then((res) => {

        let newEvents : Array<EventClass> = res.responseData.data

        newEvents.forEach(event => {
          this.loadUser(event);
          this.loadAssociation(event);
          this.events.push(event)
        })

        this.pagesNb = parseInt(res.responseHeaders.get('last-page'))

        this.caEvents = []
        this.caEvents = createChunkedArray(this.events, 3)
      });
  }

  loadEvents(): void {
    this.userService.get(GET_EVENTS_BY_USER_ID + this.user.id, null, this.paginationHeading)
      .then((res) => {

        this.events = res.responseData.data;
        console.log(parseInt(res.responseHeaders.get('last-page')) + ' pages')
        this.pagesNb = parseInt(res.responseHeaders.get('last-page'))

        res.responseData.data.forEach(item => {
          this.loadUser(item);
          this.loadAssociation(item);
        });

        this.caEvents = createChunkedArray(this.events, 3)
      });
  }

  loadImages(): void {
    if (this.user) {

      this.profile = this.user.images.find(item => item.name === 'profile') ?
        environment.apiHost + environment.apiVersion + '/image/id/' + this.user.images.find(item => item.name === 'profile').content :
        '/assets/img/account.png';

      this.background = this.user.images.find(item => item.name === 'background') ?
        environment.apiHost + environment.apiVersion + '/image/id/' + this.user.images.find(item => item.name === 'background').content :
        '/assets/img/default-banner.jpg';
    }
  }

  private loadUser(event: any): void {
    this.userService.activeLoader(new Semaphore(1));

    this.userService.get(GET_USER_PUBLIC_BY_ID + event.userId)
      .then(response => {
        event.user = response.responseData.data;

        if (event.user.images.find(img => img.name === 'profile') && event.user.images.find(img => img.name === 'profile').content) {
          event.user.images = environment.apiHost + environment.apiVersion + '/image/id/' +
            event.user.images.find(img => img.name === 'profile').content;
        } else {
          event.user.images = '/assets/img/account.png';
        }
      })
      .catch(() => event['user'] = null);

  }

  private loadAssociation(event: any): void {
    this.userService.activeLoader(new Semaphore(1));

    this.userService.get(GET_ASSOCIATION_BY_ID + event.associationId)
      .then(response => {
        event['association'] = response.responseData.data;
        if (event.association.images.find(img => img.name === 'profile') && event.association.images.find(img => img.name === 'profile').content) {
          event.association.images = environment.apiHost + environment.apiVersion + '/image/id/' +
            event.association.images.find(img => img.name === 'profile').content;
        } else {
          event.association.images = '';
        }
      })
      .catch((e) => {
        event['association'] = null
      })
  }

  /**
   * @description: This method is called to display the modal settings component
   */
  settings(): void {
    this.modalService.open(ModalSettingsComponent, {centered: true, windowClass: 'modal-custom'});
  }

}
