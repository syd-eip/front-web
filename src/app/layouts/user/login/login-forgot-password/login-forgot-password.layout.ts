import {Component, ElementRef, EventEmitter, Input, NgZone, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {TranslatableClass} from '../../../../shared/services/translation/translation-class/translatable.class';
import {TranslationService} from '../../../../shared/services/translation/translation.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {FormErrorClass} from '../../../../shared/classes/forms/form-error.class';

@Component({
  selector: 'app-login-forgot-password',
  templateUrl: './login-forgot-password.layout.html'
})
export class LoginForgotPasswordLayout extends TranslatableClass implements OnInit, OnDestroy {

  /**
   * @description component that tracks the value and validity state of a group of FormControl instances.
   */
  private _formGroup: FormGroup;

  /**
   * @description success parameter
   */
  private _successGroup: boolean;

  /**
   * @description form's error parameters
   */
  private _errorGroup: Array<FormErrorClass>;

  /**
   * @description email address model of the component
   */
  public email: string;

  /**
   * @description This Observable makes it possible to know the error response of the API after validation of the form
   */
  @Input() public errorEvent: Observable<string>;

  /**
   * @description This Observable makes it possible to know the success response of the API after validation of the form
   */
  @Input() public successEvent: Observable<any>;

  /**
   * @description This EventEmitter variable emits customized events synchronously
   * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
   *
   * In the LoginForgotPasswordLayout, this EventEmitter allows the sending of the form validation
   */
  @Output() public validEvent = new EventEmitter<any>();

  /**
   * @description This EventEmitter variable emits customized events synchronously
   * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
   *
   * In the LoginForgotPasswordLayout, this EventEmitter allows you to change the form (go back to the previous one)
   */
  @Output() public prevEvent = new EventEmitter<string>();

  /**
   * @description email address element (ElementRef)
   */
  @ViewChild('email', {static: true}) username: ElementRef;

  /**
   * @description Constructor of LoginForgotPasswordLayout layout
   *
   * The constructor creates an instance of the LoginForgotPasswordLayout layout and specifies the default values
   * the input and output variables of the layout.
   *
   * @param zone A reference to an injectable service for executing work inside or outside of the Angular zone
   * @param translationService A reference to the translation service of the application
   * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
   * @param router Implements the Angular Router service , which enables navigation from one view to the next as users
   * perform application tasks
   */
  constructor(private zone: NgZone, translationService: TranslationService, private formBuilder: FormBuilder,
              private router: Router) {
    super(translationService);
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.initForm();

    this.errorEvent.subscribe(value => (value) ? this.manageError(value) : null);
    this.successEvent.subscribe(value => (value === 'reset') ? this.manageSuccess() : undefined);
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
   */
  ngOnDestroy(): void {
    delete this.formGroup;
    delete this.errorEvent;
    delete this.successEvent;

    this.errorGroup.splice(0, this.errorGroup.length);
  }

  /**
   * @description This method is used to initialize the password reset form.
   * It creates a form by specifying the rules to be respected to validate user inputs
   */
  initForm(): void {
    this.errorGroup = [];
    this.successGroup = false;
    this.errorGroup = [{name: 'email', error: ''}];

    this.formGroup = this.formBuilder.group(
      {
        email: [null, Validators.compose([
          Validators.required,
          Validators.email
        ])]
      }
    );
  }

  /**
   * @description The method uses the Angular router and allows you to navigate the application's routes
   *
   * @param path Path to navigate to
   */
  navigateTo(path: string): void {
    this.router.navigate([path])
      .then(value => {
      });
  }

  /**
   * @description This method returns the value of the focus of an HTML element
   *
   * @param value HTMLElement element
   */
  isFocused(value: HTMLElement): boolean {
    return document.activeElement === value;
  }

  /**
   * @description This method is called when the form is valid and the user wishes to validate the sending of a password reset email
   */
  valid(): void {
    this.validEvent.emit({name: 'reset', data: {email: this.email}});
  }

  /**
   * @description This method is used to warn the user of an error returned by the API
   *
   * @param value error string send by the API
   */
  manageError(value: string): void {
    this.successGroup = false;

    if (value.slice(6, 11) === 'EMAIL') {
      this.errorGroup[0].error = value;
    }
  }

  /**
   * @description This method is used to validate the form after a success
   */
  manageSuccess(): void {
    this.successGroup = true;
  }

  /**
   * @description This method is used to display errors related to the email address field
   */
  errorOccurred(value: HTMLInputElement): void {
    this.successGroup = false;

    if (value.name === 'email') {

      if (this.formGroup.controls.email.errors !== null) {

        if (this.formGroup.controls.email.errors.required !== undefined) {
          this.errorGroup[0].error = 'ERROR/EMAIL_MISSING';
        } else {
          this.errorGroup[0].error = 'ERROR/EMAIL_BAD_FORMAT';
        }
      } else {
        this.errorGroup[0].error = '';
      }
    }
  }

  /**
   * @description This method allows you to switch to the next navigation tabs in the application tabs
   */
  prev(): void {
    this.prevEvent.emit('1');
  }

  /**
   * @description The method allows you to retrieve the component that tracks the value and validity state of a group of
   * FormControl instances
   */
  get formGroup(): FormGroup {
    return this._formGroup;
  }

  /**
   * @description The method allows you to assign the component that tracks the value and validity state of a group of
   * FormControl instances
   *
   * @param value Value of the new component that tracks the value and validity state of a group of FormControl instances
   */
  set formGroup(value: FormGroup) {
    this._formGroup = value;
  }

  /**
   * @description The method allows you to retrieve the form's error parameters
   */
  get errorGroup(): Array<FormErrorClass> {
    return this._errorGroup;
  }

  /**
   * @description The method allows you to assign the form's error parameters
   *
   * @param value Value of the new form's error parameters
   */
  set errorGroup(value: Array<FormErrorClass>) {
    this._errorGroup = value;
  }

  /**
   * @description The method allows you to retrieve the form's success parameters
   */
  get successGroup(): boolean {
    return this._successGroup;
  }

  /**
   * @description The method allows you to assign the form's success parameters
   *
   * @param value Value of the new form's success parameters
   */
  set successGroup(value: boolean) {
    this._successGroup = value;
  }
}
