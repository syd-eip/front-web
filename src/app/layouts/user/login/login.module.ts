/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
/**
 * Import of dependencies modules
 */
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
/**
 * Import of layouts
 */
import {LoginLayout} from './login.layout';
import {LoginFormLayout} from './login-form/login-form.layout';
import {LoginForgotPasswordLayout} from './login-forgot-password/login-forgot-password.layout';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    RouterModule
  ],
  declarations: [
    LoginLayout,
    LoginFormLayout,
    LoginForgotPasswordLayout
  ],
  exports: [
    LoginLayout,
    LoginFormLayout,
    LoginForgotPasswordLayout
  ],
  providers: []
})
export class LoginModule {
}
