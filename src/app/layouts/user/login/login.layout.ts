import {Component, Inject, NgZone, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TranslationService} from 'src/app/shared/services/translation/translation.service';
import {TranslatableClass} from '../../../shared/services/translation/translation-class/translatable.class';
import {Subject, Subscription} from 'rxjs';
import {UserService} from '../../../shared/services/web/user.service';
import {POST_USER_RESET_PASSWORD, SIGN_IN_URL} from '../../../shared/data/api/api-path.data';
import {Semaphore} from '../../../shared/classes/semaphore/semaphore.class';
import {NetworkResponseClass} from '../../../shared/services/network/network-response/network-response.class';
import {Router} from '@angular/router';
import {DOCUMENT} from '@angular/common';
import {NgbTabset} from '@ng-bootstrap/ng-bootstrap';
import {AlertService} from '../../../shared/services/alert/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.layout.html'
})
export class LoginLayout extends TranslatableClass implements OnInit, OnDestroy {

  /**
   * @description active tab id
   */
  private _activeId: string;

  /**
   * @description Subject is a special type of Observable that allows values to be multicasted to many Observers.
   * You can subscribe to this Subject to receive data (error request) sent by the API
   */
  private _errorEventSubject: Subject<string>;

  /**
   * @description Subject is a special type of Observable that allows values to be multicasted to many Observers.
   * You can subscribe to this Subject to receive data (successful request) sent by the API
   */
  private _successEventSubject: Subject<string>;

  private sib: Subscription;

  /**
   * @description A component that makes it easy to create tabbed interface.
   */
  @ViewChild('tabform', {static: true}) tabform: NgbTabset;

  /**
   * @description Constructor of LoginLayout layout
   *
   * The constructor creates an instance of the LoginLayout layout and specifies the default values
   * the input and output variables of the layout.
   *
   * @param document A DI Token representing the main rendering context. In a browser this is the DOM Document
   * @param userService A reference to the user service of the application
   * @param zone A reference to an injectable service for executing work inside or outside of the Angular zone
   * @param translationService A reference to the translation service of the application
   * @param router Implements the Angular Router service , which enables navigation from one view to the next as users
   * perform application tasks
   * @param alertService A reference to the alert service of the application
   */
  constructor(@Inject(DOCUMENT) private document: Document, public userService: UserService, private zone: NgZone,
              translationService: TranslationService, private router: Router, private alertService: AlertService) {
    super(translationService);
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.activeId = 'tab-form-1';

    this.errorEventSubject = new Subject<string>();
    this.successEventSubject = new Subject<any>();

    this.sib = this.userService.userSubject.asObservable().subscribe(value => {

      if (value) {
        this.router.navigate(['/'], {state: {data: value.favorites}}).then(_ => {
          this.alertService.success(this.translate('SUCCESS/USER_AUTHENTICATED'));
        });
      }
    });

  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
   */
  ngOnDestroy(): void {
    this.sib.unsubscribe();

    delete this.errorEventSubject;
    delete this.successEventSubject;
  }

  /**
   * @description This method intercepts the validation of the login application form or the reset password application form
   *
   * @param form Form sent after user validation
   */
  validFormEvent(form: any): void {
    if (form.name === 'login') {
      this.signin(form);
    } else {
      this.reset(form);
    }
  }

  /**
   * @description This method allows the user to navigate in the application form
   *
   * @param value Event contains the identifier of the tab on which the user wants to navigate
   */
  tabEvent(value: string): void {
    this.tabform.select('tab-form-' + value);
    this.activeId = 'tab-form-' + value;
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to signin a user of the application
   *
   * It calls the method Post of the NetworkService service
   *
   * @param form Form to send to the API
   */
  signin(form: any): void {
    this.userService.activeLoader(new Semaphore(1));

    this.userService.post(form.data, SIGN_IN_URL)
      .then(res => {

        this.successEvent(form.name, res);

        if (form.name === 'login') {
          this.userService.user = res.responseData.data;
          this.userService.userSubject.next(res.responseData.data);
        }
      })
      .catch(error => {
        this.errorEvent(error.responseError.message);
      });
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to reset the password of an application user
   *
   * It calls the method Post of the NetworkService service
   *
   * @param form Form to send to the API
   */
  reset(form): void {
    this.userService.activeLoader(new Semaphore(1));

    this.userService.post(form.data, POST_USER_RESET_PASSWORD)
      .then(res => {
        this.successEvent(form.name, res);
      }).catch(error => {
      this.errorEvent(error.responseError.message);
    });
  }

  /**
   * @description This method sends the error message received by the API to the form (child component) in order to display it
   *
   * @param value Error message sent by the API
   */
  errorEvent(value: string) {
    this.errorEventSubject.next(value);
    this.alertService.error(this.translate(value));
  }

  /**
   * @description This method sends a success event to the form (child component) in order to display it
   *
   * @param name Name of the form ('login' [ 'reset')
   * @param value Response sent by the API
   */
  successEvent(name: string, value: NetworkResponseClass<any>): void {
    this.successEventSubject.next(name);
  }

  /**
   * @description The method allows you to retrieve the Subject which receives API data (error one)
   */
  get errorEventSubject(): Subject<string> {
    return this._errorEventSubject;
  }

  /**
   * @description The method allows you to assign the Subject which receives API data (error one)
   *
   * @param value Value of the new Subject which receives API data (error one)
   */
  set errorEventSubject(value: Subject<string>) {
    this._errorEventSubject = value;
  }

  /**
   * @description The method allows you to retrieve the Subject which receives API data (success one)
   */
  get successEventSubject(): Subject<string> {
    return this._successEventSubject;
  }

  /**
   * @description The method allows you to assign the Subject which receives API data (success one)
   *
   * @param value Value of the new Subject which receives API data (success one)
   */
  set successEventSubject(value: Subject<string>) {
    this._successEventSubject = value;
  }

  /**
   * @description The method allows you to retrieve the active tab id
   */
  get activeId(): string {
    return this._activeId;
  }

  /**
   * @description The method allows you to assign the active tab id
   *
   * @param value Value of the new active tab id
   */
  set activeId(value: string) {
    this._activeId = value;
  }
}
