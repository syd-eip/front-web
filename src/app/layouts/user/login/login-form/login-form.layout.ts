import {Component, ElementRef, EventEmitter, Input, NgZone, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {TranslatableClass} from '../../../../shared/services/translation/translation-class/translatable.class';
import {TranslationService} from '../../../../shared/services/translation/translation.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {FormErrorClass} from '../../../../shared/classes/forms/form-error.class';
import {LoginClass} from '../../../../shared/classes/models/login.class';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.layout.html'
})
export class LoginFormLayout extends TranslatableClass implements OnInit, OnDestroy {

  /**
   * @description component that tracks the value and validity state of a group of FormControl instances.
   */
  private _formGroup: FormGroup;

  /**
   * @description success parameter
   */
  private _successGroup: boolean;

  /**
   * @description form's error parameters
   */
  private _errorGroup: Array<FormErrorClass>;

  /**
   * @description Data model to send to the API
   */
  public model: LoginClass;

  /**
   * @description This Observable makes it possible to know the error response of the API after validation of the form
   */
  @Input() public errorEvent: Observable<string>;

  /**
   * @description This Observable makes it possible to know the success response of the API after validation of the form
   */
  @Input() public successEvent: Observable<any>;

  /**
   * @description This EventEmitter variable emits customized events synchronously
   * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
   *
   * In the LoginFormLayout, this EventEmitter allows the sending of the form validation
   */
  @Output() public validEvent = new EventEmitter<any>();

  /**
   * @description This EventEmitter variable emits customized events synchronously
   * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
   *
   * In the LoginFormLayout, this EventEmitter allows you to change the form (go to the next one)
   */
  @Output() public nextEvent = new EventEmitter<string>();

  /**
   * @description username element of the form (ElementRef)
   */
  @ViewChild('username', {static: true}) username: ElementRef;

  /**
   * @description password element of the form (ElementRef)
   */
  @ViewChild('password', {static: true}) password: ElementRef;

  /**
   * @description Constructor of LoginFormLayout layout
   *
   * The constructor creates an instance of the LoginFormLayout layout and specifies the default values
   * the input and output variables of the layout.
   *
   * @param zone A reference to an injectable service for executing work inside or outside of the Angular zone
   * @param translationService A reference to the translation service of the application
   * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
   * @param router Implements the Angular Router service , which enables navigation from one view to the next as users
   * perform application tasks
   */
  constructor(private zone: NgZone, translationService: TranslationService, private formBuilder: FormBuilder,
              private router: Router) {
    super(translationService);
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.initForm();

    this.errorEvent.subscribe(value => (value) ? this.manageError(value) : null);
    this.successEvent.subscribe(value => (value === 'login') ? this.manageSuccess() : undefined);
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
   */
  ngOnDestroy(): void {
    delete this.formGroup;
    delete this.errorEvent;
    delete this.successEvent;

    this.errorGroup.splice(0, this.errorGroup.length);
  }

  /**
   * @description This method is used to initialize the login form.
   * It creates a form by specifying the rules to be respected to validate user inputs
   */
  initForm(): void {
    this.model = new LoginClass();

    this.errorGroup = [];
    this.successGroup = false;
    this.errorGroup = [{name: 'username', error: ''}, {name: 'password', error: ''}];
    this.formGroup = this.formBuilder.group(
      {
        username: [null, Validators.compose([
          Validators.required,
        ])],
        password: [null, Validators.compose([
          Validators.required,
        ])]
      }
    );
  }

  /**
   * @description The method uses the Angular router and allows you to navigate the application's routes
   *
   * @param path Path to navigate to
   */
  navigateTo(path: string): void {
    this.router.navigate([path])
      .then(value => {
      });
  }

  /**
   * @description This method returns the value of the focus of an HTML element
   *
   * @param value HTMLElement element
   */
  isFocused(value: HTMLElement): boolean {
    return document.activeElement === value;
  }

  /**
   * @description This method is called when the form is valid and the user wishes to validate the sending of a password reset email
   */
  valid(): void {
    this.validEvent.emit({name: 'login', data: this.model});
  }

  /**
   * @description This method is used to warn the user of an error returned by the API
   *
   * @param value error string send by the API
   */
  manageError(value: string): void {
    this.successGroup = false;

    if (value.slice(6, 11) === 'LOGIN') {
      this.errorGroup[0].error = value;
    } else if (value.slice(6, 14) === 'PASSWORD') {
      this.errorGroup[1].error = value;
    }
  }

  /**
   * @description This method is used to validate the form after a success
   */
  manageSuccess(): void {
    for (const item of this.errorGroup) {
      item.error = '';
    }
    this.successGroup = true;
  }

  /**
   * @description This method is used to display errors related to the email address field
   */
  errorOccurred(value: HTMLInputElement): void {
    this.successGroup = false;

    if (value.name === 'username') {
      if (this.formGroup.controls.username.errors !== null) {
        this.errorGroup[0].error = 'ERROR/LOGIN_MISSING';
      } else {
        this.errorGroup[0].error = '';
      }
    } else if (value.name === 'password') {
      if (this.formGroup.controls.password.errors !== null) {
        this.errorGroup[1].error = 'ERROR/PASSWORD_MISSING';
      } else {
        this.errorGroup[1].error = '';
      }
    }
  }

  /**
   * @description This method allows you to switch to the next navigation tabs in the application tabs
   */
  next(): void {
    this.nextEvent.emit('2');
  }

  /**
   * @description The method allows you to retrieve the component that tracks the value and validity state of a group of
   * FormControl instances
   */
  get formGroup(): FormGroup {
    return this._formGroup;
  }

  /**
   * @description The method allows you to assign the component that tracks the value and validity state of a group of
   * FormControl instances
   *
   * @param value Value of the new component that tracks the value and validity state of a group of FormControl instances
   */
  set formGroup(value: FormGroup) {
    this._formGroup = value;
  }

  /**
   * @description The method allows you to retrieve the form's error parameters
   */
  get errorGroup(): Array<FormErrorClass> {
    return this._errorGroup;
  }

  /**
   * @description The method allows you to assign the form's error parameters
   *
   * @param value Value of the new form's error parameters
   */
  set errorGroup(value: Array<FormErrorClass>) {
    this._errorGroup = value;
  }

  /**
   * @description The method allows you to retrieve the form's success parameters
   */
  get successGroup(): boolean {
    return this._successGroup;
  }

  /**
   * @description The method allows you to assign the form's success parameters
   *
   * @param value Value of the new form's success parameters
   */
  set successGroup(value: boolean) {
    this._successGroup = value;
  }
}
