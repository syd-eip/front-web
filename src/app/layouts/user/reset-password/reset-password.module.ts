/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
/**
 * Import of dependencies modules
 */
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
/**
 * Import of layouts
 */
import {ResetPasswordLayout} from './reset-password.layout';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    RouterModule
  ],
  declarations: [
    ResetPasswordLayout
  ],
  exports: [
    ResetPasswordLayout
  ],
  providers: []
})
export class ResetPasswordModule {
}
