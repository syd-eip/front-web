import {Component, ElementRef, NgZone, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslatableClass} from '../../../shared/services/translation/translation-class/translatable.class';
import {TranslationService} from '../../../shared/services/translation/translation.service';
import {CustomValidatorsClass} from '../../../shared/classes/validators/custom-validators.class';
import {ActivatedRoute, Router} from '@angular/router';
import {Semaphore} from '../../../shared/classes/semaphore/semaphore.class';
import {PATCH_USER_RESET_PASSWORD} from '../../../shared/data/api/api-path.data';
import {NetworkService} from '../../../shared/services/web/network.service';
import {NetworkParamsInterface} from '../../../shared/services/network/network-params/network-params.interface';
import {NetworkParamsClass} from '../../../shared/services/network/network-params/network-params.class';
import {FormErrorClass} from '../../../shared/classes/forms/form-error.class';
import {AlertService} from '../../../shared/services/alert/alert.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.layout.html'
})
export class ResetPasswordLayout extends TranslatableClass implements OnInit, OnDestroy {

  /**
   * @description Token to reset the password defined in the url
   */
  private token: string;

  /**
   * @description component that tracks the value and validity state of a group of FormControl instances.
   */
  private _formGroup: FormGroup;

  /**
   * @description success parameter
   */
  private _successGroup: boolean;

  /**
   * @description form's error parameters
   */
  private _errorGroup: Array<FormErrorClass>;

  /**
   * @description Data model to send to the API
   */
  private _model: { password: string, confirmPassword: string };

  /**
   * @description password field
   */
  @ViewChild('password', {static: true}) password: ElementRef;

  /**
   * @description confirmPassword field
   */
  @ViewChild('confirmPassword', {static: true}) confirmPassword: ElementRef;

  /**
   * @description Constructor of PrivacyPoliciesLayout layout
   *
   * The constructor creates an instance of the PrivacyPoliciesLayout layout and specifies the default values
   * the input and output variables of the layout.
   *
   * @param zone A reference to an injectable service for executing work inside or outside of the Angular zone
   * @param translationService A reference to the translation service of the application
   * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
   * @param activatedRoute Provides access to information about a route associated with a component that is loaded in an outlet
   * @param router Implements the Angular Router service , which enables navigation from one view to the next as users
   * perform application tasks
   * @param networkService A reference to the network service of the application
   * @param alertService A reference to the alert service of the application
   */
  constructor(private zone: NgZone, translationService: TranslationService, private formBuilder: FormBuilder,
              private router: Router, private activatedRoute: ActivatedRoute, private networkService: NetworkService,
              private alertService: AlertService) {
    super(translationService);
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.model = {password: '', confirmPassword: ''};

    this.token = this.activatedRoute.snapshot.queryParamMap.get('reset-token');
    if (!this.token) {
      this.router.navigate(['/']).then(() => {
      });
    }
    this.initForm();
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
   */
  ngOnDestroy(): void {
    delete this.formGroup;
  }

  /**
   * @description This method is used to initialize the password reset form.
   * It creates a form by specifying the rules to be respected to validate user inputs
   */
  initForm(): void {
    this.errorGroup = [{name: 'password', error: ''}, {name: 'confirmPassword', error: ''}];
    this.successGroup = false;

    this.formGroup = this.formBuilder.group(
      {
        password: [null, Validators.compose([
          Validators.required,
          CustomValidatorsClass.patternValidator(/\d/, {hasNumber: true}),
          CustomValidatorsClass.patternValidator(/[A-Z]/, {hasCapitalCase: true}),
          CustomValidatorsClass.patternValidator(/[a-z]/, {hasSmallCase: true}),
          CustomValidatorsClass.patternValidator(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/, {hasSpecialCharacters: true}),
          Validators.minLength(8),
          Validators.maxLength(50)
        ])],
        confirmPassword: [null, Validators.compose([
          Validators.required,
        ])]
      }, {
        validators: [
          CustomValidatorsClass.passwordMatchValidator
        ]
      }
    );
  }

  /**
   * @description This method returns the value of the focus of an HTML element
   *
   * @param value HTMLElement element
   */
  isFocused(value: HTMLElement): boolean {
    return document.activeElement === value;
  }

  /**
   * @description This method is used to display errors related to the password field
   */
  errorOccurredPassword(): void {
    this.successGroup = false;

    if (this.formGroup.controls.password.errors !== null) {
      if (this.formGroup.controls.password.errors.required) {
        this.errorGroup[0].error = 'ERROR/PASSWORD_MISSING';
      } else if (this.formGroup.controls.password.errors.minlength) {
        this.errorGroup[0].error = 'ERROR/PASSWORD_MIN_LENGTH';
      } else if (this.formGroup.controls.password.errors.maxlength) {
        this.errorGroup[0].error = 'ERROR/PASSWORD_MAX_LENGTH';
      } else {
        this.errorGroup[0].error = 'ERROR/PASSWORD_COMPOSITION';
      }
    } else {
      this.errorGroup[0].error = '';
    }
  }

  /**
   * @description This method is used to display errors related to the confirm password field
   */
  errorOccurredConfirmPassword(): void {
    this.successGroup = false;

    if (this.formGroup.controls.confirmPassword.errors !== null) {
      if (this.formGroup.controls.confirmPassword.errors.required) {
        this.errorGroup[1].error = 'ERROR/CONFIRM_PASSWORD_MISSING';
      } else if (this.formGroup.controls.confirmPassword.errors.NoPassswordMatch) {
        this.errorGroup[1].error = 'ERROR/CONFIRM_PASSWORD_MATCH';
      }
    } else {
      this.errorGroup[1].error = '';
    }
  }

  /**
   * @description This method is called when the form is valid and the user wishes to validate his password reset
   */
  valid(): void {
    const params: NetworkParamsInterface = new NetworkParamsClass();
    params.headers.push({name: 'Reset-Token', value: this.token});

    this.networkService.activeLoader(new Semaphore(1));
    this.networkService.patch(this._model, PATCH_USER_RESET_PASSWORD, params)
      .then(res => {
        (!res.responseState) ? this.errorEvent(res.responseError.message) : this.successEvent(res.responseData.message);
      })
      .catch(error => {
        this.errorEvent(error.responseError.message);
      });
  }

  /**
   * @description This method notifies the user of an error returned by the API after validation of the form
   *
   * @param value Error message sent by the API
   */
  errorEvent(value: string) {
    this.alertService.error(this.translate(value));
  }

  /**
   * @description This method notifies the user of a success returned by the API after validation of the form
   *
   * @param value Success message sent by the API
   */
  successEvent(value: string): void {
    this.router.navigate(['/user/sign/in']).then(() => {
      this.alertService.success(this.translate(value));
    });
  }

  /**
   * @description The method allows you to retrieve the component that tracks the value and validity state of a group of
   * FormControl instances
   */
  get formGroup(): FormGroup {
    return this._formGroup;
  }

  /**
   * @description The method allows you to assign the component that tracks the value and validity state of a group of
   * FormControl instances
   *
   * @param value Value of the new component that tracks the value and validity state of a group of FormControl instances
   */
  set formGroup(value: FormGroup) {
    this._formGroup = value;
  }

  /**
   * @description The method allows you to retrieve the form's error parameters
   */
  get errorGroup(): Array<FormErrorClass> {
    return this._errorGroup;
  }

  /**
   * @description The method allows you to assign the form's error parameters
   *
   * @param value Value of the new form's error parameters
   */
  set errorGroup(value: Array<FormErrorClass>) {
    this._errorGroup = value;
  }

  /**
   * @description The method allows you to retrieve the form's success parameters
   */
  get successGroup(): boolean {
    return this._successGroup;
  }

  /**
   * @description The method allows you to assign the form's success parameters
   *
   * @param value Value of the new form's success parameters
   */
  set successGroup(value: boolean) {
    this._successGroup = value;
  }

  /**
   * @description The method allows you to retrieve the data model to send to the API
   */
  get model(): any {
    return this._model;
  }

  /**
   * @description The method allows you to assign the data model to send to the API
   *
   * @param value Value of the new data model to send to the API
   */
  set model(value: any) {
    this._model = value;
  }
}
