/**
 * Import of Angular's module
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
/**
 * Import of application's layout
 */
import {UserLayout} from './user.layout';
/**
 * Import of application's modules
 */
import {UserRoutingModule} from './user-routing.module';
import {LoginModule} from './login/login.module';
import {ProfileModule} from './profile/profile.module';
import {RegisterModule} from './register/register.module';
import {ResetPasswordModule} from './reset-password/reset-password.module';
import {PaymentModule} from './payment/payment.module';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    LoginModule,
    ProfileModule,
    RegisterModule,
    ResetPasswordModule,
    PaymentModule
  ],
  declarations: [
    UserLayout
  ],
  exports: [
    LoginModule,
    ProfileModule,
    RegisterModule,
    ResetPasswordModule,
    PaymentModule
  ],
})
export class UserModule {
}
