import {AfterViewInit, Component, EventEmitter, Input, NgZone, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidatorsClass} from '../../../../shared/classes/validators/custom-validators.class';
import {TranslationService} from '../../../../shared/services/translation/translation.service';
import {TranslatableClass} from '../../../../shared/services/translation/translation-class/translatable.class';
import {Observable} from 'rxjs';
import {RegisterClass} from '../../../../shared/classes/models/register.class';
import {FormErrorClass} from '../../../../shared/classes/forms/form-error.class';

@Component({
  selector: 'app-form-step-three',
  templateUrl: './register-form-three.layout.html'
})
export class RegisterFormThreeLayout extends TranslatableClass implements OnInit, OnDestroy, AfterViewInit {


  /**
   * @description component that tracks the value and validity state of a group of FormControl instances.
   */
  private _formGroup: FormGroup;

  /**
   * @description form's error parameters
   */
  private _errorGroup: Array<FormErrorClass>;

  /**
   * @description registration form to send to the API
   */
  @Input() model: RegisterClass;

  /**
   * @description This Observable makes it possible to know the error response of the API after validation of the form
   */
  @Input() errorObservable: Observable<string>;

  /**
   * @description This EventEmitter variable emits customized events synchronously
   * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
   *
   * In the RegisterFormThreeLayout, this EventEmitter allows you to change the form (go to the next one)
   */
  @Output() nextStep = new EventEmitter<string>();

  /**
   * @description This EventEmitter variable emits customized events synchronously
   * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
   *
   * In the RegisterFormThreeLayout, this EventEmitter allows you to change the form (go to the next one)
   */
  @Output() prevStep = new EventEmitter<string>();

  /**
   * @description This EventEmitter variable emits customized events synchronously
   * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
   *
   * In the RegisterFormThreeLayout, this EventEmitter allows you to change the form (go back to the previous one)
   */
  @Output() prevEvent = new EventEmitter<string>();

  /**
   * @description This EventEmitter variable emits customized events synchronously
   * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
   *
   * In the RegisterFormThreeLayout, this EventEmitter allows the sending of the form number 2 validation
   */
  @Output() validStep = new EventEmitter<string>();

  /**
   * @description Constructor of RegisterFormThreeLayout layout
   *
   * The constructor creates an instance of the RegisterFormThreeLayout layout and specifies the default values
   * the input and output variables of the layout.
   *
   * @param zone A reference to an injectable service for executing work inside or outside of the Angular zone
   * @param translationService A reference to the translation service of the application
   * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
   */
  constructor(private formBuilder: FormBuilder, private zone: NgZone, translationService: TranslationService) {
    super(translationService);
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.initForm();
    this.errorObservable.subscribe(value => (value) ? this.manageError(value) : null);
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
   */
  ngOnDestroy(): void {
    delete this.errorObservable;
    delete this.formGroup;
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular has finished to initialize the view of a component.
   * Defining an ngAfterViewInit() method allows you to manage any additional initialization tasks.
   */
  ngAfterViewInit(): void {
    this.formGroup.statusChanges.subscribe(value => {
      this.validStep.emit(value);
    });
  }

  /**
   * @description This method is used to initialize the part two of the register form.
   * It creates a form by specifying the rules to be respected to validate user inputs
   */
  initForm(): void {
    this.errorGroup = [{name: 'username', error: ''}, {name: 'password', error: ''}, {
      name: 'confirmPassword',
      error: ''
    }];
    this.formGroup = this.formBuilder.group(
      {
        username: [null, Validators.compose([
          Validators.required,
          Validators.minLength(3)])
        ],
        password: [null, Validators.compose([
          Validators.required,
          CustomValidatorsClass.patternValidator(/\d/, {hasNumber: true}),
          CustomValidatorsClass.patternValidator(/[A-Z]/, {hasCapitalCase: true}),
          CustomValidatorsClass.patternValidator(/[a-z]/, {hasSmallCase: true}),
          CustomValidatorsClass.patternValidator(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/, {hasSpecialCharacters: true}),
          Validators.minLength(8),
          Validators.maxLength(50)])
        ],
        confirmPassword: [null, Validators.compose([
          Validators.required])]
      }, {
        validators: [
          CustomValidatorsClass.passwordMatchValidator
        ]
      }
    );
  }

  /**
   * @description This method returns the value of the focus of an HTML element
   *
   * @param value HTMLElement element
   */
  isFocused(value: HTMLElement): boolean {
    return document.activeElement === value;
  }

  /**
   * @description This method returns the value of the focus of an HTML element
   *
   * @param value HTMLElement element
   */
  manageError(value: string): void {
    if (value.slice(6, 14) === 'USERNAME') {
      this.errorGroup[0].error = value;
    } else if (value.slice(6, 14) === 'PASSWORD') {
      this.errorGroup[1].error = value;
    }
  }

  /**
   * @description This method is used to warn the user of an error
   *
   * @param value Value of the HTML element
   */
  errorOccurred(value: HTMLInputElement): void {
    if (value.name === 'username') {
      this.errorsUsernameOccurred();
    } else if (value.name === 'password') {
      this.errorsPasswordOccurred();
    } else {
      this.errorsConfirmPasswordOccurred();
    }
  }

  /**
   * @description This method is used to warn the user of an error concerning the username
   */
  errorsUsernameOccurred(): void {
    if (this.formGroup.controls.username.errors !== null) {
      if (this.formGroup.controls.username.errors.required !== undefined) {
        this.errorGroup[0].error = 'ERROR/USERNAME_MISSING';
      } else {
        this.errorGroup[0].error = 'ERROR/USERNAME_FORMAT';
      }
    } else {
      this.errorGroup[0].error = '';
    }
  }

  /**
   * @description This method is used to warn the user of an error concerning the password
   */
  errorsPasswordOccurred(): void {
    if (this.formGroup.controls.password.errors !== null) {
      if (this.formGroup.controls.password.errors.required !== undefined) {
        this.errorGroup[1].error = 'ERROR/PASSWORD_MISSING';
      } else if (this.formGroup.controls.password.errors.minlength !== undefined) {
        this.errorGroup[1].error = 'ERROR/PASSWORD_MIN_LENGTH';
      } else if (this.formGroup.controls.password.errors.maxlength !== undefined) {
        this.errorGroup[1].error = 'ERROR/PASSWORD_MAX_LENGTH';
      } else {
        this.errorGroup[1].error = 'ERROR/PASSWORD_COMPOSITION';
      }
    } else {
      this.errorGroup[1].error = '';
    }
  }

  /**
   * @description This method is used to warn the user of an error concerning the password confirmation
   */
  errorsConfirmPasswordOccurred(): void {
    if (this.formGroup.controls.confirmPassword.errors !== null) {
      if (this.formGroup.controls.confirmPassword.errors.required !== undefined) {
        this.errorGroup[2].error = 'ERROR/CONFIRM_PASSWORD_MISSING';
      } else if (this.formGroup.controls.confirmPassword.errors.NoPassswordMatch !== undefined) {
        this.errorGroup[2].error = 'ERROR/CONFIRM_PASSWORD_MATCH';
      }
    } else {
      this.errorGroup[2].error = '';
    }
  }

  /**
   * @description This method allows you to switch to the next navigation tabs in the application tabs
   */
  prev(): void {
    this.prevStep.emit('previous');
  }

  /**
   * @description This method allows you to switch to the next navigation tabs in the application tabs
   */
  next(): void {
    this.nextStep.emit('next');
  }

  /**
   * @description The method allows you to retrieve the component that tracks the value and validity state of a group of
   * FormControl instances
   */
  get formGroup(): FormGroup {
    return this._formGroup;
  }

  /**
   * @description The method allows you to assign the component that tracks the value and validity state of a group of
   * FormControl instances
   *
   * @param value Value of the new component that tracks the value and validity state of a group of FormControl instances
   */
  set formGroup(value: FormGroup) {
    this._formGroup = value;
  }

  /**
   * @description The method allows you to retrieve the form's error parameters
   */
  get errorGroup(): Array<FormErrorClass> {
    return this._errorGroup;
  }

  /**
   * @description The method allows you to assign the form's error parameters
   *
   * @param value Value of the new form's error parameters
   */
  set errorGroup(value: Array<FormErrorClass>) {
    this._errorGroup = value;
  }
}
