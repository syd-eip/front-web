import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {CustomValidatorsClass} from '../../../../shared/classes/validators/custom-validators.class';
import {TranslationService} from '../../../../shared/services/translation/translation.service';
import {CustomSubjectClass} from '../../../../shared/classes/rxjs-overrides/custom-subject.class';
import {TranslatableClass} from '../../../../shared/services/translation/translation-class/translatable.class';
import {RegisterClass} from '../../../../shared/classes/models/register.class';
import {FormErrorClass} from '../../../../shared/classes/forms/form-error.class';

@Component({
  selector: 'app-form-step-four',
  templateUrl: './register-form-four.layout.html'
})
export class RegisterFormFourLayout extends TranslatableClass implements OnInit, OnDestroy, AfterViewInit {

  /**
   * @description Selected value of the NgbTypeahead component
   */
  private _focus$: CustomSubjectClass<string>;

  /**
   * @description Selected value of the NgbTypeahead component
   */
  private _click$: CustomSubjectClass<string>;

  /**
   * @description component that tracks the value and validity state of a group of FormControl instances.
   */
  private _formGroup: FormGroup;

  /**
   * @description form's error parameters
   */
  private _errorGroup: Array<FormErrorClass>;

  /**
   * @description registration form to send to the API
   */
  @Input() model: RegisterClass;

  /**
   * @description Subject is a special type of Observable that allows values to be multicasted to many Observers.
   * You can subscribe to this Subject to receive data (error request) sent by the API
   */
  @Input() errorObservable: Observable<string>;

  /**
   * @description This EventEmitter variable emits customized events synchronously
   * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
   *
   * In the RegisterFormFourLayout, this EventEmitter allows you to change the form (go to the next one)
   */
  @Output() nextStep = new EventEmitter<string>();

  /**
   * @description This EventEmitter variable emits customized events synchronously
   * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
   *
   * In the RegisterFormFourLayout, this EventEmitter allows you to change the form (go back to the previous one)
   */
  @Output() prevStep = new EventEmitter<string>();

  /**
   * @description This EventEmitter variable emits customized events synchronously
   * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
   *
   * In the RegisterFormFourLayout, this EventEmitter allows the sending of the form number 2 validation
   */
  @Output() validStep = new EventEmitter<string>();

  @ViewChild('recaptcha', {static: true}) recaptchaElement: ElementRef;

  /**
   * @description Constructor of RegisterFormFourLayout layout
   *
   * The constructor creates an instance of the RegisterLayout layout and specifies the default values
   * the input and output variables of the layout.
   *
   * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
   * @param changeDetectorRef
   * @param zone A reference to an injectable service for executing work inside or outside of the Angular zone
   * perform application tasks
   * @param translationService A reference to the translation service of the application
   */
  constructor(private formBuilder: FormBuilder, private changeDetectorRef: ChangeDetectorRef,
              private zone: NgZone, translationService: TranslationService) {
    super(translationService);
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit() {
    this.initForm();
    this.focus$ = new CustomSubjectClass<string>();
    this.click$ = new CustomSubjectClass<string>();
    this.errorObservable.subscribe(value => (value) ? this.manageError(value) : null);
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular has finished to initialize the view of a component.
   * Defining an ngAfterViewInit() method allows you to manage any additional initialization tasks.
   */
  ngAfterViewInit(): void {
    this.initPhone();
    this.formGroup.statusChanges.subscribe(value => {
      this.validStep.emit(value);
    });
    if (this.model.phone.nationalNumber !== null &&
      this.model.phone.nationalNumber !== '') {
      this.formGroup.controls.phone.setValue(this.model.phone.nationalNumber);
    }
    this.changeDetectorRef.detectChanges();
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
   */
  ngOnDestroy(): void {
    delete this.errorObservable;
    delete this.formGroup;
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular has finished to initialize the view of a component.
   * Defining an ngAfterViewInit() method allows you to manage any additional initialization tasks.
   */
  initForm(): void {
    this.errorGroup = [
      {name: 'email', error: ''},
      {name: 'phone', error: ''},
      {name: 'location', error: ''},
      {name: 'cgu', error: ''}
    ];
    this.formGroup = this.formBuilder.group(
      {
        email: [null, Validators.compose([
          Validators.required,
          Validators.email
        ])
        ],
        phone: [null, Validators.compose([
          Validators.required
        ])
        ],
        location: [null, Validators.compose([
          Validators.required
        ])
        ],
        cgu: [null, Validators.compose([
          Validators.required
        ])
        ]
      }, {
        validators: [
          CustomValidatorsClass.checkboxRequiredChoiceValidator,
          CustomValidatorsClass.locationValidator
        ]
      }
    );

    this.formGroup.controls['location'].setValue(null);
  }

  /**
   * @description This method initializes the phone value of the Component's FormGroup
   */
  initPhone(): void {
    const parent = this;
    const phoneInput = document.getElementById('ngx-telinput-phone') as HTMLElement;
    const input = phoneInput.children[0].children[1] as HTMLInputElement;

    if (input !== null && input !== undefined) {
      input.onchange = function () {
        parent.model.phone.nationalNumber = input.value;
      };
    }
  }

  /**
   * @description This method allows you to switch to the next navigation tabs in the application tabs
   */
  prev(): void {
    this.prevStep.emit('previous');
  }

  /**
   * @description This method allows you to switch to the next navigation tabs in the application tabs
   */
  next(): void {
    this.nextStep.emit('complete');
  }

  /**
   * @description This method returns the value of the focus of an HTML element
   *
   * @param value HTMLElement element
   */
  isFocused(value: HTMLElement): boolean {
    return document.activeElement === value;
  }

  /**
   * @description This method intercepts the user's country selection and assigns it to the data model.
   *
   * @param data Country's data
   */
  countryChange(data: any): void {
    this.model.location = data;
    this.formGroup.controls['location'].setValue(data);

    this.errorsLocationOccurred();
  }

  /**
   * @description This method fills the phone number value in the application form and the country code before to send it
   */
  fillData(): void {
    this.model.phone = this.formGroup.value.phone;
  }

  /**
   * @description This method is used to warn the user of an error returned by the API
   *
   * @param value error string send by the API
   */
  manageError(value: string): void {
    if (value.slice(6, 11) === 'EMAIL') {
      this.errorGroup[0].error = value;
    } else if (value.slice(6, 11) === 'PHONE') {
      this.errorGroup[1].error = value;
    } else if (value.slice(6, 14) === 'LOCATION') {
      this.errorGroup[2].error = value;
    } else if (value.slice(6, 9) === 'CGU') {
      this.errorGroup[3].error = value;
    }
  }

  /**
   * @description This method is used to warn the user of an error
   *
   * @param value Value of the HTML element
   */
  errorOccurred(value: HTMLInputElement): void {
    if (value.name === 'email') {
      this.errorsEmailOccurred();
    } else if (value.name === 'cgu') {
      this.errorsCguOccured();
    }
  }

  /**
   * @description This method is used to warn the user of an error concerning the email address
   */
  errorsEmailOccurred(): void {
    if (this.formGroup.controls.email.errors !== null) {
      if (this.formGroup.controls.email.errors.required !== undefined) {
        this.errorGroup[0].error = 'ERROR/EMAIL_MISSING';
      } else {
        this.errorGroup[0].error = 'ERROR/EMAIL_BAD_FORMAT';
      }
    } else {
      this.errorGroup[0].error = '';
    }
  }

  /**
   * @description This method is used to warn the user of an error concerning the phone number
   */
  errorsPhoneOccurred(): void {
    if (this.formGroup.controls.phone.errors !== null) {
      if (this.formGroup.controls.phone.errors.required !== undefined) {
        this.errorGroup[1].error = 'ERROR/PHONE_NUMBER_MISSING';
      } else if (this.formGroup.controls.phone.errors.validatePhoneNumber !== undefined &&
        this.formGroup.controls.phone.errors.validatePhoneNumber.valid === false) {
        this.errorGroup[1].error = 'ERROR/PHONE_BAD_FORMAT';
      }
    } else {
      this.errorGroup[1].error = '';
    }
  }

  /**
   * @description This method is used to warn the user of an error concerning the location's country
   */
  errorsLocationOccurred(): void {
    if (this.formGroup.controls.location.errors !== null) {
      if (this.formGroup.controls.location.errors.required !== undefined) {
        this.errorGroup[2].error = 'ERROR/LOCATION_COUNTRY_CODE_MISSING';
      }
    } else if (this.formGroup.errors !== null && this.formGroup.errors.location !== undefined) {
      this.errorGroup[2].error = 'ERROR/LOCATION_COUNTRY_CODE_NOT_EXIST';
    } else {
      this.errorGroup[2].error = '';
    }
  }

  /**
   * @description This method is used to warn the user of an error concerning terms and conditions
   */
  errorsCguOccured(): void {
    if (this.formGroup.controls.cgu.errors !== null ||
      (this.formGroup.errors !== null && this.formGroup.errors.checkboxRequired !== undefined)) {
      this.errorGroup[3].error = 'ERROR/CGU_NOT_VALIDATED';
    } else {
      this.errorGroup[3].error = '';
    }
  }

  /**
   * @description The method allows you to retrieve the component that tracks the value and validity state of a group of
   * FormControl instances
   */
  get formGroup(): FormGroup {
    return this._formGroup;
  }

  /**
   * @description The method allows you to assign the component that tracks the value and validity state of a group of
   * FormControl instances
   *
   * @param value Value of the new component that tracks the value and validity state of a group of FormControl instances
   */
  set formGroup(value: FormGroup) {
    this._formGroup = value;
  }

  /**
   * @description The method allows you to retrieve the form's error parameters
   */
  get errorGroup(): Array<FormErrorClass> {
    return this._errorGroup;
  }

  /**
   * @description The method allows you to assign the form's error parameters
   *
   * @param value Value of the new form's error parameters
   */
  set errorGroup(value: Array<FormErrorClass>) {
    this._errorGroup = value;
  }

  /**
   * @description The method allows you to retrieve the selected value of the NgbTypeahead component
   */
  get click$(): CustomSubjectClass<string> {
    return this._click$;
  }

  /**
   * @description The method allows you to assign the selected value of the NgbTypeahead component
   *
   * @param value Value of the new selected value of the NgbTypeahead component
   */
  set click$(value: CustomSubjectClass<string>) {
    this._click$ = value;
  }

  /**
   * @description The method allows you to retrieve the selected value of the NgbTypeahead component
   */
  get focus$(): CustomSubjectClass<string> {
    return this._focus$;
  }

  /**
   * @description The method allows you to assign the selected value of the NgbTypeahead component
   *
   * @param value Value of the new selected value of the NgbTypeahead component
   */
  set focus$(value: CustomSubjectClass<string>) {
    this._focus$ = value;
  }
}
