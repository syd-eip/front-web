/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

/**
 * Import of dependencies modules
 */
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxIntlTelInputModule} from 'ngx-intl-tel-input';

/**
 * Import of application's modules
 */
import {MatStepperModule} from '@angular/material';

/**
 * Import of layouts
 */
import {RegisterLayout} from './register.layout';
import {RegisterFormOneLayout} from './register-form-one/register-form-one.layout';
import {RegisterFormTwoLayout} from './register-form-two/register-form-two.layout';
import {RegisterFormThreeLayout} from './register-form-three/register-form-three.layout';
import {RegisterFormFourLayout} from './register-form-four/register-form-four.layout';
import {ComponentsModule} from '../../../shared/components/components.module';

/**
 * Register layout module of the Application. This module retrieve register layout
 * You can also find Angular's modules
 */
@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    MatStepperModule,
    NgxIntlTelInputModule,
    ComponentsModule
  ],
  declarations: [
    RegisterLayout,
    RegisterFormOneLayout,
    RegisterFormTwoLayout,
    RegisterFormThreeLayout,
    RegisterFormFourLayout
  ],
  exports: [
    RegisterLayout,
    RegisterFormOneLayout,
    RegisterFormTwoLayout,
    RegisterFormThreeLayout,
    RegisterFormFourLayout
  ],
  providers: []
})
export class RegisterModule {
}
