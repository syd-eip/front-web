import {Component, EventEmitter, Input, NgZone, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidatorsClass} from '../../../../shared/classes/validators/custom-validators.class';
import {TranslatableClass} from '../../../../shared/services/translation/translation-class/translatable.class';
import {TranslationService} from '../../../../shared/services/translation/translation.service';
import {RegisterClass} from '../../../../shared/classes/models/register.class';

@Component({
  selector: 'app-form-step-one',
  templateUrl: './register-form-one.layout.html'
})
export class RegisterFormOneLayout extends TranslatableClass implements OnInit, OnDestroy {

  /**
   * @description component that tracks the value and validity state of a group of FormControl instances.
   */
  private _formGroup: FormGroup;

  /**
   * @description registration form to send to the API
   */
  @Input() public model: RegisterClass;

  /**
   * @description This EventEmitter variable emits customized events synchronously
   * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
   *
   * In the RegisterFormOneLayout, this EventEmitter allows you to change the form (go to the next one)
   */
  @Output() public nextEvent = new EventEmitter<string>();

  /**
   * @description Constructor of RegisterFormOneLayout layout
   *
   * the input and output variables of the layout.
   *
   * @param zone A reference to an injectable service for executing work inside or outside of the Angular zone
   * @param translationService A reference to the translation service of the application
   * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
   */
  constructor(private formBuilder: FormBuilder, private zone: NgZone, translationService: TranslationService) {
    super(translationService);
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit() {
    this.initForm();
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
   */
  ngOnDestroy(): void {
    delete this.nextEvent;
    delete this.formGroup;
  }

  /**
   * @description This method is used to initialize the first part of the register form.
   * It creates a form by specifying the rules to be respected to validate user inputs
   */
  initForm(): void {
    this.formGroup = this.formBuilder.group(
      {
        notinfluencer: [null, Validators.compose([])
        ],
        influencer: [null, Validators.compose([])
        ]
      }, {
        validator: CustomValidatorsClass.checkboxSingleChoiceValidator
      }
    );
  }

  /**
   * @description This method allows you to switch to the next navigation tabs in the application tabs
   */
  next(): void {
    if (this.model.wantToBeInfluencer !== true && this.model.wantToBeInfluencer !== false) {
      this.model.wantToBeInfluencer = false;
    }
    this.nextEvent.emit('2');
  }

  /**
   * @descritpioon This method allows you to enable or disable the option that was not chosen by the user.
   * It makes it possible to inform the will to become an influencer or not.
   *
   * @param input Checkbox checked
   */
  influencerChange(input: HTMLInputElement): void {
    const influencer = (document.getElementsByName('check-influencer')[0] as HTMLInputElement);
    const notinfluencer = (document.getElementsByName('check-notinfluencer')[0] as HTMLInputElement);

    if (input.name === 'check-influencer') {
      this.model.wantToBeInfluencer = input.checked;
      notinfluencer.disabled = (input.checked === true);
      if (input.checked === true) {
        notinfluencer.parentElement.parentElement.classList.add('disabled');
      } else {
        notinfluencer.parentElement.parentElement.classList.remove('disabled');
      }
    } else {
      this.model.wantToBeInfluencer = false;
      influencer.disabled = (input.checked === true);
      if (input.checked === true) {
        influencer.parentElement.parentElement.classList.add('disabled');
      } else {
        influencer.parentElement.parentElement.classList.remove('disabled');
      }
    }
  }

  /**
   * @description The method allows you to retrieve the component that tracks the value and validity state of a group of
   * FormControl instances
   */
  get formGroup(): FormGroup {
    return this._formGroup;
  }

  /**
   * @description The method allows you to assign the component that tracks the value and validity state of a group of
   * FormControl instances
   *
   * @param value Value of the new component that tracks the value and validity state of a group of FormControl instances
   */
  set formGroup(value: FormGroup) {
    this._formGroup = value;
  }
}
