import {AfterViewInit, Component, EventEmitter, Input, NgZone, OnDestroy, OnInit, Output,} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidatorsClass} from '../../../../shared/classes/validators/custom-validators.class';
import {TranslationService} from '../../../../shared/services/translation/translation.service';
import {TranslatableClass} from '../../../../shared/services/translation/translation-class/translatable.class';
import {Observable} from 'rxjs';
import {UtilsClass} from '../../../../shared/classes/utils/utils.class';
import {RegisterClass} from '../../../../shared/classes/models/register.class';
import {FormErrorClass} from '../../../../shared/classes/forms/form-error.class';

@Component({
  selector: 'app-form-step-two',
  templateUrl: './register-form-two.layout.html'
})
export class RegisterFormTwoLayout extends TranslatableClass implements OnInit, OnDestroy, AfterViewInit {

  /**
   * @description component that tracks the value and validity state of a group of FormControl instances.
   */
  private _formGroup: FormGroup;

  /**
   * @description form's error parameters
   */
  private _errorGroup: Array<FormErrorClass>;

  /**
   * @description registration form to send to the API
   */
  @Input() model: RegisterClass;

  /**
   * @description This Observable makes it possible to know the error response of the API after validation of the form
   */
  @Input() errorObservable: Observable<string>;

  /**
   * @description This EventEmitter variable emits customized events synchronously
   * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
   *
   * In the RegisterFormTwoLayout, this EventEmitter allows you to change the form (go to the next one)
   */
  @Output() nextStep = new EventEmitter<string>();

  /**
   * @description This EventEmitter variable emits customized events synchronously
   * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
   *
   * In the RegisterFormTwoLayout, this EventEmitter allows you to change the form (go back to the previous one)
   */
  @Output() prevEvent = new EventEmitter<string>();

  /**
   * @description This EventEmitter variable emits customized events synchronously
   * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
   *
   * In the RegisterFormTwoLayout, this EventEmitter allows the sending of the form number 2 validation
   */
  @Output() validStep = new EventEmitter<string>();

  /**
   * @description Constructor of RegisterFormTwoLayout layout
   *
   * The constructor creates an instance of the RegisterFormTwoLayout layout and specifies the default values
   * the input and output variables of the layout.
   *
   * @param zone A reference to an injectable service for executing work inside or outside of the Angular zone
   * @param translationService A reference to the translation service of the application
   * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
   */
  constructor(private formBuilder: FormBuilder, private zone: NgZone, translationService: TranslationService) {
    super(translationService);
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.initForm();
    this.errorObservable.subscribe(value => (value) ? this.manageError(value) : null);
    if (this.model.birthday !== null && this.model.birthday !== '') {
      this.model.birthday = UtilsClass.ISOStringToDate(this.model.birthday);
    }
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
   */
  ngOnDestroy(): void {
    delete this.errorObservable;
    delete this.prevEvent;
    delete this.formGroup;
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular has finished to initialize the view of a component.
   * Defining an ngAfterViewInit() method allows you to manage any additional initialization tasks.
   */
  ngAfterViewInit(): void {
    this.formGroup.statusChanges.subscribe(value => {
      this.validStep.emit(value);
    });
  }

  /**
   * @description This method is used to initialize the part two of the register form.
   * It creates a form by specifying the rules to be respected to validate user inputs
   */
  initForm(): void {
    const regExp = new RegExp('^((?:\\w|[\\-_ ](?![\\-_ ])|[\\u00C0\\u00C1\\u00C2\\u00C3\\u00C4\\u00C5\\u00C6\\u00C7\\u00C8\\u00C9' +
      '\\u00CA\\u00CB\\u00CC\\u00CD\\u00CE\\u00CF\\u00D0\\u00D1\\u00D2\\u00D3\\u00D4\\u00D5\\u00D6\\u00D8\\u00D9\\u00DA\\u00DB\\u00DC' +
      '\\u00DD\\u00DF\\u00E0\\u00E1\\u00E2\\u00E3\\u00E4\\u00E5\\u00E6\\u00E7\\u00E8\\u00E9\\u00EA\\u00EB\\u00EC\\u00ED\\u00EE\\u00EF' +
      '\\u00F0\\u00F1\\u00F2\\u00F3\\u00F4\\u00F5\\u00F6\\u00F9\\u00FA\\u00FB\\u00FC\\u00FD\\u00FF\\u0153])+)$', 'i');

    this.errorGroup = [{name: 'firstName', error: ''}, {name: 'lastName', error: ''}, {name: 'birthday', error: ''}];
    this.formGroup = this.formBuilder.group(
      {
        firstName: [null, Validators.compose([
          Validators.required,
          Validators.pattern(regExp)
        ])
        ],
        lastName: [null, Validators.compose([
          Validators.required,
          Validators.pattern(regExp)
        ])
        ],
        birthday: [null, Validators.compose([
          Validators.required])
        ]
      }, {
        validators: [
          CustomValidatorsClass.dateMatchValidator,
          CustomValidatorsClass.dateRangeValidator,
          CustomValidatorsClass.dateLegalValidator
        ]
      }
    );
  }

  /**
   * @description This method allows you to format the date in the format'DD/MM/YYY'.
   * It refuses any characters other than numbers and '/'.
   * It respects the pattern {2}/{2}/{4}.
   */
  replaceDateValue(event: any): void {
    event.target.value = event.target.value
      .replace(/^(\d\d)(\d)$/g, '$1/$2')
      .replace(/^(\d\d\/\d\d)(\d+)$/g, '$1/$2')
      .replace(/[^\d\/]/g, '');
  }

  /**
   * @description This method returns the value of the focus of an HTML element
   *
   * @param value HTMLElement element
   */
  isFocused(value: HTMLElement): boolean {
    return document.activeElement === value;
  }

  /**
   * @description This method allows you to switch to the next navigation tabs in the application tabs
   */
  prev(): void {
    this.prevEvent.emit('1');
  }

  /**
   * @description This method allows you to switch to the next navigation tabs in the application tabs
   */
  next(): void {
    this.nextStep.emit('next');
  }

  /**
   * @description This method is used to warn the user of an error returned by the API
   *
   * @param value error string send by the API
   */
  manageError(value: string): void {
    if (value.slice(6, 16) === 'FIRST_NAME') {
      this.errorGroup[0].error = value;
    } else if (value.slice(6, 15) === 'LAST_NAME') {
      this.errorGroup[1].error = value;
    } else if (value.slice(6, 14) === 'BIRTHDAY') {
      this.errorGroup[2].error = value;
    }
  }

  /**
   * @description This method is used to warn the user of an error
   *
   * @param value Value of the HTML element
   */
  errorOccurred(value: HTMLInputElement): void {
    if (value.name === 'firstName') {
      this.errorsFirstnameOccured();
    } else if (value.name === 'lastName') {
      this.errorsLastnameOccured();
    } else {
      if (value.value.length === 10) {
        this.errorsBirthdayOccurred();
      } else {
        this.errorGroup[2].error = '';
      }
    }
  }

  /**
   * @description This method is used to warn the user of an error concerning the first name
   */
  errorsFirstnameOccured(): void {
    if (this.formGroup.controls.firstName.errors !== null) {
      if (this.formGroup.controls.firstName.errors.required !== undefined) {
        this.errorGroup[0].error = 'ERROR/FIRST_NAME_MISSING';
      } else {
        this.errorGroup[0].error = 'ERROR/FIRST_NAME_BAD_FORMAT';
      }
    } else {
      this.errorGroup[0].error = '';
    }
  }

  /**
   * @description This method is used to warn the user of an error concerning the last name
   */
  errorsLastnameOccured(): void {
    if (this.formGroup.controls.lastName.errors !== null) {
      if (this.formGroup.controls.lastName.errors.required !== undefined) {
        this.errorGroup[1].error = 'ERROR/LAST_NAME_MISSING';
      } else {
        this.errorGroup[1].error = 'ERROR/LAST_NAME_BAD_FORMAT';
      }
    } else {
      this.errorGroup[1].error = '';
    }
  }

  /**
   * @description This method is used to warn the user of an error concerning the birthday
   */
  errorsBirthdayOccurred(): void {
    if (this.formGroup.controls.birthday.errors !== null) {
      this.errorGroup[2].error = 'ERROR/BIRTHDAY_MISSING';
    } else if (this.formGroup.errors !== null) {
      if (this.formGroup.errors.dateMatch === true) {
        this.errorGroup[2].error = 'ERROR/BIRTHDAY_BAD_FORMAT';
      } else if (this.formGroup.errors.dateRange === true) {
        this.errorGroup[2].error = 'ERROR/DATE_ENTERED_INVALID';
      } else if (this.formGroup.errors.dateLegal === true) {
        this.errorGroup[2].error = 'ERROR/BIRTHDAY_NOT_LEGAL_AGE';
      }
    } else {
      this.errorGroup[2].error = '';
    }
  }

  /**
   * @description The method allows you to retrieve the component that tracks the value and validity state of a group of
   * FormControl instances
   */
  get formGroup(): FormGroup {
    return this._formGroup;
  }

  /**
   * @description The method allows you to assign the component that tracks the value and validity state of a group of
   * FormControl instances
   *
   * @param value Value of the new component that tracks the value and validity state of a group of FormControl instances
   */
  set formGroup(value: FormGroup) {
    this._formGroup = value;
  }

  /**
   * @description The method allows you to retrieve the form's error parameters
   */
  get errorGroup(): Array<FormErrorClass> {
    return this._errorGroup;
  }

  /**
   * @description The method allows you to assign the form's error parameters
   *
   * @param value Value of the new form's error parameters
   */
  set errorGroup(value: Array<FormErrorClass>) {
    this._errorGroup = value;
  }
}
