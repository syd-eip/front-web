import {Component, NgZone, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TranslationService} from 'src/app/shared/services/translation/translation.service';
import {NgbTabset} from '@ng-bootstrap/ng-bootstrap';
import {TranslatableClass} from '../../../shared/services/translation/translation-class/translatable.class';
import {MatHorizontalStepper} from '@angular/material';
import {CustomSubjectClass} from '../../../shared/classes/rxjs-overrides/custom-subject.class';
import {UserService} from '../../../shared/services/web/user.service';
import {UtilsClass} from '../../../shared/classes/utils/utils.class';
import {SIGN_UP_URL} from '../../../shared/data/api/api-path.data';
import {Semaphore} from '../../../shared/classes/semaphore/semaphore.class';
import {NetworkResponseBodyInterface} from '../../../shared/services/network/network-response/network-response-body.interface';
import {Router} from '@angular/router';
import {RegisterClass} from '../../../shared/classes/models/register.class';
import {AlertService} from '../../../shared/services/alert/alert.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.layout.html'
})
export class RegisterLayout extends TranslatableClass implements OnInit, OnDestroy {

  /**
   * @description active tab identifier
   */
  private _activeId: string;

  /**
   * @description Component's boolean array which inform you if each steps is complete
   */
  private _completeStep: Array<boolean>;

  /**
   * @description registration form to send to the API
   */
  private _model: RegisterClass;

  /**
   * @description Subject is a special type of Observable that allows values to be multicasted to many Observers.
   * You can subscribe to this Subject to receive data (error request) sent by the API
   */
  private _errorEventSubject: CustomSubjectClass<string>;

  /**
   * @description A component that makes it easy to create tabbed interface.
   */
  @ViewChild('tabform', {static: true}) tabform: NgbTabset;

  /**
   * @description Angular Material's stepper
   */
  @ViewChild(MatHorizontalStepper, {static: false}) stepperForm: MatHorizontalStepper;

  /**
   * @description Constructor of RegisterLayout layout
   *
   * The constructor creates an instance of the RegisterLayout layout and specifies the default values
   * the input and output variables of the layout.
   *
   * @param userService A reference to the user service of the application
   * @param zone A reference to an injectable service for executing work inside or outside of the Angular zone
   * @param router Implements the Angular Router service , which enables navigation from one view to the next as users
   * perform application tasks
   * @param translationService A reference to the translation service of the application
   * @param alertService A reference to the alert service of the application
   */
  constructor(private userService: UserService, private zone: NgZone, private router: Router, private alertService: AlertService,
              translationService: TranslationService) {
    super(translationService);
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit() {
    this.activeId = 'tab-form-1';
    this.completeStep = [false, false, false];

    this.model = new RegisterClass();
    this.errorEventSubject = new CustomSubjectClass<string>();
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional initialization tasks.
   */
  ngOnDestroy() {
    delete this.model;
    delete this.errorEventSubject;
  }

  /**
   * A method which check at whether the value of the window width is less than or greater than 991px
   * @return void
   */
  isMobile() {
    if (window.innerWidth <= 991) {
      document.getElementsByClassName('card-signup')[0].parentElement.classList.remove('col-md-6');
      document.getElementsByClassName('card-signup')[0].parentElement.classList.add('col-md-8');
    } else {
      document.getElementsByClassName('card-signup')[0].parentElement.classList.remove('col-md-8');
      document.getElementsByClassName('card-signup')[0].parentElement.classList.add('col-md-6');
    }
    return window.innerWidth <= 991;
  }

  /**
   * @description This method allows the user to navigate in the application form
   *
   * @param value Event contains the identifier of the tab on which the user wants to navigate
   */
  tabEvent(value: string): void {
    this.tabform.select('tab-form-' + value);
    this.activeId = 'tab-form-' + value;
  }

  /**
   * @description This method allows the user to navigate in the application form stepper
   *
   * @param value Value which contains the identifier of the action event
   */
  stepEvent(value: string): void {
    switch (value) {
      case 'next':
        this.stepperForm.next();
        break;
      case 'previous':
        this.stepperForm.previous();
        break;
      case 'complete':
        this.register();
        break;
    }
  }

  /**
   * @description This method allows the user to navigate in the application form stepper by the mat step header
   *
   * @param value Value of the step validation
   */
  validStep(value: string) {
    if (this.stepperForm !== undefined) {
      this.completeStep[this.stepperForm.selectedIndex] = (value === 'VALID');
    }
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to register a user of the application
   *
   * It calls the method Post of the NetworkService service
   */
  register(): void {
    const data: RegisterClass = JSON.parse(JSON.stringify(this.model));

    data.birthday = UtilsClass.dateToISOString(data.birthday);

    this.userService.activeLoader(new Semaphore(1));
    this.userService.post(data, SIGN_UP_URL)
      .then(res => {
        (res.responseData.code !== 201) ? this.errorEvent(res.responseData.message) : this.successEvent(res.responseData);
      }).catch(error => {
      this.errorEvent(error.responseError.message)
    });
  }

  /**
   * @description This method sends the error message received by the API to the form (child component) in order to display it
   *
   * @param value Error message sent by the API
   */
  errorEvent(value: string) {
    if (value.slice(6, 16) === 'FIRST_NAME' || value.slice(6, 15) === 'LAST_NAME' || value.slice(6, 14) === 'BIRTHDAY') {
      this.stepperForm.previous();
      this.stepperForm.previous();
    } else if (value.slice(6, 14) === 'USERNAME' || value.slice(6, 14) === 'PASSWORD') {
      this.stepperForm.previous();
    }
    this.alertService.error(this.translate(value));
    this.errorEventSubject.next(value);
  }

  /**
   * @description This method sends a success event to the form (child component) in order to display it
   *
   * @param value Response sent by the API
   */
  successEvent(value: NetworkResponseBodyInterface): void {
    this.router.navigate(['/user/sign/in']).then(response => {
      (response) ? this.alertService.success(this.translate(value.message)) : undefined;
    });
  }

  /**
   * @description The method allows you to retrieve the Subject which receives API data (error one)
   */
  get errorEventSubject(): CustomSubjectClass<string> {
    return this._errorEventSubject;
  }

  /**
   * @description The method allows you to assign the Subject which receives API data (success one)
   *
   * @param value Value of the new Subject which receives API data (success one)
   */
  set errorEventSubject(value: CustomSubjectClass<string>) {
    this._errorEventSubject = value;
  }

  /**
   * @description The method allows you to retrieve the active tab id
   */
  get activeId(): string {
    return this._activeId;
  }

  /**
   * @description The method allows you to assign the active tab id
   *
   * @param value Value of the new active tab id
   */
  set activeId(value: string) {
    this._activeId = value;
  }

  /**
   * @description The method allows you to retrieve the component's boolean array which inform you if each steps is complete
   */
  get completeStep(): Array<boolean> {
    return this._completeStep;
  }

  /**
   * @description The method allows you to assign the component's boolean array which inform you if each steps is complete
   *
   * @param value Value of the new component's boolean array which inform you if each steps is complete
   */
  set completeStep(value: Array<boolean>) {
    this._completeStep = value;
  }

  /**
   * @description The method allows you to retrieve the registration form
   */
  get model(): RegisterClass {
    return this._model;
  }

  /**
   * @description The method allows you to assign the registration form
   *
   * @param value Value of the new the registration form
   */
  set model(value: RegisterClass) {
    this._model = value;
  }
}
