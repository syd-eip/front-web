import {Component, OnInit} from '@angular/core';
import {TranslatableClass} from '../../../shared/services/translation/translation-class/translatable.class';
import {TranslationService} from 'src/app/shared/services/translation/translation.service';
import {Router} from '@angular/router';
import {PurchaseClass} from '../../../shared/classes/models/purchase.class';
import {PaymentCardClass} from '../../../shared/classes/models/payment-card.class';
import {NetworkService} from '../../../shared/services/web/network.service';
import {GET_OFFER, PATCH_PURCHASE_TOKEN} from '../../../shared/data/api/api-path.data';
import {AlertService} from '../../../shared/services/alert/alert.service';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {createChunkedArray} from '../../../shared/functions/get-chunked-array.function';
import moment from 'moment';
import {environment} from '../../../../environments/environment';
import {Semaphore} from '../../../shared/classes/semaphore/semaphore.class';
import {UserService} from '../../../shared/services/web/user.service';
import {ModalService} from "../../../shared/services/modal/modal.service";
import {ModalSettingsComponent} from "../../../shared/components/modal/modal-settings/modal-settings.component";
import {ModalConfirmationPaymentComponent} from "../../../shared/components/modal/modal-confirmation-payment/modal-confirmation-payment.component";

@Component({
  selector: 'app-payment-page',
  templateUrl: './payment.layout.html'
})
export class PaymentLayout extends TranslatableClass implements OnInit {

  /**
   * @description Offers proposed by the application
   */
  private _offers: any;

  /**
   * @description Requested payment methods proposed by the application
   */
  private _paymentMethods: any;

  /**
   * @description Offer selected by the user
   */
  private _offerSelected: any;

  /**
   * @description Offer selected by the user used for the confirmation page
   */
  private _offerSelectedConfirmation: any;

  /**
   * @description Payment method selected by the user
   */
  private _paymentSelected: any;

  /**
   * @description Payment card
   */
  private _paymentCard: PaymentCardClass;

  /**
   * @description Component that tracks the value and validity state of a group of FormControl instances.
   */
  private _formGroup: FormGroup;

  /**
   * @description Constructor of PaymentLayout component
   *
   * The constructor creates an instance of the AssociationLayout component and specifies the default values
   * the input and output variables of the component.
   *
   * @param translation A reference to the application's i18 translation service
   * @param alertService A reference to the application's alert service
   * @param router Implements the Angular Router service , which enables navigation from one view to the next as users
   * perform application tasks
   * @param networkService A reference to the http request service of the application
   * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
   * @param userService A reference to the user service of the application
   * @param modalService A reference to the modal service of the application
   */
  constructor(translation: TranslationService, private alertService: AlertService, private router: Router,
              private networkService: NetworkService, private formBuilder: FormBuilder, private userService: UserService,
              private modalService: ModalService) {
    super(translation);
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.paymentCard = new PaymentCardClass();

    this.paymentMethods = [
      {name: 'PAYPAL', logo: '/assets/img/payments/paypal.png'},
      {name: 'CREDIT_CARD', logo: '/assets/img/payments/credit-card.png'}
    ];

    this.initForm();
    this.loadOffers();
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to retrieve all platform offers
   *
   * It calls the method Get of the NetworkService service
   */
  loadOffers(): void {
    this.networkService.activeLoader(new Semaphore(1));

    this.networkService.get(GET_OFFER)
      .then(response => {
        response.responseData.data.forEach(item => {
          item.image = environment.apiHost + environment.apiVersion + '/static/images/offers/' + item.name + '.png';
        });

        this.offers = createChunkedArray(response.responseData.data, 2);
      })
      .catch(error => {
        this.alertService.error(this.translate('ERROR/OFFER_LOAD_ERROR'));
      });
  }

  /**
   * @description This method is called once the users clicked on purchase button to pay his asked tokens
   */
  buy(): void {
    this.networkService.activeLoader(new Semaphore(1));

    this.networkService.patch({offerId: this.offerSelected.id}, PATCH_PURCHASE_TOKEN)
      .then(() => {
        this.offerSelectedConfirmation = this.offerSelected;

        let wallet = this.userService.user.wallet;
        this.userService.update();

        this.modalService.open(ModalConfirmationPaymentComponent, {centered: true, windowClass: 'modal-custom', });
        this.modalService.modalRef.componentInstance.offerConfirmed = this.offerSelectedConfirmation;
        this.modalService.modalRef.componentInstance.previousWallet = wallet;

        let paymentMethod = 0;
        let spoofedCard = '**** **** **** **'
        switch (this.paymentSelected) {
          case 'PAYPAL':
            paymentMethod = 1;
            spoofedCard = null;
            break;
          case 'CREDIT_CARD':
            paymentMethod = 2;
            spoofedCard += this.paymentCard.cardNumber.substring(this.paymentCard.cardNumber.length - 2)
            break;
          default:
            break;
        }

        this.modalService.modalRef.componentInstance.paymentMethod = paymentMethod;
        this.modalService.modalRef.componentInstance.cardNumber = spoofedCard;

        this.offerSelected = null;
        this.paymentSelected = null;

      })
      .catch(() => {
        this.alertService.error(this.translate('ERROR/OFFER_ADD_ERROR'));
      });
  }

  /**
   * @description This method makes it possible to know the size of the screen.
   * If the size is less than 992px, the function will return the value true
   */
  isMobile(): boolean {
    return window.innerWidth < 992;
  }

  /**
   * @description This method allows you to format the date in the format'MM/YYY'.
   * It refuses any characters other than numbers and '/'.
   * It respects the pattern {2}/{4}.
   *
   * @param event Input value
   */
  replaceExpirationDate(event: any): void {
    event.target.value = event.target.value
      .replace(/^(\d\d)(\d)$/g, '$1/$2')
      .replace(/[^\d\/]/g, '');
  }

  /**
   * @description This method allows you to refuse any characters other than numbers
   *
   * @param event Input value
   * @param defaultValue Default value to assign when the value of the input is null or empty
   */
  replaceNumber(event: any, defaultValue?: number): void {
    event.target.value = event.target.value
      .replace(/[^\d]/g, '');

    if (defaultValue && (!event.target.value || event.target.value === '')) {
      event.target.value = defaultValue;
    }
  }

  /**
   * @description This method is used to initialize the login form.
   * It creates a form by specifying the rules to be respected to validate user inputs
   */
  private initForm(): void {
    this.formGroup = this.formBuilder.group(
      {
        fullname: [null, Validators.compose([
          Validators.required,
        ])],
        cardNumber: [null, Validators.compose([
          Validators.required,
        ])],
        cvv: [null, Validators.compose([
          Validators.required,
        ])],
        expiration: [null, Validators.compose([
          Validators.required,
        ])]
      },
      {
        validators: [
          PaymentLayout.expirationDateValidator
        ]
      }
    );
  }

  /**
   * @description This method is used to validate whether the format of a date is valid
   *
   * @param control This is the base class for FormControl, FormGroup, and FormArray
   */
  private static expirationDateValidator(control: AbstractControl) {
    const date = control.get('expiration').value;
    const check = moment(date, 'MM/YYYY');

    if (date === null || !moment(date, 'MM/YYYY').isValid() || !check.isSameOrAfter(moment(new Date()))) {
      control.get('expiration').setErrors({'dateError': true});
    }
  }

  /**
   * @description The method allows you to retrieve the component that tracks the value and validity state of a group of
   * FormControl instances
   */
  get formGroup(): FormGroup {
    return this._formGroup;
  }

  /**
   * @description The method allows you to assign the component that tracks the value and validity state of a group of
   * FormControl instances
   *
   * @param value Value of the new component that tracks the value and validity state of a group of FormControl instances
   */
  set formGroup(value: FormGroup) {
    this._formGroup = value;
  }

  /**
   * @description The method allows you to retrieve the offers proposed by the application
   */
  get offers(): any {
    return this._offers;
  }

  /**
   * @description The method allows you to assign the offers proposed by the application
   *
   * @param value Value of the new offers proposed by the application
   */
  set offers(value: any) {
    this._offers = value;
  }

  /**
   * @description The method allows you to retrieve the requested payment methods proposed by the application
   */
  get paymentMethods(): any {
    return this._paymentMethods;
  }

  /**
   * @description The method allows you to assign the requested payment methods proposed by the application
   *
   * @param value Value of the new requested payment methods proposed by the application
   */
  set paymentMethods(value: any) {
    this._paymentMethods = value;
  }

  /**
   * @description The method allows you to retrieve the payment method selected by the user
   */
  get paymentSelected(): any {
    return this._paymentSelected;
  }

  /**
   * @description The method allows you to assign the payment method selected by the user
   *
   * @param value Value of the new payment method selected by the user
   */
  set paymentSelected(value: any) {
    this._paymentSelected = value;
  }

  /**
   * @description The method allows you to retrieve the payment card
   */
  get paymentCard(): PaymentCardClass {
    return this._paymentCard;
  }

  /**
   * @description The method allows you to assign the payment card
   *
   * @param value Value of the new payment card
   */
  set paymentCard(value: PaymentCardClass) {
    this._paymentCard = value;
  }

  /**
   * @description The method allows you to retrieve the offer selected by the user
   */
  get offerSelected(): any {
    return this._offerSelected;
  }

  /**
   * @description The method allows you to assign the offer selected by the user
   *
   * @param value Value of the offer selected by the user
   */
  set offerSelected(value: any) {
    this._offerSelected = value;
  }

  /**
   * @description The method allows you to retrieve the offer selected by the user
   */
  get offerSelectedConfirmation(): any {
    return this._offerSelectedConfirmation;
  }

  /**
   * @description The method allows you to assign the offer selected by the user
   *
   * @param value Value of the offer selected by the user
   */
  set offerSelectedConfirmation(value: any) {
    this._offerSelectedConfirmation = value;
  }
}
