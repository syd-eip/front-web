/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
/**
 * Import of dependencies modules
 */
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
/**
 * Import of layouts
 */
import {PaymentLayout} from './payment.layout';

/**
 * Payment layout module of the Application. This module retrieve payment layout
 * You can also find Angular's modules
 */
@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
  ],
  declarations: [
    PaymentLayout
  ],
  exports: [
    PaymentLayout
  ],
  providers: []
})
export class PaymentModule {
}
