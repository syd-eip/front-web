/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

/**
 * Route of the application
 */
const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./layouts/layouts.module')
      .then(m => m.LayoutsModule),
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
      scrollOffset: [0, 85],
      onSameUrlNavigation: 'reload'
    })
  ],
  exports: [
    RouterModule,
  ],
})
export class AppRoutingModule {
}
