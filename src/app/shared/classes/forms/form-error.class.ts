/**
 * @description This class describes an error received by the API
 */
export class FormErrorClass {
  name: string;
  error: string;
}
