import {FormControl} from '@angular/forms';

/**
 * @description This class describes a form model for an input component
 */
export class FormInputClass {
  inputName: string;
  type: string;
  placeholder?: string;
  control?: FormControl;
}
