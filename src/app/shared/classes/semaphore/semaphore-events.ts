import {SemaphoreEvent} from './semaphore-event';

/**
 * @description This class represents a container for the events managed by semaphore
 */
export class SemaphoreEvents {
  public change = new SemaphoreEvent<number>();
  public release = new SemaphoreEvent<boolean>();
}
