/**
 * @description This class defines the model of the privacy policies menu iteml
 */
export class PrivacyPoliciesMenuItemClass {
  public id: number;
  public title: string;
  public subtitles: Array<{title: string, id: string}>;
}
