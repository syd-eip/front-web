import moment from 'moment';
import 'moment/locale/en-gb';
import 'moment/locale/fr';

export class UtilsClass {

  private static getLocale(lang: string): string {
    switch (lang) {
      case 'fr':
        return 'fr';
      case 'en':
      default:
        return 'en-gb';
    }
  }

  /**
   * @description This method transform a date in ISO string date
   *
   * @param value Value of the date
   * @param format Format of the date
   */
  static dateToISOString(value: string, format?: string): string {
    return (format) ? moment(value, format).toISOString() :
      moment(value, 'DD/MM/YYYY').toISOString();
  }

  /**
   * @description This method transform an ISO string date to a formatted date
   *
   * @param value Value of the date
   * @param format Format of the date
   * @param lang The target locale the date must be printed
   */
  static ISOStringToDate(value: string, format?: string, lang?: string): string {
    if (moment(value, moment.ISO_8601, true).isValid()) {
      const baseLocale = moment.locale();
      if (format) {
        const ret = moment(value, moment.ISO_8601).locale(UtilsClass.getLocale(lang)).format(format);
        moment.locale(baseLocale);
        return ret;
      } else {
        const ret = moment(value, moment.ISO_8601).locale(UtilsClass.getLocale(lang)).format('DD/MM/YYYY');
        moment.locale(baseLocale);
        return ret;
      }
    }
    return value;
  }

  /**
   * @description This method transform a date into an excepted format
   *
   * @param value Value of the date
   * @param format Format of the date
   * @param formatExcepted Format excepted after transformation
   * @param lang The target locale the date must be printed
   */
  static dateToExceptedFormat(value: string, format: string, formatExcepted: string, lang?: string): string {
    const baseLocale = moment.locale();
    if (format) {
      const ret = moment(value, moment.ISO_8601).locale(UtilsClass.getLocale(lang)).format(format);
      moment.locale(baseLocale);
      return ret;
    } else {
      const ret = moment(value, moment.ISO_8601).locale(UtilsClass.getLocale(lang)).format('DD/MM/YYYY');
      moment.locale(baseLocale);
      return ret;
    }
  }

  /**
   * @description This method checks if the value passed as parameter is a number
   *
   * @param value Value to check
   */
  static isNumber(value: any): value is number {
    return !isNaN(UtilsClass.toInteger(value));
  }

  /**
   * @description This method transforms the value passed as parameter into an integer
   *
   * @param value Value to transform in Integer
   */
  static toInteger(value: any): number {
    return parseInt(`${value}`, 10);
  }

  /**
   * @description This method transforms the value passed as parameter by padding this value
   *
   * @param value Value to pad (number)
   */
  static padNumber(value: number) {
    if (UtilsClass.isNumber(value)) {
      return `0${value}`.slice(-2);
    } else {
      return '';
    }
  }

  /**
   * @description This method transforms the value passed as parameter into a string
   *
   * @param value Value to transform in string
   */
  static toString(value: any): string {
    return (value !== undefined && value !== null) ? `${value}` : '';
  }

  /**
   * @description This method retrieves the range of the number passed as parameter
   *
   * @param value Number to provide and check
   * @param max Value max
   * @param min Value min
   */
  static getValueInRange(value: number, max: number, min = 0): number {
    return Math.max(Math.min(value, max), min);
  }

  /**
   * @description This method checks if the value passed as parameter is a string
   *
   * @param value Value to check
   */
  static isString(value: any): value is string {
    return typeof value === 'string';
  }

  /**
   * @description This method checks if the value passed as parameter is an integer
   *
   * @param value Value to check
   */
  static isInteger(value: any): value is number {
    return typeof value === 'number' && isFinite(value) && Math.floor(value) === value;
  }

  /**
   * @description This method checks if the value passed as parameter is defined
   *
   * @param value Value to check
   */
  static isDefined(value: any): boolean {
    return value !== undefined && value !== null;
  }

  /**
   * @description This method checks if the object passed as parameter contains the property passed has parameter
   *
   * @param obj Object to check
   */
  static isEmptyProperties(obj: any): boolean {
    return Object.keys(obj).every(function (x) {
      return obj[x] === '' || obj[x] === null;
    });
  }
}
