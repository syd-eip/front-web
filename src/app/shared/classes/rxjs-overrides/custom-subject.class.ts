import {Subject} from 'rxjs';

/**
 * @description This class is an extension to Subject class from RxJS.
 * This extension keeps the last value passed through next() call.
 */
export class CustomSubjectClass<T> extends Subject<T> {

  /**
   * @description Last value kept after the next() call
   */
  private _last: T = null;

  /**
   * @description Constructor of CustomSubjectClass class
   */
  constructor() {
    super();
  }

  /**
   * @description This method allows you to feed a new value to the Subject
   *
   * @param value Value to pass to the Subject
   */
  next(value?: T): void {
    this._last = value;
    super.next(value);
  }

  /**
   * @description This method allows you to retrieve the fed last value
   */
  get last(): T {
    return this._last;
  }
}
