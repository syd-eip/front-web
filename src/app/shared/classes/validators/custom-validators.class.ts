import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';
import moment from 'moment';
import {countriesWithCode} from '../../data/countries/countries-with-code.data';

export class CustomValidatorsClass {

  /**
   * @description This method is used to check if the field pattern is correct
   *
   * @param regex Regex used to check the pattern
   * @param error Errors if the validation is not correct
   */
  static patternValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null;
      }
      const valid = regex.test(control.value);
      return valid ? null : error;
    };
  }

  /**
   * @description This method validates whether the password and the password confirmation match.
   *
   * @param control This is the base class for FormControl, FormGroup, and FormArray
   */
  static passwordMatchValidator(control: AbstractControl) {
    const password: string = control.get('password').value;
    const confirmPassword: string = control.get('confirmPassword').value;

    if (confirmPassword === '' || confirmPassword === null) {
      control.get('confirmPassword').setErrors({required: true});
    } else if (password !== confirmPassword) {
      control.get('confirmPassword').setErrors({NoPassswordMatch: true});
    }
  }

  /**
   * @description This method is used to validate whether the format of a date is valid
   *
   * @param control This is the base class for FormControl, FormGroup, and FormArray
   */
  static dateMatchValidator(control: AbstractControl) {
    const birthday = control.get('birthday').value;

    if (birthday !== null && moment(birthday, 'DD/MM/YYYY').isValid() === true) {
      return null;
    }
    return {'dateMatch': true};
  }

  /**
   * @description This method validates if the anniversary date is not greater than the current date and less than the current
   * date minus 100 years
   *
   * @param control This is the base class for FormControl, FormGroup, and FormArray
   */
  static dateRangeValidator(control: AbstractControl) {
    const endDate = new Date();
    const startDate = new Date().setFullYear(endDate.getFullYear() - 100);
    const birthday = moment(control.get('birthday').value, 'DD/MM/YYYY');

    if (birthday !== null && birthday.isBetween(moment(startDate), moment(endDate)) === true) {
      return null;
    }
    return {'dateRange': true};
  }

  /**
   * @description This method is used to validate whether the user's birthday is correct (the user has to be minimum 13)
   *
   * @param control This is the base class for FormControl, FormGroup, and FormArray
   */
  static dateLegalValidator(control: AbstractControl) {
    const startDate = new Date().setFullYear(new Date().getFullYear() - 13);
    const birthday = moment(control.get('birthday').value, 'DD/MM/YYYY');

    if (birthday !== null && birthday.isBefore(moment(startDate)) === true) {
      return null;
    }
    return {'dateLegal': true};
  }

  /**
   * @description This method allows you to check if a checkbox has been selected (account type checkbox)
   *
   * @param control This is the base class for FormControl, FormGroup, and FormArray
   */
  static checkboxSingleChoiceValidator(control: AbstractControl) {
    if (control.get('influencer').value || control.get('notinfluencer').value) {
      return null;
    } else {
      return {'checkboxSingle': true};
    }
  }

  /**
   * @description This method allows you to check if a checkbox has been selected (CGU checkbox)
   *
   * @param control This is the base class for FormControl, FormGroup, and FormArray
   */
  static checkboxRequiredChoiceValidator(control: AbstractControl) {
    if (control.get('cgu').value) {
      return null;
    } else {
      return {'checkboxRequired': true};
    }
  }

  /**
   * @description This method verifies whether the selected country is correct
   *
   * @param control This is the base class for FormControl, FormGroup, and FormArray
   */
  static locationValidator(control: AbstractControl) {
    const location = control.get('location').value;

    if (location) {
      const country: string = location.country;

      for (const item of countriesWithCode) {
        if (country === item.country) {
          return null;
        }
      }
    }
    return {'location': true};
  }
}
