/**
 * @description This class defines the model to provide to the API to create an image
 */
export class ImageClass {
  name?: string;
  content?: string
}
