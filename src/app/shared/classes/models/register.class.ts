/**
 * @description This class defines the information to provide to the API to register a new user
 */
export class RegisterClass {
  username: string;
  firstName: string;
  lastName: string;
  birthday: string;
  email: string;
  phone: {
    nationalNumber: string;
    countryCode: string;
  };
  location: {
    countryCode: string,
    country: string;
  };
  password: string;
  confirmPassword: string;
  cguValidated: string;
  wantToBeInfluencer: boolean;

  constructor() {
    this.phone = {nationalNumber: '', countryCode: ''};
    this.location = {countryCode: '', country: ''};
  }
}
