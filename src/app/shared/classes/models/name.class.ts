/**
 * @description This class defines the model to provide to the API to patch a name
 */
export class NameClass {
  firstName: string;
  lastName: string;

  constructor(firstName: string, lastName: string) {
    this.firstName = firstName;
    this.lastName = lastName;
  }
}
