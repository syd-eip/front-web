/**
 * @description This class defines the model to provide to the API to create an image
 */
import {SafeResourceUrl} from '@angular/platform-browser';

export class ImageConvertedClass {
  name?: string;
  content?: SafeResourceUrl;
  link?: string;
}
