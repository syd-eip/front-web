import {ImageClass} from './image.class';
import {UtilsClass} from '../utils/utils.class';

/**
 * @description This class defines the model to provide to the API to create or retrieve an event
 */
export class EventClass {
  id?: string;
  userId?: string;
  associationId?: string;
  information: {
    name: string;
    association?: string;
    drawDate: string;
    eventDate: string;
    tags: Array<string>;
    description: string;
    lotteriesSize?: number;
  };
  lotteriesSize: number;
  lotteries: Array<any>;
  images: Array<ImageClass>;
  creationDate: string;
  updateDate: string;
  finished: boolean;

  constructor() {
    this.lotteries = [];
    this.images = new Array<ImageClass>();
    this.information = {name: null, drawDate: null, eventDate: null, tags: [], description: null, lotteriesSize: 1};
    this.finished = true;
  }

  formatDate(): void {
    this.information.drawDate = UtilsClass.dateToISOString(this.information.drawDate, 'DD/MM/YYYY');
    this.information.eventDate = UtilsClass.dateToISOString(this.information.eventDate, 'DD/MM/YYYY');
  }
}

export interface EventInformationModel {
  name: string;
  drawDate: string;
  eventDate: string;
  tags: any[];
  description: string;
}

export interface EventLotteryModel {
  totalValue: number;
  associationValue: number;
  influencerValue: number;
  winnerId: string;
  drawError: string;
  participations: any[];
}

export interface EventModel {
  id: string;
  userId: string;
  associationId: string;
  information: EventInformationModel;
  lotteriesSize: number;
  lotteries: EventLotteryModel[];
  images: ImageClass[];
  creationDate: Date;
  updateDate: Date;
  finished: boolean;
}
