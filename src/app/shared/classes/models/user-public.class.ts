/**
 * @description This class defines the user model (global) of the application
 */
import {ImageClass} from './image.class';

export class UserPublicClass {
  id: string;
  username: string;
  country: string;
  images: Array<ImageClass>;
  isInfluencer: boolean;
  favorites: any;
  creationDate: string

  constructor() {

  }
}
