export interface Location {
  country: string;
  countryCode: string;
  city: string;
  zipCode: string;
  streetAddress: string;
}

export interface Phone {
  countryCode: string;
  nationalNumber: string;
  internationalNumber: string;
  number: string;
}

export interface AssociationStatistic {
  gains: number;
  events: number;
  influencers: any[];
  users: any[];
}

export interface AssociationModel {
  id: string;
  name: string;
  description: string;
  link: string;
  email: string;
  location: Location;
  phone: Phone;
  images?: any;
  associationStatistic: AssociationStatistic;
  creationDate: Date;
  updateDate: string;
}
