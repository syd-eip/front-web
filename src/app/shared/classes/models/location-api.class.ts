/**
 * @description This class defines the model to provide to the API to create or retrieve a bet
 */
export class LocationApi {
  countryCode: string;
  city: string;
  streetAddress: string;
  zipCode: string

  constructor(countryCode: string) {
    this.countryCode = countryCode;
    this.city = 'Bordeaux';
    this.streetAddress = '11 Rue du test';
    this.zipCode = '33000';
  }
}
