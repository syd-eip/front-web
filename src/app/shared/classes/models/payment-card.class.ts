/**
 * @description This class defines the model that retrieve user card from the form on the interface
 * when selecting payment method "Credit Card" on local.shareyourdreams.fr/purchase
 */
export class PaymentCardClass {
  name: string;
  cardNumber: string;
  cvv: number;
  expiration: string;
}
