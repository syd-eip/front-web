import {ImageClass} from './image.class';

/**
 * @description This class defines the user model (global) of the application
 */
export class UserClass {
  birthday: string;
  cguValidated: boolean;
  currentEvents: Array<any>;
  email: string;
  favorites: Array<any>;
  firstName: string;
  id: string;
  images: ImageClass[];
  lastName: string;
  phone: {
    nationalNumber: string;
    countryCode: string;
    number: string;
    internationalNumber: string;
  };
  location: {
    countryCode: string;
    country: string;
    city: string;
    zipCode: string;
    streetAddress: string;
  };
  oldEvents: Array<any>;
  role: Array<string>;
  username: string;
  wallet: number;
  wantToBeInfluencer: boolean;

  constructor() {
    this.phone = {nationalNumber: '', countryCode: '', number: '', internationalNumber: ''};
    this.location = {countryCode: '', country: '', city: '', streetAddress: '', zipCode: ''};
    this.role = [];
    this.oldEvents = [];
    this.favorites = [];
    this.currentEvents = [];
  }
}
