/**
 * @description This class defines the model to provide to the API to create or retrieve a bet
 */
export class BetClass {
  value: any;
  splitRatio: number;

  constructor() {
    this.value = 0;
  }
}
