/**
 * @description This class defines the model to provide to the API to sign in a user to the application
 */
export class LoginClass {
  login: string;
  password: string;
}
