/**
 * @description This class defines the model to provide to the API to create or retrieve a bet
 */
export class ChangePassword {
  password: string;
  confirmPassword: string;

  constructor() {
    this.password = '';
    this.confirmPassword = '';
  }
}
