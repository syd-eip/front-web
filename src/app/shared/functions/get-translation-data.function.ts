import {data as TranslationsFr} from '../data/translations/translations-fr.data';
import {data as TranslationsEn} from '../data/translations/translations-en.data';

/**
 * @description This method allows you to retrieve the translations of the application for each translated component or layout
 *
 * @param name name of the route for retrieving translations '/translations/fr' | '/translations/en'
 */
export function getTranslationData(name: string): any {
  const sampleDatas: any = [
    {
      route: '/translations/fr',
      data: TranslationsFr,
    },
    {
      route: '/translations/en',
      data: TranslationsEn,
    }
  ];

  for (const item of sampleDatas) {
    if (item.route === name) {
      return item.data;
    }
  }
  return null;
}
