import {Pipe, PipeTransform} from '@angular/core';
import {UtilsClass} from '../classes/utils/utils.class';
import { TranslationService } from '../services/translation/translation.service';

@Pipe({
  name: 'datePipe'
})
export class DatePipe implements PipeTransform {

  /**
   * @description Constructor of DatePipe pipe
   *
   * The constructor creates an instance of the DatePipe pipe and specifies the default values
   * the input and output variables of the component.
   */
  constructor(private _translationService: TranslationService) {
  }

  /**
   * @description The "transform" method of the PipeTransform interface allows a transformation to be performed.
   *
   * It is used to transform a date into a specific format.
   *
   * @param value Value of the date to be transformed
   * @param format Current date format (before transformation)
   * @param formatExcepted Format in which to transform the date
   */
  transform(value: string, format: string, formatExcepted: string, lang?: string): string {
    if (!lang) {
      lang = this._translationService.lang
    }
    if (value && format && formatExcepted) {
      if (format === 'ISO') {
        return UtilsClass.ISOStringToDate(value, formatExcepted, lang);
      } else {
        return UtilsClass.dateToExceptedFormat(value, format, formatExcepted, lang);
      }
    }
    return value;
  }
}
