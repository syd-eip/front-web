import {Pipe, PipeTransform} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Pipe({
  name: 'safeResouceUrl'
})
export class SafeResourceUrlPipe implements PipeTransform {

  /**
   * @description Constructor of SafeResourceUrlPipe pipe
   *
   * The constructor creates an instance of the SafeResourceUrlPipe pipe
   *
   * @param sanitizer A reference to the Angular service that helps prevent XSS security breaches
   * by disinfecting the values so that they can be used safely in the different contexts of the DOM.
   **/
  constructor(private sanitizer: DomSanitizer) {
  }

  /**
   * @description The "transform" method of the PipeTransform interface allows a transformation to be performed.
   *
   * It is used to bypass the security check regarding the authenticity of an element's source
   * (e. g. an image source returned by the API).
   *
   * @param value Value to transform
   * @param args Optional list of arguments
   */
  transform(value: any, args?: any): any {
    return this.sanitizer.bypassSecurityTrustResourceUrl(value);
  }
}
