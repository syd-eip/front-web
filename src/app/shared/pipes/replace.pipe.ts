import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'replace'
})
export class ReplacePipe implements PipeTransform {

    /**
     * @description Constructor of ReplacePipe pipe
     *
     * The constructor creates an instance of the ReplacePipe pipe and specifies the default values
     * input and output variables of the pipe.
     */
    constructor() {
    }

    /**
     * @description The 'transform' method of the PipeTransform interface allows to perform a transformation
     *
     * It is used to search a string for a specified value, or a regular expression, and returns a new string
     * where the specified values are replaced
     *
     * @param value Reference text
     * @param searchValue Searched string
     * @param replaceValue New string used to replace
     */
    transform(value: any, searchValue: string, replaceValue: string): string {
        return (value) ? value.replace(searchValue, replaceValue) : value;
    }
}
