/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of application's pipes
 */
import {SafeResourceUrlPipe} from './safe-resource-url.pipe';
import {HighlightPipe} from './highlight.pipe';
import {DatePipe} from './date.pipe';
import {PhonePipe} from './phone.pipe';
import {ReplacePipe} from './replace.pipe';

@NgModule({
  imports: [
    CommonModule,
  ],
  exports: [
    CommonModule,
    SafeResourceUrlPipe,
    HighlightPipe,
    DatePipe,
    PhonePipe,
    ReplacePipe
  ],
  declarations: [
    SafeResourceUrlPipe,
    HighlightPipe,
    DatePipe,
    PhonePipe,
    ReplacePipe
  ]
})
export class PipesModule {
}
