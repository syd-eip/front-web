import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'highlight'
})
export class HighlightPipe implements PipeTransform {

  /**
   * @description Constructor of HighlightPipe
   *
   * The constructor creates an instance of the HighlightPipe component
   **/
  constructor() {
  }

  /**
   * @description The "transform" method of the PipeTransform interface allows a transformation to be performed.
   *
   * It is used to find an occurrence between a text (given as the first argument) and a search element.
   * If an occurrence is found, the text is automatically highlighted.
   *
   * @param text Regression text in which the occurrence search for the highlight is performed
   * @param search Search term with which the occurrence search for the highlight is performed
   */
  transform(text: string, search): string {
    let pattern = search.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');

    pattern = pattern.split(' ').filter((t) => {
      return t.length > 0;
    }).join('|');

    const regex = new RegExp(pattern, 'gi');
    return search ? text.replace(regex, (match) => `<span class="highlight">${match}</span>`) : text;
  }
}
