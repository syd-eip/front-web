import {Pipe, PipeTransform} from '@angular/core';
import {AsYouType, CountryCode, parsePhoneNumber} from 'libphonenumber-js';

@Pipe({
    name: 'phone'
})
export class PhonePipe implements PipeTransform {

    constructor() {
    }

    transform(phoneValue: number | string, country: string): any {
        try {
            const phoneNumber = parsePhoneNumber(phoneValue + '', country as CountryCode);
            return new AsYouType().input(phoneNumber.formatInternational());

        } catch (error) {
            return phoneValue;
        }
    }
}
