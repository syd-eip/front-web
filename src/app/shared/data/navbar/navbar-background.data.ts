/**
 * @description This constant defines the background of the navigation bar. The system is not quite finished yet. You just have to fill
 * in the url of the page for which you want a white background or a transparent background.
 */
export const navbarBackground: Array<{ path: Array<string>, background: string }> = [
  {
    path: [],
    background: 'navbar-transparent'
  },
  {
    path: ['/', '/user/reset-password', '/user/sign/in', '/user/sign/up', '/privacy-policies', '/user/profile'],
    background: 'bg-white'
  }
];
