import {PrivacyPoliciesMenuItemClass} from '../../classes/privacy-policies-menu/privacy-policies-menu-item.class';

/**
 * @description This constant contains all the title and parts of the application privacy policies (for the menu)
 */
export const privacyPoliciesItems: Array<PrivacyPoliciesMenuItemClass> = [
  {
    id: 0,
    title: 'Terms of use',
    subtitles: [
      {
        id: 'terms-of-use-1',
        title: 'Terms of use - Title 1'
      },
      {
        id: 'terms-of-use-2',
        title: 'Terms of use - Title 2'
      },
      {
        id: 'terms-of-use-3',
        title: 'Terms of use - Title 3'
      },
      {
        id: 'terms-of-use-4',
        title: 'Terms of use - Title 4'
      },
      {
        id: 'terms-of-use-5',
        title: 'Terms of use - Title 5'
      },
      {
        id: 'terms-of-use-6',
        title: 'Terms of use - Title 6'
      },
      {
        id: 'terms-of-use-7',
        title: 'Terms of use - Title 7'
      },
      {
        id: 'terms-of-use-8',
        title: 'Terms of use - Title 8'
      },
      {
        id: 'terms-of-use-9',
        title: 'Terms of use - Title 9'
      },
      {
        id: 'terms-of-use-10',
        title: 'Terms of use - Title 10'
      },
    ]
  },
  {
    id: 1,
    title: 'Terms of sale',
    subtitles: [
      {
        id: 'terms-of-sale-1',
        title: 'Terms of sale - Title 1'
      },
      {
        id: 'terms-of-sale-2',
        title: 'Terms of sale - Title 2'
      },
      {
        id: 'terms-of-sale-3',
        title: 'Terms of sale - Title 3'
      },
      {
        id: 'terms-of-sale-4',
        title: 'Terms of sale - Title 4'
      },
      {
        id: 'terms-of-sale-5',
        title: 'Terms of sale - Title 5'
      },
      {
        id: 'terms-of-sale-6',
        title: 'Terms of sale - Title 6'
      },
      {
        id: 'terms-of-sale-7',
        title: 'Terms of sale - Title 7'
      },
      {
        id: 'terms-of-sale-8',
        title: 'Terms of sale - Title 8'
      },
      {
        id: 'terms-of-sale-9',
        title: 'Terms of sale - Title 9'
      },
      {
        id: 'terms-of-sale-10',
        title: 'Terms of sale - Title 10'
      },
    ]
  },
  {
    id: 2,
    title: 'Legal Mentions',
    subtitles: [
      {
        id: 'legal-mentions-1',
        title: 'Legal Mentions - Title 1'
      },
      {
        id: 'legal-mentions-2',
        title: 'Legal Mentions - Title 2'
      },
      {
        id: 'legal-mentions-3',
        title: 'Legal Mentions - Title 3'
      },
      {
        id: 'legal-mentions-4',
        title: 'Legal Mentions - Title 4'
      },
      {
        id: 'legal-mentions-5',
        title: 'Legal Mentions - Title 5'
      },
      {
        id: 'legal-mentions-6',
        title: 'Legal Mentions - Title 6'
      },
      {
        id: 'legal-mentions-7',
        title: 'Legal Mentions - Title 7'
      },
      {
        id: 'legal-mentions-8',
        title: 'Legal Mentions - Title 8'
      },
      {
        id: 'legal-mentions-9',
        title: 'Legal Mentions - Title 9'
      },
      {
        id: 'legal-mentions-10',
        title: 'Legal Mentions - Title 10'
      }
    ]
  }
];
