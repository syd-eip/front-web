/**
 * @description This constant contains all countries dial code to display in the ng-select component
 */
export const countriesDialCode: Array<any> = [
  {
    name: 'Afghanistan',
    dial_code: '+93',
    countryCode: 'AF'
  },
  {
    name: 'Aland Islands',
    dial_code: '+358',
    countryCode: 'AX'
  },
  {
    name: 'Albania',
    dial_code: '+355',
    countryCode: 'AL'
  },
  {
    name: 'Algeria',
    dial_code: '+213',
    countryCode: 'DZ'
  },
  {
    name: 'AmericanSamoa',
    dial_code: '+1 684',
    countryCode: 'AS'
  },
  {
    name: 'Andorra',
    dial_code: '+376',
    countryCode: 'AD'
  },
  {
    name: 'Angola',
    dial_code: '+244',
    countryCode: 'AO'
  },
  {
    name: 'Anguilla',
    dial_code: '+1 264',
    countryCode: 'AI'
  },
  {
    name: 'Antarctica',
    dial_code: '+672',
    countryCode: 'AQ'
  },
  {
    name: 'Antigua and Barbuda',
    dial_code: '+1268',
    countryCode: 'AG'
  },
  {
    name: 'Argentina',
    dial_code: '+54',
    countryCode: 'AR'
  },
  {
    name: 'Armenia',
    dial_code: '+374',
    countryCode: 'AM'
  },
  {
    name: 'Aruba',
    dial_code: '+297',
    countryCode: 'AW'
  },
  {
    name: 'Australia',
    dial_code: '+61',
    countryCode: 'AU'
  },
  {
    name: 'Austria',
    dial_code: '+43',
    countryCode: 'AT'
  },
  {
    name: 'Azerbaijan',
    dial_code: '+994',
    countryCode: 'AZ'
  },
  {
    name: 'Bahamas',
    dial_code: '+1 242',
    countryCode: 'BS'
  },
  {
    name: 'Bahrain',
    dial_code: '+973',
    countryCode: 'BH'
  },
  {
    name: 'Bangladesh',
    dial_code: '+880',
    countryCode: 'BD'
  },
  {
    name: 'Barbados',
    dial_code: '+1 246',
    countryCode: 'BB'
  },
  {
    name: 'Belarus',
    dial_code: '+375',
    countryCode: 'BY'
  },
  {
    name: 'Belgium',
    dial_code: '+32',
    countryCode: 'BE'
  },
  {
    name: 'Belize',
    dial_code: '+501',
    countryCode: 'BZ'
  },
  {
    name: 'Benin',
    dial_code: '+229',
    countryCode: 'BJ'
  },
  {
    name: 'Bermuda',
    dial_code: '+1 441',
    countryCode: 'BM'
  },
  {
    name: 'Bhutan',
    dial_code: '+975',
    countryCode: 'BT'
  },
  {
    name: 'Bolivia, Plurinational State of',
    dial_code: '+591',
    countryCode: 'BO'
  },
  {
    name: 'Bosnia and Herzegovina',
    dial_code: '+387',
    countryCode: 'BA'
  },
  {
    name: 'Botswana',
    dial_code: '+267',
    countryCode: 'BW'
  },
  {
    name: 'Brazil',
    dial_code: '+55',
    countryCode: 'BR'
  },
  {
    name: 'British Indian Ocean Territory',
    dial_code: '+246',
    countryCode: 'IO'
  },
  {
    name: 'Brunei Darussalam',
    dial_code: '+673',
    countryCode: 'BN'
  },
  {
    name: 'Bulgaria',
    dial_code: '+359',
    countryCode: 'BG'
  },
  {
    name: 'Burkina Faso',
    dial_code: '+226',
    countryCode: 'BF'
  },
  {
    name: 'Burundi',
    dial_code: '+257',
    countryCode: 'BI'
  },
  {
    name: 'Cambodia',
    dial_code: '+855',
    countryCode: 'KH'
  },
  {
    name: 'Cameroon',
    dial_code: '+237',
    countryCode: 'CM'
  },
  {
    name: 'Canada',
    dial_code: '+1',
    countryCode: 'CA'
  },
  {
    name: 'Cape Verde',
    dial_code: '+238',
    countryCode: 'CV'
  },
  {
    name: 'Cayman Islands',
    dial_code: '+ 345',
    countryCode: 'KY'
  },
  {
    name: 'Central African Republic',
    dial_code: '+236',
    countryCode: 'CF'
  },
  {
    name: 'Chad',
    dial_code: '+235',
    countryCode: 'TD'
  },
  {
    name: 'Chile',
    dial_code: '+56',
    countryCode: 'CL'
  },
  {
    name: 'China',
    dial_code: '+86',
    countryCode: 'CN'
  },
  {
    name: 'Christmas Island',
    dial_code: '+61',
    countryCode: 'CX'
  },
  {
    name: 'Cocos (Keeling) Islands',
    dial_code: '+61',
    countryCode: 'CC'
  },
  {
    name: 'Colombia',
    dial_code: '+57',
    countryCode: 'CO'
  },
  {
    name: 'Comoros',
    dial_code: '+269',
    countryCode: 'KM'
  },
  {
    name: 'Congo',
    dial_code: '+242',
    countryCode: 'CG'
  },
  {
    name: 'Congo, The Democratic Republic of the Congo',
    dial_code: '+243',
    countryCode: 'CD'
  },
  {
    name: 'Cook Islands',
    dial_code: '+682',
    countryCode: 'CK'
  },
  {
    name: 'Costa Rica',
    dial_code: '+506',
    countryCode: 'CR'
  },
  {
    name: 'Cote d\'Ivoire',
    dial_code: '+225',
    countryCode: 'CI'
  },
  {
    name: 'Croatia',
    dial_code: '+385',
    countryCode: 'HR'
  },
  {
    name: 'Cuba',
    dial_code: '+53',
    countryCode: 'CU'
  },
  {
    name: 'Cyprus',
    dial_code: '+357',
    countryCode: 'CY'
  },
  {
    name: 'Czech Republic',
    dial_code: '+420',
    countryCode: 'CZ'
  },
  {
    name: 'Denmark',
    dial_code: '+45',
    countryCode: 'DK'
  },
  {
    name: 'Djibouti',
    dial_code: '+253',
    countryCode: 'DJ'
  },
  {
    name: 'Dominica',
    dial_code: '+1 767',
    countryCode: 'DM'
  },
  {
    name: 'Dominican Republic',
    dial_code: '+1 849',
    countryCode: 'DO'
  },
  {
    name: 'Ecuador',
    dial_code: '+593',
    countryCode: 'EC'
  },
  {
    name: 'Egypt',
    dial_code: '+20',
    countryCode: 'EG'
  },
  {
    name: 'El Salvador',
    dial_code: '+503',
    countryCode: 'SV'
  },
  {
    name: 'Equatorial Guinea',
    dial_code: '+240',
    countryCode: 'GQ'
  },
  {
    name: 'Eritrea',
    dial_code: '+291',
    countryCode: 'ER'
  },
  {
    name: 'Estonia',
    dial_code: '+372',
    countryCode: 'EE'
  },
  {
    name: 'Ethiopia',
    dial_code: '+251',
    countryCode: 'ET'
  },
  {
    name: 'Falkland Islands (Malvinas)',
    dial_code: '+500',
    countryCode: 'FK'
  },
  {
    name: 'Faroe Islands',
    dial_code: '+298',
    countryCode: 'FO'
  },
  {
    name: 'Fiji',
    dial_code: '+679',
    countryCode: 'FJ'
  },
  {
    name: 'Finland',
    dial_code: '+358',
    countryCode: 'FI'
  },
  {
    name: 'France',
    dial_code: '+33',
    countryCode: 'FR'
  },
  {
    name: 'French Guiana',
    dial_code: '+594',
    countryCode: 'GF'
  },
  {
    name: 'French Polynesia',
    dial_code: '+689',
    countryCode: 'PF'
  },
  {
    name: 'Gabon',
    dial_code: '+241',
    countryCode: 'GA'
  },
  {
    name: 'Gambia',
    dial_code: '+220',
    countryCode: 'GM'
  },
  {
    name: 'Georgia',
    dial_code: '+995',
    countryCode: 'GE'
  },
  {
    name: 'Germany',
    dial_code: '+49',
    countryCode: 'DE'
  },
  {
    name: 'Ghana',
    dial_code: '+233',
    countryCode: 'GH'
  },
  {
    name: 'Gibraltar',
    dial_code: '+350',
    countryCode: 'GI'
  },
  {
    name: 'Greece',
    dial_code: '+30',
    countryCode: 'GR'
  },
  {
    name: 'Greenland',
    dial_code: '+299',
    countryCode: 'GL'
  },
  {
    name: 'Grenada',
    dial_code: '+1 473',
    countryCode: 'GD'
  },
  {
    name: 'Guadeloupe',
    dial_code: '+590',
    countryCode: 'GP'
  },
  {
    name: 'Guam',
    dial_code: '+1 671',
    countryCode: 'GU'
  },
  {
    name: 'Guatemala',
    dial_code: '+502',
    countryCode: 'GT'
  },
  {
    name: 'Guernsey',
    dial_code: '+44',
    countryCode: 'GG'
  },
  {
    name: 'Guinea',
    dial_code: '+224',
    countryCode: 'GN'
  },
  {
    name: 'Guinea-Bissau',
    dial_code: '+245',
    countryCode: 'GW'
  },
  {
    name: 'Guyana',
    dial_code: '+595',
    countryCode: 'GY'
  },
  {
    name: 'Haiti',
    dial_code: '+509',
    countryCode: 'HT'
  },
  {
    name: 'Holy See (Vatican City State)',
    dial_code: '+379',
    countryCode: 'VA'
  },
  {
    name: 'Honduras',
    dial_code: '+504',
    countryCode: 'HN'
  },
  {
    name: 'Hong Kong',
    dial_code: '+852',
    countryCode: 'HK'
  },
  {
    name: 'Hungary',
    dial_code: '+36',
    countryCode: 'HU'
  },
  {
    name: 'Iceland',
    dial_code: '+354',
    countryCode: 'IS'
  },
  {
    name: 'India',
    dial_code: '+91',
    countryCode: 'IN'
  },
  {
    name: 'Indonesia',
    dial_code: '+62',
    countryCode: 'ID'
  },
  {
    name: 'Iran, Islamic Republic of Persian Gulf',
    dial_code: '+98',
    countryCode: 'IR'
  },
  {
    name: 'Iraq',
    dial_code: '+964',
    countryCode: 'IQ'
  },
  {
    name: 'Ireland',
    dial_code: '+353',
    countryCode: 'IE'
  },
  {
    name: 'Isle of Man',
    dial_code: '+44',
    countryCode: 'IM'
  },
  {
    name: 'Israel',
    dial_code: '+972',
    countryCode: 'IL'
  },
  {
    name: 'Italy',
    dial_code: '+39',
    countryCode: 'IT'
  },
  {
    name: 'Jamaica',
    dial_code: '+1 876',
    countryCode: 'JM'
  },
  {
    name: 'Japan',
    dial_code: '+81',
    countryCode: 'JP'
  },
  {
    name: 'Jersey',
    dial_code: '+44',
    countryCode: 'JE'
  },
  {
    name: 'Jordan',
    dial_code: '+962',
    countryCode: 'JO'
  },
  {
    name: 'Kazakhstan',
    dial_code: '+7 7',
    countryCode: 'KZ'
  },
  {
    name: 'Kenya',
    dial_code: '+254',
    countryCode: 'KE'
  },
  {
    name: 'Kiribati',
    dial_code: '+686',
    countryCode: 'KI'
  },
  {
    name: 'Korea, Democratic People\'s Republic of Korea',
    dial_code: '+850',
    countryCode: 'KP'
  },
  {
    name: 'Korea, Republic of South Korea',
    dial_code: '+82',
    countryCode: 'KR'
  },
  {
    name: 'Kuwait',
    dial_code: '+965',
    countryCode: 'KW'
  },
  {
    name: 'Kyrgyzstan',
    dial_code: '+996',
    countryCode: 'KG'
  },
  {
    name: 'Laos',
    dial_code: '+856',
    countryCode: 'LA'
  },
  {
    name: 'Latvia',
    dial_code: '+371',
    countryCode: 'LV'
  },
  {
    name: 'Lebanon',
    dial_code: '+961',
    countryCode: 'LB'
  },
  {
    name: 'Lesotho',
    dial_code: '+266',
    countryCode: 'LS'
  },
  {
    name: 'Liberia',
    dial_code: '+231',
    countryCode: 'LR'
  },
  {
    name: 'Libyan Arab Jamahiriya',
    dial_code: '+218',
    countryCode: 'LY'
  },
  {
    name: 'Liechtenstein',
    dial_code: '+423',
    countryCode: 'LI'
  },
  {
    name: 'Lithuania',
    dial_code: '+370',
    countryCode: 'LT'
  },
  {
    name: 'Luxembourg',
    dial_code: '+352',
    countryCode: 'LU'
  },
  {
    name: 'Macao',
    dial_code: '+853',
    countryCode: 'MO'
  },
  {
    name: 'Macedonia',
    dial_code: '+389',
    countryCode: 'MK'
  },
  {
    name: 'Madagascar',
    dial_code: '+261',
    countryCode: 'MG'
  },
  {
    name: 'Malawi',
    dial_code: '+265',
    countryCode: 'MW'
  },
  {
    name: 'Malaysia',
    dial_code: '+60',
    countryCode: 'MY'
  },
  {
    name: 'Maldives',
    dial_code: '+960',
    countryCode: 'MV'
  },
  {
    name: 'Mali',
    dial_code: '+223',
    countryCode: 'ML'
  },
  {
    name: 'Malta',
    dial_code: '+356',
    countryCode: 'MT'
  },
  {
    name: 'Marshall Islands',
    dial_code: '+692',
    countryCode: 'MH'
  },
  {
    name: 'Martinique',
    dial_code: '+596',
    countryCode: 'MQ'
  },
  {
    name: 'Mauritania',
    dial_code: '+222',
    countryCode: 'MR'
  },
  {
    name: 'Mauritius',
    dial_code: '+230',
    countryCode: 'MU'
  },
  {
    name: 'Mayotte',
    dial_code: '+262',
    countryCode: 'YT'
  },
  {
    name: 'Mexico',
    dial_code: '+52',
    countryCode: 'MX'
  },
  {
    name: 'Micronesia, Federated States of Micronesia',
    dial_code: '+691',
    countryCode: 'FM'
  },
  {
    name: 'Moldova',
    dial_code: '+373',
    countryCode: 'MD'
  },
  {
    name: 'Monaco',
    dial_code: '+377',
    countryCode: 'MC'
  },
  {
    name: 'Mongolia',
    dial_code: '+976',
    countryCode: 'MN'
  },
  {
    name: 'Montenegro',
    dial_code: '+382',
    countryCode: 'ME'
  },
  {
    name: 'Montserrat',
    dial_code: '+1664',
    countryCode: 'MS'
  },
  {
    name: 'Morocco',
    dial_code: '+212',
    countryCode: 'MA'
  },
  {
    name: 'Mozambique',
    dial_code: '+258',
    countryCode: 'MZ'
  },
  {
    name: 'Myanmar',
    dial_code: '+95',
    countryCode: 'MM'
  },
  {
    name: 'Namibia',
    dial_code: '+264',
    countryCode: 'NA'
  },
  {
    name: 'Nauru',
    dial_code: '+674',
    countryCode: 'NR'
  },
  {
    name: 'Nepal',
    dial_code: '+977',
    countryCode: 'NP'
  },
  {
    name: 'Netherlands',
    dial_code: '+31',
    countryCode: 'NL'
  },
  {
    name: 'Netherlands Antilles',
    dial_code: '+599',
    countryCode: 'AN'
  },
  {
    name: 'New Caledonia',
    dial_code: '+687',
    countryCode: 'NC'
  },
  {
    name: 'New Zealand',
    dial_code: '+64',
    countryCode: 'NZ'
  },
  {
    name: 'Nicaragua',
    dial_code: '+505',
    countryCode: 'NI'
  },
  {
    name: 'Niger',
    dial_code: '+227',
    countryCode: 'NE'
  },
  {
    name: 'Nigeria',
    dial_code: '+234',
    countryCode: 'NG'
  },
  {
    name: 'Niue',
    dial_code: '+683',
    countryCode: 'NU'
  },
  {
    name: 'Norfolk Island',
    dial_code: '+672',
    countryCode: 'NF'
  },
  {
    name: 'Northern Mariana Islands',
    dial_code: '+1 670',
    countryCode: 'MP'
  },
  {
    name: 'Norway',
    dial_code: '+47',
    countryCode: 'NO'
  },
  {
    name: 'Oman',
    dial_code: '+968',
    countryCode: 'OM'
  },
  {
    name: 'Pakistan',
    dial_code: '+92',
    countryCode: 'PK'
  },
  {
    name: 'Palau',
    dial_code: '+680',
    countryCode: 'PW'
  },
  {
    name: 'Palestinian Territory, Occupied',
    dial_code: '+970',
    countryCode: 'PS'
  },
  {
    name: 'Panama',
    dial_code: '+507',
    countryCode: 'PA'
  },
  {
    name: 'Papua New Guinea',
    dial_code: '+675',
    countryCode: 'PG'
  },
  {
    name: 'Paraguay',
    dial_code: '+595',
    countryCode: 'PY'
  },
  {
    name: 'Peru',
    dial_code: '+51',
    countryCode: 'PE'
  },
  {
    name: 'Philippines',
    dial_code: '+63',
    countryCode: 'PH'
  },
  {
    name: 'Pitcairn',
    dial_code: '+872',
    countryCode: 'PN'
  },
  {
    name: 'Poland',
    dial_code: '+48',
    countryCode: 'PL'
  },
  {
    name: 'Portugal',
    dial_code: '+351',
    countryCode: 'PT'
  },
  {
    name: 'Puerto Rico',
    dial_code: '+1 939',
    countryCode: 'PR'
  },
  {
    name: 'Qatar',
    dial_code: '+974',
    countryCode: 'QA'
  },
  {
    name: 'Romania',
    dial_code: '+40',
    countryCode: 'RO'
  },
  {
    name: 'Russia',
    dial_code: '+7',
    countryCode: 'RU'
  },
  {
    name: 'Rwanda',
    dial_code: '+250',
    countryCode: 'RW'
  },
  {
    name: 'Reunion',
    dial_code: '+262',
    countryCode: 'RE'
  },
  {
    name: 'Saint Barthelemy',
    dial_code: '+590',
    countryCode: 'BL'
  },
  {
    name: 'Saint Helena, Ascension and Tristan Da Cunha',
    dial_code: '+290',
    countryCode: 'SH'
  },
  {
    name: 'Saint Kitts and Nevis',
    dial_code: '+1 869',
    countryCode: 'KN'
  },
  {
    name: 'Saint Lucia',
    dial_code: '+1 758',
    countryCode: 'LC'
  },
  {
    name: 'Saint Martin',
    dial_code: '+590',
    countryCode: 'MF'
  },
  {
    name: 'Saint Pierre and Miquelon',
    dial_code: '+508',
    countryCode: 'PM'
  },
  {
    name: 'Saint Vincent and the Grenadines',
    dial_code: '+1 784',
    countryCode: 'VC'
  },
  {
    name: 'Samoa',
    dial_code: '+685',
    countryCode: 'WS'
  },
  {
    name: 'San Marino',
    dial_code: '+378',
    countryCode: 'SM'
  },
  {
    name: 'Sao Tome and Principe',
    dial_code: '+239',
    countryCode: 'ST'
  },
  {
    name: 'Saudi Arabia',
    dial_code: '+966',
    countryCode: 'SA'
  },
  {
    name: 'Senegal',
    dial_code: '+221',
    countryCode: 'SN'
  },
  {
    name: 'Serbia',
    dial_code: '+381',
    countryCode: 'RS'
  },
  {
    name: 'Seychelles',
    dial_code: '+248',
    countryCode: 'SC'
  },
  {
    name: 'Sierra Leone',
    dial_code: '+232',
    countryCode: 'SL'
  },
  {
    name: 'Singapore',
    dial_code: '+65',
    countryCode: 'SG'
  },
  {
    name: 'Slovakia',
    dial_code: '+421',
    countryCode: 'SK'
  },
  {
    name: 'Slovenia',
    dial_code: '+386',
    countryCode: 'SI'
  },
  {
    name: 'Solomon Islands',
    dial_code: '+677',
    countryCode: 'SB'
  },
  {
    name: 'Somalia',
    dial_code: '+252',
    countryCode: 'SO'
  },
  {
    name: 'South Africa',
    dial_code: '+27',
    countryCode: 'ZA'
  },
  {
    name: 'South Georgia and the South Sandwich Islands',
    dial_code: '+500',
    countryCode: 'GS'
  },
  {
    name: 'Spain',
    dial_code: '+34',
    countryCode: 'ES'
  },
  {
    name: 'Sri Lanka',
    dial_code: '+94',
    countryCode: 'LK'
  },
  {
    name: 'Sudan',
    dial_code: '+249',
    countryCode: 'SD'
  },
  {
    name: 'Suriname',
    dial_code: '+597',
    countryCode: 'SR'
  },
  {
    name: 'Svalbard and Jan Mayen',
    dial_code: '+47',
    countryCode: 'SJ'
  },
  {
    name: 'Swaziland',
    dial_code: '+268',
    countryCode: 'SZ'
  },
  {
    name: 'Sweden',
    dial_code: '+46',
    countryCode: 'SE'
  },
  {
    name: 'Switzerland',
    dial_code: '+41',
    countryCode: 'CH'
  },
  {
    name: 'Syrian Arab Republic',
    dial_code: '+963',
    countryCode: 'SY'
  },
  {
    name: 'Taiwan',
    dial_code: '+886',
    countryCode: 'TW'
  },
  {
    name: 'Tajikistan',
    dial_code: '+992',
    countryCode: 'TJ'
  },
  {
    name: 'Tanzania, United Republic of Tanzania',
    dial_code: '+255',
    countryCode: 'TZ'
  },
  {
    name: 'Thailand',
    dial_code: '+66',
    countryCode: 'TH'
  },
  {
    name: 'Timor-Leste',
    dial_code: '+670',
    countryCode: 'TL'
  },
  {
    name: 'Togo',
    dial_code: '+228',
    countryCode: 'TG'
  },
  {
    name: 'Tokelau',
    dial_code: '+690',
    countryCode: 'TK'
  },
  {
    name: 'Tonga',
    dial_code: '+676',
    countryCode: 'TO'
  },
  {
    name: 'Trinidad and Tobago',
    dial_code: '+1 868',
    countryCode: 'TT'
  },
  {
    name: 'Tunisia',
    dial_code: '+216',
    countryCode: 'TN'
  },
  {
    name: 'Turkey',
    dial_code: '+90',
    countryCode: 'TR'
  },
  {
    name: 'Turkmenistan',
    dial_code: '+993',
    countryCode: 'TM'
  },
  {
    name: 'Turks and Caicos Islands',
    dial_code: '+1 649',
    countryCode: 'TC'
  },
  {
    name: 'Tuvalu',
    dial_code: '+688',
    countryCode: 'TV'
  },
  {
    name: 'Uganda',
    dial_code: '+256',
    countryCode: 'UG'
  },
  {
    name: 'Ukraine',
    dial_code: '+380',
    countryCode: 'UA'
  },
  {
    name: 'United Arab Emirates',
    dial_code: '+971',
    countryCode: 'AE'
  },
  {
    name: 'United Kingdom',
    dial_code: '+44',
    countryCode: 'GB'
  },
  {
    name: 'United States',
    dial_code: '+1',
    countryCode: 'US'
  },
  {
    name: 'Uruguay',
    dial_code: '+598',
    countryCode: 'UY'
  },
  {
    name: 'Uzbekistan',
    dial_code: '+998',
    countryCode: 'UZ'
  },
  {
    name: 'Vanuatu',
    dial_code: '+678',
    countryCode: 'VU'
  },
  {
    name: 'Venezuela, Bolivarian Republic of Venezuela',
    dial_code: '+58',
    countryCode: 'VE'
  },
  {
    name: 'Vietnam',
    dial_code: '+84',
    countryCode: 'VN'
  },
  {
    name: 'Virgin Islands, British',
    dial_code: '+1 284',
    countryCode: 'VG'
  },
  {
    name: 'Virgin Islands, U.S.',
    dial_code: '+1 340',
    countryCode: 'VI'
  },
  {
    name: 'Wallis and Futuna',
    dial_code: '+681',
    countryCode: 'WF'
  },
  {
    name: 'Yemen',
    dial_code: '+967',
    countryCode: 'YE'
  },
  {
    name: 'Zambia',
    dial_code: '+260',
    countryCode: 'ZM'
  },
  {
    name: 'Zimbabwe',
    dial_code: '+263',
    countryCode: 'ZW'
  }
];
