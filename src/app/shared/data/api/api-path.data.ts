/**
 * @description This constant corresponds to the API route to register a user
 */
export const SIGN_UP_URL: string = '/user/sign/up';

/**
 * @description This constant corresponds to the API route to login a user
 */
export const SIGN_IN_URL: string = '/user/sign/in';

/**
 * @description This constant corresponds to the API route to logout a user
 */
export const SIGN_OUT_URL: string = '/user/sign/out';

/**
 * This constant corresponds to the API route to get the public user data
 */
export const GET_USER_PUBLIC_BY_ID: string = '/user/public/id/';

/**
 * This constant corresponds to the API route to get all events created
 */
export const GET_EVENTS: string = '/event';

/**
 * This constant corresponds to the API route to get the events created by a user
 */
export const GET_EVENTS_BY_USER_ID: string = '/event/user/';

/**
 * This constant corresponds to the API route to get the public user data
 */
export const GET_USER_BY_ID: string = '/user/id/';

/**
 * @description This constant corresponds to the API route to get user data
 */
export const GET_USER_ME: string = '/user/me';

/**
 * This constants corresponds to the API route to purchase tokens
 */
export const PATCH_PURCHASE_TOKEN: string = '/user/me/wallet/add';

/**
 * @description This constant corresponds to the API route to reset a user password (when the user isn't logged)
 */
export const POST_USER_RESET_PASSWORD: string = '/user/reset/password';

/**
 * @description This constant corresponds to the API route to verify the validity of the reset password token
 */
export const GET_USER_RESET_PASSWORD_VERIFY: string = '/user/reset/password/check/token';

/**
 * @description This constant corresponds to the API route to update the password
 */
export const PATCH_USER_RESET_PASSWORD: string = '/user/reset/password/update';

/**
 * @description This constant corresponds to the API route to retrieve user discover events
 */
export const GET_USER_ME_DISCOVER: string = '/user/me/discover';

/**
 * @description This constant corresponds to the API route to retrieve all associations;
 */
export const GET_ASSOCIATIONS: string = '/association';

/**
 * @description This constant corresponds to the API route to retrieve an association thanks to its id
 */
export const GET_ASSOCIATION_BY_ID: string = '/association/id/';

/**
 * @description This constant corresponds to the API route to retrieve all tags;
 */
export const GET_TAGS: string = '/tag';

/**
 * @description This constant corresponds to the API route to create a new event;
 */
export const POST_EVENT_NEW: string = '/event/new';

/**
 * @description This constant corresponds to the API route to add an image to an event;
 */
export const POST_EVENT_IMAGE: string = '/event/id/(?id?)/setting/image';

/**
 * @description This constant corresponds to the API route to add a bet to participate to an event;
 */
export const POST_EVENT_BET: string = '/event/id/(?id?)/bet/add';

/**
 * @description This constant corresponds to the API route to retrieved a bet linked to an event;
 */
export const GET_EVENT_BET: string = '/event/id/(?id?)/bet';

/**
 * @description This constant corresponds to the API route to retrieved a event by its tags;
 */
export const GET_EVENT_BY_TAG_NAME: string = '/event/tag/(?name1&name2?)';

/**
 * @description This constant corresponds to the API route to get an image
 */
//export const GET_IMAGE: string = '/image/id/';

/**
 * @description This constant corresponds to the API route to patch an user image
 */
export const PATCH_IMAGE: string = '/user/me/setting/image';

/**
 * @description This constant corresponds to the API route to patch an user name
 */
export const PATCH_NAME: string = '/user/me/setting/name';

/**
 * @description This constant corresponds to the API route to patch an user username
 */
export const PATCH_USERNAME: string = '/user/me/setting/username';

/**
 * @description This constant corresponds to the API route to patch an user email
 */
export const PATCH_EMAIL: string = '/user/me/setting/email';

/**
 * @description This constant corresponds to the API route to patch an user phone
 */
export const PATCH_PHONE: string = '/user/me/setting/phone';

/**
 * @description This constant corresponds to the API route to patch an user birthday
 */
export const PATCH_BIRTHDAY: string = '/user/me/setting/birthday';

/**
 * @description This constant corresponds to the API route to patch an user location
 */
export const PATCH_LOCATION: string = '/user/me/setting/location';

/**
 * @description This constant corresponds to the API route to patch an user location
 */
export const PATCH_PASSWORD: string = '/user/me/setting/password';

/**
 * @description This constant corresponds to the API route to get user data
 */
export const GET_OFFER: string = '/offer';

/**
 * @description This constant corresponds to the API route to path an association image
 */
export const PATCH_ASSOCIATION_IMAGE: string = '/association/id/(?id?)/setting/image';

/**
 * @description This constant corresponds to the API route to path an association website link
 */
export const PATCH_ASSOCIATION_LINK: string = '/association/id/(?id?)/setting/link';

/**
 * @description This constant corresponds to the API route to path an association name
 */
export const PATCH_ASSOCIATION_NAME: string = '/association/id/(?id?)/setting/name';

/**
 * @description This constant corresponds to the API route to path an association email
 */
export const PATCH_ASSOCIATION_EMAIL: string = '/association/id/(?id?)/setting/email';

/**
 * @description This constant corresponds to the API route to path an association email
 */
export const PATCH_ASSOCIATION_LOCATION: string = '/association/id/(?id?)/setting/location';

/**
 * @description This constant corresponds to the API route to path an association email
 */
export const PATCH_ASSOCIATION_PHONE: string = '/association/id/(?id?)/setting/phone';

/**
 * @description This constant corresponds to the API route to path an association description
 */
export const PATCH_ASSOCIATION_DESCRIPTION: string = '/association/id/(?id?)/setting/description';
