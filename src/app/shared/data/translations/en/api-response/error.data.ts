export const data = {

  /**
   * @description This section contains all global user's errors
   */
  USER_NOT_AUTHENTICATED: 'Session expired. You are not authenticated',
  USER_NOT_FOUND: 'User doesn\'t exist',
  USER_NOT_AUTHORIZED: 'Session expired. You are not authorized',

  /**
   * @description This section contains all user's role errors
   */
  ROLE_MISSING: 'Role is required',
  ROLE_NOT_EXIST: 'One of the roles does not exist',
  USER_IS_GUEST: 'You cannot access the website',
  USER_NOT_USER: 'You don\'t have the user rights',
  USER_NOT_INFLUENCER: 'You don\'t have the influencer rights',
  USER_NOT_ADMIN: 'You don\'t have the admin rights',
  USER_NOT_SUPER_ADMIN: 'You don\'t have the super admin rights',

  /**
   * @description This section contains all user's login errors
   */
  LOGIN_BAD: 'Wrong username or email address',
  LOGIN_MISSING: 'Username or email address is required',

  /**
   * @description This section contains all user's password errors
   */
  PASSWORD_BAD: 'Wrong password',
  PASSWORD_MISSING: 'Password is required',
  PASSWORD_MIN_LENGTH: 'Password must be at least 8 characters long',
  PASSWORD_MAX_LENGTH: 'Password must be 50 characters maximum',
  PASSWORD_COMPOSITION: 'Must contains at least a number, a special character, an uppercase and a lowercase letter',
  CONFIRM_PASSWORD_MISSING: 'Password confirmation is required',
  CONFIRM_PASSWORD_MATCH: 'Password confirmation doesn\'t match',
  NEW_PASSWORD_MATCH_OLD: 'Your new password cannot match your old password',

  /**
   * @description This section contains all user's username errors
   */
  USERNAME_ALREADY_USED: 'Username already used',
  USERNAME_MISSING: 'Username is required',
  USERNAME_FORMAT: 'Username must be at least 3 characters long',
  USERNAME_BAD_FORMAT: 'Username format is bad',

  /**
   * @description This section contains all user's first name errors
   */
  FIRST_NAME_MISSING: 'First name is required',
  FIRST_NAME_BAD_FORMAT: 'First name format is bad',

  /**
   * @description This section contains all user's last name errors
   */
  LAST_NAME_MISSING: 'Last name is required',
  LAST_NAME_BAD_FORMAT: 'Last name format is bad',

  /**
   * @description This section contains all user's birthday errors
   */
  BIRTHDAY_MISSING: 'Birthday date is required',
  BIRTHDAY_BAD_FORMAT: 'Birthday format is invalid',
  BIRTHDAY_NOT_LEGAL_AGE: 'You aren\'t of legal age',
  BIRTHDAY_ENTERED_INVALID: 'Birthday entered is invalid',

  /**
   * @description This section contains all user's email errors
   */
  EMAIL_BAD_FORMAT: 'Wrong email address format',
  EMAIL_DOES_NOT_EXIST: 'Email does not exist',
  EMAIL_MISSING: 'Email address is required',
  EMAIL_ALREADY_USED: 'Email address already used',

  /**
   * @description This section contains all user's location errors
   */
  LOCATION_COUNTRY_CODE_MISSING: 'Location\'s country is required',
  LOCATION_CITY_MISSING: 'Location\'s city is required',
  LOCATION_STREET_ADDRESS_MISSING: 'Location\'s street address is required',
  LOCATION_ZIP_CODE_MISSING: 'Location\'s zip code is required',
  LOCATION_BAD_FORMAT: 'Location format is bad',
  LOCATION_COUNTRY_CODE_NOT_EXIST: 'Location\'s country doesn\'t exist',

  /**
   * @description This section contains all user's phone number errors
   */
  PHONE_BAD_FORMAT: 'Phone number format is bad',
  PHONE_ALREADY_USED: 'Phone number already used',
  PHONE_COUNTRY_CODE_MISSING: 'Phone\'s country code is required',
  PHONE_NUMBER_MISSING: 'Phone number is required',

  /**
   * @description This section contains all event's errors
   */
  EVENT_NAME_MISSING: 'Event\'s name is required',
  EVENT_NAME_BAD_FORMAT: 'Wrong event name',
  EVENT_NAME_ALREADY_USED: 'Event name already used',
  EVENT_NAME_MIN_LENGTH: 'Event\'s name must be at least 3 characters long',
  EVENT_DATE_TO_EARLY: 'Event can\'t be created less than 24 hours before it takes place',
  EVENT_DATE_MISSING: 'Event\'s date is required',
  EVENT_DATE_BAD_FORMAT: 'Event\'s format is invalid',
  EVENT_DRAW_DATE_MISSING: 'Event\'s draw date is required',
  EVENT_DRAW_DATE_BAD_FORMAT: 'Event\'s format is invalid',
  EVENT_DRAW_DATE_TO_EARLY: 'Draw date is to close to the creation of the event',
  EVENT_ID_MISSING: 'Event\'s ID is required',
  EVENT_NOT_FOUND: 'Event doesn\'t exist',
  EVENT_LOTTERIES_SIZE_MIN_LENGTH: 'Event\'s lotteries size must be greater than 0',
  EVENT_FINISHED: 'Event already finished',
  EVENT_OWN_EVENT: 'A user cannot participate to his own event',

  /**
   * @description This section contains all date's errors
   */
  DATE_BAD_FORMAT: 'Date format is invalid',
  DATE_ENTERED_INVALID: 'Date entered is invalid',

  /**
   * @description This section contains all method's errors (not very useful in frontend part)
   */
  METHOD_NOT_ALLOWED: 'Request method is not allowed',

  /**
   * @description This section contains the terms of service error
   */
  CGU_NOT_VALIDATED: 'Terms and conditions must be validated',

  /**
   * @description This section contains all association's errors
   */
  ASSOCIATION_NAME_ALREADY_USED: 'Association\'s name already used',
  ASSOCIATION_NAME_MISSING: 'Association\'s name is required',
  ASSOCIATION_MISSING: 'Association ID or name is required',
  ASSOCIATION_NOT_FOUND: 'Association doesn\'t exist',

  /**
   * @description This section contains all user favorite's errors
   */
  FAVORITE_TAG_MISSING: 'Favorite\'s tag is required',
  FAVORITE_ASSOCIATION_MISSING: 'Favorite\'s association ID or name is required',
  FAVORITE_USER_MISSING: 'Favorite\'s user is ID or name required',

  /**
   * @description This section contains all images errors
   */
  IMAGE_NAME_MISSING: 'Image\'s name is required',
  IMAGE_NOT_FOUND: 'Image doesn\'t exist',
  IMAGE_CORRUPTED: 'Image corrupted',

  /**
   * @description This section contains all JWT tokens errors
   */
  PARSING_TOKEN: 'Token parsing has failed',
  TOKEN_INVALID: 'Token is invalid',
  RESET_PASSWORD_TOKEN_MISSING: 'Reset-Token is required',

  /**
   * @description This section contains all lotteries errors
   */
  LOTTERY_NOT_FOUND: 'Lottery doesn\'t exist',
  BET_NOT_FOUND: 'Best doesn\'t exist',
  BET_NEGATIVE_OR_NULL: 'Bet\'s value must be greater than 0',
  BET_WRONG_SPLIT_RATIO: 'The bet\'s ratio between the influencer and the association must be between 10% and 90%',
  BET_GLOBAL: 'An error has occurred. Your bet could not be registered',

  /**
   * @description This section contains all tags errors
   */
  TAG_NAME_BAD_FORMAT: 'Tag\'s name format is invalid',
  TAG_NAME_MIN_LENGTH: 'Tag\'s name must be at least 3 characters long',
  TAG_NAME_ALREADY_USED: 'Tag\'s name already used',
  TAG_NAME_MISSING: 'Tag\'s name is required',
  TAG_NOT_FOUND: 'Tag\'s name doesn\'t exist',

  /**
   * @description This section contains all user tokens errors
   */
  VALUE_NEGATIVE_OR_NULL: 'The number of tokens must be greater than 0',
  USER_NOT_ENOUGH_TOKENS: 'User\'s doesn\'t own enough tokens',

  /**
   * @description This section contains all mailing errors
   */
  SUBJECT_MISSING: 'Mail\'s subject is required',
  BODY_MISSING: 'Mail\'s body is required',
  POLITENESS_FORMULA_MISSING: 'Mail\'s politeness formula is required',
  SIGNATURE_MISSING: 'Mail\'s signature is required',
  RECEIVERS_MISSING: 'Mail\'s receivers are required',
  RECEIVER_NOT_FOUND: 'Mail\'s receiver does not exist',

  /**
   * @description This section contains all offers errors
   */
  OFFER_NAME_ALREADY_USED: 'Offer\'s name already used',
  OFFER_NAME_MISSING: 'Offer\'s name is required',
  OFFER_NOT_FOUND: 'Offer\'s doesn\'t exist',
  OFFER_ID_MISSING: 'Offer\'s ID is required',
  OFFER_PRICE_NEGATIVE: 'Offer\'s price must be positive',
  OFFER_TOKENS_NEGATIVE_OR_NULL: 'Offer\'s tokens must be strictly positive',
  OFFER_ADD_ERROR: 'A problem occurred while purchasing your tokens',
  OFFER_LOAD_ERROR: 'A problem occurred during the loading of offers',

  /**
   * @description This section contains all the captcha errors
   */
  RECAPTCHA_TOKEN_MISSING: 'Recaptcha-Token is required',

  /**
   * @description This section contains all the search errors
   */
  SEARCH_QUERY_MISSING: 'The search query is required',

  /**
   * @description This section contains all the metrics errors
   */
  METRICS_NOT_FOUND: 'A problem occured while getting the metrics',
};
