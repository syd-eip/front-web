export const data = {

  /**
   * @description This section contains successful request message
   */
  OK: 'Successful request',

  /**
   * @description This sections contains all global user success messages
   */
  USER_CREATED: 'Register successful',
  USER_AUTHENTICATED: 'Login successful',
  USER_DISCONNECTED: 'Logout successful',
  USER_DELETED: 'User has been correctly deleted',

  /**
   * @description This sections contains all username success messages
   */
  USERNAME_UPDATED: 'Username has been correctly updated',
  USERNAME_FREE: 'Username is not used by an other user',

  /**
   * @description This sections contains all users email success messages
   */
  EMAIL_UPDATED: 'Email address has been correctly updated',
  EMAIL_FREE: 'Email address is not used by an other user',

  /**
   * @description This sections contains all users phone number success messages
   */
  PHONE_NUMBER_UPDATED: 'Phone number has been correctly updated',
  PHONE_NUMBER_FREE: 'Phone number is not used by an other user',

  /**
   * @description This sections contains all users password success messages
   */
  PASSWORD_UPDATED: 'Password has been correctly updated',
  PASSWORD_RESET: 'Password has been correctly reseted',

  /**
   * @description This sections contains all users names (first and last names) success messages
   */
  NAME_UPDATED: 'First and last names have been correctly updated',

  /**
   * @description This sections contains all users location success messages
   */
  LOCATION_UPDATED: 'Location has been correctly updated',

  /**
   * @description This sections contains all users birthday success messages
   */
  BIRTHDAY_UPDATED: 'Birthday has been correctly updated',

  /**
   * @description This sections contains all roles success messages
   */
  ROLE_ADDED: 'User\'s role has been correctly added',
  ROLE_REMOVED: 'User\'s role has been correctly removed',

  /**
   * @description This sections contains all associations success messages
   */
  ASSOCIATION_CREATED: 'Association has been correctly registered',
  ASSOCIATION_DELETED: 'Association has been correctly deleted',
  ASSOCIATION_NAME_UPDATED: 'Association\'s name has been correctly updated',
  ASSOCIATION_DESCRIPTION_UPDATED: 'Association\'s description has been correctly updated',
  ASSOCIATION_LINK_UPDATED: 'Association\'s website link has been correctly updated',

  /**
   * @description This sections contains all events success messages
   */
  EVENT_CREATED: 'Event has been correctly created',
  EVENT_DELETED: 'Event has been correctly deleted',
  EVENT_NAME_UPDATED: 'Event\'s name has been correctly updated',
  EVENT_DESCRIPTION_UPDATED: 'Event\'s description has been correctly updated',
  EVENT_TAGS_UPDATED: 'Event\'s tags have been correctly updated',

  /**
   * @description This sections contains all lotteries success messages
   */
  BET_ADDED: 'Your bet has been correctly taken into consideration',

  /**
   * @description This sections contains all user's favorite success messages
   */
  FAVORITE_TAGS_UPDATED: 'Favorite\'s tags have been correctly updated',
  FAVORITE_USERS_UPDATED: 'Favorite\'s users have been correctly updated',
  FAVORITE_ASSOCIATIONS_UPDATED: 'Favorite\'s associations have been correctly updated',

  /**
   * @description This sections contains all images success messages
   */
  IMAGE_UPDATED: 'Image has been correctly updated',

  /**
   * @description This sections contains all tags success messages
   */
  TAG_CREATED: 'Tag has been correctly created',
  TAG_DELETED: 'Tag has been correctly deleted',

  /**
   * @description This sections contains all user's tokens success messages
   */
  TOKENS_ADDED: 'Tokens have been correctly added',
  TOKENS_REMOVED: 'Tokens have been correctly deleted',

  /**
   * @description This section contains all offers success messages
   */
  OFFER_CREATED: 'Offer has been correctly registered',
  OFFER_DELETED: 'Offer has been correctly deleted',
  OFFER_NAME_UPDATED: 'Offer\'s name has been correctly updated',
  OFFER_PRICE_UPDATED: 'Offer\'s price has been correctly updated',
  OFFER_TOKENS_UPDATED: 'Offer\'s tokens has been correctly updated'
};
