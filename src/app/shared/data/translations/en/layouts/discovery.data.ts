export const data = {
  PREFERENCES_SECTION: 'According to your preferences',
  RECOMMENDED_SECTION: 'Recommended events',
  RECENT_SECTION: 'Most recent events',
  POPULAR_SECTION: 'Most popular events',
};
