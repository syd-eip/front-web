export const data = {
  TITLE: 'Reset your password',
  PASSWORD: 'Password',
  CONFIRM_PASSWORD: 'Confirm Password',
  TEXT_INFORMATION: 'Please enter your new password. Please note that this link is only valid for a maximum of 24 hours!',
  RESET_BUTTON: 'Reset',
};
