export const data = {
  TEAM_DESCRIPTION: 'A true multi-function palette whose goal is to create emotion and achieve dreams.',
  MORE:   'More',
  VALORS: 'Our valors',
  VALOR_DESCRIPTION: 'We pay special attention to values. Indeed, in a world where everything is connected, where everything goes extremely fast, these are often forgotten. But for us, it is crucial to meet your expectations',
  FAST: 'Speed',
  FAST_DESCRIPTION: 'Indeed, speed is a service not to be neglected in a connected world.',
  SECURITY: 'Security',
  SECURITY_DESCRIPTION: 'From the moment the money is put into play, taking into account \n ' +
    'from this point is very important and not insignificant.',
  TEAMWORK_DESCRIPTION: 'Teamwork in sport as in any job is an asset we grant \n ' +
  'great importance.',
  MONITORING: 'Periodic Monitoring',
  PERIODIC_MONITORING: 'We offer a regular monitoring of your statistics, pots, earnings.',
  OUR_TEAM: 'Our team',
  OUR_TEAM_DESCRIPTION : 'Our team composed of heterogeneous elements and skills is a strength. \n ' +
 'Meeting your expectations and offering you various services is our guiding thread',
  FRONT_DEV_F: 'Front-end developer',
  M_DESCRIPTION: 'I\'m really passionate about computers, video games but also web technos and following some influencers in \n ' +
 'fitness and make-up, this project is for me a chance to link the two \n ' +
 'domains together.',
  APPMOBILE_DEV: 'iOS mobile developer',
  P_DESCRIPTION: 'Having a penchant for the Apple universe, be brought to realize an iOS application is giving\n ' +
    'me a lot of joy, because it will be the opportunity for me to finally start the realization \n' +
    'of applications based on Swift language.',
  FRONT_DEV_M: 'Front-end developer',
  COTO_DESCRIPTION: 'Having followed a technical course in high school taking the edges of the law, \n ' +
 'economics, management, project management and marketing I\'m lucky to have \n' +
  'many bases in these different and complementary fields.',
  PROJECT_LEADER: 'Project leader',
  BLE_DESCRIPTION: 'My previous experience in a team is an asset that I want to put forward. \n'  +
  'My technical skills in different domains allow me to be \n' +
    'versatile on the development poles set up within this team.',
  BACK_DEV: 'Back-end developer',
  A_DESCRIPTION: 'I\'m really passionate about video games, and being very present on social networks, I am very \n ' +
    'attached to this project that uses the hype around social networks and their platforms for \n' +
     'serve charities to which I am very attached.',
  K_DESCRIPTION: 'My knowledge in database (ElasticSearch) can be used to realize the \n ' +
  'user database and my algorithmic skills will help to realize a complete system of \n' +
  'calculation for the project API.',
  DEV_APP: 'Front-end / iOS developer',
  LOMB_DESCRIPTION: 'I have been able several times in the past to explore Web technologies and development \ n ' +
  'mobile (internships, high school, ...). \ n' +
  'As these experiences progressed, I was able to sharpen and diversify my skills in these areas.',
  CONTACT_SECTION_TITLE: 'Contact us',
  CONTACT_SECTION_TEXT: 'If you ever encounter a problem or just want to ' + ' contact us, fill out this form and we will be happy to answer you!',
  EMAIL_INPUT_TITLE: 'Email address',
  EMAIL_INPUT_PLACEHOLDER: 'firstname@example.com',
  NAME_INPUT_TITLE: 'Name',
  NAME_INPUT_PLACEHOLDER: 'Your name',
  MESSAGE_TEXT_AREA_TITLE: 'Your message',
  MESSAGE_BUTTON_TEXT: 'Send',
  ABOUT_US_SECTION_TITLE: 'Who are we ?',
  ABOUT_US_SECTION_TEXT: 'We are a group of seven students from EPITECH Bordeaux. We all have our particularities and our specificities. ' + 'In spite of our differences, we all came together in the realization of this' + ' project to form a young, dynamic and flexible group.',
  ABOUT_US_BUTTON_TEXT: 'Learn more',
  DESCRIPTION_TEXT: 'Participate in a lottery to hope to share a privileged ' + ' moment with YOUR star, by supporting him but also the charity ' + ' association of his choice.',
  FIRST_CARD_TITLE: 'Social',
  FIRST_CARD_TEXT: 'Chat with your star to organize the event you won.',
  SECOND_CARD_TITLE: 'Control',
  SECOND_CARD_TEXT: 'Be in control of your donation and see what impact it will have.',
  THIRD_CARD_TITLE: 'Fun',
  THIRD_CARD_TEXT: 'Play, cook, practice what you love with those you follow.',
};
