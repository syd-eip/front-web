export const data = {
  CONNECTION: 'Sign In',
  LOGIN: 'Email or Username',
  PASSWORD: 'Password',
  EMAIL: 'Email',
  CONNECTION_BUTTON: 'Let\'s Go!',
  SEND_BUTTON: 'Send',
  FORGOT_PASSWORD: 'Forgot Password ?',
  NO_ACCOUNT: 'Don\'t have an account ?',
  FORGOT_PASSWORD_TEXT: 'Please enter your email address below and we will send you an email to reset your password.',
  FORGOT_PASSWORD_CONTACT_TEXT: 'If you don\'t know your email address or it is no longer valid, ' +
    'please contact us for further assistance.',
  SEND_SUCCESS: 'Sent successfully!',
  SEND_SUCCESS_TEXT: 'A password reset email was sent to you at the address',
  SEND_SUCCESS_PROBLEM_TEXT_PART1: 'Didn\'t you get that email? Remember to check your',
  SEND_SUCCESS_PROBLEM_TEXT_PART2: 'If the problem persists please contact the platform.',
  JUNK_MAIL: 'junk mail'
};
