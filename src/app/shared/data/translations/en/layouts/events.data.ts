export const data = {
  EDIT_EVENT: 'Edit event',
  SAVE_CHANGES: 'Save changes',
  DISCARD_CHANGES: 'Cancel changes',
  CREATE_EVENT: 'Create event',
  EDITING_MENU: 'Menu',
  EDITING_MODE: 'Editing mode',
  EDITING_SAVE_CONFIRM: 'Are you sure you want to save your changes, all modifications done will be public.',
  EDITING_DISCARD_CONFIRM: 'Are you sure you want to discard your changes, all modifications done will be lost.',
  SET_EVENT_DATE: 'Set your event date',
  SET_DRAW_DATE: 'Set your draw date',

  CREATE_MODE: 'Creation mode',
  CREATE_CONFIRM: 'The event will be published. Continue ?',
  CREATE_SUCCESS: 'Event successfully created',

  EVENT_DATE: 'Event date',
  EVENT_TITLE: 'Event title',
  EVENT_DESCRIPTION: 'Event description',
  EVENT_FOR_LABEL: 'For',
  CHANGE_BANNER: 'Click to change banner',

  YES: 'Yes',
  NO: 'No',

  INFLUENCER_FOLLOW: 'Follow',
  INFLUENCER_FOLLOWERS: 'Followers',
  INFLUENCER_EVENTS: 'Events',

  N_WINNER: 'Winner',
  N_WINNERS: 'Winners',
  N_WINNERS_INPUT: 'Number of',

  DRAW_DATE: 'Draw date',
  DRAW_N_PARTICIPANTS: 'Participants',
  DRAW_PREREQUISITES: 'Prerequisites',
  DRAW_PARTICIPATE: 'Participate',

  ABOUT_ASSOCIATION: 'More about {{ASSOCIATION}}',
  ABOUT_INFLUENCER: 'More about {{INFLUENCER}}',
  ASSOCIATED_TAGS: 'Associated tags',

  ERROR_NO_TITLE: 'Your event has no title',
  ERROR_NO_DESCRIPTION: 'Your event has no description',
  ERROR_NO_ASSOCIATION: 'Your event is not associated with an association',
  ERROR_NO_DRAW_DATE: 'You must specify a draw date',
  ERROR_NO_EVENT_DATE: 'You must specify an event date',
  ERROR_WRONG_LOTTERY_SIZE: 'You must specify a number of winners',
  ERROR_DATE_DRAW_HIGHER_THAN_EVENT: 'The draw date cannot be later than the date of the event',
  ERROR_DATE_DRAW_LOWER_THAN_NOW: 'The draw date cannot be before now',
  ERROR_DATE_EVENT_LOWER_THAN_NOW: 'The event date cannot be before now',
  RECOMMENDED_BY_US: 'Recommended by us',

}
