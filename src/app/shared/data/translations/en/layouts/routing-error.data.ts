export const data = {
  ERROR_MESSAGE: 'The page you are looking for might have been removed had its name changed or is temporarily unavailable',
  HOME: 'Go to Home Page',
  NOT_FOUND: 'Page not found'
};
