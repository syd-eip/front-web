export const data = {
  REGISTER: 'Register',
  SOCIAL_AUTH: 'or be classical',
  ACCOUNT_TYPE: 'What type of account do you want to create ?',
  STANDARD_ACCOUNT: 'I want to create a standard account',
  INFLUENCER_ACCOUNT: 'I want to create an influencer account',
  FIRSTNAME: 'Firstname',
  LASTNAME: 'Lastname',
  BIRTHDAY: 'DD/MM/YYYY',
  USERNAME: 'Username',
  PASSWORD: 'Password',
  CONFIRM_PASSWORD: 'Confirm Password',
  PHONE: 'Phone number',
  EMAIL : 'E-mail address',
  LOCATION: 'Location',
  CGU: 'I agree to the terms and conditions',

  REGISTER_BUTTON: 'Get Started',
  PREVIOUS: 'Previous',
  NEXT: 'Next',

  ASSOCIATION: 'Associations',
  INFLUENCER: 'Influencers',
  COMMUNITY: 'Community',

  ASSOCIATION_TEXT: 'Come discover which associations you support by participating in the different events.',
  INFLUENCER_TEXT: 'Every day, influencers organize charity events by engaging their communities.',
  COMMUNITY_TEXT: 'In just a few clicks, try your luck and support a charity event.',
};
