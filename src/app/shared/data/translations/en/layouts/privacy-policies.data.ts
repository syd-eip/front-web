export const data = {
  MENU_TITLE: "Informations about SYD",
  LEGAL_MENTIONS: "Legal Mentions",
  TERMS_OF_USE: "Terms of use",
  TERMS_OF_SALE: "Terms of sale",
};
