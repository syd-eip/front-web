export const data = {
  CREATE_EVENT: 'Create an event',
  FAVORITE: 'Add to favorites',
  SETTINGS: 'Settings',
  CURRENT_EVENTS: 'Current Events',
  PAST_EVENTS: 'Past Events',
  NO_CURRENT: 'There is no event.',
  NO_PAST: 'There is no past event.'
};
