export const data = {
  TITLE: 'Choose your offer',
  PURCHASE: 'Purchase',
  CGU_TEXT: 'By clicking purchase you are agreeing to the Terms and Conditions of Share Your Dreams',
  EXPIRATION_DATE: 'Expiration date',
  CARD_NUMBER: 'Card number',
  FULL_NAME: 'Full name',
  TOKENS: 'tokens',
  CREDIT_CARD: 'Credit Card',
  PAYPAL: 'Paypal'
};

