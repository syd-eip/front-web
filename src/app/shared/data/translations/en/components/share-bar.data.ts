export const data = {
  SAVED_SUCCESS: 'The event has indeed been saved in your favorites!',
  SHARING_TWITTER: 'I\'m sharing an event from the Share Your Dreams platform on Twitter!'
};
