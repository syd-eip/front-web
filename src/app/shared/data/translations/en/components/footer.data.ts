export const data = {
  TEAM: 'Team',
  ABOUT_US: 'About Us',
  ADDRESS: 'Address',
  ADDRESS_STATUS: 'SYD - Share Your Dreams',
  ADDRESS_SYD: '85 Rue du Jardin Public, 33000, Bordeaux, Nouvelle-Aquitaine, France',
  GCU: 'Terms of use',
  GCS: 'Terms of sale',
  LEGAL_MENTIONS: 'Legal Mentions'
};
