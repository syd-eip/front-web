export const data = {
  SETTINGS: 'Settings',
  PERSONAL_INFOS: 'Personal Informations',
  CHANGE_PASSWORD: 'Change Password',
  FIRSTNAME: 'First name',
  LASTNAME: 'Last name',
  USERNAME: 'Username',
  EMAIL: 'Email',
  BIRTHDAY: 'Birthday',
  PASSWORD: 'New password',
  CONFIRM_PASSWORD: 'Confirm new password',
  SAVE: 'Save',
  BANNER_CONSTRAINT : 'Banner must be 1000x350 or it will be cropped.',
  CLOSE : 'Close'
};
