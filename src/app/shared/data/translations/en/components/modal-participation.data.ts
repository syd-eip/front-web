export const data = {
  NUMBER_TOKENS_TITLE: 'How much token would you like to bet?',
  NUMBER_TOKENS_PLACEHOLDER: 'Enter the number of tokens',
  NUMBER_TOKENS: 'tokens available',
  NUMBER_TOKEN: 'token available',
  INFLUENCER: 'Influencer',
  CHARITY: 'Charity',
  NOTE: 'Note: There is a 10% minimum for both parties',
  CGU: 'I agree to the terms and conditions',
  SAVE: 'Save'
};
