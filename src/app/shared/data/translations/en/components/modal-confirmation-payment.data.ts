export const data = {
  CONF_TITLE : 'Confirmation of your purchase',
  PAYMENT_METHOD : 'Payment Method : ',
  PRICE : 'Price : ',
  OFFER_NAME : 'Offer : ',
  CREDIT_CARD : 'Credit Card',
  WALLET : 'Your Wallet : ',
  PRINT : 'Print'
};
