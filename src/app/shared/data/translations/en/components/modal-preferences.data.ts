export const data = {
  TITLE: 'User preferences',
  DESCRIPTION: 'Check the boxes of the domain that you like! We will highlight all the events most likely to please you',
  AREAS_SECTION: 'Areas of interest',
  INFLUENCERS_SECTION: 'Influencers',
  ASSOCIATIONS_SECTION: 'Associations',
  SAVE: 'Save',
  SAVED: 'User preferences saved!',
  CLOSE: 'Close'
};
