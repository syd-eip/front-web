export const data = {
  CONNECTION:   'Sign In',
  DECONNECTION:   'Sign Out',
  LANGUAGES: 'Languages',
  FRENCH: 'French',
  ENGLISH: 'English',
  DISCOVER: 'Discover',
  ASSOCIATIONS: 'Associations',
  COMPONENTS: 'Components',
  WELCOME: 'SYD Home',
  BALANCE: 'Check my balance',
  CREATE_EVENT: 'Create an event',
  BUY: 'Buy tokens',
  PROFILE: 'Profile',
  SETTINGS: 'Settings',
  SIGNOUT: 'Sign Out',
  SEARCH: 'Search'
};
