export const data = {
  LOADING_TEXT: 'Loading...',
  NOT_FOUND_TEXT: 'Country not found',
  PLACEHOLDER: 'Select a country'
};
