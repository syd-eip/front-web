/**
 * Import of API body response interface
 */
import {NetworkResponseBodyInterface} from '../../services/network/network-response/network-response-body.interface';
/**
 * Import of API response data
 */
import {data as SUCCESS} from './fr/api-response/success.data';
import {data as ERROR} from './fr/api-response/error.data';
/**
 * Import of components data
 */
import {data as NAVBAR} from './fr/components/navbar.data';
import {data as FOOTER} from './fr/components/footer.data';
import {data as MODAL_PREFERENCES} from './fr/components/modal-preferences.data';
import {data as MODAL_PARTICIPATION} from './fr/components/modal-participation.data';
import {data as EVENT_BOX} from './fr/components/event-box.data';
import {data as SELECT_COUNTRY} from './fr/components/select-country.data';
import {data as MODAL_SETTINGS} from './fr/components/modal-settings.data';
import {data as MODAL_CONFIRMATION_PAYMENT} from './fr/components/modal-confirmation-payment.data';
import {data as SHARE_BAR} from './fr/components/share-bar.data';

/**
 * Import of layouts data
 */
import {data as DISCOVERY_LAYOUT} from './fr/layouts/discovery.data';
import {data as SIGNIN_LAYOUT} from './fr/layouts/sign-in.data';
import {data as SIGNUP_LAYOUT} from './fr/layouts/sign-up.data';
import {data as TEAM_LAYOUT} from './fr/layouts/team.data';
import {data as PRIVACY_POLICIES_LAYOUT} from './fr/layouts/privacy-policies.data';
import {data as ROUTING_LAYOUT} from './fr/layouts/routing-error.data';
import {data as RESET_PASSWORD_LAYOUT} from './fr/layouts/reset-password.data';
import {data as EVENTS_LAYOUT} from './fr/layouts/events.data';
import {data as INFLUENCER_LAYOUT} from './fr/layouts/influencer.data';
import {data as USER_LAYOUT} from './fr/layouts/user.data';
import {data as PAYMENT_LAYOUT} from './fr/layouts/payment.data';

/**
 * @description The FR data data for translation management.
 * If you want to add a new Page. Simply create a data like SUCCESS / ERROR or GLOBAL.
 */
export const data: NetworkResponseBodyInterface = {
  code: 200,
  message: null,
  data: {
    // API RESPONSE DATA
    ERROR,
    SUCCESS,

    // LAYOUTS DATA
    DISCOVERY_LAYOUT,
    SIGNIN_LAYOUT,
    SIGNUP_LAYOUT,
    TEAM_LAYOUT,
    PRIVACY_POLICIES_LAYOUT,
    ROUTING_LAYOUT,
    RESET_PASSWORD_LAYOUT,
    EVENTS_LAYOUT,
    INFLUENCER_LAYOUT,
    USER_LAYOUT,
    PAYMENT_LAYOUT,

    // COMPONENTS DATA
    NAVBAR,
    FOOTER,
    MODAL_PREFERENCES,
    MODAL_PARTICIPATION,
    EVENT_BOX,
    SELECT_COUNTRY,
    MODAL_SETTINGS,
    MODAL_CONFIRMATION_PAYMENT,
    SHARE_BAR
  }
};
