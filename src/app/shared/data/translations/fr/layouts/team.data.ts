export const data = {
  TEAM_DESCRIPTION: 'Véritable palette multi-fonction dont le but est de créer l\'émotion et de réaliser les rêves.',
  MORE:   'En savoir plus',
  VALORS: 'Nos valeurs',
  VALOR_DESCRIPTION: 'Nous accordons une attention particulière aux valeurs. En effet, dans un monde où tout est connecté,\n' +
    '      où tout va extrêmement vite, ces dernières sont souvent oubliées. Or pour nous, il est crucial de\n' +
    '      répondre à vos attentes.',
  FAST: 'Rapidité',
  FAST_DESCRIPTION: 'En effet, la rapidité est un service à ne pas négliger dans un monde connecté.',
  SECURITY: 'Sécurité',
  SECURITY_DESCRIPTION: 'A partir du moment ou de l\'argent est mis en jeu, la prise en compte\n' +
    '              de ce point est très important et non négligeable.',
  TEAMWORK_DESCRIPTION: 'Le travail d\'équipe en sport tout comme dans tout travail est un atout auquel nous accordons\n' +
    '              une vive importance.',
  MONITORING: 'Suivis réguliers',
  PERIODIC_MONITORING: 'Nous vous proposons un suivi réguliers de vos statistiques, cagnotte, gains.',
  OUR_TEAM: 'Notre team',
  OUR_TEAM_DESCRIPTION : 'Notre team composée d\'éléments et de compétences hétéroclites est une force.\n' +
    '            Répondre au mieux à vos attentes et vous proposer divers services est notre fil conducteur.',
  FRONT_DEV_F: 'Développeuse front-end',
  M_DESCRIPTION: 'Passionnée d’informatique, de jeux vidéo mais aussi de technos  web et suivant quelques influenceurs dans le\n' +
    '                domaine du sport et du maquillage, ce projet est pour moi une chance de lier les deux\n' +
    '                domaines ensemble.',
  APPMOBILE_DEV: 'Développeur mobile iOS',
  P_DESCRIPTION: 'Ayant un penchant pour l’univers Apple, être amené à réaliser une application iOS me\n' +
    '                réjouit fortement, car ce sera l’occasion pour moi de commencer enfin la réalisation\n' +
    '                d’applications basées sur le langage Swift.',
  FRONT_DEV_M: 'Développeur front-end',
  COTO_DESCRIPTION: 'Ayant suivi un cursus technique au lycée reprenant les grandes lignes du droit, de\n' +
    '                l’économie, du management, de la gestion de projet et du marketing j’ai la chance d’avoir de\n' +
    '                nombreuses bases dans ces domaines différents et complémentaires.',
  PROJECT_LEADER: 'Chef de projet',
  BLE_DESCRIPTION: 'Mon expérience de travail en équipe est un atout que je souhaite mettre en avant.\n' +
    '                Mes compétences techniques dans différents domaines me permettent d’être\n' +
    '                polyvalent sur les pôles de développement mis en place au sein de cette équipe.',
  BACK_DEV: 'Développeur back-end',
  A_DESCRIPTION: 'Passionné de jeux vidéo, et étant très présent sur les réseaux sociaux, je suis très\n' +
    '                attaché à ce projet qui utilise la popularité des réseaux sociaux et de leurs plateformes pour\n' +
    '                servir des associations caritatives auxquelles je suis très attaché.',
  K_DESCRIPTION: ' Mes connaissances en base de données (ElasticSearch) pourront servir à réaliser la\n' +
    '                base de données utilisateur et mes capacités en algorithmie aideront à réaliser un système complet de\n' +
    '                calcul pour l’API du projet.',
  DEV_APP: 'Développeur front-end / mobile iOS',
  LOMB_DESCRIPTION: 'J’ai pu à plusieurs reprises par le passé explorer les technologies du Web et du développement\n' +
    '                mobile (stages, lycée, ...).\n' +
    '                Au fur et à mesure de ces expériences, j’ai pu affûter, diversifier, mes compétences dans ces domaines.',
  CONTACT_SECTION_TITLE: 'Contactez nous',
  CONTACT_SECTION_TEXT: 'Si jamais vous rencontrez un problème ou tout simplement que vous avez envie' + ' de nous contacter, remplissez ce formulaire on se fera une joie de vous répondre !',
  EMAIL_INPUT_TITLE: 'Adresse e-mail',
  EMAIL_INPUT_PLACEHOLDER: 'nom@exemple.fr',
  NAME_INPUT_TITLE: 'Nom',
  NAME_INPUT_PLACEHOLDER: 'Votre nom',
  MESSAGE_TEXT_AREA_TITLE: 'Votre Message',
  MESSAGE_BUTTON_TEXT: 'Envoyer',
  DESCRIPTION_TEXT: 'Participez à une loterie pour espérer partager un moment ' + 'privilégié avec VOTRE star, en le soutenant mais aussi l\'association' + ' caritative de son choix.',
  FIRST_CARD_TITLE: 'Social',
  FIRST_CARD_TEXT: 'Chattez avec votre star pour organiser l\'événement que vous avez gagné.',
  SECOND_CARD_TITLE: 'Contrôle',
  SECOND_CARD_TEXT: 'Soyez en contrôle de votre don et regardez quel impact il aura.',
  THIRD_CARD_TITLE: 'Amusement',
  THIRD_CARD_TEXT: 'Jouer, cuisiner, pratiquez ce que vous aimez auprès de ceux qui vous passionne.',
};
