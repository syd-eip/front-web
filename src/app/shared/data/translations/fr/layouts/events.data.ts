export const data = {
  EDIT_EVENT: 'Modifier',
  SAVE_CHANGES: 'Sauvegarder',
  DISCARD_CHANGES: 'Annuler',
  CREATE_EVENT: 'Créer',
  EDITING_MENU: 'Outils',
  EDITING_MODE: 'Mode édition',
  EDITING_SAVE_CONFIRM: 'Voulez-vous vraiment sauvegarder, les changements seront isntantanément rendus publics.',
  EDITING_DISCARD_CONFIRM: 'Voulez-vous vraiment annuler, les changements seront isntantanément perdus.',
  SET_EVENT_DATE: 'Date de l\'évènement',
  SET_DRAW_DATE: 'Date du tirrage',

  CREATE_MODE: 'Mode création',
  CREATE_CONFIRM: 'L\'évènement va être publié. Continuer ?',
  CREATE_SUCCESS: 'Evènement créé avec succès',

  EVENT_DATE: 'Date de l\'évènement',
  EVENT_TITLE: 'Titre de l\'évènement',
  EVENT_DESCRIPTION: 'Description de l\'évènement',
  EVENT_FOR_LABEL: 'Pour',
  CHANGE_BANNER: 'Cliquez pour changer la bannière',
  YES: 'Oui',
  NO: 'Non',

  INFLUENCER_FOLLOW: 'Suivre',
  INFLUENCER_FOLLOWERS: 'Abonnés',
  INFLUENCER_EVENTS: 'Evènements',

  N_WINNER: 'Gagnant',
  N_WINNERS: 'Gagnants',
  N_WINNERS_INPUT: 'Nombre de',

  DRAW_DATE: 'Date du tirage',
  DRAW_N_PARTICIPANTS: 'Participants',
  DRAW_PREREQUISITES: 'Prerequisites',
  DRAW_PARTICIPATE: 'Participer',

  ABOUT_ASSOCIATION: 'A propos de {{ASSOCIATION}}',
  ABOUT_INFLUENCER: 'A propos de {{INFLUENCER}}',
  ASSOCIATED_TAGS: 'Tags associés',

  ERROR_NO_TITLE: 'Votre évènement n\'a pas de titre',
  ERROR_NO_DESCRIPTION: 'Votre évènement n\'a pas de description',
  ERROR_NO_ASSOCIATION: 'Votre évènement n\'est pas associé à une association',
  ERROR_NO_DRAW_DATE: 'Vous devez spécifier une date de tirage',
  ERROR_NO_EVENT_DATE: 'Vous devez spécifier une date pour votre évènement',
  ERROR_WRONG_LOTTERY_SIZE: 'Vous devez spécifier un nombre de gagnants positif',
  ERROR_DATE_DRAW_HIGHER_THAN_EVENT: 'La date de tirage ne peut être postérieure à la date de l\'évènement',
  ERROR_DATE_DRAW_LOWER_THAN_NOW: 'Le tirage ne peut pas être réalisé a une date antérieure à aujourd\'hui',
  ERROR_DATE_EVENT_LOWER_THAN_NOW: 'L\'évènement ne peut pas avoir lieu à une date antérieure à aujourd\'hui',

  RECOMMENDED_BY_US: 'Recommandé par nos soins',
}
