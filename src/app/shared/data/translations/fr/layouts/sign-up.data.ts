export const data = {
  REGISTER: 'Inscription',
  SOCIAL_AUTH: 'ou plus classique',
  ACCOUNT_TYPE: 'Quel type de compte souhaitez-vous créer ?',
  STANDARD_ACCOUNT: 'Je souhaite créer un compte standard',
  INFLUENCER_ACCOUNT: 'Je souhaite créer un compte influenceur',
  FIRSTNAME: 'Prénom',
  LASTNAME: 'Nom',
  BIRTHDAY: 'DD/MM/YYYY',
  USERNAME: 'Pseudonyme',
  PASSWORD: 'Mot de Passe',
  CONFIRM_PASSWORD: 'Confirmation de Mot de Passe',
  PHONE: 'Numéro de téléphone',
  EMAIL : 'Adresse e-mail',
  LOCATION: 'Localisation',
  CGU: 'J\'accepte les conditions générales d\'utilisation',

  REGISTER_BUTTON: 'S\'enregistrer',
  PREVIOUS: 'Précédent',
  NEXT: 'Suivant',

  ASSOCIATION: 'Associations',
  INFLUENCER: 'Influenceurs',
  COMMUNITY: 'Communauté',

  ASSOCIATION_TEXT: 'Venez découvrir quelles sont les associations que vous soutenez en participants aux différents événements.',
  INFLUENCER_TEXT: 'Chaques jours, des influenceurs organisent des événements caritatifs en faisant participer leurs communautés.',
  COMMUNITY_TEXT: 'En quelques clics, tentez votre chance et soutenez un événement caritatif.',
};
