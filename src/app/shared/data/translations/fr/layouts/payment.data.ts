export const data = {
  TITLE: 'Choisissez votre offre',
  PURCHASE: 'Acheter',
  CGU_TEXT: 'En cliquant sur acheter, vous acceptez les conditions générales de Share Your Dreams',
  EXPIRATION_DATE: 'Date d\'expiration',
  CARD_NUMBER: 'Numéro de carte',
  FULL_NAME: 'Prénom et nom',
  TOKENS: 'jetons',
  CREDIT_CARD: 'Carte de crédit',
  PAYPAL: 'Paypal'
};

