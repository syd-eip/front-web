export const data = {
  PREFERENCES_SECTION: 'Selon vos préférences',
  RECOMMENDED_SECTION: 'Evénements recommandés',
  RECENT_SECTION: 'Evénements les plus récents',
  POPULAR_SECTION: 'Evénements les plus populaires'
};
