export const data = {
    MENU_TITLE: "Informations à propos de SYD",
    LEGAL_MENTIONS: "Mentions légales",
    TERMS_OF_USE: "Conditions générales d'utilisation",
    TERMS_OF_SALE: "Conditions générales de vente",
};
