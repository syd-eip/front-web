export const data = {
  CONNECTION: 'Connexion',
  LOGIN: 'Adresse e-mail ou Pseudonyme',
  PASSWORD: 'Mot de passe',
  CONNECTION_BUTTON: 'Se connecter',
  FORGOT_PASSWORD: 'Mot de passe oublié ?',
  NO_ACCOUNT: 'Pas encore de compte ?',
  EMAIL: 'Adresse e-mail',
  SEND_BUTTON: 'Envoyer',
  FORGOT_PASSWORD_TEXT: 'Veuillez indiquer votre adresse email ci-dessous et nous vous enverrons un email pour ' +
    'réinitialiser votre mot de passe.',
  FORGOT_PASSWORD_CONTACT_TEXT: 'Si vous ne connaissez pas votre adresse électronique ou qu\'elle n\'est plus valide, ' +
    'veuillez nous contacter pour obtenir une aide supplémentaire.',
  SEND_SUCCESS: 'Envoyé avec succès !',
  SEND_SUCCESS_TEXT: 'Un e-mail de réinitialisation de mot de passe vous a été envoyé à l\'adresse',
  SEND_SUCCESS_PROBLEM_TEXT_PART1: 'Vous n\'avez pas reçu cette email ? Pensez à vérifier dans vos',
  SEND_SUCCESS_PROBLEM_TEXT_PART2: 'Si le problème persiste veuillez contacter la plateforme.',
  JUNK_MAIL: 'indésirables'
};
