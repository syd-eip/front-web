export const data = {
  ERROR_MESSAGE: 'La page que vous recherchez a peut-être été supprimée, eu son nom modifié ou est temporairement indisponible.',
  HOME: 'Retour à l\'accueil',
  NOT_FOUND: 'Page introuvable'
};
