export const data = {
  CREATE_EVENT: 'Créer un événement',
  FAVORITE: 'Ajouter aux favoris',
  SETTINGS: 'Paramètres',
  CURRENT_EVENTS: 'Événéments en cours',
  PAST_EVENTS: 'Événements passés',
  NO_CURRENT: 'Il n\'y a pas d\'événement.',
  NO_PAST: 'Il n\'y a pas d\'événement passé.'
};
