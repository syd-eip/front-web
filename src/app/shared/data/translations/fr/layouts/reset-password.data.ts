export const data = {
  TITLE: 'Réinitialiser mon mot de passe',
  PASSWORD: 'Mot de Passe',
  CONFIRM_PASSWORD: 'Confirmation de Mot de Passe',
  TEXT_INFORMATION: 'Veuillez saisir votre nouveau mot de passe. Attention, ce lien n\'est valide que pour une durée maximum de 24 heure !',
  RESET_BUTTON: 'Réinitialiser',
};
