export const data = {
  CONF_TITLE : 'Confirmation de votre achat',
  PAYMENT_METHOD : 'Méthode de paiement : ',
  PRICE : 'Prix : ',
  OFFER_NAME : 'Offre : ',
  CREDIT_CARD : 'Carte de crédit',
  WALLET : 'Votre porte-token : ',
  PRINT : 'Imprimer le reçu'
};
