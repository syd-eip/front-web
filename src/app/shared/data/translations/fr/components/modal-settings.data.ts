export const data = {
  SETTINGS: 'Paramètres',
  PERSONAL_INFOS: 'Informations personnelles',
  CHANGE_PASSWORD: 'Changer de mot de passe',
  FIRSTNAME: 'Prénom',
  LASTNAME: 'Nom',
  USERNAME: 'Pseudonyme',
  EMAIL: 'Courriel',
  BIRTHDAY: 'Anniversaire',
  PASSWORD: 'Nouveau mot de passe',
  CONFIRM_PASSWORD: 'Confirmer le nouveau mot de passe',
  SAVE: 'Sauvegarder',
  BANNER_CONSTRAINT : 'La banniére doit être du 1000x350 ou elle sera coupée.',
  CLOSE : 'Fermer'
};
