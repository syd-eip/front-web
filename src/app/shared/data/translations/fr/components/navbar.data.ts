export const data = {
  CONNECTION:   'Connexion',
  DECONNECTION:   'Déconnexion',
  LANGUAGES: 'Langues',
  FRENCH: 'Français',
  ENGLISH: 'Anglais',
  DISCOVER: 'Découverte',
  ASSOCIATIONS: 'Associations',
  COMPONENTS: 'Composants',
  WELCOME: 'Page d\'accueil de SYD',
  CREATE_EVENT: 'Créer un évènement',
  BALANCE: 'Consulter mon solde',
  BUY: 'Acheter des tokens',
  PROFILE: 'Profil',
  SETTINGS: 'Paramètres',
  SIGNOUT: 'Déconnexion',
  SEARCH: 'Rechercher'
};
