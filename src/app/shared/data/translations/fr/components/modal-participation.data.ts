export const data = {
  NUMBER_TOKENS_TITLE: 'Combien de jetons voulez-vous parier ?',
  NUMBER_TOKENS_PLACEHOLDER: 'Entrer le nombre de jetons',
  NUMBER_TOKENS: 'jetons disponibles',
  NUMBER_TOKEN: 'jeton disponible',
  INFLUENCER: 'Influenceur',
  CHARITY: 'Association',
  NOTE: 'Note : Il y a un minimum de 10% pour les deux parties',
  CGU: 'J\'accepte les termes et conditions',
  SAVE: 'Miser'
};
