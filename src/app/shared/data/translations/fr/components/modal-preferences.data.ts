export const data = {
  TITLE: 'Préférences utilisateur',
  DESCRIPTION: 'Coche les cases des domaines que tu aimes ! Nous mettrons en avant les événements susceptibles de te plaire',
  AREAS_SECTION: 'Domaines d\'intéret',
  INFLUENCERS_SECTION: 'Influenceurs',
  ASSOCIATIONS_SECTION: 'Associations',
  SAVE: 'Enregistrer',
  SAVED: 'Préférences utilisateur sauvegardées !',
  CLOSE: 'Fermer'
};
