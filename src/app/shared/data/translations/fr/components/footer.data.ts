export const data = {
  TEAM: 'Équipe',
  ABOUT_US: 'A propos',
  ADDRESS: 'Adresse',
  ADDRESS_STATUS: 'SYD - Share Your Dreams',
  ADDRESS_SYD: '85 Rue du Jardin Public, 33000, Bordeaux, Nouvelle-Aquitaine, France',
  GCU: 'Conditions générales d\'utilisation',
  GCS: 'Conditions générales de vente',
  LEGAL_MENTIONS: 'Mentions Légales'
};
