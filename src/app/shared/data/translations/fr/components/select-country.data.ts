export const data = {
  LOADING_TEXT: 'Chargement en cours...',
  NOT_FOUND_TEXT: 'Aucun pays trouvé',
  PLACEHOLDER: 'Sélectionner un pays'
};
