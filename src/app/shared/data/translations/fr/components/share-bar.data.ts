export const data = {
  SAVED_SUCCESS: 'L\'événement a bel et bien été sauvegardé dans vos favoris !',
  SHARING_TWITTER: 'Je partage sur Twitter un événement de la plateforme Share Your Dreams !'
};
