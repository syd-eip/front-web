export const data = {

  /**
   * @description This section contains all global user's errors
   */
  USER_NOT_AUTHENTICATED: 'Session expirée. Vous n\'êtes pas authentifié',
  USER_NOT_FOUND: 'L\'utilisateur n\'existe pas',
  USER_NOT_AUTHORIZED: 'Session expirée. Vous n\'êtes pas autorisé',

  /**
   * @description This section contains all user's role errors
   */
  ROLE_MISSING: 'Le rôle est requis',
  ROLE_NOT_EXIST: 'L\'un des rôles n\'existe pas',
  USER_IS_GUEST: 'Vous ne pouvez pas accéder au site Web',
  USER_NOT_USER: 'Vous n\'avez pas les droits utilisateur',
  USER_NOT_INFLUENCER: 'Vous n\'avez pas les droits influenceur',
  USER_NOT_ADMIN: 'Vous n\'avez pas les droits administrateur',
  USER_NOT_SUPER_ADMIN: 'Vous n\'avez pas les droits super administrateur',

  /**
   * @description This section contains all user's login errors
   */
  LOGIN_BAD: 'Nom d\'utilisateur ou adresse e-mail incorrects',
  LOGIN_MISSING: 'Nom d\'utilisateur ou adresse e-mail requis',

  /**
   * @description This section contains all user's password errors
   */
  PASSWORD_BAD: 'Mauvais mot de passe',
  PASSWORD_MISSING: 'Mot de passe requis',
  PASSWORD_MIN_LENGTH: 'Le mot de passe doit comporter au moins 8 caractères',
  PASSWORD_MAX_LENGTH: 'Le mot de passe doit être de 50 caractères maximum',
  PASSWORD_COMPOSITION: 'Doit contenir au moins un chiffre, un caractère spécial, une majuscule et une minuscule',
  CONFIRM_PASSWORD_MISSING: 'La confirmation de mot de passe est requise',
  CONFIRM_PASSWORD_MATCH: 'La confirmation du mot de passe ne correspond pas',
  NEW_PASSWORD_MATCH_OLD: 'Votre nouveau mot de passe ne peut pas correspondre à votre ancien mot de passe',

  /**
   * @description This section contains all user's username errors
   */
  USERNAME_ALREADY_USED: 'Nom d\'utilisateur déjà utilisé',
  USERNAME_MISSING: 'Nom d\'utilisateur requis',
  USERNAME_FORMAT: 'Le nom d\'utilisateur doit comporter au moins 3 caractères',
  USERNAME_BAD_FORMAT: 'Le format du nom d\'utilisateur est mauvais',

  /**
   * @description This section contains all user's first name errors
   */
  FIRST_NAME_MISSING: 'Prénom requis',
  FIRST_NAME_BAD_FORMAT: 'Le format du prénom est mauvais',

  /**
   * @description This section contains all user's last name errors
   */
  LAST_NAME_MISSING: 'Nom requis',
  LAST_NAME_BAD_FORMAT: 'Le format du nom est mauvais',

  /**
   * @description This section contains all user's birthday errors
   */
  BIRTHDAY_MISSING: 'Date d\'anniversaire requise',
  BIRTHDAY_BAD_FORMAT: 'Le format de la date d\'anniversaire n\'est pas valide',
  BIRTHDAY_NOT_LEGAL_AGE: 'Vous n\'avez pas l\'âge légal',
  BIRTHDAY_ENTERED_INVALID: 'La date d\'anniversaire saisie n\'est pas valide',

  /**
   * @description This section contains all user's email errors
   */
  EMAIL_BAD_FORMAT: 'Mauvais format d\'adresse e-mail',
  EMAIL_DOES_NOT_EXIST: 'L\'adresse e-mail n\'existe pas',
  EMAIL_MISSING: 'Adresse e-mail requise',
  EMAIL_ALREADY_USED: 'Adresse e-mail déjà utilisée',

  /**
   * @description This section contains all user's location errors
   */
  LOCATION_COUNTRY_CODE_MISSING: 'Le pays de résidence est requis',
  LOCATION_CITY_MISSING: 'La ville de résidence est requise',
  LOCATION_STREET_ADDRESS_MISSING: 'L\'adresse postale est requise',
  LOCATION_ZIP_CODE_MISSING: 'Le code postal est requis',
  LOCATION_BAD_FORMAT: 'Le format de l\'adresse de résidence est mauvais',
  LOCATION_COUNTRY_CODE_NOT_EXIST: 'Le pays de résidence n\'existe pas',

  /**
   * @description This section contains all user's phone number errors
   */
  PHONE_BAD_FORMAT: 'Le format du numéro de téléphone est mauvais',
  PHONE_ALREADY_USED: 'Numéro de téléphone déjà utilisé',
  PHONE_COUNTRY_CODE_MISSING: 'Indicatif du pays du numéro téléphone requis',
  PHONE_NUMBER_MISSING: 'Numéro de téléphone requis',

  /**
   * @description This section contains all event's errors
   */
  EVENT_NAME_MISSING: 'Nom de l\'événement requis',
  EVENT_NAME_BAD_FORMAT: 'Mauvais nom d\'événement',
  EVENT_NAME_ALREADY_USED: 'Nom de l\'événement déjà utilisé',
  EVENT_NAME_MIN_LENGTH: 'Le nom de l\'événement doit comporter au moins 3 caractères',
  EVENT_DATE_TO_EARLY: 'L\'événement ne peut pas être créé moins de 24 heures avant qu\'il n\'ait lieu',
  EVENT_DATE_MISSING: 'Date de l\'événement requise',
  EVENT_DATE_BAD_FORMAT: 'Format de la date de l\'événement n\'est pas valide',
  EVENT_DRAW_DATE_MISSING: 'Date de tirage au sort de l\'événement requise',
  EVENT_DRAW_DATE_BAD_FORMAT: 'Format de la date de tirage au sort de l\'événement n\'est pas valide',
  EVENT_DRAW_DATE_TO_EARLY: 'La date du tirage au sort est proche de la date de création de l\'événement',
  EVENT_ID_MISSING: 'ID de l\'événement requis',
  EVENT_NOT_FOUND: 'Événement non trouvé',
  EVENT_LOTTERIES_SIZE_MIN_LENGTH: 'La taille des lotteries doit être supérieur à 0',
  EVENT_FINISHED: 'L\'événement est déjà terminé',
  EVENT_OWN_EVENT: 'Un utilisateur ne peut pas participer à son propre événement',

  /**
   * @description This section contains all date's errors
   */
  DATE_BAD_FORMAT: 'Le format de date n\'est pas valide',
  DATE_ENTERED_INVALID: 'La date saisie n\'est pas valide',

  /**
   * @description This section contains all method's errors (not very useful in frontend part)
   */
  METHOD_NOT_ALLOWED: 'La méthode de requête n\'est pas autorisée',

  /**
   * @description This section contains the terms of service error
   */
  CGU_NOT_VALIDATED: 'Les termes et conditions doivent être validés',

  /**
   * @description This section contains all association's errors
   */
  ASSOCIATION_NAME_ALREADY_USED: 'Nom de l\'association déjà utilisé',
  ASSOCIATION_NAME_MISSING: 'Nom de l\'association requis',
  ASSOCIATION_MISSING: 'Nom de l\'association ou son ID requis',
  ASSOCIATION_NOT_FOUND: 'L\'association n\'existe pas',

  /**
   * @description This section contains all user favorite's errors
   */
  FAVORITE_TAG_MISSING: 'Tag favoris requis',
  FAVORITE_ASSOCIATION_MISSING: 'Association favorite requise',
  FAVORITE_USER_MISSING: 'Utilisateur favoris requis',

  /**
   * @description This section contains all images errors
   */
  IMAGE_NAME_MISSING: 'Nom de l\'image requis',
  IMAGE_NOT_FOUND: 'L\'image n\'existe pas',
  IMAGE_CORRUPTED: 'Image corrompue',

  /**
   * @description This section contains all JWT tokens errors
   */
  PARSING_TOKEN: 'Le parsing du token a échoué',
  TOKEN_INVALID: 'Le token est invalide',
  RESET_PASSWORD_TOKEN_MISSING: 'Reset-Token requis',

  /**
   * @description This section contains all lotteries errors
   */
  LOTTERY_NOT_FOUND: 'La lotterie n\'existe pas',
  BET_NOT_FOUND: 'Le paris n\'existe pas',
  BET_NEGATIVE_OR_NULL: 'La valeur du paris doit être supérieur à 0',
  BET_WRONG_SPLIT_RATIO: 'Le ratio de la mise entre l\'influenceur et l\'association doit être compris entre 10% et 90%',

  /**
   * @description This section contains all tags errors
   */
  TAG_NAME_BAD_FORMAT: 'Format du nom de tag n\'est pas valide',
  TAG_NAME_MIN_LENGTH: 'Le nom du tag doit comporter au moins 3 caractères',
  TAG_NAME_ALREADY_USED: 'Nom du tag déjà utilisé',
  TAG_NAME_MISSING: 'Nom du tag requis',
  TAG_NOT_FOUND: 'Le tag n\'existe pas',

  /**
   * @description This section contains all user tokens errors
   */
  VALUE_NEGATIVE_OR_NULL: 'Le nombre de tokens doit être supérieur à 0',
  USER_NOT_ENOUGH_TOKENS: 'L\'utilisateur ne possède pas assez de tokens',

  /**
   * @description This section contains all mailing errors
   */
  SUBJECT_MISSING: 'Sujet du mail requis',
  BODY_MISSING: 'Corps du mail requis',
  POLITENESS_FORMULA_MISSING: 'Formule de politesse du mail requis',
  SIGNATURE_MISSING: 'Signature du mail requis',
  RECEIVERS_MISSING: 'Destinataires du mail requis',
  RECEIVER_NOT_FOUND: 'Le mail du destinataire n\'existe pas',

  /**
   * @description This section contains all offers errors
   */
  OFFER_NAME_ALREADY_USED: 'Nom de l\'offre déjà utilisé',
  OFFER_NAME_MISSING: 'Nom de l\'offre requis',
  OFFER_NOT_FOUND: 'L\'offre n\'existe pas',
  OFFER_ID_MISSING: 'ID de l\'offre requis',
  OFFER_PRICE_NEGATIVE: 'Le prix de l\'offre doit être positif',
  OFFER_TOKENS_NEGATIVE_OR_NULL: 'Le nombre de tokens de l\'offre doit être strictement positif',
  OFFER_ADD_ERROR: 'Un problème est survenu lors de l\'achat de vos jetons',
  OFFER_LOAD_ERROR: 'Un problème est survenu lors du chargement des offres',

  /**
   * @description This section contains all the captcha errors
   */
  RECAPTCHA_TOKEN_MISSING: 'Recaptcha-Token requis',

  /**
   * @description This section contains all the search errors
   */
  SEARCH_QUERY_MISSING: 'Query de recherche requise',

  /**
   * @description This section contains all the metrics errors
   */
  METRICS_NOT_FOUND: 'Un problème est survenu lors de la récupération des metrics',
};
