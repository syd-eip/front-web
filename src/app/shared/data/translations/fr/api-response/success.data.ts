export const data = {

  /**
   * @description This section contains successful request message
   */
  OK: 'Requête réussie',

  /**
   * @description This sections contains all global user success messages
   */
  USER_CREATED: 'Création de compte réussite',
  USER_AUTHENTICATED: 'Connexion réussie',
  USER_DISCONNECTED: 'Déconnexion réussie',
  USER_DELETED: 'L\'utilisateur a correctement été supprimé',

  /**
   * @description This sections contains all username success messages
   */
  USERNAME_UPDATED: 'Le nom d\'utilisateur a correctement été mis à jour',
  USERNAME_FREE: 'Le nom d\'utilisateur n\'est pas utilisé par un autre utilisateur',

  /**
   * @description This sections contains all users email success messages
   */
  EMAIL_UPDATED: 'L\'adresse e-mail a correctement été mise à jour',
  EMAIL_FREE: 'L\'adresse e-mail n\'est pas utilisée par un autre utilisateur',

  /**
   * @description This sections contains all users phone number success messages
   */
  PHONE_NUMBER_UPDATED: 'Le numéro de téléphone a correctement été mis à jour',
  PHONE_NUMBER_FREE: 'Le numéro de téléphone n\'est pas utilisé par un autre utilisateur',

  /**
   * @description This sections contains all users password success messages
   */
  PASSWORD_UPDATED: 'Le mot de passe a correctement été mis à jour',
  PASSWORD_RESET: 'Le mot de passe a correctement été réinitialisé',

  /**
   * @description This sections contains all users names (first and last names) success messages
   */
  NAME_UPDATED: 'Les noms et prénoms ont correctement été mis à jour',

  /**
   * @description This sections contains all users location success messages
   */
  LOCATION_UPDATED: 'Le lieu de réisdence a correctement été mis à jour',

  /**
   * @description This sections contains all users birthday success messages
   */
  BIRTHDAY_UPDATED: 'L\'anniversaire a correctement été mis à jour',

  /**
   * @description This sections contains all role success messages
   */
  ROLE_ADDED: 'Le rôle de l\'utilisateur a correctement été ajouté',
  ROLE_REMOVED: 'Le rôle de l\'utilisateur a correctement été supprimé',

  /**
   * @description This sections contains all associations success messages
   */
  ASSOCIATION_CREATED: 'Création de l\'association réussite',
  ASSOCIATION_DELETED: 'L\'association a correctement été supprimée',
  ASSOCIATION_NAME_UPDATED: 'Le nom de l\'association a correctement mise à jour',
  ASSOCIATION_DESCRIPTION_UPDATED: 'La description de l\'association a correctement mise à jour',
  ASSOCIATION_LINK_UPDATED: 'Le lien du site web de l\'association a correctement mise à jour',

  /**
   * @description This sections contains all events success messages
   */
  EVENT_CREATED: 'Création de l\'événement réussite',
  EVENT_DELETED: 'L\'événement a correctement été supprimé',
  EVENT_NAME_UPDATED: 'Le nom de l\'événement a correctement été mise à jour',
  EVENT_DESCRIPTION_UPDATED: 'La description de l\'événement a correctement été mise à jour',
  EVENT_TAGS_UPDATED: 'Les tags de l\'événement ont correctement été mise à jour',

  /**
   * @description This sections contains all lotteries success messages
   */
  BET_ADDED: 'Votre mise a correctement été prise en compte',

  /**
   * @description This sections contains all user's favorite success messages
   */
  FAVORITE_TAGS_UPDATED: 'Les tags favoris ont correctement été mise à jour',
  FAVORITE_USERS_UPDATED: 'Les utilisateurs favoris ont correctement été mise à jour',
  FAVORITE_ASSOCIATIONS_UPDATED: 'Les associations favoris ont correctement été mise à jour',

  /**
   * @description This sections contains all images success messages
   */
  IMAGE_UPDATED: 'L\'image a correctement été mise à jour',

  /**
   * @description This sections contains all tags success messages
   */
  TAG_CREATED: 'Création du tag réussite',
  TAG_DELETED: 'Le tag a correctement été supprimé',

  /**
   * @description This sections contains all user's tokens success messages
   */
  TOKENS_ADDED: 'Les jetons ont correctement été ajouté',
  TOKENS_REMOVED: 'Les jetons ont correctement été supprimé',

  /**
   * @description This section contains all offers success messages
   */
  OFFER_CREATED: 'Création de l\'offre réussite',
  OFFER_DELETED: 'L\'offre a correctement été supprimé',
  OFFER_NAME_UPDATED: 'Le nom de l\'offre a correctement été mise à jour',
  OFFER_PRICE_UPDATED: 'Le prix de l\'offre a correctement été mise à jour',
  OFFER_TOKENS_UPDATED: 'Le nombre de tokens de l\'offre a correctement été mise à jour'
};

