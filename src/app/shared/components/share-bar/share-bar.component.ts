import {Component, Input, OnInit} from '@angular/core';
import {TranslationService} from '../../services/translation/translation.service';
import {TranslatableClass} from '../../services/translation/translation-class/translatable.class';
import {UserService} from '../../services/web/user.service';
import {UserClass} from '../../classes/models/user.class';
import {AlertService} from '../../services/alert/alert.service';

@Component({
  selector: 'app-share-bar',
  templateUrl: './share-bar.component.html'
})
export class ShareBarComponent extends TranslatableClass implements OnInit {

  /**
   * @description user's login status
   */
  private _connected: boolean;

  /**
   * @description User information model
   */
  private _user: any;

  /**
   * @description Event page information to share on social network
   */
  private _eventPage: any;

  /**
   * @description Event model displayed in the component template
   */
  @Input() public event: any

  /**
   * @description Constructor of ShareBarComponent component
   *
   * The constructor creates an instance of the NavbarComponent component and specifies the default values
   * the input and output variables of the component.
   *
   * @param translationService A reference to the translation service of the application
   * @param userService A reference to the user service of the application
   * @param alertService A reference to the notification service of the application
   */
  constructor(translationService: TranslationService, private userService: UserService, private alertService: AlertService) {
    super(translationService);

    this.eventPage = {facebook: null, twitter: null};
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.userService.connectedSubject.subscribe(value => {
      this.connected = value;
    });
    this.userService.isAuthenticated();

    this.user = (this.userService.userSubject.last === null) ? null : this.userService.userSubject.last;
    this.eventPage = {
      facebook: 'https://www.facebook.com/sharer/sharer.php?u=' + encodeURI(document.location.href + 'event/' + this.event.id) + '&t=' +
        encodeURI(this.event.information.name),
      twitter: 'http://twitter.com/share?text=' + encodeURI(this.translate('SHARE_BAR/SHARING_TWITTER')) + '&url=' +
        encodeURI(document.location.href + 'event/' + this.event.id)
    };
  }

  /**
   * This method is used to define whether the event is saved in the user's favourites.
   */
  public saveEvent(): void {
    if (this.user && this.connected) {
      if ('events' in (this.user.favorites as Array<any>)) {
        this.user.favorites['events'] = [...this.user.favorites['events'], this.event.id];
      } else {
        this.user.favorites['events'] = [];
        this.user.favorites['events'] = [...this.user.favorites['events'], this.event.id];
      }
      this.alertService.success(this.translate('SHARE_BAR/SAVED_SUCCESS'));
    }
  }

  /**
   * This method is used to define whether the event is saved in the user's favourites.
   */
  public isEventSaved(): boolean {
    if (this.user && this.connected && 'events' in (this.user.favorites as Array<any>)) {
      return this.user.favorites['events'].find(item => item === this.event.id);
    }
    return false;
  }

  /**
   * @description The method allows you to retrieve the user's login status
   */
  get connected(): boolean {
    return this._connected;
  }

  /**
   * @description The method allows you to assign a boolean representing the user's login status
   *
   * @param value The new assigned value for a boolean representing the user's login status
   */
  set connected(value: boolean) {
    this._connected = value;
  }

  /**
   * @description The method allows you to retrieve user information
   */
  get user(): UserClass {
    return this._user;
  }

  /**
   * @description The method allows you to assign the user information
   *
   * @param value The new assigned value for the user information
   */
  set user(value: UserClass) {
    this._user = value;
  }

  /**
   * @description The method allows you to retrieve the event page information to share on social network
   */
  get eventPage(): any {
    return this._eventPage;
  }

  /**
   * @description The method allows you to assign the event page information to share on social network
   *
   * @param value The new assigned event page information to share on social network
   */
  set eventPage(value: any) {
    this._eventPage = value;
  }
}
