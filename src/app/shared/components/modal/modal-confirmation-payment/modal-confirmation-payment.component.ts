import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {ModalService} from '../../../services/modal/modal.service';
import {TranslationService} from '../../../services/translation/translation.service';
import {TranslatableClass} from '../../../services/translation/translation-class/translatable.class';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService} from '../../../services/alert/alert.service';
import {Observable, Subscription} from 'rxjs';
import {NgxImageCompressService} from 'ngx-image-compress';
import {Router} from '@angular/router';
import {UserService} from '../../../services/web/user.service';

@Component({
  selector: 'app-modal-confirmation-payment',
  templateUrl: './modal-confirmation-payment.component.html'
})
export class ModalConfirmationPaymentComponent extends TranslatableClass implements OnInit, OnDestroy {

  /**
   * @description Array of Subscription objects that have an unsubscribe() method, which you call to stop
   * receiving event about offline and online status
   */
  private subscriptions: Array<Subscription>;

  /**
   * @description Offer selected by the user used for the confirmation page
   */
  public offerConfirmed: any;

  /**
   * @description Offer selected by the user used for the confirmation page
   */
  public previousWallet: number;

  /**
   * @description Payment method used (1: Paypal, 2: Credit Card)
   */
  public paymentMethod: number;

  /**
   * @description Card Number used to pay tokens if payment method is 2)
   */
  public cardNumber: string;

  /**
   * @description ModalParticipationComponent's constructor
   *
   * The constructor creates an instance of the ModalParticipationComponent component and specifies the default values of the component's
   * input variables
   *
   * @param modalService A reference to the ModalService service of the application
   * @param translationService A reference to the TranslationService service of the application
   * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
   * @param alertService A reference to the application's alert service
   * @param userService A reference to the http request service of the application
   * @param imageCompress A reference to the NgxImageCompressService of the application
   * @param router A reference to the router of the application
   */
  constructor(private modalService: ModalService, translationService: TranslationService, private formBuilder: FormBuilder,
              private alertService: AlertService, public userService: UserService, private imageCompress: NgxImageCompressService,
              private router: Router) {
    super(translationService);

    this.subscriptions = [];
  }

  /**
   * @description: This method is part of the component life cycle: lifecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    console.log(this.offerConfirmed)
  }

  /**
   * @description: This method is part of the component life cycle: lifecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional deletion tasks.
   */
  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  /**
   * @description This method is used to know the size of the window and therefore whether it is a desktop or mobile format
   */
  isMobile(): boolean {
    return window.innerWidth <= 992;
  }

  /**
   * @description The method closes the open modal. The result of this closure can be recovered as a Promise thanks to NgbModalRef.result
   */
  close(): void {
    this.modalService.close();
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }


}
