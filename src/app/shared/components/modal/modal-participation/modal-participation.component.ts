import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {ModalService} from '../../../services/modal/modal.service';
import {TranslationService} from '../../../services/translation/translation.service';
import {TranslatableClass} from '../../../services/translation/translation-class/translatable.class';
import {BetClass} from '../../../classes/models/bet.class';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidatorsClass} from '../../../classes/validators/custom-validators.class';
import {POST_EVENT_BET} from '../../../data/api/api-path.data';
import {Semaphore} from '../../../classes/semaphore/semaphore.class';
import {AlertService} from '../../../services/alert/alert.service';
import {Subscription} from 'rxjs';
import {UserService} from '../../../services/web/user.service';
import { NetworkResponseClass } from '../../../services/network/network-response/network-response.class';

@Component({
  selector: 'app-modal-participation',
  templateUrl: './modal-participation.component.html'
})
export class ModalParticipationComponent extends TranslatableClass implements OnInit, OnDestroy, AfterViewInit {

  /**
   * @description Event id (used to create a bet)
   */
  public id: string;

  /**
   * @description Event name (used to be displayed)
   */
  public name: string;

  /**
   * @description Data model to be sent to the API to create a bet
   */
  public data: BetClass;

  /**
   * @description
   */
  private _ratio: { ratioInfluencer: number, ratioAssociation: number };

  /**
   * @description Component that tracks the value and validity state of a group of FormControl instances.
   */
  private _formGroup: FormGroup;

  /**
   * @description Number of user tokens
   */
  private _tokens: number;

  /**
   * @description Array of Subscription objects that have an unsubscribe() method, which you call to stop
   * receiving event about offline and online status
   */
  private subscriptions: Array<Subscription>;

  /**
   * @description ModalParticipationComponent's constructor
   *
   * The constructor creates an instance of the ModalParticipationComponent component and specifies the default values of the component's
   * input variables
   *
   * @param modalService A reference to the ModalService service of the application
   * @param translationService A reference to the TranslationService service of the application
   * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
   * @param alertService A reference to the application's alert service
   * @param userService A reference to the http request service of the application
   */
  constructor(private modalService: ModalService, translationService: TranslationService, private formBuilder: FormBuilder,
              private alertService: AlertService, private userService: UserService) {
    super(translationService);

    this.subscriptions = [];
    this.data = new BetClass();
  }

  /**
   * @description: This method is part of the component life cycle: lifecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    (document.getElementsByClassName('modal-content')[0] as HTMLElement).style.height = '575px';

    this.tokens = this.userService.userSubject.last.wallet;
    this.ratio = {ratioAssociation: 10, ratioInfluencer: 10};

    this.initForm();
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular has finished to initialize the view of a component.
   * Defining an ngAfterViewInit() method allows you to manage any additional initialization tasks.
   */
  ngAfterViewInit(): void {
    this.updateRatio('slider-influencer', this.ratio.ratioInfluencer);
    this.updateRatio('slider-charity', this.ratio.ratioAssociation);
  }

  /**
   * @description: This method is part of the component life cycle: lifecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional deletion tasks.
   */
  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  /**
   * @description This method is used to know the size of the window and therefore whether it is a desktop or mobile format
   */
  isMobile(): boolean {
    return window.innerWidth <= 992;
  }

  /**
   * @description This method is called when a change in value is detected.
   *
   * @param event Ratio value
   * @param slider Slider (nouislider) identifier
   */
  change(event: number, slider: string): void {
    if (slider === 'slider-influencer') {
      this.updateRatio('slider-charity', 100 - event);
    } else {
      this.updateRatio('slider-influencer', 100 - event);
    }
    this.updateRatio(slider, event);
  }

  /**
   * @description The method closes the open modal. The result of this closure can be recovered as a Promise thanks to NgbModalRef.result
   */
  close(): void {
    this.modalService.close();
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  /**
   * @description The method save the data filled by an user
   */
  save(): void {
    const path: string = POST_EVENT_BET.replace('(?id?)', this.id);

    this.data.value = parseInt(this.data.value, 10);
    this.data.splitRatio = this.ratio.ratioAssociation / 100;

    this.userService.activeLoader(new Semaphore(1));
    this.userService.post(this.data, path)
      .then(response => {
        this.userService.update();
        this.subscriptions.push(this.alertService.success(this.translate(response.responseData.message)).subscribe(() => {
          this.close();
        }));
      })
      .catch((res) => {
        if (res instanceof NetworkResponseClass) {
          this.subscriptions.push(this.alertService.error(this.translate(res.responseError.message)).subscribe(() => {
            this.close();
          }));
        } else {
          this.subscriptions.push(this.alertService.error(this.translate('ERROR/BET_GOLBAL')).subscribe(() => {
            this.close();
          }));
        }
      });
  }

  /**
   * @description This method allows to format the user's setting if the user enters characters other than numbers
   *
   * @param event Bet provided by the user
   */
  replaceBet(event: any): void {
    event.target.value = event.target.value
      .replace(/[^\d]/g, '');

    if (parseInt(event.target.value, 10) > this.tokens) {
      event.target.value = this.tokens
    } else if (parseInt(event.target.value, 10) < 0) {
      event.target.value = 0
    }
    this.data.value = event.target.value;
  }

  /**
   * @description This method is used to initialize the form of the component
   * It creates a form by specifying the rules to be respected to validate user inputs
   */
  private initForm(): void {
    this.formGroup = this.formBuilder.group(
      {
        bet: [null, Validators.compose([
          Validators.required,
          Validators.min(1),
          Validators.max(this.tokens)
        ])],
        cgu: [null, Validators.compose([
          Validators.required,
        ])]
      },
      {
        validators: [
          CustomValidatorsClass.checkboxRequiredChoiceValidator
        ]
      }
    );
  }

  /**
   * @description This method is used to modify the HMTL behavior of the NouiSliderComponent
   *
   * @param slider Slider (nouislider) identifier
   * @param value Ratio value
   */
  private updateRatio(slider: string, value: number): void {
    const connect: HTMLElement = document.getElementById(slider).childNodes[0].childNodes[0].childNodes[0] as HTMLElement;
    const origin: HTMLElement = document.getElementById(slider).childNodes[0].childNodes[0].childNodes[1] as HTMLElement;

    if (value >= 90) {
      connect.style.width = '90%';
      origin.style.transform = 'translate(-10%, 0px)';

      this.updateRatioValue(slider, 90);
    } else if (value <= 10) {
      connect.style.width = '10%';
      origin.style.transform = 'translate(-90%, 0px)';

      this.updateRatioValue(slider, 10);
    } else {
      connect.style.width = value + '%';

      this.updateRatioValue(slider, value);
    }
  }

  /**
   * @description This method is used to assign the value of the ratio between influence and association.
   * It takes into account the values of the sliders
   *
   * @param slider Slider (nouislider) identifier
   * @param value Ratio value
   */
  private updateRatioValue(slider: string, value: number): void {
    if (slider === 'slider-influencer') {
      this.ratio.ratioInfluencer = value;
    } else {
      this.ratio.ratioAssociation = value;
    }
  }

  /**
   * @description The method allows you to retrieve the component that tracks the value and validity state of a group of
   * FormControl instances
   */
  get formGroup(): FormGroup {
    return this._formGroup;
  }

  /**
   * @description The method allows you to assign the component that tracks the value and validity state of a group of
   * FormControl instances
   *
   * @param value Value of the new component that tracks the value and validity state of a group of FormControl instances
   */
  set formGroup(value: FormGroup) {
    this._formGroup = value;
  }

  /**
   * @description The method allows you to retrieve the tokens of the connected user
   */
  get tokens(): number {
    return this._tokens;
  }

  /**
   * @description The method allows you to assign the tokens of the connected user
   *
   * @param value Value of the new tokens of the connected user
   */
  set tokens(value: number) {
    this._tokens = value;
  }

  /**
   * @description The method allows you to retrieve the ratio between the influencer and the association
   */
  get ratio(): { ratioInfluencer: number; ratioAssociation: number } {
    return this._ratio;
  }

  /**
   * @description The method allows you to assign the ratio between the influencer and the association
   *
   * @param value Value of the new ratio between the influencer and the association
   */
  set ratio(value: { ratioInfluencer: number; ratioAssociation: number }) {
    this._ratio = value;
  }
}
