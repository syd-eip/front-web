import {Component, OnDestroy, OnInit} from '@angular/core';
import {ModalService} from '../../../services/modal/modal.service';
import {TranslationService} from '../../../services/translation/translation.service';
import {TranslatableClass} from '../../../services/translation/translation-class/translatable.class';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidatorsClass} from '../../../classes/validators/custom-validators.class';
import {
  PATCH_BIRTHDAY,
  PATCH_EMAIL,
  PATCH_IMAGE, PATCH_LOCATION,
  PATCH_NAME,
  PATCH_PASSWORD,
  PATCH_PHONE,
  PATCH_USERNAME
} from '../../../data/api/api-path.data';
import {AlertService} from '../../../services/alert/alert.service';
import {Observable, Subscription} from 'rxjs';
import {UserClass} from '../../../classes/models/user.class';
import moment from 'moment';
import {NameClass} from '../../../classes/models/name.class';
import {ChangePassword} from '../../../classes/models/change-password.class';
import {UtilsClass} from '../../../classes/utils/utils.class';
import {FormErrorClass} from '../../../classes/forms/form-error.class';
import {CustomSubjectClass} from '../../../classes/rxjs-overrides/custom-subject.class';
import {Router} from '@angular/router';
import {UserService} from '../../../services/web/user.service';
import {environment} from '../../../../../environments/environment';
import {LocationApi} from '../../../classes/models/location-api.class';

@Component({
  selector: 'app-modal-settings',
  templateUrl: './modal-settings.component.html'
})
export class ModalSettingsComponent extends TranslatableClass implements OnInit, OnDestroy {

  /**
   * @description Component that tracks the value and validity state of a group of FormControl instances.
   */
  private _formGroup: FormGroup;

  /**
   * @description Array of Subscription objects that have an unsubscribe() method, which you call to stop
   * receiving event about offline and online status
   */
  private subscriptions: Array<Subscription>;

  /**
   * @description User class object that represent the user as me
   */
  private _me: UserClass

  /**
   * @description User class object that represent the user as me that's going to be modified
   */
  private _meModified: UserClass

  /**
   * @description Map of images used in this component
   */
  private _images: Map<string, string>;

  /**
   * @description Object use to change the password and send it to the api
   */
  private _password: ChangePassword;

  /**
   * @description Object used to change the phone with the ngx tel component
   */
  private _phone: any;

  /**
   * @description form's error observable
   */
  private _errorObservable: Observable<string>;

  /**
   * @description form's error parameters
   */
  private _errorGroup: Array<FormErrorClass>;

  /**
   * @description ModalParticipationComponent's constructor
   *
   * The constructor creates an instance of the ModalParticipationComponent component and specifies the default values of the component's
   * input variables
   *
   * @param modalService A reference to the ModalService service of the application
   * @param translationService A reference to the TranslationService service of the application
   * @param formBuilder A reference to provides syntactic sugar that shortens creating instances of a FormControl, FormGroup, or FormArray
   * @param alertService A reference to the application's alert service
   * @param userService A reference to the http request service of the application
   * @param router A reference to the router of the application
   */
  constructor(private modalService: ModalService, translationService: TranslationService, private formBuilder: FormBuilder,
              private alertService: AlertService, private userService: UserService, private router: Router) {
    super(translationService);

    this.subscriptions = [];
  }

  /**
   * @description: This method is part of the component life cycle: lifecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.errorObservable = new CustomSubjectClass<string>().asObservable();
    this.initForm();
    this.errorObservable.subscribe(value => (value) ? this.manageError(value) : null);

    this.me = this.userService.userSubject.last;
    this.setImages();


    this.password = new ChangePassword()


    this.meModified = {...this.me};
    this.meModified.birthday = moment(new Date(this.meModified.birthday)).format('DD/MM/YYYY');
    this.phone = {...this.me.phone}
  }

  /**
   * @description: This method is part of the component life cycle: lifecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional deletion tasks.
   */
  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  setImages(): void {
    this.images = new Map<string, string>();

    this.images.set('background', this.me.images.find(item => item.name === 'background') ?
      environment.apiHost + environment.apiVersion + '/image/id/' + this.me.images.find(item => item.name === 'background').content :
      '/assets/img/default-banner.jpg');
    this.images.set('profile', this.me.images.find(item => item.name === 'profile') ?
      environment.apiHost + environment.apiVersion + '/image/id/' + this.me.images.find(item => item.name === 'profile').content :
      '/assets/img/account.png');
  }

  /**
   * @description This method is used to know the size of the window and therefore whether it is a desktop or mobile format
   */
  isMobile(): boolean {
    return window.innerWidth <= 992;
  }

  /**
   * @description The method closes the open modal. The result of this closure can be recovered as a Promise thanks to NgbModalRef.result
   */
  close(): void {
    this.modalService.close();
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  /**
   * @description This method is used to warn the user of an error
   *
   * @param value Value of the HTML element
   */
  errorOccurred(value: HTMLInputElement): void {
    if (value.name === 'firstname') {
      this.errorsFirstnameOccured();
    } else if (value.name === 'lastname') {
      this.errorsLastnameOccured()
    } else if (value.name === 'username') {
      this.errorsUsernameOccurred();
    } else if (value.name === 'birthday') {
      this.errorsBirthdayOccurred();
    } else if (value.name === 'email') {
      this.errorsEmailOccurred();
    }
  }

  /**
   * @description This method allows you to load an image when creating an event
   */
  loadImage(event: Event, name: string) {
    const parent = this;
    const reader = new FileReader();

    reader.onloadend = function (event) {
      parent.images.set(name, reader.result.toString());
    };
    reader.readAsDataURL((event.target as HTMLInputElement).files[0]);
  }

  /**
   * @description This method checks the integrity of the password if modified
   * @return true if password is ok else false
   */
  checkPasswords(): boolean {
    if (this.password.password.length >= 8 && this.password.password.length <= 50) {
      const hasNumber = /\d/;
      const hasCapital = /[A-Z]/;
      const hasSmall = /[a-z]/;
      const hasSpecial = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

      const number = this.password.password.match(hasNumber)
      const capital = this.password.password.match(hasCapital)
      const small = this.password.password.match(hasSmall)
      const special = this.password.password.match(hasSpecial)

      return !(number.length === 0 || capital.length === 0 || small.length === 0 || special.length === 0);
    }
  }

  /**
   * @description This method is used to send the modifications to the api
   */
  save(): void {

    const apiErrorList: Array<string> = [];
    let formValid: boolean = true;

    if (this.password.password.length !== 0 && this.password.confirmPassword.length !== 0) {
      if (this.password.password === this.password.confirmPassword) {
        if (this.checkPasswords()) {
          this.userService.patch(this.password, PATCH_PASSWORD).then(res => {
          })
        } else {
          this.alertService.error('Your password must be minimum 8 characters long, maximum 50, have a special, a small and a' +
            'capital case character.');
          formValid = false;
        }
      } else {
        this.alertService.error('Your password does not match.');
        formValid = false;
      }
    }

    if (formValid) {

      this.images.forEach((value, key) => {

        if (value.slice(0, 4) === 'data') {
          const content = value.split(',')[1];

          this.userService.patch({name: key, content: content}, PATCH_IMAGE)
            .then(response => {

              if (document.getElementById('influencer-layout-header')) {
                document.getElementById('influencer-layout-header').style.backgroundImage = 'url(' + this.images.get('background') + ')';
              }

              document.getElementById('influencer-layout-bg').style.backgroundImage = 'url(' + this.images.get('background') + ')';
              (document.getElementById('influencer-layout-profile') as HTMLImageElement).src = this.images.get('profile');

              this.images.set(key, environment.apiHost + environment.apiVersion + '/image/id/' +
                response.responseData.data.find(item => item.name === key).content);

              this.userService.update();
            })
            .catch(() => {
              this.alertService.error('Error occured during image loading');
            });
        }
      });

      if (this.meModified.firstName !== this.me.firstName || this.meModified.lastName !== this.me.lastName) {
        this.userService.patch(new NameClass(this.meModified.firstName, this.meModified.lastName), PATCH_NAME).then(res => {
        }).catch(() => {
          apiErrorList.push('Firstname & Lastname')
        })
      }

      if (this.meModified.username !== this.me.username) {
        this.userService.patch(new Object({username: this.meModified.username}), PATCH_USERNAME).then(res => {
        }).catch(() => {
          apiErrorList.push('Username')
        })
      }

      if (this.meModified.email !== this.me.email) {
        this.userService.patch(new Object({email: this.meModified.email}), PATCH_EMAIL).then(res => {
        }).catch(() => {
          apiErrorList.push('Email')
        })
      }

      if (this.phone.number !== this.me.phone) {
        this.userService.patch(this.phone.number, PATCH_PHONE).then(res => {
        }).catch(() => {
          apiErrorList.push('Phone')
        })
      }

      const date = new Date(this.me.birthday)
      this.me.birthday = moment(date).format('DD/MM/YYYY')
      if (this.meModified.birthday !== this.me.birthday) {
        this.userService.patch(new Object(
          {birthday: UtilsClass.dateToISOString(this.meModified.birthday)}), PATCH_BIRTHDAY).then(res => {
        }).catch(() => {
          apiErrorList.push('Birthday')
        })
      }

    }

    if (this.meModified.location !== this.me.location) {
      this.userService.patch(new LocationApi(this.meModified.location.countryCode), PATCH_LOCATION).then((res) => {
        console.log(res)
      })
        .catch((err) => {
          console.log(err)
        })
    }

  }

  /**
   * @description This method intercepts the user's country selection and assigns it to the data model.
   *
   * @param data Country's data
   */
  countryChange(data: any): void {
    this.meModified.location = data;
  }

  /**
   * @description This method allows you to format the date in the format'DD/MM/YYY'.
   * It refuses any characters other than numbers and '/'.
   * It respects the pattern {2}/{2}/{4}.
   */
  replaceDateValue(event: any): void {
    event.target.value = event.target.value
      .replace(/^(\d\d)(\d)$/g, '$1/$2')
      .replace(/^(\d\d\/\d\d)(\d+)$/g, '$1/$2')
      .replace(/[^\d\/]/g, '');
  }

  /**
   * @description This method returns the value of the focus of an HTML element
   *
   * @param value HTMLElement element
   */
  isFocused(value: HTMLElement): boolean {
    return document.activeElement === value;
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular has finished to initialize the view of a component.
   * Defining an ngAfterViewInit() method allows you to manage any additional initialization tasks.
   */
  initForm(): void {
    const regExp = new RegExp('^((?:\\w|[\\-_ ](?![\\-_ ])|[\\u00C0\\u00C1\\u00C2\\u00C3\\u00C4\\u00C5\\u00C6\\u00C7\\u00C8\\u00C9' +
      '\\u00CA\\u00CB\\u00CC\\u00CD\\u00CE\\u00CF\\u00D0\\u00D1\\u00D2\\u00D3\\u00D4\\u00D5\\u00D6\\u00D8\\u00D9\\u00DA\\u00DB\\u00DC' +
      '\\u00DD\\u00DF\\u00E0\\u00E1\\u00E2\\u00E3\\u00E4\\u00E5\\u00E6\\u00E7\\u00E8\\u00E9\\u00EA\\u00EB\\u00EC\\u00ED\\u00EE\\u00EF' +
      '\\u00F0\\u00F1\\u00F2\\u00F3\\u00F4\\u00F5\\u00F6\\u00F9\\u00FA\\u00FB\\u00FC\\u00FD\\u00FF\\u0153])+)$', 'i');

    this.errorGroup = [
      {name: 'firstname', error: ''},
      {name: 'lastname', error: ''},
      {name: 'username', error: ''},
      {name: 'birthday', error: ''},
      {name: 'email', error: ''},
      {name: 'phone', error: ''},
    ];
    this.formGroup = this.formBuilder.group(
      {
        firstname: [null, Validators.compose([
          Validators.required,
          Validators.pattern(regExp)
        ])
        ],
        lastname: [null, Validators.compose([
          Validators.required,
          Validators.pattern(regExp)
        ])
        ],
        username: [null, Validators.compose([
          Validators.required,
          Validators.minLength(3)
        ])
        ],
        birthday: [null, Validators.compose([
          Validators.required
        ])
        ],
        email: [null, Validators.compose([
          Validators.required,
          Validators.email
        ])
        ],
        phone: [null, Validators.compose([
          Validators.required
        ])
        ],
      }, {
        validators: [
          CustomValidatorsClass.dateMatchValidator,
          CustomValidatorsClass.dateRangeValidator,
          CustomValidatorsClass.dateLegalValidator
        ]
      }
    );
  }

  /**
   * @description This method is used to warn the user of an error returned by the API
   *
   * @param value error string send by the API
   */
  manageError(value: string): void {
    if (value.slice(6, 16) === 'FIRST_NAME') {
      this.errorGroup[0].error = value;
    } else if (value.slice(6, 15) === 'LAST_NAME') {
      this.errorGroup[1].error = value;
    } else if (value.slice(6, 14) === 'USERNAME') {
      this.errorGroup[2].error = value;
    }
  }

  /**
   * @description This method is used to warn the user of an error concerning the first name
   */
  errorsFirstnameOccured(): void {
    if (this.formGroup.controls.firstname.errors !== null) {
      if (this.formGroup.controls.firstname.errors.required !== undefined) {
        this.errorGroup[0].error = 'ERROR/FIRST_NAME_MISSING';
      } else {
        this.errorGroup[0].error = 'ERROR/FIRST_NAME_BAD_FORMAT';
      }
    } else {
      this.errorGroup[0].error = '';
    }
  }

  /**
   * @description This method is used to warn the user of an error concerning the email address
   */
  errorsPhoneOccurred(): void {
    if (this.formGroup.controls.phone.errors !== null) {
      if (this.formGroup.controls.phone.errors.required !== undefined) {
        this.errorGroup[5].error = 'ERROR/PHONE_NUMBER_MISSING';
      } else if (this.formGroup.controls.phone.errors.validatePhoneNumber !== undefined &&
        this.formGroup.controls.phone.errors.validatePhoneNumber.valid === false) {
        this.errorGroup[5].error = 'ERROR/PHONE_BAD_FORMAT';
      }
    } else {
      this.errorGroup[5].error = '';
    }
  }

  /**
   * @description This method is used to warn the user of an error concerning the location's country
   */
  errorsLocationOccurred(): void {
    if (this.formGroup.controls.location.errors !== null) {
      if (this.formGroup.controls.location.errors.required !== undefined) {
        this.errorGroup[6].error = 'ERROR/LOCATION_COUNTRY_CODE_MISSING';
      }
    } else if (this.formGroup.errors !== null && this.formGroup.errors.location !== undefined) {
      this.errorGroup[6].error = 'ERROR/LOCATION_COUNTRY_CODE_NOT_EXIST';
    } else {
      this.errorGroup[6].error = '';
    }
  }

  /**
   * @description This method is used to warn the user of an error concerning the email address
   */
  errorsEmailOccurred(): void {
    if (this.formGroup.controls.email.errors !== null) {
      if (this.formGroup.controls.email.errors.required !== undefined) {
        this.errorGroup[4].error = 'ERROR/EMAIL_MISSING';
      } else {
        this.errorGroup[4].error = 'ERROR/EMAIL_BAD_FORMAT';
      }
    } else {
      this.errorGroup[4].error = '';
    }
  }

  /**
   * @description This method is used to warn the user of an error concerning the username
   */
  errorsUsernameOccurred(): void {
    if (this.formGroup.controls.username.errors !== null) {
      if (this.formGroup.controls.username.errors.required !== undefined) {
        this.errorGroup[2].error = 'ERROR/USERNAME_MISSING';
      } else {
        this.errorGroup[2].error = 'ERROR/USERNAME_FORMAT';
      }
    } else {
      this.errorGroup[2].error = '';
    }
  }

  /**
   * @description This method is used to warn the user of an error concerning the last name
   */
  errorsLastnameOccured(): void {
    if (this.formGroup.controls.lastname.errors !== null) {
      if (this.formGroup.controls.lastname.errors.required !== undefined) {
        this.errorGroup[1].error = 'ERROR/LAST_NAME_MISSING';
      } else {
        this.errorGroup[1].error = 'ERROR/LAST_NAME_BAD_FORMAT';
      }
    } else {
      this.errorGroup[1].error = '';
    }
  }

  /**
   * @description This method is used to warn the user of an error concerning the birthday
   */
  errorsBirthdayOccurred(): void {
    if (this.formGroup.controls.birthday.errors !== null) {
      this.errorGroup[3].error = 'ERROR/BIRTHDAY_MISSING';
    } else if (this.formGroup.errors !== null) {
      if (this.formGroup.errors.dateMatch === true) {
        this.errorGroup[3].error = 'ERROR/BIRTHDAY_BAD_FORMAT';
      } else if (this.formGroup.errors.dateRange === true) {
        this.errorGroup[3].error = 'ERROR/DATE_ENTERED_INVALID';
      } else if (this.formGroup.errors.dateLegal === true) {
        this.errorGroup[3].error = 'ERROR/BIRTHDAY_NOT_LEGAL_AGE';
      }
    } else {
      this.errorGroup[3].error = '';
    }
  }

  /**
   * @description The method allows you to retrieve the component that tracks the value and validity state of a group of
   * FormControl instances
   */
  get formGroup(): FormGroup {
    return this._formGroup;
  }

  /**
   * @description The method allows you to assign the component that tracks the value and validity state of a group of
   * FormControl instances
   *
   * @param value Value of the new component that tracks the value and validity state of a group of FormControl instances
   */
  set formGroup(value: FormGroup) {
    this._formGroup = value;
  }

  /**
   * @description The method allows you to retrieve the value of the user
   */
  get me(): UserClass {
    return this._me;
  }

  /**
   * @description The method allows you to set the value of the images
   */
  set me(value: UserClass) {
    this._me = value;
  }

  /**
   * @description The method allows you to retrieve the value of the user modified
   */
  get meModified(): UserClass {
    return this._meModified;
  }

  /**
   * @description The method allows you to set the value of the user modified
   */
  set meModified(value: UserClass) {
    this._meModified = {...value};
  }

  /**
   * @description The method allows you to retrieve the value of the images
   */
  get images(): Map<string, string> {
    return this._images;
  }

  /**
   * @description The method allows you to set the value of the images
   */
  set images(value: Map<string, string>) {
    this._images = value;
  }

  /**
   * @description The method allows you to retrieve the value of the password object
   */
  get password(): ChangePassword {
    return this._password;
  }

  /**
   * @description The method allows you to set the value of the password object
   */
  set password(value: ChangePassword) {
    this._password = value;
  }

  /**
   * @description The method allows you to retrieve the value of the phone
   */
  get phone(): any {
    return this._phone;
  }

  /**
   * @description The method allows you to set the value of the phone
   */
  set phone(value: any) {
    this._phone = value;
  }

  /**
   * @description The method allows you to retrieve the form's error parameters
   */
  get errorGroup(): Array<FormErrorClass> {
    return this._errorGroup;
  }

  /**
   * @description The method allows you to assign the form's error parameters
   *
   * @param value Value of the new form's error parameters
   */
  set errorGroup(value: Array<FormErrorClass>) {
    this._errorGroup = value;
  }

  /**
   * @description The method allows you to retrieve the value of the error observable
   */
  get errorObservable(): Observable<string> {
    return this._errorObservable;
  }

  /**
   * @description The method allows you to set the value of the error observable
   */
  set errorObservable(value: Observable<string>) {
    this._errorObservable = value;
  }

}
