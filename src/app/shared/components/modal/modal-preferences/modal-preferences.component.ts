import {Component, OnDestroy, OnInit} from '@angular/core';
import {ModalService} from '../../../services/modal/modal.service';
import {TranslationService} from '../../../services/translation/translation.service';
import {TranslatableClass} from '../../../services/translation/translation-class/translatable.class';
import {createChunkedArray} from '../../../functions/get-chunked-array.function';
import {UserService} from '../../../services/web/user.service';
import {NetworkResponseInterface} from '../../../services/network/network-response/network-response.interface';
import {environment} from '../../../../../environments/environment';
import {NotificationType} from '../../../services/notification/notification-class/notification.class';
import {NotificationService} from '../../../services/notification/notification.service';

@Component({
  selector: 'app-modal-preferences',
  templateUrl: './modal-preferences.component.html'
})
export class ModalPreferencesComponent extends TranslatableClass implements OnInit, OnDestroy {

  /**
   * @description Array which define the number of section
   */
  private _sections: Array<boolean>;

  /**
   * @description model defined to save user preferences
   */
  private _preferences: { tag: Array<any>; user: Array<any>; association: Array<any> };

  /**
   * @description model defined to retrieve saved user preferences
   */
  private _preferencesSaved: { tag: Array<any>; user: Array<any>; association: Array<any> };

  /**
   * @description ModalPreferencesComponent's constructor
   *
   * The constructor creates an instance of the ModalComponent component and specifies the default values of the component's
   * input variables
   *
   * @param modalService A reference to the ModalService service of the application
   * @param translationService A reference to the TranslationService service of the application
   * @param notificationService A reference to the application's NotificationService
   * @param userService A reference to the UserService service of the application
   */
  constructor(private modalService: ModalService, translationService: TranslationService,
              private notificationService: NotificationService, private userService: UserService) {
    super(translationService);
  }

  /**
   * @description: This method is part of the component life cycle: lifecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.sections = [true, false, false];
    this.preferences = {tag: [], user: [], association: []};
    this.preferencesSaved = {tag: [], user: [], association: []};

    this.getTags();
    this.getUsers();
    this.getAssociations();

    document.getElementsByTagName('body')[0].classList.add('modal-preferences');
  }

  /**
   * @description: This method is part of the component life cycle: lifecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional deletion tasks.
   */
  ngOnDestroy(): void {
    document.getElementsByTagName('body')[0].classList.remove('modal-preferences');
  }

  /**
   * @description The method closes the open modal. The result of this closure can be recovered as a Promise thanks to NgbModalRef.result
   */
  close(): void {
    this.modalService.close();
  }

  /**
   *
   * @param preference
   * @param elem
   */
  change(preference: string, elem: any): void {
    const result: string = this.preferencesSaved[preference].find(item => item === elem);

    if (result) {
      this.preferencesSaved[preference].splice(this.preferencesSaved[preference].indexOf(result), 1);
    } else {
      this.preferencesSaved[preference].push(elem);
    }
  }

  /**
   * @description This method is used to change the section in the preference selection modal
   *
   * @param idx Section index
   */
  changeSection(idx: number): void {
    if (!this.sections[idx]) {
      this.sections = [false, false, false];
      this.sections[idx] = true;
    }
  }

  /**
   * @description This method allows you to save the preferences selected by the user
   *
   * @param preference Name of the preference section 'tag' | 'association' | 'user'
   * @param tag Name of the tag selected
   */
  selected(preference: string, tag: string): boolean {
    return !!this.preferencesSaved[preference].find(item => item === tag);
  }

  /**
   * @description This method disables the submit button before the user is selected at least one preference
   */
  disabled(): boolean {
    return this.preferencesSaved.association.length === 0 && this.preferencesSaved.user.length === 0 &&
      this.preferencesSaved.tag.length === 0;
  }

  /**
   * @description The method save the data filled by an user
   */
  savePreferences(): void {
    (this.preferencesSaved.tag.length > 0) ? this.saveTags() : undefined;
    (this.preferencesSaved.user.length > 0) ? this.saveUsers() : undefined;
    (this.preferencesSaved.association.length > 0) ? this.saveAssociations() : undefined;

    this.close();
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to retrieve the event tags in the application
   *
   * It calls the method Get of the NetworkService service
   */
  private getTags(): void {
    this.userService.get('/tag/name').then(response => {
      response.responseData.data = response.responseData.data.map(item => ({
        name: item,
        image: environment.apiHost + environment.apiVersion + '/static/images/tags/' + item + '.jpg'
      }));

      this.preferences.tag = createChunkedArray(response.responseData.data as Array<any>, 4);
    }).catch((error: NetworkResponseInterface) => {
      this.notificationService.notify(NotificationType.Error, this.translate(error.responseStatusText));
    });
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to retrieve the influencers in the application
   *
   * It calls the method Get of the NetworkService service
   */
  private getUsers(): void {
    this.userService.get('/user/influencer/all').then(response => {
      response.responseData.data.forEach(item => {
        ModalPreferencesComponent.getImage(item.images, item);
      });

      this.preferences.user = createChunkedArray(response.responseData.data as Array<any>, 4);
    }).catch((error: NetworkResponseInterface) => {
      this.notificationService.notify(NotificationType.Error, this.translate(error.responseStatusText));
    });
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to retrieve the associations in the application
   *
   * It calls the method Get of the NetworkService service
   */
  private getAssociations(): void {
    this.userService.get('/association').then(response => {
      response.responseData.data.forEach(item => {
        ModalPreferencesComponent.getImage(item.images, item);
      });

      this.preferences.association = createChunkedArray(response.responseData.data as Array<any>, 4);
    }).catch((error: NetworkResponseInterface) => {
      this.notificationService.notify(NotificationType.Error, this.translate(error.responseError.message));
    });
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to retrieve an image by its id
   *
   * It calls the method Get of the NetworkService service
   */
  private static getImage(images: any, data?: any): void {
    if (images && images.length > 0 && images.find(image => image.name === 'profile').content) {
      data['images'] = environment.apiHost + environment.apiVersion + '/image/id/' + images.find(image => image.name === 'profile').content;
    } else {
      data['images'] = '/assets/img/account.png';
    }
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to save the associations in the application
   *
   * It calls the method Patch of the NetworkService service
   */
  private saveTags(): void {
    this.userService.patch(this.preferencesSaved.tag, 'user/me/favorite/tag/add').then(response => {
    }).catch((error: NetworkResponseInterface) => {
      this.notificationService.notify(NotificationType.Error, this.translate(error.responseError.message));
    });
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to save the associations in the application
   *
   * It calls the method Patch of the NetworkService service
   */
  private saveUsers(): void {
    this.preferencesSaved.user.forEach(item => delete item.images);

    this.userService.patch(this.preferencesSaved.user, 'user/me/favorite/user/add').then(() => {
    }).catch((error: NetworkResponseInterface) => {
      this.notificationService.notify(NotificationType.Error, this.translate(error.responseError.message));
    });
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to save the associations in the application
   *
   * It calls the method Patch of the NetworkService service
   */
  private saveAssociations(): void {
    this.preferencesSaved.association.forEach(item => delete item.images);

    this.userService.patch(this.preferencesSaved.association, 'user/me/favorite/association/add').then(() => {
    }).catch((error: NetworkResponseInterface) => {
      this.notificationService.notify(NotificationType.Error, this.translate(error.responseError.message));
    });
  }

  /**
   * @description The method allows you to retrieve model defined to save user preferences
   */
  get preferences(): { tag: Array<any>; user: Array<any>; association: Array<any> } {
    return this._preferences;
  }

  /**
   * @description The method allows you to assign the model defined to save user preferences
   *
   * @param value Value of the new model defined to save user preferences
   */
  set preferences(value: { tag: Array<any>; user: Array<any>; association: Array<any> }) {
    this._preferences = value;
  }

  /**
   * @description The method allows you to retrieve model defined to retrieve saved user preferences
   */
  get preferencesSaved(): { tag: Array<any>; user: Array<any>; association: Array<any> } {
    return this._preferencesSaved;
  }

  /**
   * @description The method allows you to assign the model defined to retrieve saved user preferences
   *
   * @param value Value of the new model defined to retrieve saved user preferences
   */
  set preferencesSaved(value: { tag: Array<any>; user: Array<any>; association: Array<any> }) {
    this._preferencesSaved = value;
  }

  /**
   * @description The method allows you to retrieve the array which define the number of section
   */
  get sections(): Array<boolean> {
    return this._sections;
  }

  /**
   * @description The method allows you to assign the array which define the number of section
   *
   * @param value Value of the new array which define the number of section
   */
  set sections(value: Array<boolean>) {
    this._sections = value;
  }

}
