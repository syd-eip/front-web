/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
/**
 * Import of angular material's module
 */
import {MatIconModule} from '@angular/material';
/**
 * Import of components
 */
import {ModalPreferencesComponent} from './modal-preferences/modal-preferences.component';
import {ModalParticipationComponent} from './modal-participation/modal-participation.component';
import {ModalSettingsComponent} from './modal-settings/modal-settings.component';
import { ModalConfirmationPaymentComponent } from './modal-confirmation-payment/modal-confirmation-payment.component';
/**
 * Import of shared services
 */
import {ModalService} from '../../services/modal/modal.service';
/**
 * Import of dependencies modules
 */
import {NouisliderModule} from 'ng2-nouislider';
import {NgxIntlTelInputModule} from 'ngx-intl-tel-input';
import {PipesModule} from '../../pipes/pipes.module';
import {SelectCountryModule} from '../select-country/select-country.module';

/**
 * Modal module of the Application. This module retrieve all application modal components
 * You can also find Angular's modules
 */
@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        MatIconModule,
        NouisliderModule,
        FormsModule,
        ReactiveFormsModule,
        NgxIntlTelInputModule,
        PipesModule,
        SelectCountryModule
    ],
  declarations: [
    ModalPreferencesComponent,
    ModalParticipationComponent,
    ModalSettingsComponent,
    ModalConfirmationPaymentComponent
  ],
  entryComponents: [
    ModalPreferencesComponent,
    ModalParticipationComponent,
    ModalSettingsComponent,
    ModalConfirmationPaymentComponent
  ],
  exports: [
    ModalPreferencesComponent,
    ModalParticipationComponent,
    ModalSettingsComponent,
    ModalConfirmationPaymentComponent
  ],
  providers: [ModalService]
})
export class ModalModule {
}
