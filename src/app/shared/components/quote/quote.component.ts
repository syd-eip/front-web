import {Component, Input, OnInit} from '@angular/core';
import {LoaderQuoteService} from '../../services/loader/loader-quote.service';
import {LoaderQuoteInterface} from '../../services/loader/loader-quote.interface';

@Component({
  selector: 'app-quotes',
  templateUrl: './quote.component.html',
})
export class QuoteComponent implements OnInit {

  /**
   * @description Quote model to display
   */
  @Input() public quote: LoaderQuoteInterface;

  /**
   * @description Constructor of FooterComponent component
   *
   * The constructor creates an instance of the FooterComponent component and specifies the default values
   * the input and output variables of the component.
   *
   * @param loaderQuoteService reference to the loader quote service of the application
   */
  constructor(private loaderQuoteService: LoaderQuoteService) {
  }

  /**
   * @description: This method is part of the component life cycle: lifecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit() {
    if (!this.quote) {
      this.quote = this.loaderQuoteService.random;
    }
  }
}
