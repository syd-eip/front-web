/**
 * Import of Angular's module
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

/**
 * Import of component
 */
import {QuoteComponent} from './quote.component';

/**
 * Quote module of the Application. This module retrieves quote component
 * You can also find Angular's modules
 */
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    QuoteComponent
  ],
  exports: [
    QuoteComponent
  ]
})
export class QuoteModule {
}
