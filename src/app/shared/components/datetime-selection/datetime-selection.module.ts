/**
 * Import of Angular's modules
 */
import {NgModule}                   from '@angular/core';
import {CommonModule}               from '@angular/common';
import {RouterModule}               from '@angular/router';
import { FormsModule }              from '@angular/forms';
/**
 * Import of components
 */
import {DatetimeSelectionComponent} from './datetime-selection.component';
import {
  NgbDatepickerModule,
  NgbTimepickerModule,
}                                   from '@ng-bootstrap/ng-bootstrap';
import { PipesModule }              from '../../pipes/pipes.module';

@NgModule({
  imports: [CommonModule, RouterModule, FormsModule, NgbDatepickerModule, NgbTimepickerModule, PipesModule],
  declarations: [
	  DatetimeSelectionComponent
  ],
  entryComponents: [
    DatetimeSelectionComponent,
  ],
  exports: [
	  DatetimeSelectionComponent
  ],
})
export class DatetimeSelectionModule {
}
