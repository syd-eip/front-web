import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
}                             from '@angular/core';
import {
  NgbDate,
  NgbDateStruct,
  NgbTimeStruct,
}                             from '@ng-bootstrap/ng-bootstrap';
import {
  animate,
  style,
  transition,
  trigger,
}                             from '@angular/animations';
import { TranslatableClass }  from '../../services/translation/translation-class/translatable.class';
import { TranslationService } from '../../services/translation/translation.service';

@Component({
  selector:    'app-datetime-selection',
  templateUrl: './datetime-selection.component.html',
  animations:  [trigger('fadeAnimation', [transition(':enter', [style({
    opacity:   0,
    transform: 'scale(0.9)',
  }), animate('200ms', style({
    opacity:   1,
    transform: 'scale(1)',
  }))]), transition(':leave', [style({
    opacity:   .5, // <-- "Trix" to make the calendar and the time picker stacked
    position:  'absolute',
    top:       0,
    left:      0,
    right:     0,
    bottom:    0, // "Trix" to make the calendar and the time picker stacked -->
    transform: 'scale(1)',
  }), animate('200ms', style({
    opacity:   0,
    transform: 'scale(0.9)',
  }))])])],
})
export class DatetimeSelectionComponent extends TranslatableClass implements OnInit {

  public date: NgbDate;
  public lastTime: NgbTimeStruct = null;
  public time: NgbTimeStruct = null;
  public step: number = 0;

  public minDate_: NgbDateStruct = undefined;
  public maxDate_: NgbDateStruct = undefined;

  @Input() public minDate: Date = undefined;
  @Input() public maxDate: Date = undefined;
  @Input() public value: Date = new Date();
  @Output() public valueChange: EventEmitter<Date> = new EventEmitter<Date>();

  public validity: boolean = false;
  public errors: any = {};

  /**
   * @description Constructor of ColumnComponent component
   *
   * The constructor creates an instance of the ColumnComponent component and specifies the default values
   * the input and output variables of the component.
   */
  constructor(private _translationService: TranslationService) {
    super(_translationService);
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.minDate = DatetimeSelectionComponent.toDate(this.minDate);
    this.maxDate = DatetimeSelectionComponent.toDate(this.maxDate);
    this.value = DatetimeSelectionComponent.toDate(this.value);

    // Convert any type of date (string, number or Date) to a couple of Date and Time.
    let tupple = DatetimeSelectionComponent.toDateTupple(this.minDate);
    this.minDate_ = tupple.date;

    tupple = DatetimeSelectionComponent.toDateTupple(this.maxDate);
    this.maxDate_ = tupple.date;

    tupple = DatetimeSelectionComponent.toDateTupple(this.value);
    // The default value must be now
    if (tupple.date == undefined) tupple = DatetimeSelectionComponent.toDateTupple(Date.now());

    const {date, time} = tupple;
    this.date = date;
    this.time = time;
    this.lastTime = this.time;
    this.validate()
    console.log(this.validity)
  }

  private static toDate(input: any): Date {
    if (input !== undefined && input !== null) {
      if (input instanceof Number || typeof input === 'number') {
        return new Date(input as number) as Date;
      } else if (input instanceof String || typeof input === 'string') {
        return new Date(input as string) as Date;
      } else if (input instanceof Object && input.year && input.month && input.day) {
        return new Date(input.year, input.month, input.day);
      } else if (!(input instanceof Date)) {
        console.error('type of ', typeof input, ' or instance of ', (input as any).constructor.name, ' is not supported as date type for ', input);
        return undefined;
      }
    }

    return input;
  }

  /**
   * @desc This function converts any type of date to a couple of NgbDate and NgbTimeStruct.
   * @param input
   * @private
   */
  private static toDateTupple(input: any) {
    let ret: { date: NgbDate, time: NgbTimeStruct } = {
      date: undefined,
      time: undefined,
    };

    input = DatetimeSelectionComponent.toDate(input);
    if (input !== undefined && input !== null) {
      if (input instanceof Date) {
        const date = input as Date;
        ret = {
          date: new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate()),
          time: {
            hour:   date.getHours(),
            minute: date.getMinutes(),
            second: 0,
          },
        };
      }
    }
    return ret;
  }

  /**
   * @desc This function is called whenever the NgbDatepicker triggers a change in the selected value.
   * @param $event
   */
  dateSelect($event: NgbDate) {
    this.date = $event;
    this.makeValue();
    this.validate();
    this.step++;
  }

  /**
   * @desc This function is called whenever the NgbTimepicker triggers a change in the selected value.
   *       It increments or decrement the day when hours overflows 23 or 0
   * @param $event
   */
  timeSelect($event: Event) {
    if (this.lastTime.hour == 23 && this.time.hour == 0) {
      this.date.day++;
    } else if (this.lastTime.hour == 0 && this.time.hour == 23) {
      this.date.day--;
    }
    this.makeValue();
    this.validate();
    if (this.validity) this.lastTime = this.time;
  }

  /**
   * @desc This function is called to change the state of the datetime selector (to select a date or a time)
   */
  back() {
    this.step--;
    if (this.step < 0) this.step = 0;
  }

  /**
   * @desc This function is called to change the state of the datetime selector (to select a date or a time)
   *       If the state is the last, it emit the new value
   */
  next() {
    if (this.step < 1) {
      this.step++;
      return;
    } else if (this.step > 1) {
      this.step = 1;
      return;
    }
    this.valueChange.emit(this.makeValue());
  }

  validate() {
    if (this.minDate && this.value.getTime() < this.minDate.getTime()) {
      this.errors.passedTime = true;
    } else if (this.maxDate && this.value.getTime() > this.maxDate.getTime()) {
      this.errors.futureTime = true;
    } else {
      this.errors = {};
    }
    this.validity = JSON.stringify({}) === JSON.stringify(this.errors)
  }

  private makeValue(): Date {
    const {year, month, day} = this.date;
    const {hour, minute} = this.time;

    this.value = new Date(year, month - 1, day, hour, minute, 0, 0);
    console.log(this.date, this.time, this.value);
    return this.value;
  }
}
