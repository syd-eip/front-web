import {Component, ComponentFactory, ComponentFactoryResolver, ViewChild, ViewContainerRef,} from '@angular/core';
import {NotificationComponent} from './notification/notification.component';
import {NotificationService} from '../../services/notification/notification.service';
import {NotificationClass} from '../../services/notification/notification-class/notification.class';

@Component({
  selector: 'app-notification-host',
  templateUrl: './notification-host.component.html',
  styles: [':host {top: 50px; right: 10px; position: fixed; z-index: 999999999}']
})
export class NotificationHostComponent {

  /**
   * @description NotificationComponent container in the HTML DOM
   */
  @ViewChild('notifContainer', {read: ViewContainerRef, static: true}) container;

  /**
   * @description Constructor of NotificationHostComponent component
   *
   * The constructor creates an instance of the NotificationHostComponent component and specifies the default values
   * the input and output variables of the component.
   *
   * @param resolver A simple registry that maps Components to generated ComponentFactory classes that can be used
   * to create instances of components
   * @param notificationService A reference to the notification service of the application
   */
  constructor(private resolver: ComponentFactoryResolver, private notificationService: NotificationService) {
    this.notificationService.observable().subscribe((notification) => {
      this.popNotification(notification);
    });
  }

  /**
   * @description This method put a notification inside the container and inform the notification service
   *
   * @param notification Describe the notification properties
   */
  private popNotification(notification: NotificationClass) {
    const factory: ComponentFactory<NotificationComponent> = this.resolver.resolveComponentFactory(NotificationComponent);

    const cref = this.container.createComponent(factory);
    cref.instance.title = notification.title;
    cref.instance.message = notification.message;
    cref.instance.type = notification.type;
    cref.instance.kill = () => {
      cref.destroy();
    };
    setTimeout(() => {
      cref.instance.end.subscribe(() => {
        cref.destroy();
      });
      cref.instance.close();
    }, notification.duration);
  }
}
