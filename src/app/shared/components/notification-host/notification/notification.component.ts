import {Component, EventEmitter, Output} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {NotificationType} from '../../../services/notification/notification-class/notification.class';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  animations: [
    trigger('simpleFadeAnimation', [
      state('in', style({opacity: 1, top: '20px'})),
      transition('* => in', [
        animate(250),
      ]),
      state('void, hidden', style({opacity: 0, right: '-100px'})),
      transition('* => hidden', animate('250ms cubic-bezier(0.4, 0.0, 1, 1)')),
    ]),
  ],
})
export class NotificationComponent {

  /**
   * @description Title of the notification component
   */
  public title;

  /**
   * @description Message of the notification component
   */
  public message;

  /**
   * @description Function which kills the notification component
   */
  public kill: () => void = null;

  /**
   * @description Type of the notification component
   */
  public type: NotificationType;

  /**
   * @description Visibility of the notification component
   */
  public visibility;

  /**
   * @description This EventEmitter variable emits customized events synchronously
   * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
   *
   * In the NotificationComponent, this EventEmitter allows the sending of the animation end of the component
   */
  @Output() public end: EventEmitter<any>;

  /**
   * @description NotificationComponent's constructor
   *
   * The constructor creates an instance of the NotificationComponent component and specifies the default values of the component's
   * input variables
   */
  constructor() {
    this.visibility = 'in';
    this.type = NotificationType.Info;
    this.end = new EventEmitter<any>();
  }

  /**
   * @description This method handle the click on the close button
   */
  click() {
    if (this.kill) {
      this.kill();
    }
  }

  /**
   * @description The method send an event when the animation 'hidden' terminates
   *
   * @param event Event sent when the animation is done
   */
  animationDone(event) {
    if (event.toState === 'hidden') {
      this.end.emit(true);
    }
  }

  /**
   * @description This method close the notification
   */
  close() {
    this.visibility = 'hidden';
  }

  /**
   * @description This method transform the notification type into css class
   */
  get alertClass(): string {
    switch (this.type) {
      case NotificationType.Warning:
        return 'notification-alert-warning';
      case NotificationType.Debug:
        return 'notification-alert-dark';
      case NotificationType.Error:
        return 'notification-alert-danger';
      case NotificationType.Info:
        return 'notification-alert-info';
      case NotificationType.Success:
        return 'notification-alert-success';
      default:
        return '';
    }
  }
}
