/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

/**
 * Import of angular module's module
 */
import {MatIconModule} from '@angular/material';

/**
 * Import of components
 */
import {NotificationComponent} from './notification/notification.component';
import {NotificationHostComponent} from './notification-host.component';

/**
 * Notification module of the Application. This module retrieve all notification components
 * You can also find Angular's modules
 */
@NgModule({
  imports: [
    CommonModule,
    MatIconModule
  ],
  declarations: [
    NotificationComponent,
    NotificationHostComponent
  ],
  entryComponents: [NotificationComponent],
  exports: [
    NotificationComponent,
    NotificationHostComponent
  ],
})
export class NotificationHostModule {
}
