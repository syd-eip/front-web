/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

/**
 * Import of components
 */
import {FooterComponent} from './footer.component';

/**
 * Footer module of the Application. This module retrieve footer component
 * You can also find Angular's modules
 */
@NgModule({
  imports: [
    CommonModule,
    NgbModule,
  ],
  declarations: [
    FooterComponent
  ],
  exports: [
    FooterComponent
  ],
})
export class FooterModule {
}
