import {Component, OnInit} from '@angular/core';
import {TranslatableClass} from 'src/app/shared/services/translation/translation-class/translatable.class';
import {TranslationService} from '../../services/translation/translation.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent extends TranslatableClass implements OnInit {

  /**
   * @description Used to get the actual year in the Copyright Section
   */
  private _date: Date;

  /**
   * @description Constructor of FooterComponent component
   *
   * The constructor creates an instance of the FooterComponent component and specifies the default values
   * the input and output variables of the component.
   *
   * @param translationService A reference to the i18n translation service of the application
   * @param router Implements the Angular Router service , which enables navigation from one view to the next as users
   * perform application tasks
   */
  constructor(translationService: TranslationService, private router: Router) {
    super(translationService);
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.date = new Date();
  }

  /**
   * @description The method uses the Angular router and allows you to navigate the application's routes
   *
   * @param path Path to navigate to
   * @param topic Chosen topic (optional) used to navigate through the privacy policies
   */
  navigateTo(path: string, topic?: number): void {
    this.router.navigate([path], {queryParams: {topic: topic}})
      .then(value => {
      });
  }

  /**
   * @description The method allows you to retrieve the actual year in the Copyright Section
   */
  get date(): Date {
    return this._date;
  }

  /**
   * @description The method allows you to assign the actual year in the Copyright Section
   *
   * @param value Value of the new actual year in the Copyright Section
   */
  set date(value: Date) {
    this._date = value;
  }
}
