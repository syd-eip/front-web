/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of application's components
 */
import {SelectCountryComponent} from './select-country.component';
/**
 * Import of application's modules
 */
import {NgSelectModule} from '@ng-select/ng-select';
import {PipesModule} from '../../pipes/pipes.module';
import {FormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        NgSelectModule,
        PipesModule,
        FormsModule
    ],
  exports: [
    SelectCountryComponent
  ],
  declarations: [
    SelectCountryComponent
  ]
})
export class SelectCountryModule {
}
