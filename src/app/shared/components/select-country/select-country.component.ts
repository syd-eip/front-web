import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NetworkService} from '../../services/web/network.service';
import {countriesWithCode} from '../../data/countries/countries-with-code.data';
import {TranslatableClass} from '../../services/translation/translation-class/translatable.class';
import {TranslationService} from '../../services/translation/translation.service';

@Component({
  selector: 'app-select-country',
  templateUrl: './select-country.component.html'
})
export class SelectCountryComponent extends TranslatableClass implements OnInit, AfterViewInit {

  /**
   * @description Data available via component ng-select
   */
  private _items: any;

  /**
   * @description Boolean to block or allow the loading of data
   */
  private _loading: boolean;

  /**
   * @description Data selected (used with [(ngModel)] )
   */
  @Input() public selected: any;

  /**
   * @description Activates multiple selection
   */
  @Input() public multiple: boolean;

  /**
   * @description Boolean allowing to assign the 'has-danger' class to the ng-select component (useful in a form)
   */
  @Input() public hasDanger: boolean;

  /**
   * @description This EventEmitter variable emits customized events synchronously
   * or asynchronous. To receive these events, simply subscribe to the instance of this EventEmitter.
   *
   * In the SelectDialCodeComponent, this EventEmitter allows the sending of the country data
   */
  @Output() public eventEmitter: EventEmitter<any>;

  /**
   * @description Constructor of SelectCountryComponent
   *
   * The constructor creates an instance of the SelectCountryComponent and specifies the default values
   * input and output variables of the component.
   *
   * @param translationService A reference to the i18n translation service of the application
   * @param networkService A reference to the http request service of the application
   */
  constructor(translationService: TranslationService, private networkService: NetworkService) {
    super(translationService);

    this.eventEmitter = new EventEmitter<any>();
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.loading = false;
    this.items = countriesWithCode;
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular has initialized the view of a component.
   * Defining an ngAfterViewInit() method allows you to manage any additional post initialization tasks.
   */
  ngAfterViewInit(): void {
  }

  /**
   * @description The method sends an event (to the subscribed component) to indicate that the selected item
   * has been updated (value change).
   *
   * The method provides the element(s) selected in the 'data' field of the transmitted object.
   *
   * @param data Data received when the value of the selected items changes.
   */
  onChange(data: any): void {
    this.eventEmitter.emit(data);
  }

  /**
   * @description The method allows you to retrieve the data available via component ng-select
   */
  get items(): any {
    return this._items;
  }

  /**
   * @description The method allows you to assign the data available via component ng-select
   *
   * @param value Value of the new data available via component ng-select
   */
  set items(value: any) {
    this._items = value;
  }

  /**
   * @description The method allows you to retrieve the boolean to block or allow the loading of data
   */
  get loading(): boolean {
    return this._loading;
  }

  /**
   * @description The method allows you to assign the boolean to block or allow the loading of data
   *
   * @param value Value of the new boolean to block or allow the loading of data
   */
  set loading(value: boolean) {
    this._loading = value;
  }
}
