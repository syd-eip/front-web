/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of application's components
 */
import {SelectDialCodeComponent} from './select-dial-code.component';
/**
 * Import of application's modules
 */
import {NgSelectModule} from '@ng-select/ng-select';
import {PipesModule} from '../../pipes/pipes.module';
import {FormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        NgSelectModule,
        PipesModule,
        FormsModule
    ],
  exports: [
    SelectDialCodeComponent
  ],
  declarations: [
    SelectDialCodeComponent
  ]
})
export class SelectDialCodeModule {
}
