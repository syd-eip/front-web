import {Component, HostListener, Inject, NgZone, OnInit} from '@angular/core';
import {TranslationService} from '../../services/translation/translation.service';
import {TranslatableClass} from '../../services/translation/translation-class/translatable.class';
import {UserService} from '../../services/web/user.service';
import {SIGN_OUT_URL} from '../../data/api/api-path.data';
import {Semaphore} from '../../classes/semaphore/semaphore.class';
import {DOCUMENT} from '@angular/common';
import {Router} from '@angular/router';
import {UserClass} from '../../classes/models/user.class';
import {AlertService} from '../../services/alert/alert.service';
import {ModalService} from '../../services/modal/modal.service';
import {ModalSettingsComponent} from '../modal/modal-settings/modal-settings.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent extends TranslatableClass implements OnInit {

  /**
   * @description user's login status
   */
  private _connected: boolean;

  /**
   * @description Is the user an influencer ?
   */
  private _isInfluencer: boolean;

  /**
   * @description navbar transparency in the layout
   */
  private _transparent: boolean;

  /**
   * @description visibility of the navigation sidebar
   */
  private _visible: boolean;

  /**
   * @description User information model
   */
  private _user: any;

  /**
   * @description Constructor of NavbarComponent component
   *
   * The constructor creates an instance of the NavbarComponent component and specifies the default values
   * the input and output variables of the component.
   *
   * @param document A DI Token representing the main rendering context. In a browser this is the DOM Document
   * @param zone A reference to an injectable service for executing work inside or outside of the Angular zone.
   * @param translationService A reference to the translation service of the application
   * @param alertService A reference to the notification service of the application
   * @param userService A reference to the user service of the application
   * @param router A reference to the Angular injectable router service
   * @param modalService A reference to the modal service of the application
   */
  constructor(@Inject(DOCUMENT) private document: Document, private zone: NgZone, translationService: TranslationService,
              private alertService: AlertService, private userService: UserService, private router: Router,
              private modalService: ModalService) {
    super(translationService);
  }

  /**
   * @description Decorator that declares a DOM event to listen for, and provides a handler method to run when that event occurs.
   * * This DOM event listen to 'scroll' event. If the scroll position is greater than 60px, application remove the navbar transparency.
   *
   * @param event DOM event
   */
  @HostListener('window:scroll', ['$event'])
  onScroll(event) {
    const navbar: HTMLElement = document.getElementsByClassName('navbar')[0] as HTMLElement;

    if (window.pageYOffset > 60 && navbar.classList.contains('navbar-transparent')) {
      this.transparent = true;
      navbar.classList.remove('navbar-transparent');
      navbar.classList.add('bg-white');
    } else if (window.pageYOffset <= 60 && this.transparent) {
      navbar.classList.add('navbar-transparent');
      navbar.classList.remove('bg-white');
    }
  }

  /**
   * @description Decorator that declares a DOM event to listen for, and provides a handler method to run when that event occurs.
   * This DOM event listen to 'resize' event
   *
   * @param event DOM event
   */
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth > 991) {
      this.close();
    }
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.visible = false;

    this.userService.userSubject.subscribe(value => {
      this.user = value;
      this.isInfluencer = (this.user) ? !!this.user.role.find(item => item === 'INFLUENCER') : false;
    });
    this.userService.connectedSubject.subscribe(value => {
      this.connected = value;
    });

    this.user = (this.userService.userSubject.last === null) ? new UserClass() : this.userService.userSubject.last;
  }

  /**
   * @description The method uses the Angular router and allows you to navigate the application's routes
   *
   * @param path Path to navigate to
   */
  navigateTo(path: string): void {
    this.router.navigate([path]).then();
  }

  /**
   * @description This method opens the navigation sidebar
   */
  open(): void {
    const toggle = document.getElementsByClassName('navbar-toggler')[0] as HTMLElement;
    const navbar = document.getElementsByClassName('navbar-collapse')[0] as HTMLElement;

    setTimeout(function () {
      toggle.classList.add('toggled');
    }, 500);
    document.getElementsByTagName('html')[0].classList.add('nav-open');
    navbar.style.width = '300px';

    this.visible = true;
  }

  /**
   * @description This method closes the navigation sidebar
   */
  close(): void {
    const toggle = document.getElementsByClassName('navbar-toggler')[0] as HTMLElement;
    const navbar = document.getElementsByClassName('navbar-collapse')[0] as HTMLElement;

    toggle.classList.remove('toggled');
    navbar.style.width = '0';
    document.getElementsByTagName('html')[0].classList.remove('nav-open');

    this.visible = false;
  }

  /**
   * @description This method closes or opens, thanks to the toggle, the navigation sidebar
   */
  toggle(): void {
    if (this.visible === false) {
      this.open();
    } else {
      this.close();
    }
  }

  /**
   * @description This method allows to know if the window's with is greater than or less than 991px
   */
  mobile(): boolean {
    return window.innerWidth < 991;
  }

  /**
   * @description This method changes the language of the platform and reload the Angular zone with ngZone
   *
   * @param value The new assigned value for the translation service's language
   */
  switch(value: string): void {
    if (this.translationService.lang !== value) {
      this.translationService.changeLang(value);
      this.translationService.reloadTranslation()
        .then(() => {
          this.zone.run(() => {
          });
        });
    }

  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to signout a user of the application
   *
   * It calls the method Post of the NetworkService service
   */
  signout(): void {
    this.userService.activeLoader(new Semaphore(1));

    this.userService.post(null, SIGN_OUT_URL)
      .then(res => {
        this.successEvent(res.responseData.message);
      })
      .catch(error => {
        this.errorEvent(error.responseError.message);
      });
  }

  /**
   * @description This method sends the error message received by the API
   *
   * @param value error message sent by the API
   */
  errorEvent(value: string): void {
    this.alertService.error(this.translate(value));
  }

  /**
   * @description This method sends a success event
   */
  successEvent(value: string): void {
    delete this.userService.user;

    this.userService.connectedSubject.next(false);
    this.userService.userSubject.next(null);

    this.router.navigate(['/']).then(response => {
      (response) ?
        this.alertService.success(this.translate(value)) : undefined;
    });
  }

  settings(): void {
    this.modalService.open(ModalSettingsComponent, {centered: true, windowClass: 'modal-custom'});
  }

  /**
   * @description The method allows you to retrieve the visibility of the navigation sidebar
   */
  get visible(): boolean {
    return this._visible;
  }

  /**
   * @description The method allows you to assign the visibility of the navigation sidebar
   *
   * @param value The new assigned value for the visibility of the navigation sidebar
   */
  set visible(value: boolean) {
    this._visible = value;
  }

  /**
   * @description The method allows you to retrieve the user's login status
   */
  get connected(): boolean {
    return this._connected;
  }

  /**
   * @description The method allows you to assign a boolean representing the user's login status
   *
   * @param value The new assigned value for a boolean representing the user's login status
   */
  set connected(value: boolean) {
    this._connected = value;
  }

  /**
   * @description The method allows you to retrieve user information
   */
  get user(): UserClass {
    return this._user;
  }

  /**
   * @description The method allows you to assign the user information
   *
   * @param value The new assigned value for the user information
   */
  set user(value: UserClass) {
    this._user = value;
  }

  /**
   * @description The method allows you to retrieve the navbar transparency in the layout
   */
  get transparent(): boolean {
    return this._transparent;
  }

  /**
   * @description The method allows you to assign a boolean representing the navbar transparency in the layout
   *
   * @param value The new assigned value for a navbar transparency in the layout
   */
  set transparent(value: boolean) {
    this._transparent = value;
  }

  /**
   * @description The method allows you to retrieve the user influencer role
   */
  get isInfluencer(): boolean {
    return this._isInfluencer;
  }

  /**
   * @description The method allows you to assign the user influencer role
   *
   * @param value The new assigned value for he user influencer role
   */
  set isInfluencer(value: boolean) {
    this._isInfluencer = value;
  }

}
