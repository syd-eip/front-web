/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';

/**
 * Import of components
 */
import {NavbarComponent} from './navbar.component';

/**
 * NavBar module of the Application. This module retrieve navbar component
 * You can also find Angular's modules
 */
@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    RouterModule
  ],
  declarations: [
    NavbarComponent
  ],
  exports: [
    NavbarComponent
  ],
})
export class NavbarModule {
}
