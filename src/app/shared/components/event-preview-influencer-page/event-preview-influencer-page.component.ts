import {Component, Input, OnInit} from '@angular/core';
import {EventClass} from '../../classes/models/event.class';
import {SafeResourceUrl} from '@angular/platform-browser';
import {UserPublicClass} from '../../classes/models/user-public.class';
import {Router} from '@angular/router';

@Component({
  selector: 'app-event-preview-influencer-page',
  templateUrl: './event-preview-influencer-page.component.html'
})
export class EventPreviewInfluencerPageComponent implements OnInit {

  /**
   * @description Event model displayed in the component
   * It's a props received on its selector
   */
  @Input() public event: EventClass;

  /**
   * @description User public displayed in the component
   * It's a props received on its selector
   */
  @Input() public user: UserPublicClass;

  /**
   * @description Map of images displayed in the component
   * It's a props received on its selector
   */
  @Input() public images: Map<string, SafeResourceUrl>;

  /**
   * @description Is the event over
   * It's a props received on its selector
   */
  @Input() public past: boolean;

  /**
   * @description It corresponds to the number of participations on an event
   */
  private _participations: number = 0;

  /**
   * @description Constructor of EventPreviewInfluencerPageComponent component
   *
   * The constructor creates an instance of the EventPreviewInfluencerPageComponent component and specifies the default values
   * the input and output variables of the component.
   */
  constructor(public router: Router) {
  }

  /**
   * @description: This method is part of the component life cycle: lifecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    this.event.lotteries.map((lottery) => {
      this.participations += lottery.totalValue
    })
  }

  /**
   * @description The method allows you to retrieve the value of the participations
   */
  get participations(): number {
    return this._participations;
  }

  /**
   * @description The method allows you to set the value of the participations
   */
  set participations(value: number) {
    this._participations = value;
  }
}
