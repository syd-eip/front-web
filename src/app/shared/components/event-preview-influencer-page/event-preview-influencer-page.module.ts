/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';
/**
 * Import of components
 */
import {EventPreviewInfluencerPageComponent} from './event-preview-influencer-page.component';
/**
 * Import of pipe's modules
 */
import {PipesModule} from '../../pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    RouterModule,
    PipesModule
  ],
  declarations: [
    EventPreviewInfluencerPageComponent
  ],
  exports: [
    EventPreviewInfluencerPageComponent
  ],
})
export class EventPreviewInfluencerPageModule {
}
