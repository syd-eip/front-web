/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';
/**
 * Import of components
 */
import {EventBoxComponent} from './event-box.component';
/**
 * Import of pipe's modules
 */
import {PipesModule} from '../../pipes/pipes.module';
import {ShareBarModule} from '../share-bar/share-bar.module';

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        RouterModule,
        PipesModule,
        ShareBarModule
    ],
  declarations: [
    EventBoxComponent
  ],
  exports: [
    EventBoxComponent
  ],
})
export class EventBoxModule {
}
