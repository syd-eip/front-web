import {Component, Input, OnInit} from '@angular/core';
import {TranslatableClass} from '../../services/translation/translation-class/translatable.class';
import {TranslationService} from 'src/app/shared/services/translation/translation.service';
import {NetworkService} from '../../services/web/network.service';
import {Router} from '@angular/router';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-event-box',
  templateUrl: './event-box.component.html'
})
export class EventBoxComponent extends TranslatableClass implements OnInit {

  /**
   * @description Boolean value indicating if an event has been realized
   */
  private _past: boolean;

  /**
   * @description Event background image
   */
  private _background: string;

  /**
   * @description Event model displayed in the component template
   */
  @Input() public event: any;

  /**
   * @description Constructor of EventBoxComponent component
   *
   * The constructor creates an instance of the EventBoxComponent component and specifies the default values
   * the input and output variables of the component.
   *
   * @param translationService A reference to the i18n translation service of the application
   * @param networkService A reference to the network service of the application
   * @param router Implements the Angular Router service, which enables navigation from one view to the next as users
   * perform application tasks
   */
  constructor(translationService: TranslationService, private networkService: NetworkService, private router: Router) {
    super(translationService);
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
    const banner = this.event.images.find(item => item.name === 'banner');

    this.past = (new Date().getTime() > Date.parse(this.event.information.drawDate));
    this.background = banner ? environment.apiHost + environment.apiVersion + '/image/id/' + banner.content :
      '/assets/img/default-event.png';

  }

  /**
   * @description The method uses the Angular router and allows you to navigate the application's routes
   *
   * @param path Path to navigate to
   */
  navigateTo(path: string): void {
    this.router.navigate([path]).then();
  }

  /**
   * @description The method allows you to retrieve the event background image
   */
  get background(): string {
    return this._background;
  }

  /**
   * @description The method allows you to assign the event background image
   *
   * @param value Value of the event background image
   */
  set background(value: string) {
    this._background = value;
  }

  /**
   * @description The method allows you to retrieve the boolean value indicating if an event has been realized
   */
  get past(): boolean {
    return this._past;
  }

  /**
   * @description The method allows you to assign the boolean value indicating if an event has been realized
   *
   * @param value Value of the boolean value indicating if an event has been realized
   */
  set past(value: boolean) {
    this._past = value;
  }
}
