/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
/**
 * Import of module's applications
 */
import {ModalModule} from './modal/modal.module';
import {FooterModule} from './footer/footer.module';
import {NavbarModule}              from './navbar/navbar.module';
import {NotificationHostModule}    from './notification-host/notification-host.module';
import {LoadableModule}            from './loadable/loadable.module';
import {SpinnerModule}             from './spinner/spinner.module';
import {QuoteModule}               from './quote/quote.module';
import {EventCarouselBoxModule}    from './event-carousel-box/event-carousel-box.module';
import {EventBoxModule}            from './event-box/event-box.module';
import {ColumnModule}              from './column/column.module';
import {SelectCountryModule}       from './select-country/select-country.module';
import {SelectDialCodeModule}      from './select-dial-code/select-dial-code.module';
import {ShareBarModule}            from './share-bar/share-bar.module';
import { DatetimeSelectionModule } from './datetime-selection/datetime-selection.module';
import { NgbDatepickerService }    from '@ng-bootstrap/ng-bootstrap/datepicker/datepicker-service';
import {
  NgbDatepickerModule,
  NgbTimepickerModule,
} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    ColumnModule,
    ModalModule,
    FooterModule,
    NavbarModule,
    ShareBarModule,
    NotificationHostModule,
    LoadableModule,
    SpinnerModule,
    QuoteModule,
    EventCarouselBoxModule,
    EventBoxModule,
    SelectCountryModule,
    SelectDialCodeModule,
    DatetimeSelectionModule,
  ],
  exports: [
    ColumnModule,
    ModalModule,
    FooterModule,
    NavbarModule,
    ShareBarModule,
    NotificationHostModule,
    LoadableModule,
    QuoteModule,
    SpinnerModule,
    EventCarouselBoxModule,
    EventBoxModule,
    SelectCountryModule,
    SelectDialCodeModule,
    DatetimeSelectionModule,
  ],
})
export class ComponentsModule {
}
