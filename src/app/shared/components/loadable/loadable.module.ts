/**
 * Import of Angular's modules
 */
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

/**
 * Import of components
 */
import {LoadableContentComponent} from './loadable-content/loadable-content.component';
import {LoadableDescriptionComponent} from './loadable-description/loadable-description.component';
import {LoadableSpinnerComponent} from './loadable-spinner/loadable-spinner.component';
import {LoadableComponent} from './loadable.component';

/**
 * Loadable module of the Application. This module retrieve all loadable components
 * You can also find Angular's modules
 */
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    LoadableContentComponent,
    LoadableDescriptionComponent,
    LoadableSpinnerComponent,
    LoadableComponent
  ],

  exports: [
    LoadableContentComponent,
    LoadableDescriptionComponent,
    LoadableSpinnerComponent,
    LoadableComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class LoadableModule {
}
