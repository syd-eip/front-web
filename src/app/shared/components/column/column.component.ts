import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-column',
  template: `
    <div class="layout-column">
      <ng-content></ng-content>
    </div>`
})
export class ColumnComponent implements OnInit {

  /**
   * @description Constructor of ColumnComponent component
   *
   * The constructor creates an instance of the ColumnComponent component and specifies the default values
   * the input and output variables of the component.
   */
  constructor() {
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular initializes the view of a component.
   * It first displays the properties related to the data and defines the input properties of the component.
   * Defining an ngOnInit() method allows you to manage any additional initialization tasks.
   */
  ngOnInit(): void {
  }
}
