/**
 * Import of Angular's modules
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
/**
 * Import of components
 */
import {ColumnComponent} from './column.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
  ],
  declarations: [
    ColumnComponent
  ],
  exports: [
    ColumnComponent
  ],
})
export class ColumnModule {
}
