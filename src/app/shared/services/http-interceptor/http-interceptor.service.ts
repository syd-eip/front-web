import {Inject, Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DOCUMENT} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  /**
   * @description Construct a HttpInterceptorService which implements HttpInterceptor
   *
   * @param document A DI Token representing the main rendering context. In a browser this is the DOM Document.
   */
  constructor(@Inject(DOCUMENT) private document) {
  }

  /**
   * @description This method intercepts an http request to add information such as headers, JavaScript cookies...
   *
   * @param request An outgoing HTTP request with an optional typed body.
   * @param next Transforms an HttpRequest into a stream of HttpEvents, one of which will likely be a HttpResponse.
   */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({withCredentials: true});
    request = request.clone({headers: request.headers.set('Content-Type', 'application/json')});
    request = request.clone({headers: request.headers.set('Origin-Request-Front', this.document.location.hostname)});
    return next.handle(request);
  }
}
