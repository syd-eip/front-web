/**
 * @description This interface defines a basic response to the api
 */
export interface NetworkResponseBodyInterface {
  code: number;
  message: string;
  data: any;
}
