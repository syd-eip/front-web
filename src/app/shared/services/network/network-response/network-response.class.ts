import {HttpErrorResponse, HttpEventType, HttpHeaders, HttpResponse} from '@angular/common/http';
import {NetworkResponseInterface} from './network-response.interface';
import {NetworkResponseBodyInterface} from './network-response-body.interface';

/**
 * @description NetworkResponseInterface is a class of response object of an Http request
 */
export class NetworkResponseClass<T> implements NetworkResponseInterface {

  /**
   * @description The property defines the response data if it has been returned by the API.
   */
  responseData: NetworkResponseBodyInterface;

  /**
   * @description The property defines all the headers of the response
   */
  responseHeaders: HttpHeaders;

  /**
   * @description The property defines the state (true | false) if the status code of the response is located
   * in the range 2XX.
   */
  responseState: boolean;

  /**
   * @description The property defines the status code of the response returned by the API
   */
  responseStatus: number;

  /**
   * @description The property defines the textual description of the status code of the response returned by the API.
   */
  responseStatusText: string;

  /**
   * @description The property defines the type of response (complete response or response header).
   *
   * Available values: HttpEventType.Response (4) | HttpEventType.ResponseHeader (2)
   */
  responseType: HttpEventType.Response | HttpEventType.ResponseHeader;

  /**
   * @description The property defines the body of the error
   */
  responseError: any;

  /**
   * @description The property defines the name of the error 'HttpErrorResponse'
   */
  responseErrorName: string;

  /**
   * @description The property defines the error message related to the received error
   */
  responseErrorMessage: string;

  /**
   * @description The property defines the error status text related to the received error
   */
  responseErrorStatusText: string;

  /**
   * @description Constructor of the class NetworkResponseClass
   */
  constructor() {
    this.responseStatus = 0;
    this.responseData = null;
    this.responseState = false;
    this.responseStatusText = '';
    this.responseHeaders = new HttpHeaders();

    this.responseError = null;
    this.responseErrorName = '';
    this.responseErrorMessage = '';
  }

  /**
   * @description This method is used to map the data received when executing an Http request.
   *
   * @param response A complete HTTP response, including the response body which may be null if
   * the answer has not been returned.
   */
  map(response: HttpResponse<any>): void {

    if (response !== undefined && response !== null) {
      this.responseData = response.body;
      this.responseStatus = response.status;
      this.responseState = response.ok;
      this.responseHeaders = response.headers;
      this.responseStatusText = response.statusText;
      this.responseType = response.type;
    }
  }

  /**
   * @description This method is used to map the data received when the translation service is reloaded
   *
   * @param body A complete body response, including the translations for the selected language.
   */
  mapTranslation(body: NetworkResponseBodyInterface): void {

    if (body !== undefined && body !== null) {
      this.responseData = body;
      this.responseStatus = body.code;
      this.responseState = null;
      this.responseHeaders = null;
      this.responseStatusText = body.message;
      this.responseType = null;
    }
  }

  /**
   * @description This method is used to map the error received when executing an Http request.
   *
   * @param error Response that represents an error or failure, whether it is an unsuccessful HTTP state,
   * an error in the execution of the request or other failure during the analysis of the response.
   */
  mapError(error: HttpErrorResponse): void {

    if (error !== undefined && error !== null) {
      this.responseError = error.error;
      this.responseErrorMessage = error.message;
      this.responseStatusText = error.statusText;
      this.responseErrorStatusText = error.statusText;
      this.responseErrorName = error.name;
      this.responseState = error.ok;
    }
  }
}
