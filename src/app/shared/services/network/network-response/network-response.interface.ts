import {HttpErrorResponse, HttpEventType, HttpHeaders, HttpResponse} from '@angular/common/http';
import {NetworkResponseBodyInterface} from './network-response-body.interface';

/**
 * @description NetworkResponseInterface is an interface that defines the properties of a response object of a
 * Http request
 */
export interface NetworkResponseInterface {

  /**
   * @description The property defines the response data if it has been returned by the API.
   */
  responseData: NetworkResponseBodyInterface;

  /**
   * @description The property defines all the headers of the response
   */
  responseHeaders: HttpHeaders;

  /**
   * @description The property defines the state (true | false) if the status code of the response is located
   * in the range 2XX.
   */
  responseState: boolean;

  /**
   * @description The property defines the status code of the response returned by the API
   */
  responseStatus: number;

  /**
   * @description The property defines the textual description of the status code of the response returned by the API.
   */
  responseStatusText: string;

  /**
   * @description The property defines the type of response (complete response or response header).
   *
   * Available values: HttpEventType.Response (4) | HttpEventType.ResponseHeader (2)
   */
  responseType: HttpEventType.Response | HttpEventType.ResponseHeader;

  /**
   * @description The property defines the body of the error
   */
  responseError: any;

  /**
   * @description The property defines the name of the error 'HttpErrorResponse'
   */
  responseErrorName: string;

  /**
   * @description The property defines the error message related to the received error
   */
  responseErrorMessage: string;

  /**
   * @description The property defines the error status text related to the received error
   */
  responseErrorStatusText: string;

  /**
   * @description This method is used to map the data received when executing an Http request.
   *
   * @param response A complete HTTP response, including the response body which may be null if
   * the answer has not been returned.
   */
  map(response: HttpResponse<any>): void;

  /**
   * @description This method is used to map the data received when the translation service is reloaded
   *
   * @param body A complete body response, including the translations for the selected language.
   */
  mapTranslation(body: NetworkResponseBodyInterface): void;

  /**
   * @description This method is used to map the error received when executing an Http request.
   *
   * @param error Response that represents an error or failure, whether it is an unsuccessful HTTP state,
   * an error in the execution of the request or other failure during the analysis of the response.
   */
  mapError(error: HttpErrorResponse): void;
}
