import {Injectable} from "@angular/core";
import {EventModel} from "../../classes/models/event.class";
import {NetworkService} from "./network.service";
import {TranslationService} from "../translation/translation.service";
import {ImageClass} from "../../classes/models/image.class";

@Injectable({
  providedIn: 'root'
})
export class EventService {

  /**
   * @desc This is the Event service constructor.
   *
   * The purpose of this service is to simplify and unify any operation about an Event
   *
   * @param networkService The networkService allow the application to request the backend
   */
  constructor(private networkService: NetworkService, private translationService: TranslationService) {
  }

  /**
   * @desc This method is a getter for any event by his id
   *
   * @param id
   */
  public async getEvent(id: string): Promise<EventModel> {
    return (await this.networkService.get('/event/id/' + id)).responseData.data as EventModel;
  }

  /**
   * @desc This method allow the application to update an event by deltas.
   *
   * @param id
   * @param deltas The deltas of the events
   */
  public async patch(id: string, deltas: { [key: string]: any }) {
    const {name, description, tags, image} = deltas;

    if (name) {
      await this.patchName(id, name)
    }
    if (description) {
      await this.patchDescription(id, description)
    }
    if (tags != undefined) {
      await this.patchTags(id, tags)
    }
    if (image) {
      await this.patchImage(id, image)
    }
  }

  /**
   * @desc This method allow the application to place a bet on an event
   *
   * @param eventId The event id
   * @param bet The ammount of tokens and their repartition
   */
  public async bet(eventId: string, bet: any) {
    const res = await this.networkService.post({
      lotteryId: 0,
      value: bet.tokens,
      splitRatio: bet.repartition,
    }, '/event/id/' + eventId + '/bet/add');

    if (res.responseStatus == 200) {
      return true;
    }
    return this.translationService.get(res.responseData.message);
  }

  /**
   * @desc This method allow the application to update an event name
   *
   * @param id The event id
   * @param name The new event name
   */
  public async patchName(id: string, name: string): Promise<any> {
    return await this.networkService.patch({name}, '/event/id/' + id + '/setting/name');
  }

  /**
   * @desc This method allow the application to update an event description
   *
   * @param id The event id
   * @param description The new event description
   */
  public async patchDescription(id: string, description: string): Promise<any> {
    return await this.networkService.patch({description}, '/event/id/' + id + '/setting/description')
  }

  /**
   * @desc This method allow the application to update the event tags.
   *
   * @param id The event id
   * @param tags The new tag list
   */
  public async patchTags(id: string, tags: string[]): Promise<any> {
    return await this.networkService.patch({tags}, '/event/id/' + id + '/setting/tags')
  }

  /**
   * @desc This method allow the application to update the event image list
   *
   * @param id The event id
   * @param image The image to update
   */
  public async patchImage(id: string, image: ImageClass): Promise<any> {
    image.content = image.content.slice(image.content.indexOf(',') + 1)
    return await this.networkService.patch(image, '/event/id/' + id + '/setting/image')
  }

  /**
   * @desc This method allow the application to create an image via his content and his banner
   *
   * @param event The event content
   * @param image The banner
   */
  public async create(event: any, image: ImageClass): Promise<string> {
    const res = await this.networkService.post(event, '/event/new')
    const evt = res.responseData.data as EventModel;

    await this.patchImage(evt.id, image)
    return evt.id;
  }
}
