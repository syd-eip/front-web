import {Injectable} from "@angular/core";
import {NetworkService} from "./network.service";
import {AssociationModel} from "../../classes/models/association.model";
import {GET_ASSOCIATIONS} from "../../data/api/api-path.data";

@Injectable({
  providedIn: 'root'
})
export class AssociationService {

  /**
   * @desc This is the Association service constructor.
   *
   * The purpose of this service is to simplify and unify any operation about an assocaition
   *
   * @param networkService The networkService allow the application to request the backend
   */
  constructor(private networkService: NetworkService) {
  }

  /**
   * @desc This method is a getter for any association by his id
   * @param id
   */
  public async getById(id: string): Promise<AssociationModel> {
    const res = await this.networkService.get('/association/id/' + id)

    return res.responseData.data as AssociationModel;
  }

  /**
   * @desc This method gets all associations in the API
   */
  public async getAll(): Promise<AssociationModel[]> {
    const res = await this.networkService.get(GET_ASSOCIATIONS)

    return res.responseData.data as AssociationModel[];
  }

}
