import {Injectable} from "@angular/core";
import {NetworkService} from "./network.service";

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  /**
   * @desc This is the Image service constructor.
   *
   * The purpose of this service is to simplify and unify any operation about an Image
   *
   * @param networkService The networkService allow the application to request the backend
   */
  constructor(private networkService: NetworkService) {
  }

  /**
   * @desc This method allow the application to retrieve an image
   *
   * @param id The desired image id
   */
  public async getById(id: string): Promise<string> {
    const res = await this.networkService.get('/image/id/' + id)

    return res.responseData.data as string;
  }
}
