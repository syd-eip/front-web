import {Injectable} from '@angular/core';
import {NetworkClass} from '../network/network.class';
import {LoaderService} from '../loader/loader.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NetworkService extends NetworkClass {

  /**
   * @description Constructor of WebServices service
   *
   * @param httpClient HttpClient offers a simplified client HTTP API for Angular applications that rests on the XMLHttpRequest
   * interface exposed by browsers
   * @param loaderService A reference to the LoaderService service
   */
  constructor(httpClient: HttpClient, loaderService: LoaderService) {
    super(httpClient, loaderService);
  }
}
