import {Injectable} from '@angular/core';
import {NetworkClass} from '../network/network.class';
import {CustomSubjectClass} from '../../classes/rxjs-overrides/custom-subject.class';
import {LoaderService} from '../loader/loader.service';
import {HttpClient} from '@angular/common/http';
import {GET_USER_ME} from '../../data/api/api-path.data';
import {CustomCookieService} from '../cookies/custom-cookie.service';

@Injectable({
  providedIn: 'root'
})
export class UserService extends NetworkClass {

  /**
   * @description Listener to know if the user is connected
   *
   * A Subject is a special type of Observable that allows values to be multicasted to many Observers.
   * While plain Observables are unicast (each subscribed Observer owns an independent execution of the Observable),
   *
   * Subject are multicast.
   */
  public connectedSubject: CustomSubjectClass<boolean> = new CustomSubjectClass<boolean>();

  /**
   * @description Listener and sender to retrieve user model data (when a user is connected)
   *
   * A Subject is a special type of Observable that allows values to be multicasted to many Observers.
   * While plain Observables are unicast (each subscribed Observer owns an independent execution of the Observable),
   *
   * Subject are multicast.
   */
  public userSubject: CustomSubjectClass<any> = new CustomSubjectClass<any>();

  /**
   * @description User model (defined when a user is connected)
   */
  private _user: any;

  /**
   * @description Constructor of WebServices service
   *
   * @param httpClient HttpClient offers a simplified client HTTP API for Angular applications that rests on the XMLHttpRequest
   * interface exposed by browsers
   * @param loaderService A reference to the LoaderService service of the application
   * @param cookieService A reference to the custom cookie service of the application
   */
  constructor(httpClient: HttpClient, loaderService: LoaderService, private cookieService: CustomCookieService) {
    super(httpClient, loaderService);
  }

  /**
   * @description This method allows you to know if a user is logged in.
   * If a user is logged in then the application will load the data of this user.
   */
  async isAuthenticated(): Promise<boolean> {
    let authed: boolean = false;
    const auth: string = this.cookieService.get('CookieAuthentication');

    if (auth === null || auth === '') {
      this.connectedSubject.next(authed);
      return authed;
    }

    authed = Boolean(JSON.parse(auth));

    if (authed && !this.userSubject.last) {
      await this.get(GET_USER_ME)
        .then(res => {
          if (res.responseStatus === 200) {
            this.user = res.responseData.data;
            this.userSubject.next(res.responseData.data);
          } else {
            this.user = null;
            this.userSubject.next(null);
          }
        });
    } else if (authed && this.userSubject.last) {
      this.user = this.userSubject.last;
      this.userSubject.next(this.user);
    }

    this.connectedSubject.next(authed);
    return new Promise(resolve => resolve(authed));
  }

  /**
   * @description The method performs the Http request according to the parameters passed to it.
   * It allows to retrieve all information of the connected user
   *
   * It calls the method Get of the NetworkClass class
   */
  update(): void {
    this.get(GET_USER_ME).then(res => {
      this.user = res.responseData.data;
      this.userSubject.next(res.responseData.data);
    })
    .catch(err => {
      console.error(err);
      this.user = null;
      this.userSubject.next(null);
    });
  }

  /**
   * @description The method allows you to retrieve the user model (defined when a user is connected)
   */
  get user(): any {
    return this._user;
  }

  /**
   * @description The method allows you to assign the user model (defined when a user is connected)
   *
   * @param value The new assigned value for the user model (defined when a user is connected)
   */
  set user(value: any) {
    this._user = value;
  }
}

/**
 * @description A function that will be executed when an application is initialized.
 */
export function appInitializerFactory(service: UserService) {
  return () => service.isAuthenticated();
}
