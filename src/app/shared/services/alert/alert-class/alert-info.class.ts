/**
 * @description This class describe the data scheme to inject to the InfoAlertComponent
 */
export class AlertInfoClass {
  message: string;

  constructor(props) {
    this.message = '';
  }
}
