/**
 * @description This class describe the data scheme to inject to the ConfirmAlertComponent
 */
export class AlertConfirmClass {
  question: string;

  constructor() {
    this.question = '';
  }
}
