import {ComponentFactory} from '@angular/core';
import {FormInputClass} from '../../../classes/forms/form-input.class';

/**
 * @description This class describe the data scheme to inject to the FormAlertComponent
 */
export class AlertFormClass {
  question: string;
  form?: Array<FormInputClass>;
  component?: ComponentFactory<any>;

  constructor() {
    this.question = '';
    this.form = [];
  }
}
