/**
 * @description This class describe what an syd-notification must contain.
 */
export class AlertClass {
  id?: number;
  type: string;
  strong?: string;
  message: string;
  icon?: string;

  constructor() {
    this.type = '';
    this.message = '';
  }
}
