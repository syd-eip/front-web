import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {MatDialog} from '@angular/material';
import Swal, {SweetAlertType} from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  /**
   * @description Constructor of AlertService service
   *
   * The constructor creates an instance of the AlertService service
   *
   * @param dialog A reference to the material dialog manager
   */
  constructor(private dialog: MatDialog) {
  }

  /**
   * This method ask the user to confirm something or not
   *
   * @param question The question to ask
   * @param acceptText The accept button label
   * @param denyText The deny button label
   */
  public confirm(question: string, acceptText: string = 'Oui', denyText: string = 'Non'): Observable<boolean> {
    const subj = new Subject<boolean>();
    const ref = Swal.fire({
      text: question,
      type: 'question',
      confirmButtonText: acceptText,
      showCancelButton: true,
      cancelButtonText: denyText,
      cancelButtonColor: '#d33',
    });

    ref.then((val) => {
      subj.next(val.value);
    }).catch(subj.error);

    return subj.asObservable();
  }

  /**
   * @description This method ask a data to the user
   *
   * @param question The question to ask
   * @param submitText The submit button label
   * @param dismissText The cancel or dismiss button label
   */
  public question(question: string,
                  submitText: string = 'Envoyer',
                  dismissText: string = 'Annuler'): Observable<string> {
    const subj = new Subject<string>();
    const ref = Swal.fire({
      text: question,
      input: 'text',
      confirmButtonText: submitText,
      showCancelButton: true,
      cancelButtonText: dismissText,
    });

    ref.then((val) => {
      if (val.value) {
        subj.next(val.value);
      } else {
        subj.next(null);
      }
    }).catch(subj.error);

    return subj.asObservable();
  }

  /**
   * @description This method throw an alert with title as title, message as text body and type to specify the layout
   *   to use.
   *
   * @param message The message to display
   * @param title The title of the message
   * @param type The type of the message
   */
  private alert(message: string, title: string = null, type: SweetAlertType): Observable<boolean> {
    const subj = new Subject<boolean>();
    const ref = Swal.fire({
      titleText: title,
      text: message,
      timer: 2000,
      type,
      showConfirmButton: !(type === 'success' || type === 'error')
    });

    ref.then((val) => {
      if (val.value) {
        subj.next(val.value);
      } else {
        subj.next(null);
      }
    }).catch(subj.error);
    return subj.asObservable();
  }

  /**
   * @description This method is a shortcut for _alert(xx, xx, 'error')
   *
   * @param message The message to display
   * @param title The title of the message
   */
  public error(message: string, title: string = null): Observable<boolean> {
    return this.alert(message, title, 'error');
  }

  /**
   * @description This method is a shortcut for _alert(xx, xx, 'warning')
   *
   * @param message The message to display
   * @param title The title of the message
   */
  public warning(message: string, title: string = null): Observable<boolean> {
    return this.alert(message, title, 'warning');
  }

  /**
   * @description This method is a shortcut for _alert(xx, xx, 'info')
   *
   * @param message The message to display
   * @param title The title of the message
   */
  public info(message: string, title: string = null): Observable<boolean> {
    return this.alert(message, title, 'info');
  }

  /**
   * @description This method is a shortcut for _alert(xx, xx, 'success')
   *
   * @param message The message to display
   * @param title The title of the message
   */
  public success(message: string, title: string = null): Observable<boolean> {
    return this.alert(message, title, 'success');
  }
}
