import {Injectable} from '@angular/core';
import {CompatClient, Stomp, StompConfig, StompSubscription} from '@stomp/stompjs';
import {environment} from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class WebsocketService {

    /**
     * @description WebSocket handler subscriber
     */
    private messages: StompSubscription;

    /**
     * @description WebSocket client
     */
    private stompClient: CompatClient;

    /**
     * @description WebSocket client configuration
     */
    private readonly stompConfig: StompConfig;

    /**
     * @description Constructor of WebsocketService service
     *
     * The constructor creates an instance of the WebsocketService service and specifies the default values
     * the input and output variables of the service.
     */
    constructor() {
        this.stompConfig = {
            debug: (msg: string) => null,
        };
    }

    /**
     * @description This method is used to connect to the WebSocket server.
     */
    public connect(): void {
        this.stompClient = Stomp.client(environment.apiWebSocket + '/connection');
        this.stompClient.configure(this.stompConfig);

        this.stompClient.connect({}, () => {
            this.messages = this.stompClient.subscribe('', (message) =>
                this.receive(message));
        }, this.error);
    }

    /**
     * @description This method is used to disconnect to the WebSocket server.
     */
    public disconnect(): void {
        if (this.stompClient !== null) {
            this.messages.unsubscribe();
            this.stompClient.disconnect();
        }
    }

    /**
     * @description This method is a callback to process error received by the WebSocket client.
     *
     * @param error error occurred
     */
    public error(error) {
        setTimeout(() => {
            this.connect();
        }, 5000);
    }

    /**
     * @description This method is a callback to process messages received by the WebSocket client.
     *
     * @param message message received by the WebSocket client
     */
    public receive(message: any): void {
    }
}
