import { TranslatableClass } from './translatable.class';

export class TranslationClass {

  /**
   * @description Data to translate
   */
  private readonly translationCollection: TranslatableClass;

  /**
   * @description Current language in which the application is translated
   */
  private readonly lang: string;

  /**
   * @description Construct a TranslationClass class
   *
   * The constructor creates an instance of the TranslationClass class and specifies the default values
   * the input and output variables of the component.
   *
   * @param collection Data to translate
   * @param lang Application's language
   */
  constructor(collection: TranslatableClass, lang: string) {
    this.translationCollection = collection;
    this.lang = lang;
  }

  /**
   * @description This method return the corresponding text.
   *
   * @param code code to retrieve the associated text
   */
  public translation(code: string, args: {[key: string]: string} = undefined): string {
    if (code) {
      const split = code.split('/');

      if (!split || !this.translationCollection) {
        return null;
      }

      let ret: TranslatableClass | string = this.translationCollection;

      for (const s of split) {
        ret = ret[s];
        if (typeof ret === 'string') {
          break;
        }
        if (!ret) {
          ret = null;
          break;
        }
      }
      if (typeof ret === 'string') {
        let r = ret as string;
        if (args !== undefined) {
          for (const key in args) {
            if (args.hasOwnProperty(key))
              r = r.replace(`{{${key}}}`, args[key]);
          }
        }
        return r;
      }
    }
    return '';
  }
}
