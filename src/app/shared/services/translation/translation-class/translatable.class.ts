import {OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {TranslationService} from '../translation.service';
import {TranslationClass} from './translation.class';

/**
 * @description This class is used to enable translations inside a component. Just extend it from your component.
 *
 * @example
 * @Component({
 *              selector:    'any-selector',
 *              template:    '{{translate('GLOBAL/WEBSITE_NAME)}}',
 *              styleUrls:   ['./shared/demo-network.component.css'],
 *            })
 * export class TranslationExempleComponent extends TranslatableClass implements OnDestroy {
 *     // traductService is an injection that should not be in the current class (public, private or protected)
 *     // (TranslatableClass implements its own).
 *     constructor(traductService: TranslationService) {
 *         super(traductService)
 *     }
 *
 *     ngOnDestroy(): void {
 *         super();
 *         // your stuff
 *     }
 * }
 */
export class TranslatableClass implements OnDestroy {

  /**
   * @description The event listener of the translation service
   *
   * Note: You should never access it or override it. That's the reason why it don't have any getter or setter.
   */
  private subscription: Subscription;

  /**
   * @description The current loaded translation.
   *
   * Note: It is dangerous to access it directly. That's the reason why it don't have any getter or setter.
   */
  private translation: TranslationClass;

  /**
   * @description Construct the TranslatableClass class and setup a subscription
   *
   * The constructor creates an instance of the TranslatableClass class and specifies the default values
   * the input and output variables of the component.
   *
   * @param translationService A reference to the i18n translation service of the application
   */
  constructor(protected translationService: TranslationService) {
    this.translation = translationService.getTranslation();
    this.subscription = translationService.getTranslationObservable().subscribe(translation => {
      this.translation = translation;
    });
  }

  /**
   * @description: This method is part of the component life cycle: ligecycle hook.
   *
   * It is called when Angular destroys the view of a component.
   * Defining an ngOnDestroy() method allows you to manage any additional deletion tasks.
   */
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /**
   * @description The method provides access to the translation of the passed element
   */
  public translate(wildcard: string, args: {[key: string]: string} = undefined): string {
    if (!this.translation || !this.translation.translation) {
      return '';
    }
    return this.translation.translation(wildcard, args);
  }
}
