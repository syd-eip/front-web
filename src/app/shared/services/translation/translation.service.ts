import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {TranslationClass} from './translation-class/translation.class';
import {CustomSubjectClass} from '../../classes/rxjs-overrides/custom-subject.class';
import {NetworkClass} from '../network/network.class';
import {CustomCookieService} from '../cookies/custom-cookie.service';
import {HttpClient} from '@angular/common/http';
import {LoaderService} from '../loader/loader.service';

@Injectable({
  providedIn: 'root',
})
export class TranslationService extends NetworkClass {

  /**
   * @description Current language of the application
   */
  private _lang: string;

  /**
   * @description Custom Subject which allows to keep the last value of next() call
   */
  private translation: CustomSubjectClass<TranslationClass>;

  /**
   * @description Construct TranslationService service
   *
   * The constructor creates an instance of the TranslationService service and specifies the default values
   * the input and output variables of the service.
   *
   * @param httpClient A reference to Angular's HttpClient internal service
   * @param loaderService A reference to the loader service of the application
   * @param cookieService A reference to the custom cookie service of the application
   */
  constructor(httpClient: HttpClient, loaderService: LoaderService, private cookieService: CustomCookieService) {
    super(httpClient, loaderService);

    this._lang = 'en';

    if (this.cookieService.get('lang')) {
      this._lang = this.cookieService.get('lang');
    } else {
      const date: Date = new Date();

      date.setDate(date.getDate() + 1);
      this.cookieService.create('lang', 'en', date);
    }

    this.translation = new CustomSubjectClass<TranslationClass>();
    this.reloadTranslation().catch(console.error);
  }

  /**
   * @description This method returns an observable to translation.
   */
  public getTranslationObservable(): Observable<TranslationClass> {
    return this.translation.asObservable();
  }

  /**
   * @description This method returns the last used translation
   */
  public getTranslation(): TranslationClass {
    return this.translation.last;
  }

  /**
   * @description This methods reload the Translation (used if the Translation change at runtime)
   */
  public async reloadTranslation(): Promise<any> {
    const TranslationData = await this.get('/translations/' + this._lang);

    this.translation.next(new TranslationClass(TranslationData.responseData.data, this._lang));
    return TranslationData.responseData.data;
  }

  /**
   * @description This method changes the language of the application
   *
   * @param value New value of the language of the application
   */
  public changeLang(value: string) {
    const date: Date = new Date();

    date.setDate(date.getDate() + 1);
    this.lang = value;
    this.cookieService.create('lang', value, date);
  }

  /**
   * @description This method allows to retrieve the current language of the application
   */
  get lang(): string {
    return this._lang;
  }

  /**
   * @description This method allows to assign the current language of the application
   *
   * @param value New value of the language of the application
   */
  set lang(value: string) {
    this._lang = value;
  }
}
