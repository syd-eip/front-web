import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {NetworkService} from '../web/network.service';
import {GET_ASSOCIATION_BY_ID} from '../../data/api/api-path.data';

@Injectable({
  providedIn: 'root'
})
export class AssociationGuardService implements CanActivate {

  /**
   * @description Construct a AssociationGuardService
   *
   * @param networkService A reference to the http request service of the application
   * @param router A reference to the Angular injectable router service
   */
  constructor(private networkService: NetworkService, private router: Router) {
  }

  /**
   *  @description This method is a guard and decides if a route can be activated
   *  If all guards return true, navigation will continue.
   *  If any guard returns false, navigation will be cancelled.
   *  If any guard returns a UrlTree, current navigation will be cancelled and a new navigation will be kicked off to the UrlTree returned
   *  from the guard.
   *
   * @param route contains the information about a route associated with a component loaded in an outlet at a particular moment in time.
   * @param state represents the state of the router at a moment in time.
   * @return Observable
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> |
    boolean | UrlTree {
    return new Promise(resolve => {
      const name: string = route.paramMap.get('name');
      const id: string = route.queryParamMap.get('id');

      if (name && id) {
        this.networkService.get(GET_ASSOCIATION_BY_ID + id)
          .then(() => {
            resolve(true);
          })
          .catch(() => {
            resolve(this.router.parseUrl('/association/list'));
          })
      } else {
        resolve(this.router.parseUrl('/association/list'));
      }
    });
  }
}
