import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {NetworkService} from '../web/network.service';
import {GET_USER_RESET_PASSWORD_VERIFY} from '../../data/api/api-path.data';
import {NetworkParamsClass} from '../network/network-params/network-params.class';
import {NetworkParamsInterface} from '../network/network-params/network-params.interface';

@Injectable({
  providedIn: 'root'
})
export class ResetPasswordGuardService implements CanActivate {

  /**
   * @description Construct a ResetPasswordGuardService
   *
   * @param router A reference to the Angular injectable router service
   * @param webServices A reference to the web services of the application
   */
  constructor(private router: Router, private webServices: NetworkService) {
  }

  /**
   *  @description This method is a guard and decides if a route can be activated
   *  If all guards return true, navigation will continue.
   *  If any guard returns false, navigation will be cancelled.
   *  If any guard returns a UrlTree, current navigation will be cancelled and a new navigation will be kicked off to the UrlTree returned
   *  from the guard.
   *
   * @param route contains the information about a route associated with a component loaded in an outlet at a particular moment in time.
   * @param state represents the state of the router at a moment in time.
   * @return Observable
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> |
    boolean | UrlTree {
    const token: string = route.queryParamMap.get('reset-token');

    return new Promise(resolve => {
      if (!token) {
        resolve(this.router.parseUrl('/'));
      } else {
        const params: NetworkParamsInterface = new NetworkParamsClass();

        params.headers.push({name: 'Reset-Token', value: token});
        this.webServices.get(GET_USER_RESET_PASSWORD_VERIFY, null, params)
          .then(response => {
            if (response.responseStatus === 200) {
              resolve(true);
            } else {
              resolve(this.router.parseUrl('/'));
            }
          }).catch(error => {
          resolve(this.router.parseUrl('/'));
        });
      }
    });
  }
}
