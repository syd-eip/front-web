import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {UserService} from '../web/user.service';

@Injectable({
  providedIn: 'root'
})
export class EventOwnerGuardService implements CanActivate {

  /**
   * @description Construct a EventOwnerGuardService
   *
   * @param userService A reference to the user service
   * @param router A reference to the Angular injectable router service
   */
  constructor(private userService: UserService, private router: Router) {
  }

  /**
   *  @description This method is a guard and decides if a route can be activated
   *  If all guards return true, navigation will continue.
   *  If any guard returns false, navigation will be cancelled.
   *  If any guard returns a UrlTree, current navigation will be cancelled and a new navigation will be kicked off to the UrlTree returned
   *  from the guard.
   *
   * @param route contains the information about a route associated with a component loaded in an outlet at a particular moment in time.
   * @param state represents the state of the router at a moment in time.
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean | UrlTree> {
    return new Promise(resolve => {
      resolve(true);
    });
  }
}
