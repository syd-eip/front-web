import {Injectable} from '@angular/core';
import {NgbModal, NgbModalOptions, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  /**
   * @description A reference to the newly opened modal returned by the method NgbModal.open()
   */
  private _modalRef: NgbModalRef;

  /**
   * @description ModalService's constructor
   * The constructor creates an instance of the ModalService service a
   *
   * @param modalService A reference to the service of the ng-bootstrap module allowing to open modals in the DOM.
   */
  constructor(private modalService: NgbModal) {
  }

  /**
   * @description The method opens a new modal with the specified content and options provided.
   *
   * The content can be provided as a TemplateRef or a component type. If a component type is passed as content, then the instance of
   * this component can be injected with an instance of the class NgbActiveModal.
   */
  open(content: any, options?: NgbModalOptions): NgbModalRef {
    if (content) {
      this._modalRef = this.modalService.open(content, options);
    }
    return this.modalRef
  }

  /**
   * @description The method closes the open modal. The result of this closure can be recovered as a Promise thanks to NgbModalRef.result
   */
  close(): void {
    if (this.modalRef) {
      this.modalRef.close();
    }
  }

  /**
   * @description The method cancels the open modal. The result of this closure can be recovered as a Promise thanks to NgbModalRef.result
   */
  dismiss(): void {
    if (this.modalRef) {
      this.modalRef.dismiss();
    }
  }

  /**
   * @description The method dismisses all currently displayed modal windows with the supplied reason
   *
   * @param reason Supplied reason to dismiss all curently displayed modal
   */
  dismissAll(reason?: any): void {
    this.modalService.dismissAll(reason);
  }

  /**
   * @description The method indicates if there are currently any open modal windows in the application.
   */
  hasOpenModals(): boolean {
    return this.modalService.hasOpenModals();
  }

  /**
   * @description The method allows to retrieve the newly opened modal returned by the method NgbModal.open()
   *
   * @return NgbModalRef A reference of the newly opened modal
   */
  get modalRef(): NgbModalRef {
    return this._modalRef;
  }

  /**
   * @description The method allows you to assign the newly opened modal of the ModalComponent
   */
  set modalRef(value: NgbModalRef) {
    this._modalRef = value;
  }
}
