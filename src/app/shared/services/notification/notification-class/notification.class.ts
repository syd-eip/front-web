/**
 * @description This enum efines all possible types of notifications instantiable
 */
export enum NotificationType {
  Error,
  Warning,
  Info,
  Success,
  Debug,
}

/**
 * @description This class characterizes all needed information to make a notification
 */
export class NotificationClass {

  /**
   * @description Notification title
   */
  private _title: string;

  /**
   * @description Notification message
   */
  private _message: string;

  /**
   * @description Notification duration
   */
  private _duration: number;

  /**
   * @description Notification id
   */
  private _id: number;

  /**
   * @description Notification type
   */
  private _type: NotificationType;

  /**
   * @description Construct the NotificationClass class
   *
   * The constructor creates an instance of the NotificationClass class and specifies the default values
   * the input and output variables of the class
   *
   * @param title The title of the new notification
   * @param message The message / body of the new notification
   * @param type The type of notification
   * @param duration The time the notification is active
   */
  constructor(title: string, message: string, type: NotificationType, duration: number) {
    this._title = title;
    this._message = message;
    this._duration = duration;
    this._type = type;
    this._id = Date.now();
  }

  /**
   * @description This method allows to retrieve the notification title
   */
  get title(): string {
    return this._title;
  }

  /**
   * @description This method allows to assign the notification title
   *
   * @param value New value of the notification title
   */
  set title(value: string) {
    this._title = value;
  }

  /**
   * @description This method allows to retrieve the notification message
   */
  get message(): string {
    return this._message;
  }

  /**
   * @description This method allows to assign the notification message
   *
   * @param value New value of the notification message
   */
  set message(value: string) {
    this._message = value;
  }

  /**
   * @description This method allows to retrieve the notification duration
   */
  get duration(): number {
    return this._duration;
  }

  /**
   * @description This method allows to assign the notification duration
   *
   * @param value New value of the notification duration
   */
  set duration(value: number) {
    this._duration = value;
  }

  /**
   * @description This method allows to retrieve the notification type
   */
  get type(): NotificationType {
    return this._type;
  }

  /**
   * @description This method allows to assign the notification type
   *
   * @param value New value of the notification type
   */
  set type(value: NotificationType) {
    this._type = value;
  }

  /**
   * @description This method allows to retrieve the notification id
   */
  get id(): number {
    return this._id;
  }
}
