import {Injectable} from '@angular/core';
import {CustomSubjectClass} from '../../classes/rxjs-overrides/custom-subject.class';
import {TranslationService} from '../translation/translation.service';
import {NotificationClass, NotificationType} from './notification-class/notification.class';
import {Observable} from 'rxjs';
import {TranslationClass} from '../translation/translation-class/translation.class';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {

  /**
   * @description Listener to know if there is a new notification to display
   *
   * An Subject is a special type of Observable that allows values to be multicasted to many Observers.
   * While plain Observables are unicast (each subscribed Observer owns an independent execution of the Observable),
   *
   * Subject are multicast.
   */
  private _notifListener: CustomSubjectClass<NotificationClass>;

  /**
   * @description Data to translate
   */
  private _translations: TranslationClass;

  /**
   * @description Constructor of NotificationService service
   *
   * The constructor creates an instance of the NotificationService service and specifies the default values
   * the input and output variables of the component.
   *
   * @param translationService A reference to the translation service
   */
  constructor(private translationService: TranslationService) {
    this._notifListener = new CustomSubjectClass();

    this._translations = translationService.getTranslation();

    this.translationService.getTranslationObservable().subscribe((translations) => {
      this._translations = translations;
    });
  }

  /**
   * @description This method makes an observable to get when there is a new notification to display
   */
  public observable(): Observable<NotificationClass> {
    return this._notifListener.asObservable();
  }

  /**
   * @description This method makes a new notification
   *
   * @param type The type of the new notification
   * @param message The message of the notification
   * @param duration The delay of the notification
   * @param title The notification title
   */
  public notify(type: NotificationType, message: string, duration: number = 4000, title: string = '') {
    let titleString = title;

    if (!titleString) {
      titleString = title;
    }
    const notif = new NotificationClass(titleString, message, type, duration);
    this._notifListener.next(notif);
  }

  /**
   * @description This method is an alias for the notify method (see notify for more information).
   * It's used to send a debug notification
   *
   * @param message Notification message
   * @param duration Notification duration
   * @param title Notification title
   */
  public debug(message: string, duration: number = 4000, title: string = '') {
    this.notify(NotificationType.Debug, message, duration, title);
  }

  /**
   * @description This method is an alias for the notify method (see notify for more information)
   * It's used to send an information notification
   *
   * @param message Notification message
   * @param duration Notification duration
   * @param title Notification title
   */
  public info(message: string, duration: number = 4000, title: string = '') {
    this.notify(NotificationType.Info, message, duration, title);
  }

  /**
   * @description This method is an alias for the notify method (see notify for more information)
   * It's used to send a success notification
   *
   * @param message Notification message
   * @param duration Notification duration
   * @param title Notification title
   */
  public success(message: string, duration: number = 4000, title: string = '') {
    this.notify(NotificationType.Success, message, duration, title);
  }

  /**
   * @description This method is an alias for the notify method (see notify for more information)
   * It's used to send a warning notification
   *
   * @param message Notification message
   * @param duration Notification duration
   * @param title Notification title
   */
  public warning(message: string, duration: number = 4000, title: string = '') {
    this.notify(NotificationType.Warning, message, duration, title);
  }

  /**
   * @description This method is an alias for the notify method (see notify for more information)
   * It's used to send an error notification
   *
   * @param message Notification message
   * @param duration Notification duration
   * @param title Notification title
   */
  public error(message: string, duration: number = 4000, title: string = '') {
    this.notify(NotificationType.Error, message, duration, title);
  }
}
