/**
 * @description This interface defines the loader quote model
 */
export interface LoaderQuoteInterface {
  proverb: string;
  origin: string;
}
