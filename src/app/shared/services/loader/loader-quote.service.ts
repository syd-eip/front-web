import {Injectable} from '@angular/core';
import {LoaderQuoteInterface} from './loader-quote.interface';

@Injectable({
  providedIn: 'root',
})
export class LoaderQuoteService {

  /**
   * @description The list of available quotes in the application
   */
  private _quotes: Array<LoaderQuoteInterface> = [
    {proverb: 'On ne joue pas en assistant à un jeu.', origin: 'Proverbe Baoulé'},
    {proverb: 'Aucun jeu ne peut se jouer sans règles.', origin: 'Vaclav Havel'},
    {proverb: 'Tout dépend du hasard, et la vie est un jeu.', origin: 'Jean de Rotrou'},
    {proverb: 'Le destin met beaucoup de hasard dans son jeu.', origin: 'Jacques Folch-Ribas'},
    {
      proverb: 'Les grands ont toujours tort de plaisanter avec leurs inférieurs. La plaisanterie est un jeu, ' +
        'le jeu suppose l\'égalité.', origin: 'Honoré de Balzac'
    },
  ];

  /**
   * @description Constructor of LoaderQuoteService service
   */
  constructor() {
  }

  /**
   * @description This accessor returns a random quote from the internal quote list
   */
  get random(): LoaderQuoteInterface {
    const random = Math.round(Math.random() * 1000);

    return this.quotes[random % this.quotes.length];
  }


  /**
   * @description This method allows you to retrieve the list of available quotes in the application
   */
  get quotes(): Array<LoaderQuoteInterface> {
    return this._quotes;
  }

  /**
   * @description This method allows you to assign the list of available quotes in the application
   *
   * @param value New value of the list of available quotes in the application
   */
  set quotes(value: Array<LoaderQuoteInterface>) {
    this._quotes = value;
  }
}
