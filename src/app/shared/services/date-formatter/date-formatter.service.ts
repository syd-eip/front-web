import {NgbDateParserFormatter, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {Injectable} from '@angular/core';

@Injectable()
export class DateFormatterService extends NgbDateParserFormatter {

  /**
   * @description Delimiter used to format the given date
   */
  private readonly delimiter = '/';

  /**
   * @description This method allows you to parse the given `string` to an `NgbDateStruct`
   * The method must return `null` if the value can't be parsed.
   *
   * @param value String value to be parsed
   */
  parse(value: string): NgbDateStruct | null {
    if (value) {
      const date: Array<string> = value.split(this.delimiter);

      return {
        day: parseInt(date[0], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[2], 10)
      };
    }
    return null;
  }

  /**
   * @description This method allows you to format the given `NgbDateStruct` to a `string`.
   *
   * The method should return an empty string if the given date is `null`,
   * and try to provide a partial result if the given date is incomplete or invalid.
   *
   * @param date Given date to format
   */
  format(date: NgbDateStruct | null): string {
    if (date) {
      const day: string = (date.day < 10) ? '0' + date.day : date.day.toString();
      const month: string = (date.month < 10) ? '0' + date.month : date.month.toString();

      return day + this.delimiter + month + this.delimiter + date.year;
    }
    return '';
  }
}
