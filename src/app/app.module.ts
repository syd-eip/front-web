/**
 * Import of Angular's modules
 */
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import localeFr from '@angular/common/locales/fr';
import {registerLocaleData} from '@angular/common';
/**
 * Import of app's modules
 */
import {AppRoutingModule} from './app-routing.module';
import {LayoutsModule} from './layouts/layouts.module';
import {ComponentsModule} from './shared/components/components.module';
/**
 * Import of shared services
 */
import {TranslationService} from './shared/services/translation/translation.service';
import {HttpInterceptorService} from './shared/services/http-interceptor/http-interceptor.service';
import {CookieService} from 'ngx-cookie-service';
import {ModalService} from './shared/services/modal/modal.service';
import {NgxImageCompressService} from 'ngx-image-compress';
/**
 * Import of components
 */
import {AppComponent} from './app.component';
/**
 * Import of angular's material modules
 */
import {MatDialogModule} from '@angular/material';
/**
 * Import of shared classes
 */
import {NetworkClass} from './shared/services/network/network.class';


/**
 * @description Register global data to be used internally by Angular.
 */
registerLocaleData(localeFr);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    RouterModule,
    MatDialogModule,
    AppRoutingModule,
    LayoutsModule,
    ComponentsModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true},
    CookieService,
    NgxImageCompressService,
    TranslationService,
    ModalService,
    NetworkClass
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
